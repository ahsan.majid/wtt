from typing import List, Any
import numpy as np
from datetime import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import csv
import pandas as pd
import importlib
import runpy
import subprocess
import atexit

rootevent = Toplevel()
rootevent.geometry("800x790+300+0")
rootevent.title("")
color = "black"
rootevent.configure(bg="black", bd=10, width=800, height=750, relief=RIDGE)
my_frame = Frame(rootevent, highlightbackground='whitesmoke', highlightthickness=1, width=500, height=700, bd=0)
Sched_Message = "Press Me"
font_sizeK = 14
name_selected = ""
time_bypass = 0

SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)
InfoTable_matrixshape = np.shape(InfoTable_matrix)

file_base = pd.read_table('Base Data.txt', delimiter=',')

if InfoTable_matrix[0, 0] == "DuPont Schedule":
    shift_base = pd.read_csv('ShiftDupont.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "4on/4off Schedule":
    shift_base = pd.read_csv('ShiftFourFour.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "EOWEO Schedule":
    shift_base = pd.read_csv('ShiftEOWEO.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "7on/7off Schedule":
    shift_base = pd.read_csv('ShiftSeven.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "Four/Five Schedule":
    shift_base = pd.read_csv('ShiftFourFive.txt', delimiter=',')
#shift_base = pd.read_table('ShiftDupont.txt', delimiter=',')
sdf_name = pd.DataFrame(file_base)
sdf_basename = np.array(sdf_name.sort_values(by=['Team', 'Salary']))
sdf_shift = pd.DataFrame(shift_base)
sdf_shiftfinal = np.array(sdf_shift)

style = ttk.Style()
style.theme_use('clam')

Title_label = Label(rootevent, text="Input Calendar Events", width=20, bg="white", font="Arial 20 bold")
Title_label.pack(pady=5)

tree_frame1 = Frame(rootevent)
tree_frame1.place(x=60, y=70)

tree_scroll = Scrollbar(tree_frame1)
tree_scroll.pack(side=RIGHT, fill=Y)

input_tree = ttk.Treeview(tree_frame1, yscrollcommand=tree_scroll.set, height=16)
input_tree.pack()

tree_scroll.config(command=input_tree.yview)

input_tree['columns'] = ("Date", "Name", "Team", "Event")
input_tree.column('#0', anchor=W, width=0, stretch=NO)
input_tree.column('Date', anchor=W, width=150)
input_tree.column('Name', anchor=W, width=250)
input_tree.column('Team', anchor=CENTER, width=50)
input_tree.column('Event', anchor=W, width=200)

input_tree.heading('#0', text='', anchor=W)
input_tree.heading('Date', text='Date', anchor=W)
input_tree.heading('Name', text='Name', anchor=W)
input_tree.heading('Team', text='Team', anchor=CENTER)
input_tree.heading('Event', text='Event', anchor=W)

input_tree.tag_configure('Ateam', background='#9DD0F3')
input_tree.tag_configure('Bteam', background='#F7CDAB')
input_tree.tag_configure('Cteam', background='#DCBBF6')
input_tree.tag_configure('Dteam', background='#E5F7AB')

def display_all():
    global time_bypass
    time_bypass = 1
    display_events()


def display_events():
    global time_bypass, Row1, matrix_children, input_tree
    for input_record in input_tree.get_children():
        input_tree.delete(input_record)
    Event_table = pd.read_table('Shift Appointments.txt', delimiter=',')
    sdf_EventTable = pd.DataFrame(Event_table)
    sdf_EventTablesort = sdf_EventTable.sort_values(by=(['Date', 'Team', 'Name']))
    Event_matrix = np.array(sdf_EventTablesort)
    Event_shape = np.shape(Event_matrix)[0]
    sort_record = ""
    countidevent = 0
    if time_bypass == 1:
        numberofdays = 365
    else:
        numberofdays = 10
    for eventdata_record in Event_matrix:
        #eventdata_first = str(eventdata_record[0])
        eventrecord_stuff1 = str(eventdata_record).replace("'", '')
        eventrecord_stuff = str(eventrecord_stuff1).strip("[]")
        if datetime.strptime(eventdata_record[0], '%Y-%m-%d') >= (datetime.now() - timedelta(numberofdays)):
            eventrecord_child = str(eventdata_record[1:4]).replace(",", '')
            if eventdata_record[2] == "A":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Ateam',))
            elif eventdata_record[2] == "B":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Bteam',))
            elif eventdata_record[2] == "C":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Cteam',))
            elif eventdata_record[2] == "D":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Dteam',))
        countidevent += 1
        sort_record = eventdata_record[0]
        time_bypass = 0


names_names = []
names_shape = np.shape(sdf_basename)[0]
shift_shift = []
shift_shape = np.shape(sdf_shiftfinal)[0]

onshift_listA = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamB', 'Swap-TeamC', 'Swap-TeamD']
onshift_listA1 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamB', 'Swap-TeamC']
onshift_listA2 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamB', 'Swap-TeamD']
onshift_listA3 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamC', 'Swap-TeamD']
onshift_listB = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamC', 'Swap-TeamD']
onshift_listB1 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamC']
onshift_listB2 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamD']
onshift_listB3 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamC', 'Swap-TeamD']
onshift_listC = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamB', 'Swap-TeamD']
onshift_listC1 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamB']
onshift_listC2 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamD']
onshift_listC3 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamB', 'Swap-TeamD']
onshift_listD = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamB', 'Swap-TeamC']
onshift_listD1 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamB']
onshift_listD2 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamA', 'Swap-TeamC']
onshift_listD3 = ['Vacation', 'Off', 'Train/SpecialAssign', 'Swap-TeamB', 'Swap-TeamC']
offshift_listA = ['OT-Refuse', 'OT-Block', 'Swap-TeamB',
                  'Swap-TeamC', 'Swap-TeamD']
offshift_listB = ['OT-Refuse', 'OT-Block', 'Swap-TeamA',
                  'Swap-TeamC', 'Swap-TeamD']
offshift_listC = ['OT-Refuse', 'OT-Block', 'Swap-TeamA',
                  'Swap-TeamB', 'Swap-TeamD']
offshift_listD = ['OT-Refuse', 'OT-Block', 'Swap-TeamA',
                  'Swap-TeamB', 'Swap-TeamC']
shift_choiceevent = []


def enter_Name(eventObject):
    global name_selected
    name_selected = Name_select.get()
    return name_selected


def exit_appoint():
    rootevent.destroy()


def extra_appoint():
    rootevent.destroy()
    runpy.run_path(path_name="EventCalLongTerm.py")


def check_appoint(event):
    global input_tree
    appoint_check = pd.read_table('Shift Appointments.txt', delimiter=',')
    sdf_appcheck = pd.DataFrame(appoint_check)
    appoint_matrix = np.array(sdf_appcheck)
    countapp = 1
    if np.shape(appoint_matrix)[0] != 0:
        for app_items in range(np.shape(appoint_matrix)[0]):
            if name_selected == appoint_matrix[app_items, 1] and str(ScdCal_event.selection_get()) == \
                    appoint_matrix[app_items, 0]:
                messagebox.showwarning('Error', "There is an existing selection for "
                                       + appoint_matrix[app_items, 1] + "!" + "\n" + "\n" +
                                       appoint_matrix[app_items, 0] + " : " +
                                       appoint_matrix[app_items, 3] + "\n" + "\n" +
                                       "Delete existing selection and SAVE before adding new!")

            else:
                Sched_Start()
    else:
        Sched_Start()


def delete_selectedevent():
    removex = input_tree.selection()[0]
    input_tree.delete(removex)


def Sched_Start():
    global shift_team, label_date1event, label_choiceevent, onshift_comboevent, offshift_comboevent, \
        Event_select, EventSelect_combo
    my_date = ScdCal_event.selection_get()
    label_date1event = Label(rootevent, text=my_date, font=("Arial", font_sizeK), padx=19)
    label_date1event.place(x=269, y=500)
    label_choiceevent = Label(rootevent, text="Select Option:", bg=color, fg='white', font=("Arial", font_sizeK))
    label_choiceevent.place(x=152, y=540)
    for i_find in range(names_shape):
        if name_selected == sdf_basename[i_find, 1]:
            for i_shift in range(shift_shape):
                if str(my_date) == sdf_shiftfinal[i_shift, 0]:
                    if sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] or sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0]:
                        if sdf_basename[i_find, 0] == "A":
                            if sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "D":
                                shift_choice = onshift_listA1
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "D":
                                shift_choice = onshift_listA1
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "C":
                                shift_choice = onshift_listA2
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "C":
                                shift_choice = onshift_listA2
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "B":
                                shift_choice = onshift_listA3
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "B":
                                shift_choice = onshift_listA3
                            else:
                                shift_choice = onshift_listA
                        elif sdf_basename[i_find, 0] == "B":
                            if sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "D":
                                shift_choice = onshift_listB1
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "D":
                                shift_choice = onshift_listB1
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "C":
                                shift_choice = onshift_listB2
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "C":
                                shift_choice = onshift_listB2
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "A":
                                shift_choice = onshift_listB3
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "A":
                                shift_choice = onshift_listB3
                            else:
                                shift_choice = onshift_listB
                        elif sdf_basename[i_find, 0] == "C":
                            if sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "D":
                                shift_choice = onshift_listC1
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "D":
                                shift_choice = onshift_listC1
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "B":
                                shift_choice = onshift_listC2
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "B":
                                shift_choice = onshift_listC2
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "A":
                                shift_choice = onshift_listC3
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "A":
                                shift_choice = onshift_listC3
                            else:
                                shift_choice = onshift_listC
                        elif sdf_basename[i_find, 0] == "D":
                            if sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "C":
                                shift_choice = onshift_listD1
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "C":
                                shift_choice = onshift_listD1
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "B":
                                shift_choice = onshift_listD2
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 1] == "B":
                                shift_choice = onshift_listD2
                            elif sdf_shiftfinal[i_shift, 1] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift-1, 2] == "A":
                                shift_choice = onshift_listD3
                            elif sdf_shiftfinal[i_shift, 2] == sdf_basename[i_find, 0] and \
                                    sdf_shiftfinal[i_shift+1, 2] == "A":
                                shift_choice = onshift_listD3
                            else:
                                shift_choice = onshift_listD
                        Event_select = StringVar()
                        EventSelect_combo = ttk.Combobox(rootevent, font="Arial 14 bold", width=15, justify="center",
                                                         textvariable=Event_select)
                        EventSelect_combo['value'] = shift_choice
                        EventSelect_combo.place(x=267, y=540)
                        bookon = Event_select.get()
                        EventSelect_combo.bind("<<ComboboxSelected>>", Schedule_Book)
                        shift_team = sdf_basename[i_find, 0]
                    else:
                        if sdf_basename[i_find, 0] == "A":
                            shift_choice = offshift_listA
                        elif sdf_basename[i_find, 0] == "B":
                            shift_choice = offshift_listB
                        elif sdf_basename[i_find, 0] == "C":
                            shift_choice = offshift_listC
                        else:
                            shift_choice = offshift_listD
                        Event_select = StringVar()
                        EventSelect_combo = ttk.Combobox(rootevent, font="Arial 14 bold", width=15, justify="center",
                                                         textvariable=Event_select)
                        EventSelect_combo['value'] = shift_choice
                        EventSelect_combo.place(x=267, y=540)
                        bookoff = Event_select.get()
                        EventSelect_combo.bind("<<ComboboxSelected>>", Schedule_Book)
                        shift_team = sdf_basename[i_find, 0]
    return Event_select, shift_team, EventSelect_combo, label_date1event


def Schedule_Book(event):
    global book_selected, confirm_pressevent, reset_pressevent, Event_select, EventSelect_combo, label_date1event
    book_selected = Event_select.get()
    #check_appoint()
    confirm_pressevent = Button(rootevent, text="Press to Confirm", font=("Arial", font_sizeK+4), cursor="circle", command=write_to_file)
    confirm_pressevent.place(x=120, y=580)
    reset_pressevent = Button(rootevent, text="Press to Reset", font=("Arial", font_sizeK+4), cursor="circle", command=reset_event)
    reset_pressevent.place(x=280, y=580)
    return book_selected


def reset_event():
    global confirm_pressevent, reset_pressevent, label_date1event, label_choiceevent, EventSelect_combo, Name_select
    confirm_pressevent.destroy()
    reset_pressevent.destroy()
    label_choiceevent.destroy()
    EventSelect_combo.destroy()
    label_date1event.destroy()
    Name_select.set("<Select>")


def write_to_file():
    global shift_team, confirm_pressevent, reset_pressevent, label_date1event, label_choiceevent, book_selected, EventSelect_combo
    with open("Shift Appointments.txt", 'a', newline="") as appFile:
        writer = csv.writer(appFile)
        writeList = [ScdCal_event.selection_get(), name_selected, shift_team, book_selected]
        writer.writerow(writeList)
        appFile.close()
    messagebox.showinfo("Success!", "Event Successfully Booked!")
    confirm_pressevent.destroy()
    reset_pressevent.destroy()
    label_date1event.destroy()
    label_choiceevent.destroy()
    EventSelect_combo.destroy()
    Name_select.set("<Select>")
    display_events()


def save_update():
    erase_input = open('Shift Appointments.txt', 'w+')
    erase_input.close
    SetupInput_header = ["Date", "Name", "Team", "Appointment"]
    with open("Shift Appointments.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    save_list = ""
    for child_row in input_tree.get_children():
        with open("Shift Appointments.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            #save_list = child_row
            writer.writerow(input_tree.item(child_row)['values'])
            appFile1.close()
    messagebox.showinfo("Success!", "Events Saved Successfully!")


#Set up the calendar, selection triggers Sched_Start
ScdCal_event = Calendar(rootevent, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                        showweeknumbers=False)
ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
ScdCal_event.place(x=440, y=460)
ScdCal_event.bind("<<CalendarSelected>>", check_appoint)

#Get the names from the Base Data.txt file
for i_name in range(names_shape):
    names_names += [sdf_basename[i_name, 1]]

#Set up the form
title_labelevent = Label(rootevent, text="Add Event", font='Arial 20 bold', width=55, bg=color, fg='white')
title_labelevent.place(x=60, y=420)
name_labelevent = Label(rootevent, text="Select Name from List:", bg=color, fg='white', font=("Arial", font_sizeK))
name_labelevent.place(x=97, y=460)
date_labelevent = Label(rootevent, text="Select Date from Calendar:", bg=color, fg='white', font=("Arial", font_sizeK))
date_labelevent.place(x=70, y=500)
blank_dateevent = Label(rootevent, text='                  ', font=("Arial", font_sizeK), padx=19)
blank_dateevent.place(x=269, y=500)

Name_select = StringVar()
Name_select.set("<Select>")
NameSelect_combo = ttk.Combobox(rootevent, font="Arial 14 bold", width=12, justify="center",
                                textvariable=Name_select)
NameSelect_combo['value'] = names_names
NameSelect_combo.place(x=270, y=460)
NameSelect_combo.bind("<<ComboboxSelected>>", enter_Name)

Display_button = Button(rootevent, text="Display Recent Events", font=("Arial bold", font_sizeK), width=20, cursor="circle", fg='blue',
                        command=display_events)
Display_button.place(x=63, y=45)

Remove_button = Button(rootevent, text="Display 12 Months", font=("Arial bold", font_sizeK), width=20, cursor="circle", fg='blue',
                       command=display_all)
Remove_button.place(x=230, y=45)

Remove_button = Button(rootevent, text="Delete Selected Event", font=("Arial bold", font_sizeK), width=20, cursor="circle", fg='blue',
                       command=delete_selectedevent)
Remove_button.place(x=397, y=45)

Remove_button = Button(rootevent, text="Save Update", font=("Arial bold", font_sizeK), width=20, cursor="circle", fg='blue',
                       command=save_update)
Remove_button.place(x=564, y=45)

exitappoint_button = Button(rootevent, text="Exit", width=10, font="Arial 16 bold", cursor="circle", command=exit_appoint)
exitappoint_button.configure(fg='blue')
exitappoint_button.place(x=670, y=740)

extraappoint_button = Button(rootevent, text="Long Term Calendar Entry", width=25, font="Arial 16 bold", cursor="circle", command=extra_appoint)
extraappoint_button.configure(fg='blue')
extraappoint_button.place(x=20, y=740)

rootevent.mainloop()
