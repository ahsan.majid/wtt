import pandas as pd
import numpy as np
import csv
from scipy.optimize import linear_sum_assignment
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfgen import canvas
from reportlab.lib.colors import blue, green, white, lightgrey
from tkinter import messagebox
import subprocess
from datetime import *
import runpy

#Read in the Input Data File and convert to Matrix
iris = pd.read_table('Base Data.txt', delimiter=',')
df = pd.DataFrame(iris)
dfOT = pd.DataFrame(iris)
matrix_try = np.array(df)
matrix_sal = np.array(df)
matrix_OThours = np.array(dfOT)
matrix_tryshape = np.shape(matrix_try)

TeamName_table = pd.read_table('TeamName.txt')
sdf_TeamNameTable = pd.DataFrame(TeamName_table)
TeamName_matrix = np.array(sdf_TeamNameTable)

A_peopleNames = []
B_peopleNames = []
C_peopleNames = []
D_peopleNames = []
A_peoplecount = 0
B_peoplecount = 0
C_peoplecount = 0
D_peoplecount = 0


#Determine Number of people on each team
for count_people in range(matrix_tryshape[0]):
    if matrix_try[count_people, 0] == "A":
        A_peoplecount += 1
        A_peopleNames += [matrix_try[count_people, 1]]
    if matrix_try[count_people, 0] == "B":
        B_peoplecount += 1
        B_peopleNames += [matrix_try[count_people, 1]]
    if matrix_try[count_people, 0] == "C":
        C_peoplecount += 1
        C_peopleNames += [matrix_try[count_people, 1]]
    if matrix_try[count_people, 0] == "D":
        D_peoplecount += 1
        D_peopleNames += [matrix_try[count_people, 1]]
SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)
InfoTable_matrixshape = np.shape(InfoTable_matrix)
#Unit_Name = InfoTable_matrix[0, 4]
EBSlide_tolerance = InfoTable_matrix[0, 1]
EBSlide_plus = 1
if EBSlide_tolerance == 1:
    EBSlide_plus = 0
    QuitSlidetol = 7
if EBSlide_tolerance == 2:
    QuitSlidetol = 7
else:
    QuitSlidetol = 7
Double_value = InfoTable_matrix[0, 2]

CompInfo_table = pd.read_csv('Base Data CAS.txt', delimiter=',')
sdf_CompTable = pd.DataFrame(CompInfo_table)
CompTable_matrix = np.array(sdf_CompTable)
CompTable_matrixshape = np.shape(CompTable_matrix)


senioradj_list = []
for senioradj in range(np.shape(matrix_OThours)[0]):
    if InfoTable_matrix[0, 3] != "OT Hours":
        if matrix_OThours[senioradj, 2] == "Salary":
            senioradj_list += [0]
        else:
            senioradj_list += [matrix_OThours[senioradj, 3] * 4]
    else:
        senioradj_list += [0]

senioradj_list = np.reshape(senioradj_list, (np.shape(senioradj_list)[0], 1))
matrix_OThours = np.hstack((matrix_OThours, senioradj_list))

Dupont_Flag = 0
Four_Flag = 0
Max_Flag = 0
if InfoTable_matrix[0, 0] == "DuPont Schedule":
    shift_table = pd.read_csv('ShiftDupont.txt', delimiter=',')
    countdays_factor = 5
    Dupont_Flag = 1
    Max_Flag = 4
elif InfoTable_matrix[0, 0] == "4on/4off Schedule":
    shift_table = pd.read_csv('ShiftFourFour.txt', delimiter=',')
    countdays_factor = 5
    Four_Flag = 1
    Max_Flag = 4
elif InfoTable_matrix[0, 0] == "EOWEO Schedule":
    shift_table = pd.read_csv('ShiftEOWEO.txt', delimiter=',')
    countdays_factor = 4
    Max_Flag = 3
elif InfoTable_matrix[0, 0] == "7on/7off Schedule":
    shift_table = pd.read_csv('ShiftSeven.txt', delimiter=',')
    countdays_factor = 8
    Max_Flag = 7
elif InfoTable_matrix[0, 0] == "Four/Five Schedule":
    shift_table = pd.read_csv('ShiftFourFive.txt', delimiter=',')
    countdays_factor = 6
    Four_Flag = 1
    Max_Flag = 5
sdf = pd.DataFrame(shift_table)
sdf_matrix = np.array(sdf)
sdf_matrixshape = np.shape(sdf_matrix)

appoint_table = pd.read_table('Shift Appointments.txt', delimiter=',')
sdf_appoint = pd.DataFrame(appoint_table)
appoint_matrix2 = np.array(sdf_appoint)
appoint_shape2 = np.shape(appoint_matrix2)[0]

input_table = pd.read_table('PassDate.txt', delimiter=',')
sdf_inputtable = pd.DataFrame(input_table)
Pass_matrix = np.array(sdf_inputtable)
Pass_shape = np.shape(Pass_matrix)[0]
pdfstart_date = datetime.strptime(Pass_matrix[0][0], "%Y-%m-%d")
pdfstart_datefinal = datetime.strftime(pdfstart_date, "%b %d, %Y")

OT_table = pd.read_table('OTData.Txt', delimiter=',')
sdf_OTtable = pd.DataFrame(OT_table)
OTTable_matrix = np.array(sdf_OTtable)
OTTable_shape = np.shape(OTTable_matrix)[0]

InitInput_table = pd.read_table('Initial Input.txt', delimiter=',')
sdf_InitInputTable = pd.DataFrame(InitInput_table)
InitInput_matrix = np.array(sdf_InitInputTable)
InitInput_shape = np.shape(InitInput_matrix)[0]

keithDayPosts = []
keithNightPosts = []
for post_items in range(InitInput_shape):
    if InitInput_matrix[post_items, 1] == "Y" or InitInput_matrix[post_items, 2] == "Y":
        keithDayPosts += [InitInput_matrix[post_items, 0]]
    if InitInput_matrix[post_items, 1] == "Y":
        keithNightPosts += [InitInput_matrix[post_items, 0]]

erase_Temp = open("TempSchedule.txt", "w+")
erase_Temp.close()
erase_Shift = open("ShiftCount.txt", "w+")
erase_Shift.close()
matrix_tempHeader = ["Name", "OT Hours Sched"]
with open("TempSchedule.txt", 'a', newline="") as appFile:
    writer = csv.writer(appFile)
    writeList = matrix_tempHeader
    writer.writerow(writeList)
    appFile.close()

matrix_shiftHeader = ["Name", "Shifts Worked"]
with open("ShiftCount.txt", 'a', newline="") as appFile:
    writer = csv.writer(appFile)
    writeList = matrix_shiftHeader
    writer.writerow(writeList)
    appFile.close()

#This is how to manipulate the Dates-----------------------
kwcdate = datetime.strptime(sdf_matrix[25][0], "%Y-%m-%d")
kwcdate1 = datetime.strftime(kwcdate, "%b %d, %Y")
year = kwcdate.date().year
month = kwcdate.date().month
day = kwcdate.date().day
#----------------------------------------------------------

keithPosts = np.hstack((keithDayPosts, keithNightPosts))
keithPostsize = np.shape(keithPosts)[0]

#Setup Base Data (matrix_try) to usable matrix
df.drop(df.columns[3], inplace=True, axis=1)
matrix_posts2 = np.array(df.columns)
post_size2 = np.shape(keithNightPosts)[0]
matrix_try = np.delete(matrix_try, 2, 1)
matrix_try = np.delete(matrix_try, 2, 1)
matrix_try = np.where(matrix_try == 0, 17000, matrix_try)
night_color = "#D2CDCE"
VacTrain_color = "#C1F3FA"
row_kwc = 0
FinishFlag = 0

#Extract the Posts from the Panda df and put into another Matrix
matrix_posts1 = np.array(df.columns)
post_size = keithPostsize

matrix_postsD = []
matrix_postsD = [postsD + "D" for postsD in keithDayPosts]
matrix_postsN = []
matrix_postsN = [postsN + "N" for postsN in keithNightPosts]
matrix_posts = []
matrix_posts = np.hstack((matrix_postsD, matrix_postsN))
post_size = np.shape(matrix_posts)[0]
matrix_vertpost = []
matrix_vertpost = np.reshape(matrix_posts, (post_size, 1))
Number_Dayposts = np.shape(keithDayPosts)[0]
Number_Nightposts = np.shape(keithNightPosts)[0]
A_DaysEBnumber = A_peoplecount - Number_Dayposts + 1
B_DaysEBnumber = B_peoplecount - Number_Dayposts + 1
C_DaysEBnumber = C_peoplecount - Number_Dayposts + 1
D_DaysEBnumber = D_peoplecount - Number_Dayposts + 1
A_NightsEBnumber = A_peoplecount - Number_Nightposts + 1
B_NightsEBnumber = B_peoplecount - Number_Nightposts + 1
C_NightsEBnumber = C_peoplecount - Number_Nightposts + 1
D_NightsEBnumber = D_peoplecount - Number_Nightposts + 1

#-------------------------------------------------------------------------------------
start_date = Pass_matrix[-1, 0]
base_date = sdf_matrix[0, 0]
row_adj1 = (datetime.strptime(start_date, "%Y-%m-%d") - datetime.strptime(base_date, "%Y-%m-%d")).days

if InfoTable_matrix[0, 0] != "EOWEO Schedule" and Dupont_Flag != 1:
    for kwcCheckSeven in range(row_adj1+1, row_adj1+6):
        if kwcCheckSeven > 0 and sdf_matrix[kwcCheckSeven, 1] != sdf_matrix[kwcCheckSeven-1, 1]:
            Find_Check_Seven = kwcCheckSeven - row_adj1
            break

Shift_Adj = 20
no_of_weeks = Pass_matrix[-1, 1]
total_days = no_of_weeks*7
end_date = datetime.strptime(start_date, "%Y-%m-%d") + timedelta(total_days)
end_date = str(datetime.strftime(end_date, "%Y-%m-%d"))
week_enter = 1+int(row_adj1/7)
weeks_try = (week_enter-1)*7
week_tryadj = 0
wrap_value = 0
row_adj = 0
nameslide = ""
Start_time3 = 0
Start_time = datetime.now()
End_Time3 = 0
Prev_EBSlideadj = 0
EBBlock_End = kwcdate - timedelta(1)
EBBlock_Start = pdfstart_date - timedelta(1)
EBBlock_StartHold = pdfstart_date
EBBlock_EndHold = EBBlock_End
Count_QuitSlideD = 0
Count_QuitSlideN = 0

#Minimize the appointment matrix
appoint_matrix1 = []
for appoint_min in appoint_matrix2:
    if datetime.strptime(str(start_date), "%Y-%m-%d") <= datetime.strptime(appoint_min[0], "%Y-%m-%d") <= datetime.strptime(str(end_date), "%Y-%m-%d"):
        if np.shape(appoint_matrix1)[0] == 0:
            appoint_matrix1 = appoint_min
        else:
            appoint_matrix1 = np.vstack((appoint_matrix1, appoint_min))
appoint_shape = np.shape(appoint_matrix1)[0]

#=============================================================================================================
Current_slide = ""
Current_slidepost = 0
Bad_Slide = []
BadName = ""
BadNameD1 = ""
BadNameD2 = ""
BadNameD3 = ""
BadNameD4 = ""
BadNameN1 = ""
BadNameN2 = ""
BadNameN3 = ""
BadNameN4 = ""
Bad_SlideStopD = 0
Bad_SlideStopN = 0
Bad_SlideHaltD = 0
Bad_SlideHaltN = 0
EBSlide_boost = 0
EBSlide_tolboost = 0
NightStop_Name = ""
SchedStartFlag = 0
kwcNightVac_Name = []
kwcNightVac_Date = []
kwcDayVac_Name = []
kwcDayVac_Date = []
matrix_NamesNights = []
matrix_NamesDays = []
Comb_NamesDaysV = []
Comb_NamesNightsV = []
Comb_NamesDaysV2 = []
Comb_NamesNightsV2 = []
Current_slidematrixName = []
Current_slidematrixEBBlock_Start = []
Current_slidematrixEBBlock_End = []
CurrentSlide1 = ""
Possible_Slide = ""
Possible_SlideFlag = 0
Slide_to_DaysFlag = 0
Slide_to_NightsFlag = 0
Dupont_MondaySlide_toNights = ""
Dupont_FridaySlide_toNights = ""
extra_boardNadj = 8000
extra_boardDadj = 8000
OT_ableadj = 3000
OT_ablenotadj = 10000
OT_ablenoway = 15000
Big_Blocker = 17000
total_daysStart = 0
Comb_Dates = []
Comb_Dates2 = []
Comb_TeamNameD = []
Comb_TeamNameN = []
Comb_fireteamD = []
Comb_fireteamN = []
Comb_EMTteamD = []
Comb_EMTteamN = []
Comb_extraD = []
Comb_extraN = []
Comb_DayVacNames = []
Comb_NightVacNames = []
Comb_DayTrainNames = []
Comb_NightTrainNames = []
Comb_CompNumbD = []
Comb_CompNumbN = []
Restart_Flag = 0
Restart_FlagE = 0
Reset_WT_Flag = 0
Reset_MT_Flag = 0
Reset_FSS_Flag = 0
Count_STOP = 0
Detect_Two_EBlocks = []
SingleDay_Slide = ""
SingleNight_Slide = ""
EBBlock_EndN2 = kwcdate
EBBlock_StartD2 = pdfstart_date - timedelta(1)
OTReset_Count = 0
OTReset_CountMax = 5
Shift_CountTemp_matrix = []
Shift_max_Name = ""
Shift_max_row = -1

def Sched_Start(total_daysStart):
    global kwcdate, appoint_shape1, Current_slide, Current_slidepost, Bad_Slide, Bad_SlideStopD, Bad_SlideStopN, \
        EBBlock_Start, EBBlock_End, Count_QuitSlideD, Count_QuitSlideN, Bad_SlideHaltD, Bad_SlideHaltN, EBSlide_boost, weeks_try, \
        week_tryadj, EBSlide_tolboost, QuitSlidetol, NightStop_Name, kwcNightVac_Name, kwcNightVac_Date, \
        kwcDayVac_Name, kwcDayVac_Date, extra_boardNadj, extra_boardDadj, OT_ableadj, OT_ablenotadj, OT_ablenoway, \
        matrix_NamesDays, matrix_NamesNights, Comb_NamesDaysV2, Comb_NamesNightsV2, Current_slidematrixName, \
        Current_slidematrixEBBlock_Start, Current_slidematrixEBBlock_End, Possible_Slide, Possible_SlideFlag, \
        Slide_to_DaysFlag, Slide_to_NightsFlag, Dupont_MondaySlide_toNights, Dupont_FridaySlide_toNights, \
        Comb_Dates2, Comb_TeamNameD, Comb_TeamNameN, Comb_Dates, Comb_fireteamD, Comb_fireteamN, Comb_EMTteamD, \
        Comb_EMTteamN, Comb_extraD, Comb_extraN, Comb_DayVacNames, Comb_NightVacNames, Comb_DayTrainNames, \
        Comb_NightTrainNames, Comb_CompNumbD, Comb_CompNumbN, FinishFlag, Comb_NamesDaysV, \
        Comb_NamesNightsV, Restart_Flag, Count_STOP, Find_Check_Seven, Max_Flag, Reset_WT_Flag, Reset_MT_Flag, \
        Reset_FSS_Flag, EBBlock_StartHold, EBBlock_EndHold, BadName, Detect_Two_EBlocks, EBBlock_StartD2, \
        EBBlock_EndN2, OTReset_Count, OTReset_Count, BadNameD1, BadNameD2, BadNameN1, BadNameN2, SingleNight_Slide, \
        SingleDay_Slide, Shift_CountTemp_matrix, Restart_FlagE, BadNameD3, BadNameD4, BadNameN3, BadNameN4, Big_Blocker, Shift_max_Name, \
        Shift_max_row, start_date, end_date, appoint_matrix, appoint_shape, newStart_Date, newEnd_Date, appoint_matrix1, \
        TeamName_matrix

    OT_add = 0
    ExtraBoardD_valueAdj = 0
    ExtraBoardN_valueAdj = 0
    countname = 0
    extra_boardcurrent = 0
    extra_boardcurrentAdj = -5
    extraD_tol = 0
    extraN_tol = 0
    EB_Flag = 0
    EBSlide_toleranceAdj = 0
    ExtraBoardD_value = 0
    ExtraBoardN_value = 0
    Appointloop = 0
    Start_Time1 = datetime.now()
    FatigueDaysFlag1 = ""
    FatigueDaysFlag2 = ""
    FatigueNightsFlag1 = ""
    FatigueNightsFlag2 = ""
    FinalFatigueFlag = 0
    flagDays_erase1 = 0
    flagDays_erase2 = 0
    flagNights_erase1 = 0
    flagNights_erase2 = 0
    SeniorityFlag = 0
    FatigueDays_addFlag = 0
    FatigueNights_addFlag = 0
    Swapcount = 0
    Swapname = ""
    Swapname2 = ""
    Swapdate = pdfstart_date - timedelta(1)
    EB_offFlag = 0
    EBDays_VacName = ""
    SlideStop_Flag = 0
    ExtraBoardD_value = 0
    ExtraBoardN_value = 0
    row_rplcRef = ""
    row_rplcRef_count = 0
    DaysVac_PostPrime1 = []
    DaysVac_PostPrime2 = []
    for row_kwc in range(total_daysStart, total_days):
        #Minimize the appointment matrix
        if (datetime.strptime(start_date, "%Y-%m-%d") + timedelta(row_kwc)).weekday() == 0 and \
                kwcdate > EBBlock_End:
            appoint_matrix = []
            newStart_Date = datetime.strptime(start_date, "%Y-%m-%d") + timedelta(row_kwc)
            if InfoTable_matrix[0, 0] == "EOWEO Schedule":
                newEnd_Date = datetime.strptime(start_date, "%Y-%m-%d") + timedelta(row_kwc+6)
            else:
                #newEnd_Date = datetime.strptime(start_date, "%Y-%m-%d") + timedelta(row_kwc+20)
                newEnd_Date = datetime.strptime(end_date, "%Y-%m-%d")
            #This fixes the appoint_matrix1 when there is only 1 entry
            appoint_matrix4 = []
            if str(np.shape(appoint_matrix1)) == '(4,)':
                appoint_matrix4 += [appoint_matrix1[0], 'DUMMYXXXYYY', appoint_matrix1[2], 'Off']
                appoint_matrix1 = np.vstack((appoint_matrix1, appoint_matrix4))
            for appoint_min in appoint_matrix1:
                if newStart_Date <= datetime.strptime(appoint_min[0], "%Y-%m-%d") <= newEnd_Date:
                    if np.shape(appoint_matrix)[0] == 0:
                        appoint_matrix = appoint_min
                    else:
                        appoint_matrix = np.vstack((appoint_matrix, appoint_min))
                appoint_shape = np.shape(appoint_matrix)[0]
        #This fixes the appoint_matrix when there is only 1 entry
        if str(np.shape(appoint_matrix)) == '(4,)':
            appoint_matrix5 = []
            appoint_matrix5 += [appoint_matrix[0], 'DUMMYXXXYYY', appoint_matrix[2], 'Off']
            appoint_matrix = np.vstack((appoint_matrix, appoint_matrix5))
            appoint_shape = np.shape(appoint_matrix)[0]
        #Reset Comb_extraD and Comb_extraN if Restart is detected
        if (np.shape(Comb_extraD)[0] > 0 or np.shape(Comb_extraN)[0] > 0) and total_daysStart > 0 \
                and Restart_Flag == 1:
            Comb_extraDTemp = Comb_extraD
            Comb_extraNTemp = Comb_extraN
            Comb_extraD = []
            Comb_extraN = []
            Count_STOP += 1
            for reset_extras in range(total_daysStart):
                Comb_extraD += [Comb_extraDTemp[reset_extras]]
                Comb_extraN += [Comb_extraNTemp[reset_extras]]
        elif row_kwc == 0:
            Comb_extraD = []
            Comb_extraN = []
            Reset_WT_Flag = 0
        if kwcdate.weekday() == 0:
            Reset_WT_Flag = 0
        if kwcdate.weekday() == 4:
            Reset_MT_Flag = 0
        if kwcdate.weekday() == 2:
            Reset_FSS_Flag = 0
        if kwcdate == EBBlock_End + timedelta(1):
            EBBlock_StartHold = pdfstart_date
        Double_add = 0

        #Reset ShiftCount.txt if Restart is detected
        if Restart_Flag == 1:
            Shift_CountTemp = pd.read_csv('ShiftCount.txt')
            sdf_ShiftCount_Temp = pd.DataFrame(Shift_CountTemp)
            Shift_CountTemp_matrix = np.array(sdf_ShiftCount_Temp)
            matrix_shiftHeader = ["Name", "Shifts Worked"]
            with open("ShiftCount.txt", 'w+', newline="") as appFile:
                writer = csv.writer(appFile)
                writeList = matrix_shiftHeader
                writer.writerow(writeList)
                appFile.close()
            for Shift_Countitems in Shift_CountTemp_matrix[0:total_daysStart*post_size2]:
                with open("ShiftCount.txt", 'a', newline="") as appFile:
                    writer = csv.writer(appFile)
                    writeList = Shift_Countitems
                    writer.writerow(writeList)
                    appFile.close()

        if row_kwc == 0 and InfoTable_matrix[0,0] != "EOWEO Schedule" and Dupont_Flag != 1:
            Max_Flag = Find_Check_Seven
        elif InfoTable_matrix[0, 0] == "7on/7off Schedule":
            Max_Flag = 7
        elif InfoTable_matrix[0, 0] == "Four/Five Schedule":
            Max_Flag = 5
        elif InfoTable_matrix[0, 0] == "4on/4off Schedule":
            Max_Flag = 4

        countrow_rplcRef = 0
        OTSchedHrs_table = pd.read_table('TempSchedule.txt', delimiter=',')
        sdf_OTSchedHrs = pd.DataFrame(OTSchedHrs_table)
        OTSchedHrs_matrix = np.array(sdf_OTSchedHrs)
        OTSchedHrs_shape = np.shape(OTSchedHrs_matrix)[0]
        ShiftCount_table = pd.read_table('ShiftCount.txt', delimiter=',')
        sdf_ShiftCount = pd.DataFrame(ShiftCount_table)
        ShiftCount_matrix = np.array(sdf_ShiftCount)
        ShiftCount_shape = np.shape(ShiftCount_matrix)[0]
        ShiftCount_matrix1 = []
        ShiftCount_matrix2 = []
        ShiftCount_matrix3 = []
        ShiftCount_matrix4 = []
        ShiftCount_matrix5 = []
        ShiftCount_matrix6 = []
        ShiftCount_matrix7 = []
        Comb_extraDTemp = []
        Comb_extraNTemp = []
        if Double_value != 0:
            if np.shape(ShiftCount_matrix)[0] > (post_size*Double_value)-1:
                for del_items in range(np.shape(ShiftCount_matrix)[0] - (post_size*(Double_value-1))):
                    ShiftCount_matrix = np.delete(ShiftCount_matrix, 0, 0)
                ShiftCount_matrix1 = ShiftCount_matrix
                for del_items in range(post_size):  #for doubleday - 2
                    ShiftCount_matrix1 = np.delete(ShiftCount_matrix1, 0, 0)
                ShiftCount_matrix2 = ShiftCount_matrix1
                for del_items in range(post_size):  #for doubleday - 3
                    ShiftCount_matrix2 = np.delete(ShiftCount_matrix2, 0, 0)
                ShiftCount_matrix3 = ShiftCount_matrix2
                for del_items in range(post_size):  #for doubleday - 4
                    ShiftCount_matrix3 = np.delete(ShiftCount_matrix3, 0, 0)
                ShiftCount_matrix4 = ShiftCount_matrix3
                for del_items in range(post_size):  #for doubleday - 5
                    ShiftCount_matrix4 = np.delete(ShiftCount_matrix4, 0, 0)
                ShiftCount_matrix5 = ShiftCount_matrix4
                for del_items in range(post_size):  #for doubleday - 6
                    ShiftCount_matrix5 = np.delete(ShiftCount_matrix5, 0, 0)
        Comb_CompNumberD = []
        Comb_CompNumberN = []
        #Copy Post Data columns to create Night Shift
        matrix_add1 = []
        for rows_add in range(np.shape(matrix_try)[0]):
            matrix_add2 = []
            for cols_add in range(post_size2):
                matrix_add2 += [matrix_try[rows_add, cols_add+2]]
            matrix_add1 += [matrix_add2]
        matrix_add = np.hstack((matrix_try, matrix_add1))
        #Update matrix_add values for Day / Night Shift from ShiftDupont.txt
        cols_night = np.shape(keithDayPosts)[0] + 2
        #The Changer
        kwcdate = datetime.strptime(sdf_matrix[row_kwc+weeks_try][0], "%Y-%m-%d")
        kwcdate1 = datetime.strftime(kwcdate, "%b %d, %Y")
        Team_NameD = "Team " + (sdf_matrix[row_kwc+weeks_try][1])
        Team_NameN = "Team " + (sdf_matrix[row_kwc+weeks_try][2])

        #Reset EBBlocks
        if kwcdate > EBBlock_End and EBBlock_End != datetime.strptime(sdf_matrix[25][0], "%Y-%m-%d") \
                and Possible_SlideFlag == 0 and Dupont_Flag != 1:
            Current_slidematrixName += [Current_slide]
            Current_slidematrixEBBlock_Start += [str(EBBlock_Start.date())]
            Current_slidematrixEBBlock_End += [str(EBBlock_End.date())]
            #EBBlock_StartHold = EBBlock_Start
            EBBlock_End = datetime.strptime(sdf_matrix[25][0], "%Y-%m-%d")
            EBBlock_Start = pdfstart_date - timedelta(1)
            kwcNightVac_Date = []
            kwcNightVac_Name = []
            kwcDayVac_Date = []
            kwcDayVac_Name = []
            OTReset_Count = 0
            extra_boardNadj = 8000
            extra_boardDadj = 8000
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Restart_FlagE = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if Dupont_Flag == 1 and kwcdate > EBBlock_End and kwcdate.weekday() == 1:
            Current_slide = ""
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if Dupont_Flag == 1 and row_kwc > 7 and EBBlock_End < kwcdate < EBBlock_Start:
            Current_slide = ""
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            kwcNightVac_Name = []
            kwcDayVac_Name = []
            kwcDayVac_Date = []
            kwcNightVac_Date = []
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if InfoTable_matrix[0,0] == "4on/4off Schedule" and row_kwc == 0 and kwcdate < EBBlock_Start:
            EBBlock_End = datetime.strptime(sdf_matrix[25][0], "%Y-%m-%d")
            EBBlock_Start = pdfstart_date - timedelta(1)
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            kwcNightVac_Name = []
            kwcDayVac_Name = []
            kwcDayVac_Date = []
            kwcNightVac_Date = []
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            extra_boardDadj = 8000
            extra_boardNadj = 8000
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if (InfoTable_matrix[0,0] != "EOWEO Schedule" or Dupont_Flag != 1) and row_kwc > 0 \
                and extra_boardDadj == 8000 and extra_boardNadj == 8000 and kwcdate > EBBlock_End:
            Current_slide = ""
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            kwcNightVac_Name = []
            kwcDayVac_Name = []
            kwcDayVac_Date = []
            kwcNightVac_Date = []
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            extra_boardDadj = 8000
            extra_boardNadj = 8000
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if Dupont_Flag == 1 and kwcdate < EBBlock_Start and kwcdate.weekday() == 0 \
                and extra_boardDadj == 8000 and extra_boardNadj == 8000:
            Current_slide = ""
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            OTReset_Count = 0
            Current_slidematrixEBBlock_End = []
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if Dupont_Flag == 1 and kwcdate < EBBlock_Start and kwcdate.weekday() == 1:
            Current_slide = ""
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if kwcdate >= EBBlock_End and Possible_Slide == "" \
                and InfoTable_matrix[0, 0] != "EOWEO Schedule" and Dupont_Flag == 1 and kwcdate.weekday() == 4 \
                and extra_boardDadj == 8000 and extra_boardNadj == 8000:
            Current_slide = ""
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if kwcdate > EBBlock_End and Possible_Slide == "" \
                and InfoTable_matrix[0, 0] != "EOWEO Schedule" and Current_slide == "":
            Current_slide = ""
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            kwcNightVac_Name = []
            kwcDayVac_Name = []
            kwcDayVac_Date = []
            kwcNightVac_Date = []
            extra_boardDadj = 8000
            extra_boardNadj = 8000
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] \
                and Dupont_Flag == 1 and kwcdate.weekday() == 4 and kwcdate > EBBlock_End:
            Current_slide = ""
            Current_slidematrixName += [Current_slide]
            Current_slidematrixEBBlock_Start += [str(EBBlock_Start.date())]
            Current_slidematrixEBBlock_End += [str(EBBlock_End.date())]
            OTReset_Count = 0
            EBBlock_End = datetime.strptime(sdf_matrix[25][0], "%Y-%m-%d")
            EBBlock_Start = pdfstart_date - timedelta(1)
            kwcNightVac_Date = []
            kwcNightVac_Name = []
            kwcDayVac_Date = []
            kwcDayVac_Name = []
            extra_boardNadj = 8000
            extra_boardDadj = 8000
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] \
                and InfoTable_matrix[0,0] == "EOWEO Schedule" and kwcdate > EBBlock_End and EBBlock_End.weekday() != 6 \
                and EBBlock_Start != 4:
            Current_slide = ""
            Current_slidematrixName += [Current_slide]
            Current_slidematrixEBBlock_Start += [str(EBBlock_Start.date())]
            Current_slidematrixEBBlock_End += [str(EBBlock_End.date())]
            EBBlock_End = datetime.strptime(sdf_matrix[25][0], "%Y-%m-%d")
            EBBlock_Start = pdfstart_date - timedelta(1)
            OTReset_Count = 0
            kwcNightVac_Date = []
            kwcNightVac_Name = []
            kwcDayVac_Date = []
            kwcDayVac_Name = []
            extra_boardNadj = 8000
            extra_boardDadj = 8000
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
            Restart_FlagE = 0
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] \
                and InfoTable_matrix[0, 0] == "EOWEO Schedule" and kwcdate.weekday() == 4 and EBBlock_Start.weekday() != 4:
            Current_slidematrixName = []
            Current_slidematrixEBBlock_Start = []
            Current_slidematrixEBBlock_End = []
            OTReset_Count = 0
            extra_boardNadj = 8000
            extra_boardDadj = 8000
            Slide_to_DaysFlag = 0
            Slide_to_NightsFlag = 0
            SingleNight_Slide = ""
            SingleDay_Slide = ""
            BadNameD1 = ""
            BadNameD2 = ""
            BadNameD3 = ""
            BadNameD4 = ""
            BadNameN1 = ""
            BadNameN2 = ""
            BadNameN3 = ""
            BadNameN4 = ""
            Bad_SlideHaltD = 0
            Bad_SlideHaltN = 0
            Count_QuitSlideD = 0
            Count_QuitSlideN = 0
            Restart_FlagE = 0
        #print(kwcdate.date(), EBBlock_Start.date(), EBBlock_End.date(), 'D1', BadNameD1, 'D2', BadNameD2, 'D3',
        #      BadNameD3, 'D4', BadNameD4, 'N1', BadNameN1, 'N2', BadNameN2, 'N3', BadNameN3, 'N4', BadNameN4,
        #      'Curr -', Current_slide, Bad_Slide, extra_boardDadj, extra_boardNadj, Count_QuitSlideD, Count_QuitSlideN, Bad_Slide)
        countdays = 1
        next3_Ateam = 0
        next3_Bteam = 0
        next3_Cteam = 0
        next3_Dteam = 0
        for next_3shifts in range(np.shape(sdf_matrix)[0]):
            if sdf_matrix[next_3shifts][0] == datetime.strftime(kwcdate + timedelta(countdays), "%Y-%m-%d"):
                if sdf_matrix[next_3shifts][1] == "A" or sdf_matrix[next_3shifts][2] == "A":
                    next3_Ateam += 1
                if sdf_matrix[next_3shifts][1] == "B" or sdf_matrix[next_3shifts][2] == "B":
                    next3_Bteam += 1
                if sdf_matrix[next_3shifts][1] == "C" or sdf_matrix[next_3shifts][2] == "C":
                    next3_Cteam += 1
                if sdf_matrix[next_3shifts][1] == "D" or sdf_matrix[next_3shifts][2] == "D":
                    next3_Dteam += 1
                countdays += 1
                if countdays == countdays_factor:
                    break
        # Adjust Numbers in matrix based on shift, appointments, and OT rules
        DayVac_Names = ""
        DayTrain_Names = ""
        NightVac_Names = ""
        NightTrain_Names = ""
        EB_Dayfactor = 0
        EB_Nightfactor = 0
        Start_time2 = datetime.now()
        for rows_rplc in range(np.shape(matrix_try)[0]):
            DavVac_RowCount = 0
            DayTrain_Count = 0
            NightVac_RowCount = 0
            NightTrain_Count = 0
            NightVac_Names1 = []
            Double_add = 0
            for cols_rplc in range(np.shape(matrix_add)[1]):
                if appoint_shape != 0:
                    #appoint_shape = 1
                    for i_appoint in range(appoint_shape):
                        if cols_rplc > 1:
                            #Slide to Nights
                            if cols_rplc < cols_night:
                                if np.shape(appoint_matrix)[0] > 0:
                                    if matrix_add[rows_rplc, 1] == appoint_matrix[i_appoint][1]:
                                        NightVac_Names1 += [matrix_add[rows_rplc, 1]]
                                if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "A":
                                    EB_Dayfactor = A_DaysEBnumber
                                    EBNightShift_Team = "A"
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "B":
                                    EB_Dayfactor = B_DaysEBnumber
                                    EBNightShift_Team = "B"
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "C":
                                    EB_Dayfactor = C_DaysEBnumber
                                    EBNightShift_Team = "C"
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "D":
                                    EB_Dayfactor = D_DaysEBnumber
                                    EBNightShift_Team = "D"
                                ExtraBoardD_value = 0
                                if Dupont_Flag == 1 and extra_boardDadj == 50 and \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 2] != \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus-1, 2] \
                                        and extra_boardNadj != 50 and EBBlock_End < kwcdate and Bad_SlideHaltD == 0 \
                                        and kwcdate.weekday() == 0:
                                    kwcNightVac_Name = []
                                    kwcNightVac_Date = []
                                    Count_tol = 1
                                    for items_tol in range(Max_Flag):
                                        if sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol, 2] == \
                                                sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol-1, 2]:
                                            Count_tol += 1
                                    for eb_rows in range(0, Count_tol):
                                        for j_appoint in range(appoint_shape):
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 1] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcDayVac_Name:
                                                    if kwcDayVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcDayVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcDayVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 2] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcNightVac_Name:
                                                    if kwcNightVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcNightVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcNightVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                        night_Vacdates_count = 0
                                        day_Vacdates_count = 0
                                        for night_Vacdates in range(np.shape(kwcNightVac_Date)[0]):
                                            if kwcNightVac_Date.count(kwcNightVac_Date[night_Vacdates]) < 2:
                                                night_Vacdates_count += EB_Dayfactor/2
                                        for day_Vacdates in kwcDayVac_Date:
                                            if day_Vacdates in kwcNightVac_Date and kwcDayVac_Date.count(day_Vacdates) == EB_Dayfactor:
                                                day_Vacdates_count += EB_Dayfactor
                                        if (np.shape(kwcNightVac_Name)[0]-night_Vacdates_count-day_Vacdates_count) / \
                                                EB_Dayfactor >= EBSlide_tolerance:
                                            extra_boardDadj = 50 + ExtraBoardD_valueAdj
                                            EBBlock_Start = kwcdate + timedelta(0)
                                            EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                            if EBBlock_End.weekday() == 0:
                                                EBBlock_End = kwcdate + timedelta(Count_tol-2)
                                            else:
                                                EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                            Current_slide = ""
                                            break
                                        else:
                                            extra_boardDadj = 8000
                                if Dupont_Flag == 1 and extra_boardDadj != 50 and \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] \
                                        and extra_boardNadj != 50 and EBBlock_End < kwcdate and Bad_SlideHaltD == 0 \
                                        and (kwcdate.weekday() != 0 or kwcdate.weekday() != 1) \
                                        and sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] \
                                        == sdf_matrix[row_kwc+weeks_try+week_tryadj+1, 2]:
                                    Count_tol = 1
                                    for items_tol in range(Max_Flag):
                                        if sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol, 2] == \
                                                sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol-1, 2]:
                                            Count_tol += 1
                                    for eb_rows in range(0, Count_tol):
                                        for j_appoint in range(appoint_shape):
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 1] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcDayVac_Name:
                                                    if kwcDayVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcDayVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcDayVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 2] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcNightVac_Name:
                                                    if kwcNightVac_Date.count(appoint_matrix[j_appoint][0]) <= 1 \
                                                            and datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d").weekday() != 0:
                                                        kwcNightVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcNightVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                        night_Vacdates_count = 0
                                        day_Vacdates_count = 0
                                        for night_Vacdates in range(np.shape(kwcNightVac_Date)[0]):
                                            if kwcNightVac_Date.count(kwcNightVac_Date[night_Vacdates]) < 2:
                                                night_Vacdates_count += EB_Dayfactor/2
                                        for day_Vacdates in kwcDayVac_Date:
                                            if day_Vacdates in kwcNightVac_Date and kwcDayVac_Date.count(day_Vacdates) == EB_Dayfactor:
                                                day_Vacdates_count += EB_Dayfactor
                                        if (np.shape(kwcNightVac_Name)[0]-night_Vacdates_count-day_Vacdates_count) / \
                                                EB_Dayfactor >= EBSlide_tolerance:
                                            extra_boardDadj = 50 + ExtraBoardD_valueAdj
                                            EBBlock_Start = kwcdate
                                            EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                            if EBBlock_End.weekday() == 0:
                                                EBBlock_End = kwcdate + timedelta(Count_tol-2)
                                            else:
                                                EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                            Current_slide = ""
                                            break
                                        else:
                                            extra_boardDadj = 8000
                                elif Dupont_Flag == 1 and extra_boardDadj != 50 and \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 2] != \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus-1, 2] \
                                        and extra_boardNadj != 50 and EBBlock_End < kwcdate and Bad_SlideHaltD == 0 \
                                        and kwcdate.weekday() == 1 and sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 2] \
                                        == sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus+1, 2]:
                                    Count_tol = 1
                                    for items_tol in range(Max_Flag):
                                        if sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol, 2] == \
                                                sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol-1, 2]:
                                            Count_tol += 1
                                    for eb_rows in range(0, Count_tol):
                                        for j_appoint in range(appoint_shape):
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 1] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcDayVac_Name:
                                                    if kwcDayVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcDayVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcDayVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 2] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcNightVac_Name:
                                                    if kwcNightVac_Date.count(appoint_matrix[j_appoint][0]) <= 1 \
                                                            and datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d").weekday() != 0:
                                                        kwcNightVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcNightVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                        night_Vacdates_count = 0
                                        day_Vacdates_count = 0
                                        for night_Vacdates in range(np.shape(kwcNightVac_Date)[0]):
                                            if kwcNightVac_Date.count(kwcNightVac_Date[night_Vacdates]) < 2:
                                                night_Vacdates_count += EB_Dayfactor/2
                                        for day_Vacdates in kwcDayVac_Date:
                                            if day_Vacdates in kwcNightVac_Date and kwcDayVac_Date.count(day_Vacdates) == EB_Dayfactor:
                                                day_Vacdates_count += EB_Dayfactor
                                        if (np.shape(kwcNightVac_Name)[0]-night_Vacdates_count - day_Vacdates_count) / \
                                                EB_Dayfactor >= EBSlide_tolerance:
                                            extra_boardDadj = 50 + ExtraBoardD_valueAdj
                                            EBBlock_Start = kwcdate
                                            #EBBlock_StartHold = EBBlock_Start
                                            EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                            if EBBlock_End.weekday() == 0:
                                                EBBlock_End = kwcdate + timedelta(Count_tol-2)
                                            else:
                                                EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                            Current_slide = ""
                                            break
                                        else:
                                            extra_boardDadj = 8000
                                if Dupont_Flag != 1 and sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 2] == \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and extra_boardDadj != 50 and \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] \
                                        and Bad_SlideHaltD == 0:
                                    Count_tol = 1
                                    for items_tol in range(Max_Flag):
                                        if sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol, 2] == \
                                                sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol-1, 2]:
                                            Count_tol += 1
                                    for eb_rows in range(0, Count_tol):
                                        for j_appoint in range(appoint_shape):
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 1] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcDayVac_Name:
                                                    if kwcDayVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcDayVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcDayVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 2] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcNightVac_Name:
                                                    if kwcNightVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcNightVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcNightVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                        night_Vacdates_count = 0
                                        day_Vacdates_count = 0
                                        for night_Vacdates in range(np.shape(kwcNightVac_Date)[0]):
                                            if kwcNightVac_Date.count(kwcNightVac_Date[night_Vacdates]) < 2:
                                                night_Vacdates_count += EB_Dayfactor/2
                                        for day_Vacdates in kwcDayVac_Date:
                                            if day_Vacdates in kwcNightVac_Date and kwcDayVac_Date.count(day_Vacdates) == EB_Dayfactor:
                                                day_Vacdates_count += EB_Dayfactor
                                        if (np.shape(kwcNightVac_Name)[0]-night_Vacdates_count-day_Vacdates_count) / \
                                                EB_Dayfactor >= EBSlide_tolerance:
                                            extra_boardDadj = 50 + ExtraBoardD_valueAdj
                                            EBBlock_Start = kwcdate
                                            if extra_boardNadj == 50:
                                                EBBlock_EndN2 = EBBlock_Start + timedelta(2)
                                            else:
                                                EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                                Current_slide = ""
                                        else:
                                            extra_boardDadj = 8000

                                if InfoTable_matrix[0, 0] != "EOWEO Schedule" and Dupont_Flag != 1 \
                                        and sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 2] == \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and extra_boardDadj != 50 and \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] \
                                        and extra_boardNadj != 50 and EBBlock_End < kwcdate and Bad_SlideHaltD == 0 and \
                                        row_kwc == 0:
                                    Count_tol = 0
                                    for items_tol in range(Max_Flag):
                                        if sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol, 2] == \
                                                sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol-1, 2]:
                                            Count_tol += 1
                                    for eb_rows in range(0, Count_tol):
                                        for j_appoint in range(appoint_shape):
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 1] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcDayVac_Name:
                                                    if kwcDayVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcDayVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcDayVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 2] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcNightVac_Name:
                                                    if kwcNightVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcNightVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcNightVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                        night_Vacdates_count = 0
                                        day_Vacdates_count = 0
                                        for night_Vacdates in range(np.shape(kwcNightVac_Date)[0]):
                                            if kwcNightVac_Date.count(kwcNightVac_Date[night_Vacdates]) < 2:
                                                night_Vacdates_count += EB_Dayfactor/2
                                        for day_Vacdates in kwcDayVac_Date:
                                            if day_Vacdates in kwcNightVac_Date and kwcDayVac_Date.count(day_Vacdates) == EB_Dayfactor:
                                                day_Vacdates_count += EB_Dayfactor
                                        if (np.shape(kwcNightVac_Name)[0]-night_Vacdates_count-day_Vacdates_count) / \
                                                EB_Dayfactor >= EBSlide_tolerance:
                                            extra_boardDadj = 50 + ExtraBoardD_valueAdj
                                            EBBlock_Start = kwcdate
                                            #EBBlock_StartHold = EBBlock_Start
                                            EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                            Current_slide = ""
                                            break
                                        else:
                                            extra_boardDadj = 8000
                            #Slide to Days
                            if cols_rplc >= cols_night:
                                if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "A":
                                    EB_Nightfactor = A_NightsEBnumber
                                    EBDayShift_Team = "A"
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "B":
                                    EB_Nightfactor = B_NightsEBnumber
                                    EBDayShift_Team = "B"
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "C":
                                    EB_Nightfactor = C_NightsEBnumber
                                    EBDayShift_Team = "C"
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "D":
                                    EB_Nightfactor = D_NightsEBnumber
                                    EBDayShift_Team = "D"
                                ExtraBoardD_value = 0
                                if Dupont_Flag == 1 and sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 1] == \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus+1, 1] and \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] and \
                                        EBBlock_End < kwcdate and Bad_SlideHaltN == 0 and \
                                        extra_boardDadj != 50 and extra_boardNadj != 50 and kwcdate.weekday() != 0:
                                    Count_tol = 1
                                    for items_tol in range(Max_Flag):
                                        if sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol, 2] == \
                                                sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol-1, 2]:
                                            Count_tol += 1
                                    for eb_rows in range(0, Count_tol):
                                        for j_appoint in range(appoint_shape):
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 1] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcDayVac_Name:
                                                    if kwcDayVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcDayVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcDayVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 2] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcNightVac_Name:
                                                    if kwcNightVac_Date.count(appoint_matrix[j_appoint][0]) <= 1 \
                                                            and datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d").weekday() != 0:
                                                        kwcNightVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcNightVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                    day_Vacdates_count = 0
                                    night_Vacdates_count = 0
                                    for day_Vacdates in range(np.shape(kwcDayVac_Date)[0]):
                                        if kwcDayVac_Date.count(kwcDayVac_Date[day_Vacdates]) < 2:
                                            day_Vacdates_count += 1
                                        if datetime.strptime(kwcDayVac_Date[day_Vacdates], "%Y-%m-%d").weekday() == 0 \
                                                and kwcdate.weekday() == 1:
                                            day_Vacdates_count += EB_Nightfactor/2
                                    for night_Vacdates in kwcNightVac_Date:
                                        if night_Vacdates in kwcDayVac_Date and kwcNightVac_Date.count(night_Vacdates) == EB_Nightfactor:
                                            night_Vacdates_count += EB_Nightfactor
                                    if (np.shape(kwcDayVac_Name)[0]-day_Vacdates_count-night_Vacdates_count) / \
                                            EB_Nightfactor >= EBSlide_tolerance:
                                        extra_boardNadj = 50 + ExtraBoardN_valueAdj
                                        EBBlock_Start = kwcdate
                                        EBBlock_End = EBBlock_Start + timedelta(Count_tol-1)
                                        if EBBlock_Start.weekday() == 4:
                                            EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                        else:
                                            EBBlock_End = kwcdate + timedelta(Count_tol-1)
                                        Current_slide = ""
                                    else:
                                        extra_boardNadj = 8000
                                if Dupont_Flag != 1 and sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 1] == \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] and \
                                        Bad_SlideHaltN == 0 and \
                                        extra_boardNadj != 50:
                                    day_Vacdates_count = 0
                                    night_Vacdates_count = 0
                                    for day_Vacdates in range(np.shape(kwcDayVac_Date)[0]):
                                        if kwcDayVac_Date.count(kwcDayVac_Date[day_Vacdates]) < EB_Nightfactor:
                                            day_Vacdates_count += EB_Nightfactor/2
                                    for night_Vacdates in kwcNightVac_Date:
                                        if night_Vacdates in kwcDayVac_Date and kwcNightVac_Date.count(night_Vacdates) == EB_Nightfactor:
                                            night_Vacdates_count += EB_Nightfactor
                                    if (np.shape(kwcDayVac_Name)[0]-day_Vacdates_count-night_Vacdates_count) / \
                                            EB_Nightfactor >= EBSlide_tolerance:
                                        extra_boardNadj = 50 + ExtraBoardN_valueAdj
                                        EBBlock_Start = kwcdate
                                        Detect_Two_EBlocksFlag = 0
                                        if extra_boardDadj == 50:
                                            EBBlock_StartD2 = EBBlock_Start + timedelta(3)
                                            EBBlock_EndN2 = EBBlock_Start + timedelta(2)
                                        else:
                                            Current_slide = ""
                                            EBBlock_End = EBBlock_Start + timedelta(Count_tol-1)
                                    else:
                                        extra_boardNadj = 8000
                                if InfoTable_matrix[0, 0] != "EOWEO Schedule" and Dupont_Flag != 1 \
                                        and sdf_matrix[row_kwc+weeks_try+week_tryadj+EBSlide_plus, 1] == \
                                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                                        row_kwc == 0 and EBBlock_End < kwcdate and Bad_SlideHaltN == 0 and \
                                        extra_boardNadj != 50 and extra_boardDadj != 50:
                                    if row_kwc == 0:
                                        Count_tol = 0
                                    else:
                                        Count_tol = 1
                                    for items_tol in range(Max_Flag):
                                        if sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol, 2] == \
                                                sdf_matrix[row_kwc+weeks_try+week_tryadj+items_tol-1, 2]:
                                            Count_tol += 1
                                    for eb_rows in range(0, Count_tol):
                                        for j_appoint in range(appoint_shape):
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 1] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcDayVac_Name:
                                                    if kwcDayVac_Date.count(appoint_matrix[j_appoint][0]) <= 1:
                                                        kwcDayVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcDayVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                            if kwcdate + timedelta(eb_rows) == datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d") \
                                                    and sdf_matrix[row_kwc+weeks_try+week_tryadj+eb_rows, 2] == \
                                                    appoint_matrix[j_appoint][2] and \
                                                    (appoint_matrix[j_appoint][3] == "Vacation"
                                                     or appoint_matrix[j_appoint][3] == "Off" or appoint_matrix[j_appoint][3] == "Train/SpecialAssign"):
                                                if appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1] not in kwcNightVac_Name:
                                                    if kwcNightVac_Date.count(appoint_matrix[j_appoint][0]) <= 1 \
                                                            and datetime.strptime(appoint_matrix[j_appoint][0], "%Y-%m-%d").weekday() != 0:
                                                        kwcNightVac_Date += [appoint_matrix[j_appoint][0]]
                                                        kwcNightVac_Name += [appoint_matrix[j_appoint][0] + ":" + appoint_matrix[j_appoint][1]]
                                    day_Vacdates_count = 0
                                    night_Vacdates_count = 0
                                    for day_Vacdates in range(np.shape(kwcDayVac_Date)[0]):
                                        if kwcDayVac_Date.count(kwcDayVac_Date[day_Vacdates]) < 2:
                                            day_Vacdates_count += EB_Nightfactor/2
                                    for night_Vacdates in kwcNightVac_Date:
                                        if night_Vacdates in kwcDayVac_Date and kwcNightVac_Date.count(night_Vacdates) == EB_Nightfactor:
                                            night_Vacdates_count += EB_Nightfactor
                                    if (np.shape(kwcDayVac_Name)[0]-day_Vacdates_count-night_Vacdates_count) / \
                                            EB_Nightfactor >= EBSlide_tolerance:
                                        extra_boardNadj = 50 + ExtraBoardN_valueAdj
                                        EBBlock_Start = kwcdate
                                        EBBlock_End = EBBlock_Start + timedelta(Count_tol-1)
                                        Current_slide = ""
                                        break
                                    else:
                                        extra_boardNadj = 8000

                            #Adjust numbers based on preferences
                            if appoint_shape != 0:
                                if matrix_add[rows_rplc, 1] == appoint_matrix[i_appoint][1] \
                                        and kwcdate == datetime.strptime(appoint_matrix[i_appoint][0], "%Y-%m-%d") \
                                        and cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < OT_ablenoway:
                                    if appoint_matrix[i_appoint][3] == "Vacation" or appoint_matrix[i_appoint][3] == "Off":
                                        matrix_add[rows_rplc, cols_rplc] = Big_Blocker
                                        if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                                                DavVac_RowCount != 1:
                                            if DayVac_Names == "":
                                                DayVac_Names = appoint_matrix[i_appoint][1]
                                                if kwcdate == pdfstart_date:
                                                    EBDays_VacName = appoint_matrix[i_appoint][1]
                                            else:
                                                DayVac_Names = DayVac_Names + "/" + appoint_matrix[i_appoint][1]
                                            DavVac_RowCount = 1
                                        elif matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                                                NightVac_RowCount != 1:
                                            if NightVac_Names == "":
                                                NightVac_Names = appoint_matrix[i_appoint][1]
                                            else:
                                                NightVac_Names = NightVac_Names + "/" + appoint_matrix[i_appoint][1]
                                            NightVac_RowCount = 1
                                    if appoint_matrix[i_appoint][3] == "Train/SpecialAssign":
                                        matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                                        if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                                                DayTrain_Count != 1:
                                            if DayTrain_Names == "":
                                                DayTrain_Names = appoint_matrix[i_appoint][1]
                                            else:
                                                DayTrain_Names = DayTrain_Names + "/" + appoint_matrix[i_appoint][1]
                                            DayTrain_Count = 1
                                        elif matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                                                NightTrain_Count != 1:
                                            if NightTrain_Names == "":
                                                NightTrain_Names = appoint_matrix[i_appoint][1]
                                            else:
                                                NightTrain_Names = NightTrain_Names + "/" + appoint_matrix[i_appoint][1]
                                            NightTrain_Count = 1
                                    SeniorityOT_Adjust = matrix_OThours[rows_rplc, np.shape(keithDayPosts)[0]+4]
                                    if appoint_matrix[i_appoint][3] == "OT-Refuse":
                                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+5100-SeniorityOT_Adjust
                                    if appoint_matrix[i_appoint][3] == "OT-Block":
                                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+Big_Blocker
                                    if appoint_matrix[i_appoint][3] == "OT-Block-Nights" and cols_rplc >= cols_night:
                                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+Big_Blocker
                                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                                            appoint_matrix[i_appoint][3].find("Swap", 0, 4) != -1 and \
                                            matrix_add[rows_rplc, cols_rplc] < 10 and appoint_matrix[i_appoint][3][9:10] \
                                            != sdf_matrix[row_kwc+weeks_try+week_tryadj, 2]:
                                        if matrix_add[rows_rplc, cols_rplc] < OT_ablenoway:
                                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ablenoway
                                            if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                                                    DavVac_RowCount != 1 and appoint_matrix[i_appoint][3][9:10] \
                                                    != sdf_matrix[row_kwc+weeks_try+week_tryadj, 2]:
                                                if DayVac_Names == "":
                                                    DayVac_Names = appoint_matrix[i_appoint][1] + "-Swap"
                                                else:
                                                    DayVac_Names = DayVac_Names + "/" + appoint_matrix[i_appoint][1] + "-Swap"
                                                DavVac_RowCount = 1
                                                Swapcount += 1
                                                Swapname = appoint_matrix[i_appoint][1]
                                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] \
                                            and appoint_matrix[i_appoint][3].find("Swap", 0, 4) != -1 and \
                                            matrix_add[rows_rplc, cols_rplc] < 10 and appoint_matrix[i_appoint][3][9:10] \
                                            != sdf_matrix[row_kwc+weeks_try+week_tryadj, 1]:
                                        if matrix_add[rows_rplc, cols_rplc] < OT_ablenoway:
                                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ablenoway
                                            if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                                                    NightVac_RowCount != 1:
                                                if NightVac_Names == "":
                                                    NightVac_Names = appoint_matrix[i_appoint][1] + "-Swap"
                                                else:
                                                    NightVac_Names = NightVac_Names + "/" + appoint_matrix[i_appoint][1] + "-Swap"
                                                NightVac_RowCount = 1
                                                Swapcount += 1
                                                Swapname = appoint_matrix[i_appoint][1]
                                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                                            appoint_matrix[i_appoint][3].find("Swap", 0, 4) != -1 and \
                                            matrix_add[rows_rplc, 0] == appoint_matrix[i_appoint][2]:
                                        if matrix_add[rows_rplc, cols_rplc] < 10 and appoint_matrix[i_appoint][3][9:10] \
                                                != sdf_matrix[row_kwc+weeks_try+week_tryadj, 2]:
                                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+Shift_Adj
                                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] \
                                            and appoint_matrix[i_appoint][3].find("Swap", 0, 4) != -1 and \
                                            matrix_add[rows_rplc, 0] == appoint_matrix[i_appoint][2]:
                                        if matrix_add[rows_rplc, cols_rplc] < 10 and appoint_matrix[i_appoint][3][9:10] \
                                                != sdf_matrix[row_kwc+weeks_try+week_tryadj, 1]:
                                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+Shift_Adj
                                    #Off Shift OT Swap adjustment
                                    if (matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and
                                        matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 2]) and \
                                            cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10:
                                        if kwcdate == datetime.strptime(appoint_matrix[i_appoint][0], "%Y-%m-%d") \
                                                and appoint_matrix[i_appoint][3].find("Swap", 0, 4) != -1:
                                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+Shift_Adj
                                            Swapname2 = matrix_add[rows_rplc, 1]
                                            Swapdate = kwcdate

                if cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and \
                        (matrix_add[rows_rplc, 1] + "-OT Fatigue"
                         in Comb_NamesDaysV or matrix_add[rows_rplc, 1] + "-Fatigue"
                         in Comb_NamesDaysV) and matrix_add[rows_rplc, 0] == \
                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and cols_rplc < cols_night:
                    if FatigueDaysFlag1 == "":
                        FatigueDaysFlag1 = matrix_add[rows_rplc, 1]
                    elif FatigueDaysFlag2 == "":
                        FatigueDaysFlag2 = matrix_add[rows_rplc, 1]
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + 12

                if cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and \
                        (matrix_add[rows_rplc, 1] + "-OT Fatigue"
                         in Comb_NamesDaysV or matrix_add[rows_rplc, 1] + "-Fatigue"
                         in Comb_NamesDaysV) and matrix_add[rows_rplc, 0] == \
                        sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and cols_rplc >= cols_night:
                    if FatigueDaysFlag1 == "":
                        FatigueDaysFlag1 = matrix_add[rows_rplc, 1]
                    elif FatigueDaysFlag2 == "":
                        FatigueDaysFlag2 = matrix_add[rows_rplc, 1]
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + 12

                if matrix_add[rows_rplc, 1] == FatigueDaysFlag1 or matrix_add[rows_rplc, 1] == FatigueDaysFlag2:
                    if cols_rplc > 1:
                        if matrix_add[rows_rplc, cols_rplc] < 10:
                            matrix_add[rows_rplc, cols_rplc] += 5
                            FatigueDays_addFlag += 1
                if matrix_add[rows_rplc, 1] == FatigueNightsFlag1 or matrix_add[rows_rplc, 1] == FatigueNightsFlag2:
                    if cols_rplc > 1:
                        if matrix_add[rows_rplc, cols_rplc] < 10:
                            matrix_add[rows_rplc, cols_rplc] += 5
                            FatigueNights_addFlag += 1
                #Day Shift Night Adjustment
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc + weeks_try + week_tryadj, 1] \
                        and cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc >= cols_night \
                        and Bad_SlideHaltD == 0 and EBBlock_Start <= kwcdate <= EBBlock_End:
                    if matrix_add[rows_rplc, 1] == Current_slide and cols_rplc == Current_slidepost:
                        extra_boardcurrent = extra_boardcurrentAdj
                    else:
                        extra_boardcurrent = 0
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+extra_boardDadj + extra_boardcurrent
                if matrix_add[rows_rplc, 1] in Bad_Slide and matrix_add[rows_rplc, 1] == Possible_Slide:
                    Possible_Slide = ""
                if kwcdate > EBBlock_End and row_kwc < total_days - 2 and Restart_Flag != 1:
                    Bad_Slide = []
                if Current_slide == "" and np.shape(Bad_Slide)[0] > 0 and extra_boardDadj == 50 and EBSlide_boost == 1 \
                        and kwcdate == EBBlock_Start and EBSlide_tolboost == 0:
                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                            cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc < cols_night \
                            and cols_rplc == Current_slidepost - cols_night:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenotadj + 20000
                if np.shape(Bad_Slide)[0] > 0 and extra_boardDadj == 50 and EBSlide_boost == 1 \
                        and EBSlide_tolboost == 1:
                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                            cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc < cols_night \
                            and (matrix_add[rows_rplc, 1] == Current_slide or Current_slide == "") and \
                            matrix_add[rows_rplc, 1] in Bad_Slide and matrix_add[rows_rplc, cols_rplc] > 0 \
                            and (matrix_add[rows_rplc, 1] != BadNameD1 or matrix_add[rows_rplc, 1] != BadNameD2
                                 or matrix_add[rows_rplc, 1] != BadNameD3 or matrix_add[rows_rplc, 1] != BadNameD4):
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] - 1
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                        cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc < cols_night and \
                        matrix_add[rows_rplc, 1] == Current_slide and matrix_add[rows_rplc, 1] not in Bad_Slide:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenotadj
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                        cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 100 and cols_rplc < cols_night and \
                        matrix_add[rows_rplc, 1] in Bad_Slide and \
                        cols_rplc == Current_slidepost - cols_night and Bad_SlideStopD == 0 and \
                        Current_slide not in Bad_Slide and matrix_add[rows_rplc, cols_rplc] > 0 and \
                        (matrix_add[rows_rplc, 1] != BadNameD1 or matrix_add[rows_rplc, 1] != BadNameD2
                         or matrix_add[rows_rplc, 1] != BadNameD3 or matrix_add[rows_rplc, 1] != BadNameD4):
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] - 1
                    Bad_SlideStopD = 1
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                        cols_rplc > 2 and matrix_add[rows_rplc, cols_rplc] < 100 and cols_rplc < cols_night and \
                        matrix_add[rows_rplc, 1] in Bad_Slide and matrix_add[rows_rplc, cols_rplc] > 0:
                    if (matrix_add[rows_rplc, 1] == BadNameD1 or matrix_add[rows_rplc, 1] == BadNameD2
                        or matrix_add[rows_rplc, 1] == BadNameD3 or matrix_add[rows_rplc, 1] != BadNameD4) and matrix_add[rows_rplc, cols_rplc] < 10:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + 1
                        Bad_Slide = []
                    elif matrix_add[rows_rplc, cols_rplc] > 0:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] - 1
                if Current_slide + '-Slide' in Comb_NamesDaysV and matrix_add[rows_rplc, 1] == Current_slide and \
                        Bad_SlideHaltD != 0 and cols_rplc > 1 and cols_rplc >= cols_night:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenoway
                elif Bad_SlideHaltN != 0 and cols_rplc > 1 and cols_rplc >= cols_night and \
                        matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                        matrix_add[rows_rplc, 1] not in Comb_NamesDaysV and extra_boardDadj == 50 and \
                        matrix_add[rows_rplc, cols_rplc] < 10:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + extra_boardDadj
                    SingleDay_Slide = matrix_add[rows_rplc, 1]
                if matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesDaysV and matrix_add[rows_rplc, 0] \
                        == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and cols_rplc > 1 and cols_rplc >= cols_night and \
                        matrix_add[rows_rplc, cols_rplc] < 10:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenoway
                #Night Shift Day Adjustment
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc + weeks_try + week_tryadj, 2] and \
                        cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc < cols_night \
                        and Bad_SlideHaltN == 0 and EBBlock_Start <= kwcdate <= EBBlock_End:
                    if matrix_add[rows_rplc, 1] == Current_slide and cols_rplc == Current_slidepost:
                        extra_boardcurrent = extra_boardcurrentAdj
                    else:
                        extra_boardcurrent = 0
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+extra_boardNadj + extra_boardcurrent
                if Current_slide == "" and np.shape(Bad_Slide)[0] > 0 and extra_boardNadj == 50 and EBSlide_boost == 1 \
                        and kwcdate == EBBlock_Start and EBSlide_tolboost == 0:
                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                            cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc >= cols_night \
                            and cols_rplc == Current_slidepost + cols_night - 2:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenotadj + 20000
                if np.shape(Bad_Slide)[0] > 0 and extra_boardNadj == 50 and EBSlide_boost == 1 \
                        and EBSlide_tolboost == 1:
                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                            cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc >= cols_night \
                            and (matrix_add[rows_rplc, 1] == Current_slide or Current_slide == "") and \
                            matrix_add[rows_rplc, 1] in Bad_Slide and matrix_add[rows_rplc, cols_rplc] > 0 and \
                            (matrix_add[rows_rplc, 1] != BadNameN1 or matrix_add[rows_rplc, 1] != BadNameN2
                             or matrix_add[rows_rplc, 1] != BadNameN3 or matrix_add[rows_rplc, 1] != BadNameN4):
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] - 1
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                        cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 10 and cols_rplc >= cols_night and \
                        matrix_add[rows_rplc, 1] == Current_slide and matrix_add[rows_rplc, 1] not in Bad_Slide:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenotadj
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                        cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 100 and cols_rplc >= cols_night and \
                        matrix_add[rows_rplc, 1] in Bad_Slide and \
                        cols_rplc == Current_slidepost + cols_night - 2 and Bad_SlideStopN == 0 and \
                        Current_slide not in Bad_Slide and matrix_add[rows_rplc, cols_rplc] > 0 and \
                        (matrix_add[rows_rplc, 1] != BadNameN1 or matrix_add[rows_rplc, 1] != BadNameN2
                         or matrix_add[rows_rplc, 1] != BadNameN3 or matrix_add[rows_rplc, 1] != BadNameN4):
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] - 2
                    Bad_SlideStopN = 1
                if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                        cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 100 and cols_rplc >= cols_night and \
                        matrix_add[rows_rplc, 1] in Bad_Slide and matrix_add[rows_rplc, cols_rplc] > 0:
                    if (matrix_add[rows_rplc, 1] == BadNameN1 or matrix_add[rows_rplc, 1] == BadNameN2
                        or matrix_add[rows_rplc, 1] == BadNameN3 or matrix_add[rows_rplc, 1] == BadNameN4) and matrix_add[rows_rplc, cols_rplc] < 10 \
                            and extra_boardNadj == 50 and extra_boardDadj != 50:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + 1
                        Bad_Slide = []
                    elif matrix_add[rows_rplc, 1] in Bad_Slide and extra_boardNadj == 50 and \
                            extra_boardDadj == 50:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] - 1
                    elif matrix_add[rows_rplc, cols_rplc] > 0:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] - 1
                elif matrix_add[rows_rplc, 1] not in Bad_Slide and extra_boardDadj == 50 and \
                        extra_boardNadj == 50 and cols_rplc > 1 and matrix_add[rows_rplc, cols_rplc] < 100 \
                        and cols_rplc >= cols_night and matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] \
                        and EBBlock_Start <= kwcdate <= EBBlock_End and \
                        matrix_add[rows_rplc, cols_rplc] < 15:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + .5
                if Current_slide + '-Slide' in Comb_NamesNightsV and matrix_add[rows_rplc, 1] == Current_slide and \
                        Bad_SlideHaltN != 0 and 1 < cols_rplc < cols_night:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenoway
                elif Bad_SlideHaltN != 0 and 1 < cols_rplc < cols_night and \
                        matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                        matrix_add[rows_rplc, 1] not in Comb_NamesNightsV and extra_boardNadj == 50 and \
                        matrix_add[rows_rplc, cols_rplc] < 10:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + extra_boardNadj
                    SingleNight_Slide = matrix_add[rows_rplc, 1]
                if matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesNightsV and matrix_add[rows_rplc, 0] \
                        == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and 1 < cols_rplc < cols_night and \
                        matrix_add[rows_rplc, cols_rplc] < 10:
                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenoway

        #Sum the OT hours from TempSchedule
        result = {}
        for card, value in OTSchedHrs_matrix:
            total = result.get(card, 0) + value
            result[card] = total
        OTstart = list(result.items())
        matrix_OTsum = np.array(OTstart)

        shift_result = {}
        for card, value in ShiftCount_matrix:
            total = shift_result.get(card, 0) + value
            shift_result[card] = total
        ShiftCountstart = list(shift_result.items())
        Shift_sum = np.array(ShiftCountstart)

        shift_result = {}
        for card, value in ShiftCount_matrix1:
            total = shift_result.get(card, 0) + value
            shift_result[card] = total
        ShiftCountstart = list(shift_result.items())
        Shift_sum1 = np.array(ShiftCountstart)

        shift_result = {}
        for card, value in ShiftCount_matrix2:
            total = shift_result.get(card, 0) + value
            shift_result[card] = total
        ShiftCountstart = list(shift_result.items())
        Shift_sum2 = np.array(ShiftCountstart)

        shift_result = {}
        for card, value in ShiftCount_matrix3:
            total = shift_result.get(card, 0) + value
            shift_result[card] = total
        ShiftCountstart = list(shift_result.items())
        Shift_sum3 = np.array(ShiftCountstart)

        shift_result = {}
        for card, value in ShiftCount_matrix4:
            total = shift_result.get(card, 0) + value
            shift_result[card] = total
        ShiftCountstart = list(shift_result.items())
        Shift_sum4 = np.array(ShiftCountstart)

        shift_result = {}
        for card, value in ShiftCount_matrix5:
            total = shift_result.get(card, 0) + value
            shift_result[card] = total
        ShiftCountstart = list(shift_result.items())
        Shift_sum5 = np.array(ShiftCountstart)
        for rows_rplc in range(np.shape(matrix_try)[0]):
            OT_add = 0
            Double_add = 0
            for cols_rplc in range(np.shape(matrix_add)[1]):
                for OT_check in range(np.shape(matrix_OTsum)[0]):
                    if matrix_OTsum[OT_check, 0] == matrix_add[rows_rplc, 1]:
                        OT_add = int(matrix_OTsum[OT_check, 1])
                        break
                for PrevOT_count in range(np.shape(matrix_OThours)[0]):
                    if matrix_add[rows_rplc, 1] == matrix_OThours[PrevOT_count, 1]:
                        if InfoTable_matrix[0, 3] == "OT Hours":
                            Prev_OTHours = matrix_OThours[PrevOT_count, 3]
                        else:
                            Prev_OTHours = 0
                            SeniorityFlag = 1
                for Shift_check in range(np.shape(Shift_sum)[0]):
                    if Shift_sum[Shift_check, 0] == matrix_add[rows_rplc, 1]:
                        if row_kwc > 6:
                            Shift_max = int(Shift_sum[Shift_check, 1])
                            if Shift_max == InfoTable_matrix[0, 2] and InfoTable_matrix[0, 2] != 0:
                                Shift_max_Name = matrix_add[rows_rplc, 1]
                                Shift_max_row = row_kwc
                            elif Shift_max < 7 and row_kwc > Shift_max_row:
                                Shift_max_Name = ""
                                Shift_max_row = -1
                            if Double_value == Shift_max+1:
                                Double_add = 10000
                                break
                for Shift_check in range(np.shape(Shift_sum1)[0]):
                    if Shift_sum1[Shift_check, 0] == matrix_add[rows_rplc, 1]:
                        if row_kwc > 6:
                            if post_size <= Shift_check < np.shape(Shift_sum1)[0]:
                                Shift_max1 = int(Shift_sum1[Shift_check, 1])
                                if matrix_add[rows_rplc, 0] == "A" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum1[Shift_check, 0]:
                                    if Double_value == Shift_max1 + next3_Ateam - 2:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "B" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum1[Shift_check, 0]:
                                    if Double_value == Shift_max1 + next3_Bteam - 2:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "C" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum1[Shift_check, 0]:
                                    if Double_value == Shift_max1 + next3_Cteam - 2:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "D" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum1[Shift_check, 0]:
                                    if Double_value == Shift_max1 + next3_Dteam - 2:
                                        Double_add = 10000
                                        break
                for Shift_check in range(np.shape(Shift_sum2)[0]):
                    if Shift_sum2[Shift_check, 0] == matrix_add[rows_rplc, 1]:
                        if row_kwc > 6:
                            if Shift_check < np.shape(Shift_sum2)[0]:
                                Shift_max2 = int(Shift_sum2[Shift_check, 1])
                                if matrix_add[rows_rplc, 0] == "A" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum2[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max2 + next3_Ateam - 1:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "B" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum2[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max2 + next3_Bteam - 1:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "C" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum2[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max2 + next3_Cteam - 1:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "D" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum2[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max2 + next3_Dteam - 1:
                                        Double_add = 10000
                                        break
                            else:
                                Double_add = 0
                for Shift_check in range(np.shape(Shift_sum3)[0]):
                    if Shift_sum3[Shift_check, 0] == matrix_add[rows_rplc, 1]:
                        if row_kwc > 6:
                            if Shift_check < np.shape(Shift_sum3)[0] and \
                                    countdays_factor > 4:
                                Shift_max3 = int(Shift_sum3[Shift_check, 1])
                                if matrix_add[rows_rplc, 0] == "A" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max3 + next3_Ateam:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "B" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max3 + next3_Bteam:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "C" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max3 + next3_Cteam:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "D" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max3 + next3_Dteam:
                                        Double_add = 10000
                                        break
                for Shift_check in range(np.shape(Shift_sum4)[0]):
                    if Shift_sum4[Shift_check, 0] == matrix_add[rows_rplc, 1]:
                        if row_kwc > 6:
                            if Shift_check < np.shape(Shift_sum4)[0] and \
                                    countdays_factor > 5:
                                Shift_max4 = int(Shift_sum4[Shift_check, 1])
                                if matrix_add[rows_rplc, 0] == "A" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max4 + next3_Ateam - 1:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "B" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max4 + next3_Bteam - 1:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "C" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max4 + next3_Cteam - 1:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "D" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max4 + next3_Dteam - 1:
                                        Double_add = 10000
                                        break
                for Shift_check in range(np.shape(Shift_sum5)[0]):
                    if Shift_sum5[Shift_check, 0] == matrix_add[rows_rplc, 1]:
                        if row_kwc > 6:
                            if post_size*5 <= Shift_check < np.shape(Shift_sum5)[0] and \
                                    countdays_factor > 6:
                                Shift_max5 = int(Shift_sum5[Shift_check, 1])
                                if matrix_add[rows_rplc, 0] == "A" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max5 + next3_Ateam - 2:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "B" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max5 + next3_Bteam - 2:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "C" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max5 + next3_Cteam - 2:
                                        Double_add = 10000
                                        break
                                if matrix_add[rows_rplc, 0] == "D" \
                                        and matrix_add[rows_rplc, 1] == Shift_sum3[Shift_check, 0]:
                                    if Double_value - 1 == Shift_max5 + next3_Dteam - 2:
                                        Double_add = 10000
                                        break
                        else:
                            Shift_max = int(Shift_sum[Shift_check, 1])
                            if Double_value == Shift_max+1:
                                Double_add = 10000 + 10
                            else:
                                Double_add = 0
                        break
                #Days/Nights OT Adjustment
                SeniorityOT_Adjust = matrix_OThours[rows_rplc, np.shape(keithDayPosts)[0]+4]
                if cols_rplc > 1:
                    if matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 3] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 4] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 5]:
                        if matrix_add[rows_rplc, cols_rplc] < 20 and cols_rplc < cols_night:
                            if matrix_sal[rows_rplc, 2] == "Salary":
                                matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                            elif matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesDaysV \
                                    and matrix_sal[rows_rplc, 2] != "Salary":
                                matrix_add[rows_rplc, cols_rplc] = OT_ableadj
                            else:
                                matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                    if matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 6] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 7] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 8]:
                        if matrix_add[rows_rplc, cols_rplc] < 20 and cols_rplc >= cols_night:
                            if matrix_sal[rows_rplc, 2] == "Salary":
                                matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                            elif matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesNightsV \
                                    and matrix_sal[rows_rplc, 2] != "Salary":
                                matrix_add[rows_rplc, cols_rplc] = OT_ableadj
                            else:
                                matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                    if matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 3] and \
                            matrix_add[rows_rplc, 0] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 4] and \
                            cols_rplc < cols_night and matrix_add[rows_rplc, cols_rplc] < 10:
                        matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc] + OT_ablenoway
                    if (matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 3] or
                        matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 4]) and \
                            matrix_add[rows_rplc, cols_rplc] < OT_ableadj and matrix_add[rows_rplc, cols_rplc] < 20:
                        if matrix_sal[rows_rplc, 2] == "Salary" and cols_rplc < cols_night:
                            matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                        elif (matrix_add[rows_rplc, 1] + '-OT' in Comb_NamesNightsV
                              or matrix_add[rows_rplc, 1] + '-OT Fatigue' in Comb_NamesNightsV
                              or matrix_add[rows_rplc, 1] + '-OT Force' in Comb_NamesNightsV or
                              matrix_add[rows_rplc, 1] + '-Swap' in Comb_NamesNightsV) and cols_rplc < cols_night:
                            matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                        elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != \
                                sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] and \
                                matrix_add[rows_rplc, 1] + '-Slide' \
                                in Comb_NamesNightsV and cols_rplc < cols_night \
                                and matrix_sal[rows_rplc, 2] != "Salary":
                            matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                        elif sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] != \
                                sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] and \
                                matrix_add[rows_rplc, 1] + '-Slide' \
                                in Comb_NamesNightsV and cols_rplc >= cols_night \
                                and matrix_sal[rows_rplc, 2] != "Salary":
                            matrix_add[rows_rplc, cols_rplc] = OT_ableadj
                        elif cols_rplc < cols_night:
                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ableadj+OT_add \
                                                               + Double_add + Prev_OTHours + SeniorityOT_Adjust
                    if (matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 6] or
                        matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 7]) \
                            and matrix_add[rows_rplc, cols_rplc] < OT_ableadj and matrix_add[rows_rplc, cols_rplc] < 20:
                        if matrix_sal[rows_rplc, 2] == "Salary" and cols_rplc >= cols_night:
                            matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                        elif (matrix_add[rows_rplc, 1] + '-OT' in Comb_NamesDaysV
                              or matrix_add[rows_rplc, 1] + '-OT Fatigue' in Comb_NamesDaysV
                              or matrix_add[rows_rplc, 1] + '-OT Force' in Comb_NamesDaysV or
                              matrix_add[rows_rplc, 1] + '-Swap' in Comb_NamesDaysV) and cols_rplc >= cols_night:
                            matrix_add[rows_rplc, cols_rplc] = OT_ableadj + 200
                        elif cols_rplc > 0 and sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                                sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                                matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesDaysV and cols_rplc >= cols_night \
                                and matrix_sal[rows_rplc, 2] != "Salary":
                            matrix_add[rows_rplc, cols_rplc] = OT_ablenotadj
                        elif cols_rplc > 0 and sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                                sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                                matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesDaysV and cols_rplc < cols_night \
                                and matrix_sal[rows_rplc, 2] != "Salary":
                            matrix_add[rows_rplc, cols_rplc] = OT_ableadj
                        elif cols_rplc >= cols_night:
                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ableadj+OT_add \
                                                               + Double_add + Prev_OTHours + SeniorityOT_Adjust
                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 5] \
                            and cols_rplc < cols_night and matrix_add[rows_rplc, cols_rplc] < OT_ablenotadj:
                        if matrix_sal[rows_rplc, 2] == "Salary":
                            matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                        elif row_kwc > 1:
                            if matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesDaysV2[row_kwc - 2] \
                                    and matrix_sal[rows_rplc, 2] != "Salary":
                                if Double_add > 0:
                                    matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ablenotadj+ \
                                                                       OT_add+Double_add+Prev_OTHours+SeniorityOT_Adjust
                                else:
                                    matrix_add[rows_rplc, cols_rplc] = OT_ableadj
                            else:
                                matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ablenotadj+ \
                                                                   OT_add+Double_add+Prev_OTHours+SeniorityOT_Adjust
                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 8] \
                            and cols_rplc >= cols_night and matrix_add[rows_rplc, cols_rplc] < OT_ablenotadj:
                        if matrix_sal[rows_rplc, 2] == "Salary":
                            matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                        elif matrix_add[rows_rplc, 1] + '-Slide' in Comb_NamesNightsV \
                                and matrix_sal[rows_rplc, 2] != "Salary":
                            if Double_add > 0:
                                matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ablenotadj+ \
                                                                   OT_add+Double_add+Prev_OTHours+SeniorityOT_Adjust
                            else:
                                matrix_add[rows_rplc, cols_rplc] = OT_ableadj
                        else:
                            matrix_add[rows_rplc, cols_rplc] = matrix_add[rows_rplc, cols_rplc]+OT_ablenotadj+ \
                                                               OT_add+Double_add+Prev_OTHours+SeniorityOT_Adjust
                    if matrix_add[rows_rplc, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj+1, 1] and \
                            cols_rplc >= cols_night and matrix_add[rows_rplc, cols_rplc] <= 5000 and \
                            matrix_add[rows_rplc, cols_rplc] >= 3000:
                        matrix_add[rows_rplc, cols_rplc] = OT_ablenoway
                    if matrix_add[rows_rplc, 1] == Shift_max_Name and matrix_add[rows_rplc, cols_rplc] < 5000:
                        matrix_add[rows_rplc, cols_rplc] = OT_ablenotadj+9999

        #Extract the Names from the Matrix and put into another Matrix
        matrix_names = []
        for m_row in range(np.shape(matrix_try)[0]):
            matrix_names += [matrix_try[m_row, 1]]

        #Extract the Table Data from the Matrix and put into another Matrix
        ncol_size = np.shape(matrix_add)[1] - 2
        matrix_kwc = []
        for n_row in range(np.shape(matrix_add)[0]):
            matrix_kwc2 = []
            for n_col in range(ncol_size):
                matrix_kwc2 += [matrix_add[n_row, n_col+2]]
            matrix_kwc += [matrix_kwc2]


        #Create 'Dummy' columns to balance the Matrix
        matrix_dum = []
        for row_dum in range(np.shape(matrix_kwc)[0]):
            cost_row = []
            for col_dum in range(np.shape(matrix_kwc)[0]-np.shape(matrix_kwc)[1]):
                cost_row += [30000]
            matrix_dum += [cost_row]
        nmatrix_kwc = np.hstack((matrix_kwc, matrix_dum))

        #Perform 2DAP on the table data matrix
        row_ind, col_ind = linear_sum_assignment(nmatrix_kwc)
        opt_assignc = col_ind
        opt_assignr = row_ind
        tc = nmatrix_kwc[row_ind, col_ind].sum()

        wrap_col = (wrap_value-1)*7

        #Align the assignments and print
        matrix_final1 = []
        matrix_finalOTSched = []
        matrix_finalcount = []
        for item_posts in matrix_posts:
            row_kwckwc = 0
            for xcol_kwc in range(np.shape(nmatrix_kwc)[1]):
                if col_ind[xcol_kwc] < np.shape(matrix_kwc)[1] and item_posts == matrix_posts[col_ind[xcol_kwc]]:
                    matrix_final1.append(matrix_names[row_kwckwc])
                    matrix_finalOTSched.append(0)
                    matrix_finalcount.append(1)
                row_kwckwc = row_kwckwc + 1
        matrix_final = []
        matrix_final = np.reshape(matrix_final1, (post_size, 1))
        matrix_finalP = np.concatenate((matrix_vertpost, matrix_final), 1)
        matrix_finalPP = np.reshape(matrix_finalP, (post_size, 2))

        #Create matrices for Names in Days and Nights to use for testing
        matrix_NamesDays_Temp = []
        matrix_NamesNights_Temp = []
        for Names_in_Days in range(np.shape(keithDayPosts)[0]):
            matrix_NamesDays_Temp.append(matrix_final1[Names_in_Days])
        for Names_in_Nights in range(np.shape(keithDayPosts)[0], np.shape(keithDayPosts)[0] + np.shape(keithNightPosts)[0]):
            matrix_NamesNights_Temp.append(matrix_final1[Names_in_Nights])
        if sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 1]:
            matrix_NamesDays = []
        if sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] != sdf_matrix[row_kwc+weeks_try+week_tryadj, 2]:
            matrix_NamesNights = []
        #Create EB and OT labels and add to names:
        for names_row in range(np.shape(matrix_finalP)[0]):
            for test_row in range(np.shape(matrix_add)[0]):
                for test_col in range(np.shape(matrix_add)[1]-2):
                    if matrix_add[test_row, 1] not in matrix_final1 and \
                            (matrix_add[test_row, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1]
                             or matrix_add[test_row, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2]) \
                            and str(kwcdate.date()) + ":" + matrix_add[test_row, 1] not in kwcDayVac_Name \
                            and str(kwcdate.date()) + ":" + matrix_add[test_row, 1] not in kwcNightVac_Name \
                            and (extra_boardDadj == 50 or extra_boardNadj == 50) and Current_slide == "" and \
                            Dupont_Flag == 1:
                        if extra_boardNadj == 50 and sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == matrix_add[test_row, 0] \
                                and matrix_add[test_row, 1] not in Bad_Slide:
                            Possible_Slide = matrix_add[test_row, 1]
                        elif extra_boardDadj == 50 and sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == matrix_add[test_row, 0] \
                                and matrix_add[test_row, 1] not in Bad_Slide:
                            Possible_Slide = matrix_add[test_row, 1]
                    elif matrix_add[test_row, 1] not in matrix_final1 and \
                            (matrix_add[test_row, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 1]
                             or matrix_add[test_row, 0] == sdf_matrix[row_kwc+weeks_try+week_tryadj, 2]) \
                            and str(kwcdate.date()) + ":" + matrix_add[test_row, 1] not in kwcDayVac_Name \
                            and str(kwcdate.date()) + ":" + matrix_add[test_row, 1] not in kwcNightVac_Name \
                            and (extra_boardDadj == 50 or extra_boardNadj == 50) and Current_slide == "" and \
                            Dupont_Flag == 1:
                        if extra_boardNadj == 50 and sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == matrix_add[test_row, 0] \
                                and matrix_add[test_row, 1] not in Bad_Slide:
                            Possible_Slide = matrix_add[test_row, 1]
                        elif extra_boardDadj == 50 and sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == matrix_add[test_row, 0] \
                                and matrix_add[test_row, 1] not in Bad_Slide:
                            Possible_Slide = matrix_add[test_row, 1]

                    elif any("-OT" in word for word in Comb_NamesDaysV) and \
                            InfoTable_matrix[0,0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            Comb_extraN[row_kwc-1] != " " and Restart_Flag == 0 \
                            and kwcdate.weekday() == 2 and Reset_WT_Flag == 0 \
                            and OTReset_Count <= OTReset_CountMax and kwcdate <= EBBlock_End:
                        if Comb_extraN[row_kwc-1] not in Bad_Slide and \
                                Comb_extraN[row_kwc-1].find('/', 0 ,100) == -1:
                            Bad_Slide += [Comb_extraN[row_kwc-1]]
                        Current_slide = ""
                        Possible_Slide = ""
                        Possible_SlideFlag = 0
                        OTReset_Count += 1
                        if OTReset_Count == OTReset_CountMax:
                            Bad_Slide = []
                        total_daysStart = row_kwc - 2
                        Restart_Flag = 1
                        Sched_Start(total_daysStart)
                        return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesDaysV) and \
                            InfoTable_matrix[0,0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            Comb_extraN[row_kwc-1] != " " and Restart_Flag == 0 \
                            and kwcdate.weekday() == 4 and Reset_FSS_Flag == 0 \
                            and OTReset_Count <= OTReset_CountMax and kwcdate <= EBBlock_End:
                        if Comb_extraN[row_kwc-1] not in Bad_Slide and \
                                Comb_extraN[row_kwc-1].find('/', 0, 100) == -1:
                            Bad_Slide += [Comb_extraN[row_kwc-1]]
                        Current_slide = ""
                        Possible_Slide = ""
                        Possible_SlideFlag = 0
                        OTReset_Count += 1
                        if OTReset_Count == OTReset_CountMax:
                            Bad_Slide = []
                        Reset_WT_Flag = 1
                        Reset_MT_Flag = 1
                        total_daysStart = row_kwc - 2
                        Restart_Flag = 1
                        Sched_Start(total_daysStart)
                        return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesNightsV) and \
                            InfoTable_matrix[0,0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            Comb_extraD[row_kwc-1] != " " and kwcdate.weekday() == 2 \
                            and Restart_Flag == 0 and Reset_WT_Flag == 0 and OTReset_Count <= OTReset_CountMax \
                            and kwcdate <= EBBlock_End:
                        if Comb_extraD[row_kwc-1] not in Bad_Slide:
                            Bad_Slide += [Comb_extraD[row_kwc-1]]
                        Current_slide = ""
                        Possible_Slide = ""
                        Reset_MT_Flag = 1
                        Possible_SlideFlag = 0
                        OTReset_Count += 1
                        if OTReset_Count == OTReset_CountMax:
                            Bad_Slide = []
                        total_daysStart = row_kwc - 2
                        Restart_Flag = 1
                        Sched_Start(total_daysStart)
                        return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesNightsV) and \
                            InfoTable_matrix[0, 0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            Comb_extraD[row_kwc-1] != " " and kwcdate.weekday() == 4 and Reset_FSS_Flag == 0 \
                            and Restart_Flag == 0 and OTReset_Count <= OTReset_CountMax and kwcdate <= EBBlock_End:
                        if Comb_extraD[row_kwc-1] not in Bad_Slide:
                            Bad_Slide += [Comb_extraD[row_kwc-1]]
                        Current_slide = ""
                        Possible_Slide = ""
                        Reset_WT_Flag = 1
                        Possible_SlideFlag = 0
                        OTReset_Count += 1
                        if OTReset_Count == OTReset_CountMax:
                            Bad_Slide = []
                        total_daysStart = row_kwc - 2
                        Restart_Flag = 1
                        Sched_Start(total_daysStart)
                        return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesNightsV) and \
                            InfoTable_matrix[0,0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            np.shape(Comb_extraD)[0] > 0 and kwcdate.weekday() == 0 and Restart_Flag == 0 \
                            and Reset_MT_Flag == 0 and OTReset_Count <= OTReset_CountMax and kwcdate <= EBBlock_End:
                        if Comb_extraD[row_kwc - 1] != " ":
                            if Comb_extraD[row_kwc-1] not in Bad_Slide:
                                Bad_Slide += [Comb_extraD[row_kwc-1]]
                            Current_slide = ""
                            Possible_Slide = ""
                            Reset_FSS_Flag = 1
                            Possible_SlideFlag = 0
                            OTReset_Count += 1
                            if OTReset_Count == OTReset_CountMax:
                                Bad_Slide = []
                            total_daysStart = row_kwc - 3
                            Restart_Flag = 1
                            Sched_Start(total_daysStart)
                            return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesNightsV) and \
                            InfoTable_matrix[0, 0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            np.shape(Comb_extraD)[0] > 0 and kwcdate.weekday() == 6 and Restart_Flag == 0 \
                            and Reset_MT_Flag == 0 and OTReset_Count <= OTReset_CountMax and kwcdate <= EBBlock_End:
                        if Comb_extraD[row_kwc - 1] != " ":
                            if Comb_extraD[row_kwc-1] not in Bad_Slide:
                                Bad_Slide += [Comb_extraD[row_kwc-1]]
                            Current_slide = ""
                            Possible_Slide = ""
                            Reset_FSS_Flag = 1
                            Possible_SlideFlag = 0
                            OTReset_Count += 1
                            if OTReset_Count == OTReset_CountMax:
                                Bad_Slide = []
                            total_daysStart = row_kwc - 2
                            Restart_Flag = 1
                            Sched_Start(total_daysStart)
                            return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesDaysV) and \
                            InfoTable_matrix[0,0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            np.shape(Comb_extraN)[0] > 0 and kwcdate.weekday() == 0 and Restart_Flag == 0 \
                            and Reset_MT_Flag == 0 and OTReset_Count <= OTReset_CountMax and kwcdate <= EBBlock_End:
                        if Comb_extraN[row_kwc - 1] != " ":
                            if Comb_extraN[row_kwc-1] not in Bad_Slide:
                                Bad_Slide += [Comb_extraD[row_kwc-1]]
                            Current_slide = ""
                            Possible_Slide = ""
                            Reset_FSS_Flag = 1
                            Possible_SlideFlag = 0
                            OTReset_Count += 1
                            if OTReset_Count == OTReset_CountMax:
                                Bad_Slide = []
                            total_daysStart = row_kwc - 3
                            Restart_Flag = 1
                            Sched_Start(total_daysStart)
                            return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesDaysV) and \
                            InfoTable_matrix[0,0] == "EOWEO Schedule" and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] and \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1] != \
                            sdf_matrix[row_kwc+weeks_try+week_tryadj-3, 1] and \
                            np.shape(Comb_extraN)[0] > 0 and kwcdate.weekday() == 6 and Restart_Flag == 0 \
                            and Reset_MT_Flag == 0 and OTReset_Count <= OTReset_CountMax and kwcdate <= EBBlock_End:
                        if Comb_extraN[row_kwc - 1] != " ":
                            if Comb_extraN[row_kwc-1] not in Bad_Slide:
                                Bad_Slide += [Comb_extraD[row_kwc-1]]
                            Current_slide = ""
                            Possible_Slide = ""
                            Reset_FSS_Flag = 1
                            Possible_SlideFlag = 0
                            OTReset_Count += 1
                            if OTReset_Count == OTReset_CountMax:
                                Bad_Slide = []
                            total_daysStart = row_kwc - 2
                            Restart_Flag = 1
                            Sched_Start(total_daysStart)
                            return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif Current_slide != "" and Current_slide not in matrix_NamesDays \
                            and Current_slide not in matrix_NamesNights and \
                            (any("-OT" in word for word in Comb_NamesDaysV) or
                             any("-OT" in word for word in Comb_NamesNightsV)) \
                            and EBBlock_Start <= kwcdate <= EBBlock_End and \
                            any(matrix_add[test_row, 0] in teamD for teamD in Team_NameD) \
                            and (sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] ==
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] or
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] ==
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2]) \
                            and InfoTable_matrix [0,0] != "EOWEO Schedule" and Restart_Flag == 0 \
                            and OTReset_Count <= OTReset_CountMax:
                        if Dupont_Flag == 1 and kwcdate.weekday() != 1 and \
                                (any("-OT" in word for word in Comb_NamesDaysV) or
                                 any("-OT" in word for word in Comb_NamesNightsV)):
                            if Current_slide not in Bad_Slide:
                                Bad_Slide += [Current_slide]
                            Current_slide = ""
                            Comb_NamesDaysV = []
                            Comb_NamesNightsV = []
                            Possible_SlideFlag = 0
                            OTReset_Count += 1
                            if OTReset_Count == OTReset_CountMax:
                                Bad_Slide = []
                            total_daysStart = (EBBlock_StartHold - pdfstart_date).days
                            Restart_Flag = 1
                            Sched_Start(total_daysStart)
                            return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                        elif Dupont_Flag != 1 and any("-OT" in word for word in Comb_NamesDaysV) and \
                                kwcdate > EBBlock_Start and OTReset_Count <= OTReset_CountMax:
                            if Comb_extraN[row_kwc - 1] != " ":
                                if Comb_extraN[row_kwc-1] not in Bad_Slide and \
                                        Comb_extraN[row_kwc-1].find('/', 0, 100) == -1:
                                    Bad_Slide += [Comb_extraN[row_kwc-1]]
                                Current_slide = ""
                                Possible_SlideFlag = 0
                                OTReset_Count += 1
                                if OTReset_Count == OTReset_CountMax:
                                    Bad_Slide = []
                                total_daysStart = (EBBlock_Start - pdfstart_date).days
                                Restart_Flag = 1
                                Sched_Start(total_daysStart)
                                return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                        elif Dupont_Flag != 1 and any("-OT" in word for word in Comb_NamesNightsV) and \
                                kwcdate > EBBlock_Start and OTReset_Count <= OTReset_CountMax:
                            if Comb_extraD[row_kwc - 1] != " ":
                                if Comb_extraD[row_kwc-1].find('/', 0, 100) != -1:
                                    if Comb_extraD[row_kwc-1][0:Comb_extraD[row_kwc-1].find('/', 0, 100)] \
                                            in eval(EBNightShift_Team + '_peopleNames') and \
                                            Comb_extraD[row_kwc-1][0:Comb_extraD[row_kwc-1].find('/', 0, 100)] \
                                            not in Bad_Slide:
                                        Bad_Slide += [Comb_extraD[row_kwc-1][0:Comb_extraD[row_kwc-1].find('/', 0, 100)]]
                                    elif Comb_extraD[row_kwc-1][Comb_extraD[row_kwc-1].find('/', 0, 100)+1:100] \
                                            in eval(EBNightShift_Team + '_peopleNames') and \
                                            Comb_extraD[row_kwc-1][Comb_extraD[row_kwc-1].find('/', 0, 100)+1:100] \
                                            not in Bad_Slide:
                                        Bad_Slide += [Comb_extraD[row_kwc-1][Comb_extraD[row_kwc-1].find('/', 0, 100)+1:100]]
                                elif Comb_extraD[row_kwc-1] not in Bad_Slide and \
                                        Comb_extraD[row_kwc-1].find('/', 0, 100) == -1:
                                    Bad_Slide += [Comb_extraD[row_kwc-1]]
                                Current_slide = ""
                                Possible_SlideFlag = 0
                                OTReset_Count += 1
                                if OTReset_Count == OTReset_CountMax:
                                    Bad_Slide = []
                                total_daysStart = (EBBlock_Start - pdfstart_date).days
                                Restart_Flag = 1
                                Sched_Start(total_daysStart)
                                return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    elif any("-OT" in word for word in Comb_NamesDaysV) \
                            and (sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] !=
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] and
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] ==
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 1]) \
                            and InfoTable_matrix [0,0] != "EOWEO Schedule" and Restart_Flag == 0 \
                            and np.shape(Comb_extraD)[0] > 0 and InfoTable_matrix[0, 1] == 1 \
                            and EBBlock_StartHold > pdfstart_date and OTReset_Count <= OTReset_CountMax:
                        if Dupont_Flag == 1 and any("-OT" in word for word in Comb_NamesDaysV) \
                                and kwcdate.weekday() != 1:
                            if Comb_extraN[row_kwc - 1] != " ":
                                if Comb_extraN[row_kwc-1] not in Bad_Slide and \
                                        Comb_extraN[row_kwc-1].find('/', 0, 100) == -1:
                                    Bad_Slide += [Comb_extraN[row_kwc-1]]
                                Current_slide = ""
                                Comb_NamesDaysV = []
                                Possible_SlideFlag = 0
                                OTReset_Count += 1
                                if OTReset_Count == OTReset_CountMax:
                                    Bad_Slide = []
                                total_daysStart = (EBBlock_StartHold - pdfstart_date).days
                                EBBlock_Start = EBBlock_StartHold
                                EBBlock_End = EBBlock_EndHold
                                extra_boardDadj = 8000
                                extra_boardNadj = 50
                                #EBBlock_StartHold = pdfstart_date
                                Restart_Flag = 1
                                Sched_Start(total_daysStart)
                                return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                        elif Dupont_Flag != 1 and any("-OT" in word for word in Comb_NamesDaysV):
                            if Comb_extraN[row_kwc - 1] != " " and kwcdate > EBBlock_Start \
                                    and OTReset_Count <= OTReset_CountMax:
                                if Comb_extraN[row_kwc-1] not in Bad_Slide and \
                                        Comb_extraN[row_kwc-1].find('/', 0, 100) == -1:
                                    Bad_Slide += [Comb_extraN[row_kwc-1]]
                                Current_slide = ""
                                Possible_SlideFlag = 0
                                OTReset_Count += 1
                                if OTReset_Count == OTReset_CountMax:
                                    Bad_Slide = []
                                Comb_NamesDaysV = []
                                total_daysStart = (EBBlock_StartHold - pdfstart_date).days
                                EBBlock_Start = EBBlock_StartHold
                                EBBlock_End = EBBlock_EndHold
                                extra_boardDadj = 8000
                                extra_boardNadj = 50
                                #EBBlock_StartHold = pdfstart_date
                                Restart_Flag = 1
                                Sched_Start(total_daysStart)
                                return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag

                    elif any("-OT" in word for word in Comb_NamesNightsV) \
                            and (sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] !=
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] and
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 2] ==
                                 sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 2]) \
                            and InfoTable_matrix [0,0] != "EOWEO Schedule" and Restart_Flag == 0 \
                            and np.shape(Comb_extraD)[0] > 0 and InfoTable_matrix[0, 1] == 1 \
                            and EBBlock_StartHold > pdfstart_date and OTReset_Count <= OTReset_CountMax:
                        if Dupont_Flag == 1 and any("-OT" in word for word in Comb_NamesNightsV) \
                                and kwcdate.weekday() != 1:
                            if Comb_extraD[row_kwc - 1] != " ":
                                if Comb_extraD[row_kwc-1] not in Bad_Slide:
                                    Bad_Slide += [Comb_extraD[row_kwc-1]]
                                Current_slide = ""
                                Comb_NamesNightsV = []
                                Possible_SlideFlag = 0
                                OTReset_Count += 1
                                if OTReset_Count == OTReset_CountMax:
                                    Bad_Slide = []
                                total_daysStart = (EBBlock_StartHold - pdfstart_date).days
                                EBBlock_Start = EBBlock_StartHold
                                EBBlock_End = EBBlock_EndHold
                                extra_boardDadj = 50
                                extra_boardNadj = 8000
                                Restart_Flag = 1
                                Sched_Start(total_daysStart)
                                return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                        elif any("-OT" in word for word in Comb_NamesNightsV) and Dupont_Flag != 1 \
                                and kwcdate > EBBlock_Start and OTReset_Count <= OTReset_CountMax:
                            if Comb_extraD[row_kwc - 1] != " ":
                                if Comb_extraD[row_kwc-1] not in Bad_Slide:
                                    Bad_Slide += [Comb_extraD[row_kwc-1]]
                                Current_slide = ""
                                Comb_NamesNightsV = []
                                Possible_SlideFlag = 0
                                OTReset_Count += 1
                                if OTReset_Count == OTReset_CountMax:
                                    Bad_Slide = []
                                total_daysStart = (EBBlock_StartHold - pdfstart_date).days
                                EBBlock_Start = EBBlock_StartHold
                                EBBlock_End = EBBlock_EndHold
                                extra_boardDadj = 50
                                extra_boardNadj = 8000
                                Restart_Flag = 1
                                Sched_Start(total_daysStart)
                                return Bad_Slide, Current_slide, Possible_SlideFlag, Restart_Flag
                    else:
                        Restart_Flag = 0
                        EBBlock_StartHold = EBBlock_Start
                        EBBlock_EndHold = EBBlock_End
                    if matrix_finalP[names_row, 1] == matrix_add[test_row, 1]:
                        if names_row == test_col:
                            if matrix_add[test_row, test_col+2] < 20:
                                matrix_final1[names_row] = matrix_final1[names_row]
                            elif 30 > matrix_add[test_row, test_col + 2] >= 20:
                                matrix_final1[names_row] = matrix_final1[names_row] + "-Swap"
                                Swapcount = 0
                            elif 30 < matrix_add[test_row, test_col + 2] < 100:
                                if Dupont_Flag == 1 and EBBlock_Start.weekday() == 0:
                                    Current_slide = matrix_final1[names_row]
                                    Current_slidepost = test_col+2
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-Slide"
                                    Dupont_MondaySlide_toNights = Current_slide
                                elif Dupont_Flag == 1 and EBBlock_Start.weekday() == 4:
                                    Current_slide = matrix_final1[names_row]
                                    Current_slidepost = test_col+2
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-Slide"
                                    Dupont_FridaySlide_toNights = Current_slide
                                elif matrix_final1[names_row] != SingleNight_Slide:
                                    Current_slide = matrix_final1[names_row]
                                    Current_slidepost = test_col+2
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-Slide"
                                elif matrix_final1[names_row] == SingleNight_Slide:
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-Slide"
                                if row_kwc > 0:
                                    #Slide to Days
                                    for text in matrix_final1[:post_size2]:
                                        if "-Slide" in text:
                                            if BadNameD1 == "":
                                                BadNameD1 = text[0:text.find('-', 0, 100)]
                                            elif BadNameD1 != "" and BadNameD2 == "" and BadNameD1 != text[0:text.find('-', 0, 100)]:
                                                BadNameD2 = text[0:text.find('-', 0, 100)]
                                            elif BadNameD1 != "" and BadNameD1 != text[0:text.find('-', 0, 100)] and \
                                                    BadNameD2 != "" and BadNameD3 == "" and BadNameD2 != text[0:text.find('-', 0, 100)]:
                                                BadNameD3 = text[0:text.find('-', 0, 100)]
                                            elif BadNameD1 != "" and BadNameD1 != text[0:text.find('-', 0, 100)] and \
                                                    BadNameD2 != "" and BadNameD3 != "" and BadNameD2 != text[0:text.find('-', 0, 100)] \
                                                    and BadNameD3 != text[0:text.find('-', 0, 100)]:
                                                BadNameD4 = text[0:text.find('-', 0, 100)]
                                    if BadNameD1 == Comb_extraN[row_kwc-1] and BadNameD1 + '-Slide' in matrix_final1[:post_size2] \
                                            and extra_boardNadj == 50 and Restart_FlagE == 0 and Dupont_Flag != 1:  #InfoTable_matrix[0,0] == "EOWEO Schedule"
                                        Current_slide = BadNameD1
                                        BadNameD1 = ""
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_FlagE = 1
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardNadj, \
                                               EBBlock_Start, EBBlock_End, Restart_FlagE

                                    if BadNameD1 in Comb_NamesNightsV and Count_QuitSlideD < QuitSlidetol \
                                            and extra_boardNadj == 50 and np.shape(matrix_NamesNights)[0] > 0 and \
                                            np.shape(matrix_NamesDays)[0] > 0 \
                                            and kwcdate.weekday() != 1 and Dupont_Flag == 1:
                                        if BadNameD1 != "" and BadNameD1 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0 \
                                                and kwcdate.weekday() != 0:
                                            Bad_Slide += [BadNameD1]
                                            BadNameD1 = ""
                                            BadNameD2 = ""
                                            BadNameD3 = ""
                                            BadNameD4 = ""
                                        if InfoTable_matrix[0, 1] == 2 or InfoTable_matrix[0, 1] == 1:
                                            EBSlide_tolboost = 1
                                        else:
                                            EBSlide_tolboost = 0
                                        Bad_SlideStopD = 0
                                        EBSlide_boost = 1
                                        Count_QuitSlideD += 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardNadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideD, Bad_SlideStopN
                                    else:
                                        Restart_Flag = 0
                                    if ((BadNameD1 + '-Slide' in Comb_NamesNightsV and BadNameD1 in matrix_final1[:post_size2]) \
                                        or (BadNameD2 + '-Slide' in Comb_NamesNightsV and BadNameD2 in matrix_final1[:post_size2]) \
                                        or (BadNameD3 + '-Slide' in Comb_NamesNightsV and BadNameD3 in matrix_final1[:post_size2]) \
                                            or (BadNameD4 + '-Slide' in Comb_NamesNightsV and BadNameD4 in matrix_final1[:post_size2])) \
                                            and Count_QuitSlideD < QuitSlidetol \
                                            and extra_boardNadj == 50 and np.shape(matrix_NamesDays)[0] > 0 and \
                                            np.shape(matrix_NamesNights)[0] > 0 and Dupont_Flag != 1:
                                        if BadNameD1 != "" and BadNameD1 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD1]
                                            BadNameD1 = ""
                                        if BadNameD2 != "" and BadNameD2 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD2]
                                            BadNameD2 = ""
                                        if BadNameD3 != "" and BadNameD3 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD3]
                                            BadNameD3 = ""
                                        if BadNameD4 != "" and BadNameD4 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD4]
                                            BadNameD4 = ""
                                        if InfoTable_matrix[0, 1] == 2 or InfoTable_matrix[0, 1] == 1:
                                            EBSlide_tolboost = 1
                                        else:
                                            EBSlide_tolboost = 0
                                        Bad_SlideStopD = 0
                                        EBSlide_boost = 1
                                        Count_QuitSlideD += 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardDadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideD, Bad_SlideStopN
                                    if ((BadNameD1 + '-Slide' in matrix_final1[:post_size2] and BadNameD1 in Comb_NamesNightsV) \
                                        or (BadNameD2 + '-Slide' in matrix_final1[:post_size2] and BadNameD2 in Comb_NamesNightsV) or \
                                        (BadNameD3 + '-Slide' in matrix_final1[:post_size2] and BadNameD3 in Comb_NamesNightsV) \
                                            or (BadNameD4 + '-Slide' in matrix_final1[:post_size2] and BadNameD4 in Comb_NamesNightsV)) \
                                            and Count_QuitSlideD < QuitSlidetol \
                                            and extra_boardNadj == 50 and np.shape(matrix_NamesDays)[0] > 0 and \
                                            np.shape(matrix_NamesNights)[0] > 0 and Dupont_Flag != 1:
                                        if BadNameD1 != "" and BadNameD1 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD1]
                                            BadNameD1 = ""
                                        if BadNameD2 != "" and BadNameD2 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD2]
                                            BadNameD2 = ""
                                        if BadNameD3 != "" and BadNameD3 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD3]
                                            BadNameD3 = ""
                                        if BadNameD4 != "" and BadNameD4 not in Bad_Slide and \
                                                Count_QuitSlideD > 0 and np.shape(kwcDayVac_Name)[0] > 0:
                                            Bad_Slide += [BadNameD4]
                                            BadNameD4 = ""
                                        if InfoTable_matrix[0, 1] == 2 or InfoTable_matrix[0, 1] == 1:
                                            EBSlide_tolboost = 1
                                        else:
                                            EBSlide_tolboost = 0
                                        Bad_SlideStopD = 0
                                        EBSlide_boost = 1
                                        Count_QuitSlideD += 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardDadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideD, Bad_SlideStopN
                                    else:
                                        Restart_Flag = 0

                                    if Count_QuitSlideD >= QuitSlidetol and \
                                            matrix_add[test_row, 0] == EBNightShift_Team and Bad_SlideHaltN != 1:
                                        EB_Extraboost = 0
                                        kwcdate2 = datetime.strftime(EBBlock_Start, "%b %d, %Y")
                                        kwcdate3 = datetime.strftime(EBBlock_End, "%b %d, %Y")
                                        Current_slide = ""
                                        Bad_Slide = []
                                        Bad_SlideHaltN = 1
                                        Restart_Flag = 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        messagebox.showinfo("Alert!", "Slide solution may not complete from:" + '\n' \
                                                            + kwcdate2 + " - " + kwcdate3  + '\n' \
                                                                                             "Vacancies will be filled with Overtime if needed!")
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardNadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideD, Bad_SlideStopN, \
                                               Bad_SlideHaltN, Bad_SlideHaltD
                                    #Slide to Nights
                                    for text in matrix_final1[post_size2: post_size]:
                                        if "-Slide" in text:
                                            if BadNameN1 == "":
                                                BadNameN1 = text[0:text.find('-', 0, 100)]
                                            elif BadNameN2 == "" and BadNameN1 != text[0:text.find('-', 0, 100)]:
                                                BadNameN2 = text[0:text.find('-', 0, 100)]
                                            elif BadNameN1 != "" and BadNameN1 != text[0:text.find('-', 0, 100)] and \
                                                    BadNameN2 != "" and BadNameN3 == "" and BadNameN2 != text[0:text.find('-', 0, 100)]:
                                                BadNameN3 = text[0:text.find('-', 0, 100)]
                                            elif BadNameN1 != "" and BadNameN1 != text[0:text.find('-', 0, 100)] and \
                                                    BadNameN2 != "" and BadNameN3 != "" and BadNameN4 == "" and \
                                                    BadNameN2 != text[0:text.find('-', 0, 100)] and BadNameN3 != text[0:text.find('-', 0, 100)]:
                                                BadNameN4 = text[0:text.find('-', 0, 100)]
                                    if BadNameN1 == Comb_extraD[row_kwc-1] and BadNameN1 + '-Slide' in matrix_final1[post_size2+1:post_size] \
                                            and extra_boardDadj == 50 and Restart_FlagE == 0 and Dupont_Flag != 1:
                                        Current_slide = BadNameN1
                                        BadNameN1 = ""
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_FlagE = 1
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardNadj, \
                                               EBBlock_Start, EBBlock_End, Restart_FlagE

                                    if ((BadNameN1 + '-Slide' in Comb_NamesNightsV and BadNameN1 in matrix_final1[:post_size2])
                                        or (BadNameN2 + '-Slide' in Comb_NamesNightsV and BadNameN2 in matrix_final1[:post_size2])
                                        or (BadNameN3 + '-Slide' in Comb_NamesNightsV and BadNameN3 in matrix_final1[:post_size2])
                                        or (BadNameN4 + '-Slide' in Comb_NamesNightsV and BadNameN4 in matrix_final1[:post_size2])) \
                                            and Count_QuitSlideN < QuitSlidetol \
                                            and extra_boardDadj == 50 and np.shape(matrix_NamesDays)[0] > 0 and \
                                            np.shape(matrix_NamesNights)[0] > 0 \
                                            and kwcdate.weekday() != 1 and Dupont_Flag == 1:
                                        if BadNameN1 != "" and BadNameN1 not in Bad_Slide and \
                                                Count_QuitSlideN > 0 and np.shape(kwcNightVac_Name)[0] > 0 \
                                                and kwcdate.weekday() != 0:
                                            Bad_Slide += [BadNameN1]
                                            BadNameN1 = ""
                                            BadNameN2 = ""
                                            BadNameN3 = ""
                                            BadNameN4 = ""
                                        if InfoTable_matrix[0, 1] == 2 or InfoTable_matrix[0, 1] == 1:
                                            EBSlide_tolboost = 1
                                        else:
                                            EBSlide_tolboost = 0
                                        Bad_SlideStopD = 0
                                        EBSlide_boost = 1
                                        Count_QuitSlideN += 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardNadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideN, Bad_SlideStopN
                                    if ((BadNameN1 + '-Slide' in Comb_NamesNightsV and BadNameN1 in matrix_final1[:post_size2])
                                        or (BadNameN2 + '-Slide' in Comb_NamesNightsV and BadNameN2 in matrix_final1[:post_size2]) or
                                        (BadNameN3 + '-Slide' in Comb_NamesNightsV and BadNameN3 in matrix_final1[:post_size2])
                                        or (BadNameN4 + '-Slide' in Comb_NamesNightsV and BadNameN4 in matrix_final1[:post_size2])) \
                                            and Count_QuitSlideN < QuitSlidetol \
                                            and extra_boardDadj == 50 and np.shape(matrix_NamesDays)[0] > 0 and \
                                            np.shape(matrix_NamesNights)[0] > 0 and Dupont_Flag != 1:
                                        if ((BadNameN1 != "" and BadNameN1 not in Bad_Slide) or
                                            (BadNameN2 != "" and BadNameN2 not in Bad_Slide) or
                                            (BadNameN3 != "" and BadNameN3 not in Bad_Slide) or
                                            (BadNameN4 != "" and BadNameN4 not in Bad_Slide)) and \
                                                np.shape(kwcNightVac_Name)[0] > 0:
                                            if BadNameN1 != "" and BadNameN1 not in Bad_Slide:
                                                Bad_Slide += [BadNameN1]
                                                BadNameN1 = ""
                                            if BadNameN2 != "" and BadNameN2 not in Bad_Slide:
                                                Bad_Slide += [BadNameN2]
                                                BadNameN2 = ""
                                            if BadNameN3 != "" and BadNameN3 not in Bad_Slide:
                                                Bad_Slide += [BadNameN3]
                                                BadNameN3 = ""
                                            if BadNameN4 != "" and BadNameN4 not in Bad_Slide:
                                                Bad_Slide += [BadNameN4]
                                                BadNameN4 = ""
                                        if InfoTable_matrix[0, 1] == 2 or InfoTable_matrix[0, 1] == 1:
                                            EBSlide_tolboost = 1
                                        else:
                                            EBSlide_tolboost = 0
                                        Bad_SlideStopD = 0
                                        EBSlide_boost = 1
                                        Count_QuitSlideN += 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardNadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideN, Bad_SlideStopN

                                    elif ((BadNameN1 + '-Slide' in matrix_final1[post_size2:post_size] and BadNameN1 in Comb_NamesDaysV)
                                          or (BadNameN2 + '-Slide' in matrix_final1[post_size2:post_size] and BadNameN2 in Comb_NamesDaysV)
                                          or (BadNameN3 + '-Slide' in matrix_final1[post_size2:post_size] and BadNameN3 in Comb_NamesDaysV)
                                          or (BadNameN4 + '-Slide' in matrix_final1[post_size2:post_size] and BadNameN4 in Comb_NamesDaysV)) \
                                            and Count_QuitSlideN < QuitSlidetol \
                                            and extra_boardDadj == 50 and np.shape(matrix_NamesDays)[0] > 0 and \
                                            np.shape(matrix_NamesNights)[0] > 0 and Dupont_Flag != 1:
                                        if ((BadNameN1 != "" and BadNameN1 not in Bad_Slide) or
                                            (BadNameN2 != "" and BadNameN2 not in Bad_Slide) or
                                            (BadNameN3 != "" and BadNameN3 not in Bad_Slide) or
                                            (BadNameN3 != "" and BadNameN3 not in Bad_Slide)) and \
                                                Count_QuitSlideN > 0 and np.shape(kwcNightVac_Name)[0] > 0:
                                            if BadNameN1 != "" and BadNameN1 not in Bad_Slide:
                                                Bad_Slide += [BadNameN1]
                                                BadNameN1 = ""
                                            if BadNameN2 != "" and BadNameN2 not in Bad_Slide:
                                                Bad_Slide += [BadNameN2]
                                                BadNameN2 = ""
                                            if BadNameN3 != "" and BadNameN3 not in Bad_Slide:
                                                Bad_Slide += [BadNameN3]
                                                BadNameN3 = ""
                                            if BadNameN4 != "" and BadNameN4 not in Bad_Slide:
                                                Bad_Slide += [BadNameN4]
                                                BadNameN4 = ""
                                        if InfoTable_matrix[0, 1] == 2 or InfoTable_matrix[0, 1] == 1:
                                            EBSlide_tolboost = 1
                                        else:
                                            EBSlide_tolboost = 0
                                        Bad_SlideStopD = 0
                                        EBSlide_boost = 1
                                        Count_QuitSlideN += 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        Restart_Flag = 1
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardNadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideN, Bad_SlideStopN
                                    else:
                                        Restart_Flag = 0

                                    if Count_QuitSlideN >= QuitSlidetol and \
                                            matrix_add[test_row, 0] == EBDayShift_Team and Bad_SlideHaltD != 1:
                                        EB_Extraboost = 0
                                        kwcdate2 = datetime.strftime(EBBlock_Start, "%b %d, %Y")
                                        kwcdate3 = datetime.strftime(EBBlock_End, "%b %d, %Y")
                                        Current_slide = ""
                                        Bad_Slide = []
                                        Bad_SlideHaltD = 1
                                        Restart_Flag = 1
                                        total_daysStart = (EBBlock_Start - pdfstart_date).days
                                        messagebox.showinfo("Alert!", "Slide solution may not complete from:" + '\n' \
                                                            + kwcdate2 + " - " + kwcdate3 + '\n' \
                                                                                            "Vacancies will be filled with Overtime if needed!")
                                        Sched_Start(total_daysStart)
                                        return Bad_Slide, Current_slide, Current_slidepost, extra_boardDadj, \
                                               EBBlock_Start, EBBlock_End, Count_QuitSlideN, Bad_SlideStopN, \
                                               Bad_SlideHaltN, Bad_SlideHaltD

                            elif 100 < matrix_add[test_row, test_col + 2] < 5000:
                                if matrix_final1[names_row] + "-OT Fatigue" in Comb_NamesDaysV or \
                                        matrix_final1[names_row] + "-OT Fatigue" in Comb_NamesNightsV:
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-OT Fatigue"
                                    matrix_finalOTSched[names_row] = matrix_finalOTSched[names_row] + 10
                                    matrix_finalcount.append(1)
                                else:
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-OT"
                                    matrix_finalOTSched[names_row] = matrix_finalOTSched[names_row] + 10
                                    matrix_finalcount.append(1)
                            elif 5000 <= matrix_add[test_row, test_col + 2] < OT_ablenotadj:
                                matrix_final1[names_row] = matrix_final1[names_row] + "-OT Force"
                                matrix_finalOTSched[names_row] = matrix_finalOTSched[names_row] + 10
                                matrix_finalcount.append(1)
                            elif matrix_add[test_row, test_col+2] >= OT_ablenotadj and names_row <= post_size2:
                                if sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == matrix_add[test_row, 0]:
                                    if FatigueDaysFlag1 == "":
                                        FatigueDaysFlag1 = matrix_final1[names_row]
                                    elif FatigueDaysFlag2 == "":
                                        FatigueDaysFlag2 = matrix_final1[names_row]
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 2] == matrix_add[test_row, 0]:
                                    if FatigueNightsFlag1 == "":
                                        FatigueNightsFlag1 = matrix_final1[names_row]
                                    elif FatigueNightsFlag2 == "":
                                        FatigueNightsFlag2 = matrix_final1[names_row]
                                if matrix_sal[test_row, 2] == "Salary":
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-Salary"
                                else:
                                    matrix_final1[names_row] = matrix_final1[names_row] + "-OT Fatigue"
                                    matrix_finalOTSched[names_row] = matrix_finalOTSched[names_row] + 12
                                    matrix_finalcount.append(1)
                            elif matrix_add[test_row, test_col+2] >= OT_ablenotadj and names_row > np.shape(keithDayPosts)[0]:
                                if sdf_matrix[row_kwc+weeks_try+week_tryadj-1, 1] == matrix_add[test_row, 0]:
                                    if FatigueDaysFlag1 == "":
                                        FatigueDaysFlag1 = matrix_final1[names_row]
                                    elif FatigueDaysFlag2 == "":
                                        FatigueDaysFlag2 = matrix_final1[names_row]
                                elif sdf_matrix[row_kwc+weeks_try+week_tryadj-2, 2] == matrix_add[test_row, 0]:
                                    if FatigueNightsFlag1 == "":
                                        FatigueNightsFlag1 = matrix_final1[names_row]
                                    elif FatigueNightsFlag2 == "":
                                        FatigueNightsFlag2 = matrix_final1[names_row]
                                matrix_final1[names_row] = matrix_final1[names_row] + "-OT Flag"
                                matrix_finalOTSched[names_row] = matrix_finalOTSched[names_row] + 12
                                matrix_finalcount.append(1)
        if Swapcount == 1:
            messagebox.showinfo("Alert!", "Swap was not successful for " + Swapname + " and " + Swapname2 + \
                                " on " + datetime.strftime(Swapdate, "%B %d, %Y") + "!" + '\n' + \
                                "Swap Post Qualifications are not Sufficient!" + '\n' + \
                                "                                   OR" + '\n' + \
                                "Swap will result in a Fatigue Day for " + Swapname2 + "!")
            Swapcount = 0

        #Create Output
        Label_widths = 15
        extra_width = 4
        #Title Block
        pdf_document = []
        countdamoney = 0

        #Post Names Block
        matrix_postDays = []
        matrix_postNights = []
        for row_label2 in range(post_size2):
            matrix_postDays.append(matrix_posts2[row_label2+3])
        for row_label10 in range(post_size2):
            matrix_postNights.append(matrix_posts2[row_label10+3])
        matrix_postDays = keithDayPosts
        matrix_postNights = keithNightPosts
        #Days / Nights Label
        if datetime.strftime(kwcdate, "%Y-%m-%d") not in Comb_Dates2:
            Comb_Dates += [kwcdate1]
            Comb_Dates2 += [datetime.strftime(kwcdate, "%Y-%m-%d")]
            Comb_TeamNameD += [Team_NameD]
            Comb_TeamNameN += [Team_NameN]
            Comb_DayVacNames += [DayVac_Names]
            Comb_DayTrainNames += [DayTrain_Names]
            Comb_NightVacNames += [NightVac_Names]
            Comb_NightTrainNames += [NightTrain_Names]

        #Names for the posts
        Posts_Length = int(len(matrix_final1)/2)

        matrix_NamesDays = []
        matrix_NamesNights = []
        matrix_DateAll1 = []
        matrix_DaysLabel = []
        matrix_NightsLabel = []
        matrix_OTDays = []
        matrix_CountDays = []
        matrix_OTNights = []
        matrix_CountNights = []
        Comb_NamesDaysV = []
        Comb_NamesNightsV = []
        for row_label3 in range(np.shape(keithDayPosts)[0]):
            if matrix_final1[row_label3] == FatigueDaysFlag1 or matrix_final1[row_label3] == FatigueDaysFlag2:
                Comb_NamesDaysV += [matrix_final1[row_label3] + "-Fatigue"]
            elif matrix_final1[row_label3] == FatigueNightsFlag1 or matrix_final1[row_label3] == FatigueNightsFlag2:
                Comb_NamesDaysV += [matrix_final1[row_label3] + "-Fatigue"]
            else:
                Comb_NamesDaysV += [matrix_final1[row_label3]]
            for comp_record in range(CompTable_matrixshape[0]):
                if matrix_final1[row_label3] == CompTable_matrix[comp_record, 1]:
                    Comb_CompNumberD += [CompTable_matrix[comp_record, row_label3+4]]
            if matrix_final1[row_label3].find("-", 0, -1) != -1:
                matrix_NamesDays.append((matrix_final1[row_label3][0:matrix_final1[row_label3].find("-", 0, -1)]))
                for comp_record in range(CompTable_matrixshape[0]):
                    if matrix_final1[row_label3][0:matrix_final1[row_label3].find("-", 0, -1)] == \
                            CompTable_matrix[comp_record, 1]:
                        Comb_CompNumberD += [CompTable_matrix[comp_record, row_label3+4]]
            else:
                matrix_NamesDays.append((matrix_final1[row_label3]))
            matrix_DateAll1.append(str(kwcdate))
            matrix_DaysLabel += ["Days"]
            matrix_OTDays.append((matrix_finalOTSched[row_label3]))
            matrix_CountDays.append((matrix_finalcount[row_label3]))
        for row_label7 in range(np.shape(keithDayPosts)[0], np.shape(keithDayPosts)[0] + np.shape(keithNightPosts)[0]):    #Posts_Length, Posts_Length*2):
            if matrix_final1[row_label7] == FatigueDaysFlag1 or matrix_final1[row_label7] == FatigueDaysFlag2:
                Comb_NamesNightsV += [matrix_final1[row_label7] + "-Fatigue"]
            elif matrix_final1[row_label7] == FatigueNightsFlag1 or matrix_final1[row_label7] == FatigueNightsFlag2:
                Comb_NamesNightsV += [matrix_final1[row_label7] + "-Fatigue"]
            else:
                Comb_NamesNightsV += [matrix_final1[row_label7]]
            for comp_record in range(CompTable_matrixshape[0]):
                if matrix_final1[row_label7] == CompTable_matrix[comp_record, 1]:
                    Comb_CompNumberN += [CompTable_matrix[comp_record, row_label7-np.shape(keithDayPosts)[0]+4]]
            if matrix_final1[row_label7].find("-", 0, -1) != -1:
                matrix_NamesNights.append((matrix_final1[row_label7][0:matrix_final1[row_label7].find("-", 0, -1)]))
                for comp_record in range(CompTable_matrixshape[0]):
                    if matrix_final1[row_label7][0:matrix_final1[row_label7].find("-", 0, -1)] == \
                            CompTable_matrix[comp_record, 1]:
                        Comb_CompNumberN += [CompTable_matrix[comp_record, row_label7-np.shape(keithDayPosts)[0]+4]]
            else:
                matrix_NamesNights.append((matrix_final1[row_label7]))
            matrix_NightsLabel += ["Nights"]
            matrix_OTNights.append((matrix_finalOTSched[row_label7]))
            matrix_CountNights.append((matrix_finalcount[row_label7]))
        if Current_slide == "" and Possible_Slide != "" \
                and kwcdate >= EBBlock_Start and Possible_SlideFlag == 0 and kwcdate <= EBBlock_End \
                and (extra_boardNadj == 50 or extra_boardDadj == 50) and Dupont_Flag != 1 \
                and Possible_Slide not in Current_slidematrixName:
            Current_slide = Possible_Slide
            Current_slidematrixName += [Possible_Slide]
            Current_slidematrixEBBlock_Start += [str(EBBlock_Start.date())]
            Current_slidematrixEBBlock_End += [str(EBBlock_End.date())]
            Possible_SlideFlag = 1
        if Current_slide == "" and Possible_Slide != "" and Possible_Slide not in Current_slidematrixName \
                and kwcdate >= EBBlock_Start and Possible_SlideFlag == 0 and kwcdate < EBBlock_End \
                and (extra_boardNadj == 50 or extra_boardDadj == 50) and Dupont_Flag == 1 and kwcdate.weekday() == 0:
            Current_slide = Possible_Slide
            Current_slidematrixName += [Possible_Slide]
            Current_slidematrixEBBlock_Start += [str(EBBlock_Start.date())]
            Current_slidematrixEBBlock_End += [str(EBBlock_End.date())]
            Possible_SlideFlag = 1
        if Current_slide == "" and Possible_Slide != "" and Possible_Slide not in Current_slidematrixName \
                and kwcdate >= EBBlock_Start and Possible_SlideFlag == 0 and kwcdate < EBBlock_End \
                and (extra_boardNadj == 50 or extra_boardDadj == 50) and Dupont_Flag == 1 and kwcdate.weekday() != 0:
            Current_slide = Possible_Slide
            Current_slidematrixName += [Possible_Slide]
            Current_slidematrixEBBlock_Start += [str(EBBlock_Start.date())]
            Current_slidematrixEBBlock_End += [str(EBBlock_End.date())]
            #Possible_SlideFlag = 1
        if kwcdate >= EBBlock_End:
            Possible_Slide = ""
            Possible_SlideFlag = 0
        if Dupont_Flag == 1 and kwcdate.weekday() == 0 and Dupont_MondaySlide_toNights != "" \
                and EBBlock_Start.weekday() == 0:
            Current_slide = Dupont_MondaySlide_toNights
            Current_slidematrixName += [Current_slide]
            Current_slidematrixEBBlock_Start += [str(EBBlock_Start.date())]
            Current_slidematrixEBBlock_End += [str(EBBlock_End.date())]
            Possible_SlideFlag = 1
        elif Dupont_Flag == 1 and kwcdate.weekday() != 0 and Dupont_MondaySlide_toNights != "" \
                and kwcdate > EBBlock_End and extra_boardNadj != 50 and extra_boardDadj != 50:
            Dupont_MondaySlide_toNights = ""
            Current_slide = ""
        #Create Extra Board and Fire Team block
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "A":
            non_matchD = []
            fire_teamD = ""
            EMT_teamD = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "B" and Current_slide in B_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "C" and Current_slide in C_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "D" and Current_slide in D_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            for namesD in A_peopleNames:
                if namesD not in matrix_NamesDays and DayVac_Names.find(namesD, 0, 100) == -1 and \
                        DayTrain_Names.find(namesD, 0, 100) == -1:
                    non_matchD.append(namesD)
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "A":
            non_matchN = []
            fire_teamN = ""
            EMT_teamN = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "B" and Current_slide in B_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "C" and Current_slide in C_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "D" and Current_slide in D_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            for namesN in A_peopleNames:
                if namesN not in matrix_NamesNights and NightVac_Names.find(namesN, 0, 100) == -1 and \
                        NightTrain_Names.find(namesN, 0, 100) == -1:
                    non_matchN.append(namesN)
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "B":
            non_matchD = []
            fire_teamD = ""
            EMT_teamD = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "A" and Current_slide in A_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "C" and Current_slide in C_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "D" and Current_slide in D_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            for namesD in B_peopleNames:
                if namesD not in matrix_NamesDays and DayVac_Names.find(namesD, 0, 100) == -1 and \
                        DayTrain_Names.find(namesD, 0, 100) == -1:
                    non_matchD.append(namesD)
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "B":
            non_matchN = []
            fire_teamN = ""
            EMT_teamN = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "A" and Current_slide in A_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "C" and Current_slide in C_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "D" and Current_slide in D_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            for namesN in B_peopleNames:
                if namesN not in matrix_NamesNights and NightVac_Names.find(namesN, 0, 100) == -1 and \
                        NightTrain_Names.find(namesN, 0, 100) == -1:
                    non_matchN.append(namesN)
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "C":
            non_matchD = []
            fire_teamD = ""
            EMT_teamD = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "A" and Current_slide in A_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "B" and Current_slide in B_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "D" and Current_slide in D_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            for namesD in C_peopleNames:
                if namesD not in matrix_NamesDays and DayVac_Names.find(namesD, 0, 100) == -1 and \
                        DayTrain_Names.find(namesD, 0, 100) == -1:
                    non_matchD.append(namesD)
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "C":
            non_matchN = []
            fire_teamN = ""
            EMT_teamN = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "A" and Current_slide in A_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "B" and Current_slide in B_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "D" and Current_slide in D_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            for namesN in C_peopleNames:
                if namesN not in matrix_NamesNights and NightVac_Names.find(namesN, 0, 100) == -1 and \
                        NightTrain_Names.find(namesN, 0, 100) == -1:
                    non_matchN.append(namesN)
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "D":
            non_matchD = []
            fire_teamD = ""
            EMT_teamD = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "A" and Current_slide in A_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "B" and Current_slide in B_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "C" and Current_slide in C_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in NightVac_Names and Current_slide not in NightTrain_Names:
                non_matchD.append(Current_slide)
                Slide_to_DaysFlag = 1
            for namesD in D_peopleNames:
                if namesD not in matrix_NamesDays and DayVac_Names.find(namesD, 0, 100) == -1 and \
                        DayTrain_Names.find(namesD, 0, 100) == -1:
                    non_matchD.append(namesD)
        if sdf_matrix[row_kwc+weeks_try+week_tryadj, 2] == "D":
            non_matchN = []
            fire_teamN = ""
            EMT_teamN = ""
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "A" and Current_slide in A_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "B" and Current_slide in B_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            if sdf_matrix[row_kwc+weeks_try+week_tryadj, 1] == "C" and Current_slide in C_peopleNames \
                    and Current_slide not in matrix_NamesDays and Current_slide not in matrix_NamesNights \
                    and Current_slide not in DayVac_Names and Current_slide not in DayTrain_Names:
                non_matchN.append(Current_slide)
                Slide_to_NightsFlag = 1
            for namesN in D_peopleNames:
                if namesN not in matrix_NamesNights and NightVac_Names.find(namesN, 0, 100) == -1 and \
                        NightTrain_Names.find(namesN, 0, 100) == -1:
                    non_matchN.append(namesN)
        if FatigueDaysFlag1 != "":
            if FatigueDaysFlag1 not in matrix_NamesDays and FatigueDaysFlag1 not in matrix_NamesNights:
                flagDays_erase1 += 1
                if flagDays_erase1 >= 2:
                    FatigueDaysFlag1 = ""
                    flagDays_erase1 = 0
            if FatigueDaysFlag1 in matrix_NamesDays or FatigueDaysFlag1 in matrix_NamesNights:
                flagDays_erase1 = 0
        if FatigueDaysFlag2 != "":
            if FatigueDaysFlag2 not in matrix_NamesDays and FatigueDaysFlag2 not in matrix_NamesNights:
                flagDays_erase2 += 1
                if flagDays_erase2 >= 1:
                    FatigueDaysFlag2 = ""
                    flagDays_erase2 = 0
            if FatigueDaysFlag2 in matrix_NamesDays or FatigueDaysFlag2 in matrix_NamesNights:
                flagDays_erase2 = 0
        if FatigueNightsFlag1 != "":
            if FatigueNightsFlag1 not in matrix_NamesDays and FatigueNightsFlag1 not in matrix_NamesNights:
                flagNights_erase1 += 1
                if flagNights_erase1 >= 2:
                    FatigueNightsFlag1 = ""
                    flagNights_erase1 = 0
            if FatigueNightsFlag1 in matrix_NamesDays or FatigueNightsFlag1 in matrix_NamesNights:
                flagNights_erase1 = 0
        if FatigueNightsFlag2 != "":
            if FatigueNightsFlag2 not in matrix_NamesDays and FatigueNightsFlag2 not in matrix_NamesNights:
                flagNights_erase2 += 1
                if flagNights_erase2 >= 2:
                    FatigueNightsFlag2 = ""
                    flagNights_erase2 = 0
            if FatigueNightsFlag2 in matrix_NamesDays or FatigueNightsFlag2 in matrix_NamesNights:
                flagNights_erase2 = 0
        if non_matchD == []:
            Comb_extraD += [" "]
            Comb_extraDTemp += [" "]
        elif np.shape(non_matchD)[0] == 2:
            Comb_extraD += [non_matchD[0] + '/' + non_matchD[1]]
            Comb_extraDTemp += non_matchD
        elif np.shape(non_matchD)[0] >= 3:
            Comb_extraD += [non_matchD[0] + '/' + non_matchD[1] + ':' + non_matchD[2]]
            Comb_extraDTemp += non_matchD
        else:
            Comb_extraD += non_matchD
            Comb_extraDTemp += non_matchD
        if non_matchN == []:
            Comb_extraN += [" "]
            Comb_extraNTemp += [" "]
        elif np.shape(non_matchN)[0] == 2:
            Comb_extraN += [non_matchN[0] + '/' + non_matchN[1]]
            Comb_extraNTemp += non_matchN
        elif np.shape(non_matchN)[0] >= 3:
            Comb_extraN += [non_matchN[0] + '/' + non_matchN[1] + ':' + non_matchN[2]]
            Comb_extraNTemp += non_matchN
        else:
            Comb_extraN += non_matchN
            Comb_extraNTemp += non_matchN
        if np.shape(Comb_extraD)[0] > row_kwc + 1 and InfoTable_matrix[0, 0] != "7on/7off Schedule":
            extra_boardNadj = 8000
            Bad_SlideHaltN = 1
            Restart_Flag = 1
            kwcDayVac_Name = []
            kwcDayVac_Date = []
            total_daysStart = (EBBlock_Start - pdfstart_date).days
            Sched_Start(total_daysStart)
            return Bad_SlideHaltN, Bad_SlideHaltD, extra_boardDadj, extra_boardNadj, kwcDayVac_Date, \
                   kwcDayVac_Name, kwcNightVac_Date, kwcNightVac_Name
        if np.shape(Comb_extraN)[0] > row_kwc + 1 and InfoTable_matrix[0, 0] != "7on/7off Schedule":
            extra_boardDadj = 8000
            Bad_SlideHaltD = 1
            Restart_Flag = 1
            kwcNightVac_Name = []
            kwcNightVac_Date = []
            total_daysStart = (EBBlock_Start - pdfstart_date).days
            Sched_Start(total_daysStart)
            return Bad_SlideHaltN, Bad_SlideHaltD, extra_boardDadj, extra_boardNadj, kwcDayVac_Date, \
                   kwcDayVac_Name, kwcNightVac_Date, kwcNightVac_Name
        #Fire and EMT Teams
        firecount = 0
        emtcount = 0
        for theNames_D in matrix_NamesDays:
            for fire_names in range(CompTable_matrixshape[0]):
                if theNames_D == CompTable_matrix[fire_names, 1] and CompTable_matrix[fire_names, 2] == "Y":
                    if fire_teamD == "":
                        fire_teamD = theNames_D
                    else:
                        fire_teamD = fire_teamD + "/" + theNames_D
                if theNames_D == CompTable_matrix[fire_names, 1] and CompTable_matrix[fire_names, 3] == "Y":
                    if EMT_teamD == "":
                        EMT_teamD = theNames_D
                    else:
                        EMT_teamD = EMT_teamD + "/" + theNames_D
                for extra_days in range(np.shape(Comb_extraDTemp)[0]):
                    if Comb_extraDTemp[extra_days] not in matrix_NamesNights and Comb_extraDTemp[extra_days] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 2] == "Y" and firecount == 0 \
                            and Comb_extraDTemp[extra_days] == Current_slide and Slide_to_DaysFlag == 1:
                        if fire_teamD == "":
                            fire_teamD = Comb_extraDTemp[extra_days]
                            firecount += 1
                        else:
                            fire_teamD = fire_teamD + "/" + Comb_extraDTemp[extra_days]
                            firecount += 1
                    elif Comb_extraDTemp[extra_days] not in matrix_NamesNights and Comb_extraDTemp[extra_days] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 2] == "Y" and firecount == 0 and Current_slide == "":
                        if fire_teamD == "":
                            fire_teamD = Comb_extraDTemp[extra_days]
                            firecount += 1
                        else:
                            fire_teamD = fire_teamD + "/" + Comb_extraDTemp[extra_days]
                            firecount += 1
                    if Comb_extraDTemp[extra_days] not in matrix_NamesNights and Comb_extraDTemp[extra_days] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 3] == "Y" and emtcount == 0 \
                            and Comb_extraDTemp[extra_days] == Current_slide and Slide_to_DaysFlag == 1:
                        if EMT_teamD == "":
                            EMT_teamD = Comb_extraDTemp[extra_days]
                            emtcount += 1
                        else:
                            EMT_teamD = EMT_teamD + "/" + Comb_extraDTemp[extra_days]
                            emtcount += 1
                    elif Comb_extraDTemp[extra_days] not in matrix_NamesNights and Comb_extraDTemp[extra_days] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 3] == "Y" and emtcount == 0 and Current_slide == "":
                        if EMT_teamD == "":
                            EMT_teamD = Comb_extraDTemp[extra_days]
                            emtcount += 1
                        else:
                            EMT_teamD = EMT_teamD + "/" + Comb_extraDTemp[extra_days]
                            emtcount += 1
        firecount = 0
        emtcount = 0

        for theNames_N in matrix_NamesNights:
            for fire_names in range(CompTable_matrixshape[0]):
                if theNames_N == CompTable_matrix[fire_names, 1] and CompTable_matrix[fire_names, 2] == "Y":
                    if fire_teamN == "":
                        fire_teamN = theNames_N
                    else:
                        fire_teamN = fire_teamN + "/" + theNames_N
                if theNames_N == CompTable_matrix[fire_names, 1] and CompTable_matrix[fire_names, 3] == "Y":
                    if EMT_teamN == "":
                        EMT_teamN = theNames_N
                    else:
                        EMT_teamN = EMT_teamN + "/" + theNames_N
                for extra_nights in range(np.shape(Comb_extraNTemp)[0]):
                    if Comb_extraNTemp[extra_nights] not in matrix_NamesDays and Comb_extraNTemp[extra_nights] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 2] == "Y" and firecount == 0 \
                            and Comb_extraDTemp[extra_days] == Current_slide and Slide_to_NightsFlag == 1:
                        if fire_teamN == "":
                            fire_teamN = Comb_extraNTemp[extra_nights]
                            firecount += 1
                        else:
                            fire_teamN = fire_teamN + "/" + Comb_extraNTemp[extra_nights]
                            firecount += 1
                    elif Comb_extraNTemp[extra_nights] not in matrix_NamesDays and Comb_extraNTemp[extra_nights] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 2] == "Y" and firecount == 0 and Current_slide == "":
                        if fire_teamN == "":
                            fire_teamN = Comb_extraNTemp[extra_nights]
                            firecount += 1
                        else:
                            fire_teamN = fire_teamN + "/" + Comb_extraNTemp[extra_nights]
                            firecount += 1
                    if Comb_extraNTemp[extra_nights] not in matrix_NamesDays and Comb_extraNTemp[extra_nights] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 3] == "Y" and emtcount == 0 \
                            and Comb_extraDTemp[extra_days] == Current_slide and Slide_to_NightsFlag == 1:
                        if EMT_teamN == "":
                            EMT_teamN = Comb_extraNTemp[extra_nights]
                            emtcount += 1
                        else:
                            EMT_teamN = EMT_teamN + "/" + Comb_extraNTemp[extra_nights]
                            emtcount += 1
                    elif Comb_extraNTemp[extra_nights] not in matrix_NamesDays and Comb_extraNTemp[extra_nights] == CompTable_matrix[fire_names, 1] and \
                            CompTable_matrix[fire_names, 3] == "Y" and emtcount == 0 and Current_slide == "":
                        if EMT_teamN == "":
                            EMT_teamN = Comb_extraNTemp[extra_nights]
                            emtcount += 1
                        else:
                            EMT_teamN = EMT_teamN + "/" + Comb_extraNTemp[extra_nights]
                            emtcount += 1
        if datetime.strftime(kwcdate, "%Y-%m-%d") not in Comb_Dates2:
            Comb_fireteamD += [fire_teamD]
            Comb_fireteamN += [fire_teamN]
            Comb_EMTteamD += [EMT_teamD]
            Comb_EMTteamN += [EMT_teamN]
        else:
            Comb_fireteamD[row_kwc:row_kwc+1] = [fire_teamD]
            Comb_fireteamN[row_kwc:row_kwc+1] = [fire_teamN]
            Comb_EMTteamD[row_kwc:row_kwc+1] = [EMT_teamD]
            Comb_EMTteamN[row_kwc:row_kwc+1] = [EMT_teamN]
        if datetime.strftime(kwcdate, "%Y-%m-%d") not in Comb_Dates2:
            if Comb_CompNumberD == []:
                pass
            else:
                Comb_CompNumbD += [Comb_CompNumberD]

            if Comb_CompNumberN == []:
                pass
            else:
                Comb_CompNumbN += [Comb_CompNumberN]
        else:
            if Comb_CompNumberD == []:
                pass
            else:
                Comb_CompNumbD[row_kwc:row_kwc+1] = [Comb_CompNumberD]

            if Comb_CompNumberN == []:
                pass
            else:
                Comb_CompNumbN[row_kwc:row_kwc+1] = [Comb_CompNumberN]
        #Setup and save output to a temp file
        matrix_DaysLabel = np.reshape(matrix_DaysLabel, (np.shape(keithDayPosts)[0], 1))
        matrix_NightsLabel = np.reshape(matrix_NightsLabel, (np.shape(keithNightPosts)[0], 1))
        matrix_DateAll2 = np.reshape(matrix_DateAll1, (np.shape(keithDayPosts)[0], 1))
        matrix_NamesDaysV = np.reshape(matrix_NamesDays, (np.shape(keithDayPosts)[0], 1))
        matrix_NamesNightsV = np.reshape(matrix_NamesNights, (np.shape(keithNightPosts)[0], 1))
        matrix_postDaysV = np.reshape(matrix_postDays, (np.shape(keithDayPosts)[0], 1))
        matrix_postNightsV = np.reshape(matrix_postNights, (np.shape(keithNightPosts)[0], 1))
        matrix_OTDays = np.reshape(matrix_OTDays, (np.shape(keithDayPosts)[0], 1))
        matrix_OTNights = np.reshape(matrix_OTNights, (np.shape(keithNightPosts)[0], 1))
        matrix_CountDays = np.reshape(matrix_CountDays, (np.shape(keithDayPosts)[0], 1))
        matrix_CountNights = np.reshape(matrix_CountNights, (np.shape(keithNightPosts)[0], 1))
        matrix_Dayscombine = np.hstack((matrix_NamesDaysV, matrix_OTDays))
        matrix_Nightscombine = np.hstack((matrix_NamesNightsV, matrix_OTNights))
        matrix_DaysShiftCombine = np.hstack((matrix_NamesDaysV, matrix_CountDays))
        matrix_NightsShiftCombine = np.hstack((matrix_NamesNightsV, matrix_CountNights))
        matrix_CombineAll = np.vstack((matrix_Dayscombine, matrix_Nightscombine))
        matrix_CombineShifts = np.vstack((matrix_DaysShiftCombine, matrix_NightsShiftCombine))

        if row_kwc > 0:
            if datetime.strftime(kwcdate, "%Y-%m-%d") not in Comb_Dates2:
                Comb_NamesDaysV2 = np.vstack((Comb_NamesDaysV2, Comb_NamesDaysV))
                Comb_NamesNightsV2 = np.vstack((Comb_NamesNightsV2, Comb_NamesNightsV))
            else:
                Comb_NamesDaysV2[row_kwc:row_kwc+1] = [Comb_NamesDaysV]
                Comb_NamesNightsV2[row_kwc:row_kwc+1] = [Comb_NamesNightsV]
        else:
            Comb_NamesDaysV2 = [Comb_NamesDaysV]
            Comb_NamesNightsV2 = [Comb_NamesNightsV]

        for date_items in matrix_CombineAll:
            with open("TempSchedule.txt", 'a', newline="") as appFile:
                writer = csv.writer(appFile)
                writeList = date_items
                writer.writerow(writeList)
                appFile.close()
        for date_items in matrix_CombineShifts:
            with open("ShiftCount.txt", 'a', newline="") as appFile:
                writer = csv.writer(appFile)
                writeList = date_items
                writer.writerow(writeList)
                appFile.close()
        countname += 1

    Total_Header = ['Date', 'Days Team']
    Total_Header = np.hstack((Total_Header, matrix_postDays))
    Total_Header1 = []
    for items in matrix_postDays:
        Total_Header1 += ([items + ' Comp'])
    Total_Header2 = ['ExtraBoard Days', 'Vac/Off Days', 'Train/Spec Days', 'Fire Days', 'EMT Days', 'Nights Team']
    Total_Header = np.hstack((Total_Header, Total_Header1, Total_Header2))
    Total_Header3 = []
    for items in matrix_postNights:
        Total_Header3 += ([items + ' Comp'])
    Total_Header4 = ['ExtraBoard Nights', 'Vac/Off Nights', 'Train/Spec Nights', 'Fire Nights', 'EMT Nights']
    Total_Header = np.hstack((Total_Header, matrix_postNights, Total_Header3, Total_Header4))

    Total_Schedule = np.hstack((np.reshape(Comb_Dates2, (np.shape(Comb_Dates2)[0], 1)),
                                np.reshape(Comb_TeamNameD, (np.shape(Comb_TeamNameD)[0], 1)), Comb_NamesDaysV2,
                                Comb_CompNumbD, np.reshape(Comb_extraD, (np.shape(Comb_extraD)[0], 1)),
                                np.reshape(Comb_DayVacNames, (np.shape(Comb_DayVacNames)[0], 1)),
                                np.reshape(Comb_DayTrainNames, (np.shape(Comb_DayTrainNames)[0], 1)),
                                np.reshape(Comb_fireteamD, (np.shape(Comb_fireteamD)[0], 1)),
                                np.reshape(Comb_EMTteamD, (np.shape(Comb_EMTteamD)[0], 1)),
                                np.reshape(Comb_TeamNameN, (np.shape(Comb_TeamNameN)[0], 1)),  Comb_NamesNightsV2,
                                Comb_CompNumbN, np.reshape(Comb_extraN, (np.shape(Comb_extraN)[0], 1)),
                                np.reshape(Comb_NightVacNames, (np.shape(Comb_NightVacNames)[0], 1)),
                                np.reshape(Comb_NightTrainNames, (np.shape(Comb_NightTrainNames)[0], 1)),
                                np.reshape(Comb_fireteamN, (np.shape(Comb_fireteamN)[0], 1)),
                                np.reshape(Comb_EMTteamN, (np.shape(Comb_EMTteamN)[0], 1))))

    erase_input = open('Total Schedule.txt', 'w+')
    erase_input.close
    with open("Total Schedule.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(Total_Header)
        appFile1.close()
    save_list = ""
    for items in Total_Schedule:
        with open("Total Schedule.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            #save_list = child_row
            writer.writerow(items)
            appFile1.close()

    #Create pdf File==========================================================================================
    c = canvas.Canvas('test.pdf', pagesize=landscape(letter))
    c.setFont("Helvetica", 8, leading=None)
    styles = getSampleStyleSheet()
    c.setFillColor(colors.black)
    Names_Spread = 104
    Names_top = 525
    Current_slide = ""
    pdf_pages = int(total_days/7)

    for page_items in range(pdf_pages):
        #Page Title
        start_date = pdfstart_date + timedelta(7*page_items)
        end_date = start_date + timedelta(6)
        pdfstart_datefinal = datetime.strftime(start_date, "%B %d, %Y")
        pdfend_datefinal = datetime.strftime((end_date), "%B %d, %Y")
        Team_Header = "Team Name: " + TeamName_matrix[0][0]
        Header_title = Team_Header + " Week " + str(page_items+1) + ": " + str(pdfstart_datefinal) + " - " + str(pdfend_datefinal)
        c.setFillColor(colors.firebrick)
        c.setFont("Helvetica", 16, leading=None)
        c.drawCentredString(400, Names_top+55, Header_title)
        c.setFont("Helvetica", 8, leading=None)

        #Days
        Days_List = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        c.setFont("Helvetica-Bold", 8, leading=None)
        countpost = 0
        for daysitems in range(7):
            c.setFillColor(colors.green)
            c.rect(x=60+countpost, y=Names_top+34, width=Names_Spread, height=12, stroke=1, fill=1)
            c.setFillColor(colors.white)
            c.drawCentredString(95+countpost+20, Names_top+37, Days_List[daysitems])
            countpost += Names_Spread
        c.setFillColor(colors.black)

        #Dates
        countpost = 0
        for datesitems in range(0+page_items*7, 7+page_items*7):
            c.setFillColor(colors.blue)
            c.rect(x=60+countpost, y=Names_top+22, width=Names_Spread, height=12, stroke=1, fill=1)
            c.setFillColor(colors.white)
            c.drawCentredString(100+countpost+20, Names_top+25, Comb_Dates[datesitems])
            countpost += Names_Spread

        #Teams Days
        countpost = 0
        for teamitems in range(0+page_items*7, 7+page_items*7):
            if Comb_TeamNameD[teamitems] == "Team A":
                c.setFillColorRGB(157/255, 208/255, 243/255)
            elif Comb_TeamNameD[teamitems] == "Team B":
                c.setFillColorRGB(247/255, 205/255, 171/255)
            elif Comb_TeamNameD[teamitems] == "Team C":
                c.setFillColorRGB(220/255, 187/255, 246/255)
            elif Comb_TeamNameD[teamitems] == "Team D":
                c.setFillColorRGB(229/255, 247/255, 171/255)
            c.rect(x=60+countpost, y=Names_top+10, width=Names_Spread-23, height=12, stroke=1, fill=1)
            c.setFillColor(colors.black)
            c.rect(x=84+countpost+57, y=Names_top+10, width=Names_Spread-81, height=12, stroke=1, fill=1)
            c.setFont("Helvetica-Bold", 8, leading=None)
            c.setFillColor(colors.black)
            c.drawCentredString(95+countpost+8, Names_top+13, Comb_TeamNameD[teamitems])
            c.setFont("Helvetica-Bold", 7, leading=None)
            c.setFillColor(colors.white)
            c.drawCentredString(96+countpost+57, Names_top+13, "Comp")
            c.setFont("Helvetica-Bold", 8, leading=None)
            countpost += Names_Spread

        #Posts
        countpost = 0
        c.setFillColorRGB(128/255, 109/255, 225/255)
        c.rect(x=4, y=Names_top-countpost+12-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.setFillColor(colors.white)
        c.drawCentredString(31, Names_top-countpost+12+1, "Days")
        c.setFont("Helvetica", 8, leading=None)

        for rowsitems_postD in range(np.shape(matrix_postDaysV)[0]):
            c.rect(x=4, y=Names_top-countpost-2, width=Names_Spread-48, height=12, stroke=1, fill=0)
            c.setFillColor(colors.black)
            c.drawCentredString(31, Names_top-countpost+1, matrix_postDaysV[rowsitems_postD, 0])
            countpost += 12

        c.setFillColor(colors.red)
        c.rect(x=4, y=Names_top-countpost-12-2, width=Names_Spread-48, height=24, stroke=1, fill=1)
        c.setFillColor(colors.fidblue)
        c.rect(x=4, y=Names_top-countpost-36-2, width=Names_Spread-48, height=24, stroke=1, fill=1)
        c.setFillColor(colors.white)
        c.setFont("Helvetica-Bold", 8, leading=None)
        c.drawCentredString(31, Names_top-countpost-6+1, "FireTeam")
        c.drawCentredString(31, Names_top-countpost-30+1, "EMTTeam")
        c.setFont("Helvetica", 8, leading=None)
        c.setFillColorRGB(193/255, 243/255, 250/255)
        c.rect(x=4, y=Names_top-countpost-48-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.rect(x=4, y=Names_top-countpost-60-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.rect(x=4, y=Names_top-countpost-72-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.rect(x=4, y=Names_top-countpost-84-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.setFillColorRGB(128/255, 109/255, 225/255)
        c.rect(x=4, y=Names_top-countpost-96-2, width=Names_Spread-48, height=12, stroke=1, fill=1)

        c.setFillColor(colors.black)
        c.drawCentredString(31, Names_top-countpost-48+1, "Extra Board")
        c.drawCentredString(31, Names_top-countpost-60+1, "Vacation/Off")
        c.drawCentredString(31, Names_top-countpost-72+1, "Train/Assign")
        c.drawCentredString(31, Names_top-countpost-84+1, "Slide to Nights")
        c.setFillColor(colors.white)
        c.setFont("Helvetica-Bold", 8, leading=None)
        c.drawCentredString(31, Names_top-countpost-96+1, "Nights")

        countteam = 0
        for teamitems in range(0+page_items*7, 7+page_items*7):
            if Comb_TeamNameN[teamitems] == "Team A":
                c.setFillColorRGB(157/255, 208/255, 243/255)
            elif Comb_TeamNameN[teamitems] == "Team B":
                c.setFillColorRGB(247/255, 205/255, 171/255)
            elif Comb_TeamNameN[teamitems] == "Team C":
                c.setFillColorRGB(220/255, 187/255, 246/255)
            elif Comb_TeamNameN[teamitems] == "Team D":
                c.setFillColorRGB(229/255, 247/255, 171/255)
            c.rect(x=60+countteam, y=Names_top-countpost-96-2, width=Names_Spread-23, height=12, stroke=1, fill=1)
            c.setFillColor(colors.black)
            c.rect(x=84+countteam+57, y=Names_top-countpost-96-2, width=Names_Spread-81, height=12, stroke=1, fill=1)
            c.setFont("Helvetica-Bold", 8, leading=None)
            c.setFillColor(colors.black)
            c.drawCentredString(95+countteam+8, Names_top-countpost-96+1, Comb_TeamNameN[teamitems])
            c.setFillColor(colors.white)
            c.setFont("Helvetica-Bold", 7, leading=None)
            c.drawCentredString(96+countteam+57, Names_top-countpost-96+1, "Comp")
            c.setFont("Helvetica-Bold", 8, leading=None)
            countteam += Names_Spread
        c.setFont("Helvetica", 8, leading=None)

        countpost = Names_top-countpost-108
        night_names = countpost
        for rowsitems_postN in range(np.shape(matrix_postNightsV)[0]):
            c.setFillColorRGB(210/255, 205/255, 206/255)
            c.rect(x=4, y=countpost-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
            c.setFillColor(colors.black)
            c.drawCentredString(31, countpost+1, matrix_postNightsV[rowsitems_postN, 0])
            countpost -= 12

        #Names
        moverows = 0
        movecols = 0

        for rowsitems_NamesD in range(0+page_items*7, 7+page_items*7):
            countcols = 0
            for colsitems_NamesD in range(np.shape(matrix_postDaysV)[0]):
                if Comb_NamesDaysV2[rowsitems_NamesD][colsitems_NamesD] in \
                        Comb_fireteamD[rowsitems_NamesD].split("/") \
                        or Comb_NamesDaysV2[rowsitems_NamesD][colsitems_NamesD] \
                        [0:Comb_NamesDaysV2[rowsitems_NamesD][colsitems_NamesD].find('-', 0, 100)] \
                        in Comb_fireteamD[rowsitems_NamesD].split("/"):
                    c.setFillColor(colors.white)
                    c.rect(x=60+movecols, y=Names_top-moverows-2, width=Names_Spread-23,
                           height=12, stroke=1, fill=1)
                    c.setFillColor(colors.black)
                else:
                    c.setFillColor(colors.white)
                    c.rect(x=60+movecols, y=Names_top-moverows-2, width=Names_Spread-23,
                           height=12, stroke=1, fill=1)
                    c.setFillColor(colors.black)
                #c.drawCentredString(95+movecols+8, Names_top-moverows+1, Comb_NamesDaysV2[rowsitems_NamesD][colsitems_NamesD])
                c.acroForm.textfield(x=60+movecols, y=Names_top-moverows-2, width=Names_Spread-23, height=12, fontSize=8,
                                     value=Comb_NamesDaysV2[rowsitems_NamesD][colsitems_NamesD], borderWidth=.5, fillColor=white)
                if Comb_CompNumbD[rowsitems_NamesD][colsitems_NamesD] == 1:
                    c.setFillColor(colors.yellow)
                else:
                    c.setFillColor(colors.white)
                c.rect(x=84+movecols+57, y=Names_top-moverows-2, width=Names_Spread-81, height=12,
                       stroke=1, fill=1)
                c.setFillColor(colors.black)
                c.drawCentredString(96+movecols+57, Names_top-moverows+1,
                                    str(Comb_CompNumbD[rowsitems_NamesD][colsitems_NamesD]))
                moverows += 12
                countcols += 1
                if countcols >= np.shape(Comb_NamesDaysV2)[1]:
                    movecols += Names_Spread
                    moverows = 0
        c.setFillColor(colors.black)
        #Days bottom block
        moveblock1 = (np.shape(matrix_postDays)[0])*12
        countblock1 = 0
        for teamitems in range(0+page_items*7, 7+page_items*7):
            c.setFillColor(colors.red)
            c.rect(x=60+countblock1, y=Names_top-moveblock1-12-2, width=Names_Spread,
                   height=24, stroke=1, fill=1)
            c.setFillColor(colors.fidblue)
            c.rect(x=60+countblock1, y=Names_top-moveblock1-36-2, width=Names_Spread,
                   height=24, stroke=1, fill=1)
            c.setFillColor(colors.white)
            c.setFont("Helvetica-Bold", 8, leading=None)
            firerecord_stuff1 = str(Comb_fireteamD[teamitems]).replace("'", '')
            firerecord_stuff = str(firerecord_stuff1).strip("[]")
            if len(firerecord_stuff) > 22:
                fireline1 = firerecord_stuff[0:firerecord_stuff.find('/', 11, 22)]
                fireline2 = firerecord_stuff[firerecord_stuff.find('/', 11, 22)+1:100]
            else:
                fireline1 = firerecord_stuff
            if len(firerecord_stuff) <= 22:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-6+1, fireline1)
            else:
                c.drawCentredString(110+countblock1, Names_top-moveblock1+1, fireline1)
                c.drawCentredString(110+countblock1, Names_top-moveblock1-12+1, fireline2)
            emtrecord_stuff1 = str(Comb_EMTteamD[teamitems]).replace("'", '')
            emtrecord_stuff = str(emtrecord_stuff1).strip("[]")
            if len(emtrecord_stuff) > 24:
                emtline1 = emtrecord_stuff[0:emtrecord_stuff.find('/', 11, 24)]
                emtline2 = emtrecord_stuff[emtrecord_stuff.find('/', 11, 24)+1:100]
            else:
                emtline1 = emtrecord_stuff
            if len(emtrecord_stuff) <= 24:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-30+1, emtline1)
            else:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-24+1, emtline1)
                c.drawCentredString(110+countblock1, Names_top-moveblock1-36+1, emtline2)
            c.setFont("Helvetica", 8, leading=None)
            c.setFillColorRGB(193/255, 243/255, 250/255)
            c.rect(x=60+countblock1, y=Names_top-moveblock1-48-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.rect(x=60+countblock1, y=Names_top-moveblock1-60-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.rect(x=60+countblock1, y=Names_top-moveblock1-72-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.rect(x=60+countblock1, y=Names_top-moveblock1-84-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.setFillColor(colors.black)
            if Comb_extraD[teamitems][0:Comb_extraD[teamitems].find('/', 0, 100)] + '-Slide' \
                    in Comb_NamesNightsV2[teamitems] \
                    and Comb_extraD[teamitems][Comb_extraD[teamitems].find('/', 0, 100)+1:100] + '-Slide' \
                    in Comb_NamesNightsV2[teamitems]:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1, Comb_extraD[teamitems])
            elif Comb_extraD[teamitems].find(':', 0, 100) != -1:
                if Comb_extraD[teamitems][0:Comb_extraD[teamitems].find('/', 0, 100)] + '-Slide' \
                        in Comb_NamesNightsV2[teamitems] \
                        and Comb_extraD[teamitems][Comb_extraD[teamitems].find('/', 0, 100)+1: \
                        Comb_extraD[teamitems].find(':', 0, 100)] + '-Slide' \
                        in Comb_NamesNightsV2[teamitems] and \
                        Comb_extraD[teamitems][Comb_extraD[teamitems].find(':', 0, 100)+1:100] + '-Slide' in \
                        Comb_NamesNightsV2[teamitems]:
                    c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1,
                                        Comb_extraD[teamitems][0:Comb_extraD[teamitems].find('/', 0, 100)] + '/' + \
                                        Comb_extraD[teamitems][Comb_extraD[teamitems].find('/', 0, 100)+1: \
                                                               Comb_extraD[teamitems].find(':', 0, 100)] + '/' + \
                                        Comb_extraD[teamitems][Comb_extraD[teamitems].find(':', 0, 100)+1:100])
            elif Comb_extraD[teamitems] + '-Slide' in Comb_NamesNightsV2[teamitems]:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1, Comb_extraD[teamitems])
            elif Comb_extraD[teamitems][0:Comb_extraD[teamitems].find('/', 0, 100)] + '-Slide' \
                    in Comb_NamesNightsV2[teamitems]:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1,
                                    Comb_extraD[teamitems][0:Comb_extraD[teamitems].find('/', 0, 100)])
            elif Comb_extraD[teamitems][Comb_extraD[teamitems].find('/', 0, 100)+1:100] + '-Slide' \
                    in Comb_NamesNightsV2[teamitems]:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1,
                                    Comb_extraD[teamitems][Comb_extraD[teamitems].find('/', 0, 100)+1:100])
            elif (Comb_extraD[teamitems] in Comb_extraN[teamitems] or Comb_extraD[teamitems] + '/'
                  in Comb_extraN[teamitems] or '/' + Comb_extraD[teamitems]
                  in Comb_extraN[teamitems]) and Comb_extraD[teamitems] + '-Slide' in Comb_NamesNightsV2[teamitems+1]:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1, Comb_extraD[teamitems])
            elif Comb_extraD[teamitems] + "-Slide" in Comb_NamesNightsV2[teamitems-1] and \
                    Comb_extraD[teamitems] in Comb_NamesDaysV2[teamitems+1]:
                if Comb_DayVacNames[teamitems].find('/', 0, 100) != -1:
                    Comb_DayVacNames[teamitems] = Comb_DayVacNames[teamitems] + '/' + Comb_extraD[teamitems]
                else:
                    Comb_DayVacNames[teamitems] = Comb_extraD[teamitems]
                MoveDate1 = start_date + timedelta(teamitems - page_items*7)
                MoveDate2 = datetime.strftime(MoveDate1, "%b %d, %Y")
                MoveDate3 = MoveDate1 + timedelta(1)
                MoveDate4 = datetime.strftime(MoveDate3, "%b %d, %Y")
                messagebox.showinfo("Alert!", Comb_extraD[teamitems] + " will need to be 'OFF WITH PAY' on:" + '\n' \
                                    + "            " + MoveDate2 + '\n' + "To move back to DAYS on:" + '\n' + \
                                    "            " + MoveDate4)
            elif Comb_extraD[teamitems] + "-Slide" in Comb_NamesNightsV2[teamitems-1]:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1, Comb_extraD[teamitems])
            elif Comb_extraD[teamitems] + "-Slide" in Comb_NamesNightsV2[teamitems-2] and \
                    Comb_extraD[teamitems] == Comb_extraD[teamitems-1]:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1, Comb_extraD[teamitems])
            elif Comb_extraD[teamitems] in Comb_extraN[teamitems] and Comb_extraD[teamitems] in \
                    eval(str(Comb_TeamNameD[teamitems][-1:]) + "_peopleNames"):
                c.drawCentredString(110+countblock1, Names_top-moveblock1-84+1, Comb_extraD[teamitems])
                # new
            else:
                c.drawCentredString(110+countblock1, Names_top-moveblock1-48+1, Comb_extraD[teamitems])
            c.drawCentredString(110+countblock1, Names_top-moveblock1-60+1, Comb_DayVacNames[teamitems])
            c.drawCentredString(110+countblock1, Names_top-moveblock1-72+1, Comb_DayTrainNames[teamitems])
            countblock1 += Names_Spread

        moverows = 0
        movecols = 0
        for rowsitems_NamesN in range(0+page_items*7, 7+page_items*7):
            countcols = 0
            for colsitems_NamesN in range(np.shape(matrix_postNightsV)[0]):
                if Comb_NamesNightsV2[rowsitems_NamesN][colsitems_NamesN] in \
                        Comb_fireteamN[rowsitems_NamesN].split("/") \
                        or Comb_NamesNightsV2[rowsitems_NamesN][colsitems_NamesN] \
                        [0:Comb_NamesNightsV2[rowsitems_NamesN][colsitems_NamesN].find('-', 0, 100)] \
                        in Comb_fireteamN[rowsitems_NamesN].split("/"):
                    c.setFillColorRGB(210/255, 205/255, 206/255)
                    c.rect(x=60+movecols, y=night_names-moverows-2, width=Names_Spread-23,
                           height=12, stroke=1, fill=1)
                else:
                    #c.setFillColor(colors.white)
                    c.setFillColorRGB(210/255, 205/255, 206/255)
                    c.rect(x=60+movecols, y=night_names-moverows-2, width=Names_Spread-23,
                           height=12, stroke=1, fill=1)
                c.setFillColor(colors.black)
                #c.drawCentredString(95+movecols+8, night_names-moverows+1, Comb_NamesNightsV2[rowsitems_NamesN][colsitems_NamesN])
                c.acroForm.textfield(x=60+movecols, y=night_names-moverows-2, width=Names_Spread-23, height=12, fontSize=8,
                                     value=Comb_NamesNightsV2[rowsitems_NamesN][colsitems_NamesN], borderWidth=.5, fillColor=lightgrey)
                if Comb_CompNumbN[rowsitems_NamesN][colsitems_NamesN] == 1:
                    c.setFillColor(colors.yellow)
                else:
                    c.setFillColorRGB(210/255, 205/255, 206/255)
                c.rect(x=84+movecols+57, y=night_names-moverows-2, width=Names_Spread-81, height=12,
                       stroke=1, fill=1)
                c.setFillColor(colors.black)
                c.drawCentredString(96+movecols+57, night_names-moverows+1,
                                    str(Comb_CompNumbN[rowsitems_NamesN][colsitems_NamesN]))
                moverows += 12
                countcols += 1
                if countcols >= np.shape(Comb_NamesNightsV2)[1]:
                    movecols += Names_Spread
                    moverows = 0

        c.setFillColor(colors.red)
        c.rect(x=4, y=countpost-12-2, width=Names_Spread-48, height=24, stroke=1, fill=1)
        c.setFillColor(colors.fidblue)
        c.rect(x=4, y=countpost-36-2, width=Names_Spread-48, height=24, stroke=1, fill=1)
        c.setFillColor(colors.white)
        c.setFont("Helvetica-Bold", 8, leading=None)
        c.drawCentredString(31, countpost-6+1, "FireTeam")
        c.drawCentredString(31, countpost-30+1, "EMTTeam")
        c.setFont("Helvetica", 8, leading=None)
        c.setFillColorRGB(193/255, 243/255, 250/255)
        c.rect(x=4, y=countpost-48-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.rect(x=4, y=countpost-60-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.rect(x=4, y=countpost-72-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.rect(x=4, y=countpost-84-2, width=Names_Spread-48, height=12, stroke=1, fill=1)
        c.setFillColor(colors.black)
        c.drawCentredString(31, countpost-48+1, "Extra Board")
        c.drawCentredString(31, countpost-60+1, "Vacation/Off")
        c.drawCentredString(31, countpost-72+1, "Train/Assign")
        c.drawCentredString(31, countpost-84+1, "Slide to Days")

        c.setFillColor(colors.black)
        #Night bottom block
        moveblock1 = (7-1)*12
        countblock1 = 0
        for teamitems in range(0+page_items*7, 7+page_items*7):
            c.setFillColor(colors.red)
            c.rect(x=60+countblock1, y=countpost-12-2, width=Names_Spread,
                   height=24, stroke=1, fill=1)
            c.setFillColor(colors.fidblue)
            c.rect(x=60+countblock1, y=countpost-36-2, width=Names_Spread,
                   height=24, stroke=1, fill=1)
            c.setFillColor(colors.white)
            c.setFont("Helvetica-Bold", 8, leading=None)
            firerecord_stuff1 = str(Comb_fireteamN[teamitems]).replace("'", '')
            firerecord_stuff = str(firerecord_stuff1).strip("[]")
            if len(firerecord_stuff) > 22:
                fireline1 = firerecord_stuff[0:firerecord_stuff.find('/', 11, 22)]
                fireline2 = firerecord_stuff[firerecord_stuff.find('/', 11, 22)+1:100]
            else:
                fireline1 = firerecord_stuff
            if len(firerecord_stuff) <= 22:
                c.drawCentredString(110+countblock1, countpost-6+1, fireline1)
            else:
                c.drawCentredString(110+countblock1, countpost+1, fireline1)
                c.drawCentredString(110+countblock1, countpost-12+1, fireline2)
            emtrecord_stuff1 = str(Comb_EMTteamN[teamitems]).replace("'", '')
            emtrecord_stuff = str(emtrecord_stuff1).strip("[]")
            if len(emtrecord_stuff) > 24:
                emtline1 = emtrecord_stuff[0:emtrecord_stuff.find('/', 11, 24)]
                emtline2 = emtrecord_stuff[emtrecord_stuff.find('/', 11, 24)+1:100]
            else:
                emtline1 = emtrecord_stuff
            if len(emtrecord_stuff) <= 24:
                c.drawCentredString(110+countblock1, countpost-30+1, emtline1)
            if len(emtrecord_stuff) > 24:
                c.drawCentredString(110+countblock1, countpost-24+1, emtline1)
                c.drawCentredString(110+countblock1, countpost-36+1, emtline2)
            c.setFont("Helvetica", 8, leading=None)
            c.setFillColorRGB(193/255, 243/255, 250/255)
            c.rect(x=60+countblock1, y=countpost-48-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.rect(x=60+countblock1, y=countpost-60-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.rect(x=60+countblock1, y=countpost-72-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.rect(x=60+countblock1, y=countpost-84-2, width=Names_Spread,
                   height=12, stroke=1, fill=1)
            c.setFillColor(colors.black)
            if Comb_extraN[teamitems][0:Comb_extraN[teamitems].find('/', 0, 100)] + '-Slide' \
                    in Comb_NamesDaysV2[teamitems] \
                    and Comb_extraN[teamitems][Comb_extraN[teamitems].find('/', 0, 100)+1:100] + '-Slide' \
                    in Comb_NamesDaysV2[teamitems]:
                c.drawCentredString(110+countblock1, countpost-84+1, Comb_extraN[teamitems])
            elif Comb_extraN[teamitems].find(':', 0, 100) != -1:
                if Comb_extraN[teamitems][0:Comb_extraN[teamitems].find('/', 0, 100)] + '-Slide' \
                        in Comb_NamesDaysV2[teamitems] \
                        and Comb_extraN[teamitems][Comb_extraN[teamitems].find('/', 0, 100)+1: \
                        Comb_extraN[teamitems].find(':', 0, 100)] + '-Slide' \
                        in Comb_NamesDaysV2[teamitems] and \
                        Comb_extraN[teamitems][Comb_extraN[teamitems].find(':', 0, 100)+1:100] + '-Slide' in \
                        Comb_NamesDaysV2[teamitems]:
                    c.drawCentredString(110+countblock1, countpost-84+1,
                                        Comb_extraN[teamitems][0:Comb_extraN[teamitems].find('/', 0, 100)] + '/' + \
                                        Comb_extraN[teamitems][Comb_extraN[teamitems].find('/', 0, 100)+1: \
                                                               Comb_extraN[teamitems].find(':', 0, 100)] + '/' + \
                                        Comb_extraN[teamitems][Comb_extraN[teamitems].find(':', 0, 100)+1:100])
            elif Comb_extraN[teamitems] + '-Slide' in Comb_NamesDaysV2[teamitems]:
                c.drawCentredString(110+countblock1, countpost-84+1, Comb_extraN[teamitems])
            elif Comb_extraN[teamitems][0:Comb_extraN[teamitems].find('/', 0, 100)] + '-Slide' \
                    in Comb_NamesDaysV2[teamitems]:
                c.drawCentredString(110+countblock1, countpost-84+1,
                                    Comb_extraN[teamitems][0:Comb_extraN[teamitems].find('/', 0, 100)])
            elif Comb_extraN[teamitems][Comb_extraN[teamitems].find('/', 0, 100)+1:100] + '-Slide' \
                    in Comb_NamesDaysV2[teamitems]:
                c.drawCentredString(110+countblock1, countpost-84+1, \
                                    Comb_extraN[teamitems][Comb_extraN[teamitems].find('/', 0, 100)+1:100])
            elif (Comb_extraN[teamitems] in Comb_extraD[teamitems] or Comb_extraN[teamitems] + '/' \
                  in Comb_extraD[teamitems] or '/' + Comb_extraN[teamitems] \
                  in Comb_extraD[teamitems]) and Comb_extraN[teamitems] in eval(str(Comb_TeamNameN[teamitems][-1:]) + "_peopleNames"):
                c.drawCentredString(110+countblock1, countpost-84+1, Comb_extraN[teamitems])
                # new
            elif Comb_extraN[teamitems] in Comb_NamesNightsV2[teamitems-1] and \
                    Comb_extraN[teamitems] + '-Slide' in Comb_NamesDaysV2[teamitems+1]:
                if Comb_NightVacNames[teamitems].find('/', 0, 100) != -1:
                    Comb_NightVacNames[teamitems] = Comb_NightVacNames[teamitems] + '/' + Comb_extraN[teamitems]
                else:
                    Comb_NightVacNames[teamitems] = Comb_extraN[teamitems]
                MoveDate1 = start_date + timedelta(teamitems - page_items*7)
                MoveDate2 = datetime.strftime(MoveDate1, "%b %d, %Y")
                MoveDate3 = MoveDate1 + timedelta(1)
                MoveDate4 = datetime.strftime(MoveDate3, "%b %d, %Y")
                messagebox.showinfo("Alert!", Comb_extraN[teamitems] + " will need to be 'OFF WITH PAY' on:" + '\n' \
                                    + "            " + MoveDate2 + '\n' + "To slide to DAYS on:" + '\n' + \
                                    "            " + MoveDate4)
            elif Comb_extraN[teamitems] + "-Slide" in Comb_NamesDaysV2[teamitems-1]:
                c.drawCentredString(110+countblock1, countpost-84+1, Comb_extraN[teamitems])
            elif Comb_extraN[teamitems] + "-Slide" in Comb_NamesDaysV2[teamitems-2] and \
                    Comb_extraN[teamitems] == Comb_extraN[teamitems-1]:
                c.drawCentredString(110+countblock1, countpost-84+1, Comb_extraN[teamitems])
            else:
                c.drawCentredString(110+countblock1, countpost-48+1, Comb_extraN[teamitems])
            c.drawCentredString(110+countblock1, countpost-60+1, Comb_NightVacNames[teamitems])
            c.drawCentredString(110+countblock1, countpost-72+1, Comb_NightTrainNames[teamitems])
            countblock1 += Names_Spread
        if Pass_matrix[0, 2] == 0:
            FinishFlag = 1
            c.showPage()


    c.save()

    file_name = '/Users/kelliecascarelli/PycharmProjects/PostScheduleMVC/test.pdf'
    if Pass_matrix[0, 2] == 0:
        subprocess.call(['open', file_name])
        SchedStartFlag = 1


if SchedStartFlag == 0 and Count_QuitSlideD < 1 and Count_QuitSlideN < 1 and FinishFlag == 0:
    total_daysStart = 0
    Sched_Start(total_daysStart)


#runpy.run_path(path_name="PostScheduleMain.py")