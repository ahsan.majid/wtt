import pandas as pd
from pandas import *
import numpy as np
from numpy import *
import csv
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import random
import time
import runpy

fileBase_Data = pd.read_csv("Base Data.txt", delimiter=',')
fileBase_sdf = pd.DataFrame(fileBase_Data.sort_values(by=['Team', 'Name']))
fileBase_sdf.reset_index()
fileBase_matrix = np.array(fileBase_sdf.sort_values(by=['Team', 'Salary']))
fileBase_shape = np.shape(fileBase_matrix)
pd.set_option("display.max_rows", None, "display.max_columns", None)
Sal_matrix = []
for Sal_items in range(np.shape(fileBase_matrix)[0]):
    if fileBase_matrix[Sal_items, 2] == "Salary":
        Sal_matrix += [fileBase_matrix[Sal_items, 1]]

Ateam = 0
Bteam = 0
Cteam = 0
Dteam = 0

for teams in range(np.shape(fileBase_matrix)[0]):
    if fileBase_matrix[teams, 0] == "A":
        Ateam += 1
    elif fileBase_matrix[teams, 0] == "B":
        Bteam += 1
    elif fileBase_matrix[teams, 0] == "C":
        Cteam += 1
    elif fileBase_matrix[teams, 0] == "D":
        Dteam += 1

InitInput_Data = pd.read_csv("Initial Input.txt", delimiter=',')
InitInput_sdf = pd.DataFrame(InitInput_Data)
InitInput_matrix = np.array(InitInput_sdf)
InitInput_shape = np.shape(InitInput_matrix)

BaseCAS_Data = pd.read_csv("Base Data CAS Manual.txt", delimiter=',')
BaseCAS_sdf = pd.DataFrame(BaseCAS_Data.sort_values(by=['Team', 'Name']))
BaseCAS_sdf = BaseCAS_sdf.sort_values(by=['Team', 'Name'])

BaseCAS_sdf.reset_index()
offname = "dummy"
offname1 = "dummy1"
foundflag = 0
for index1, row1 in fileBase_sdf.iterrows():
    found = 0
    for index, row in BaseCAS_sdf.iterrows():
        if row['Name'] == row1['Name']:
            found = 1
            break
    if found != 1:
        offname1 = row1['Name']
        for index, row in BaseCAS_sdf.iterrows():
            found = 0
            for index1, row1 in fileBase_sdf.iterrows():
                if row['Name'] == row1['Name']:
                    found = 1
                    break
            if found == 0:
                offname = row['Name']
                BaseCAS_sdf['Name'] = np.where(BaseCAS_sdf['Name'] == offname, offname1, BaseCAS_sdf['Name'])
                offname = "dummy"
                offname1 = "dummy1"
                foundflag = 1
                break

BaseCAS_matrix = np.array(BaseCAS_sdf.sort_values(by=['Team', str(BaseCAS_sdf.columns[4]),
                                                      str(BaseCAS_sdf.columns[5])],
                                                  ascending=[True, False, False]))
rootbase = Toplevel()
rootbase.title = ''
rootbase.maxsize(1500, 1000)
rootbase.geometry("+0+0")
rootbase.configure(bg="black")
adddata_count = 0
Team = StringVar()
Name = StringVar()
Salary = StringVar()
ERTQual = StringVar()
EMTQual = StringVar()

Post_Headerinfo = []
post_nameslist = {}
for post_items in range(InitInput_shape[0]):
    post_nameslist["Post" + str(post_items+1)] = InitInput_matrix[post_items,0]
    Post_Headerinfo += [InitInput_matrix[post_items, 0]]

MainFrame = Frame(rootbase, bd=10, width=1400, height=1000, relief=RIDGE, bg="black")
MainFrame.grid()

style = ttk.Style()
style.theme_use('clam')

TopFrame1 = Frame(MainFrame, bd=5, width=1400, height=200, relief=RIDGE, bg="black")
TopFrame1.grid(row=0, column=0, sticky=W)
TopFrame2 = Frame(MainFrame, bd=5, width=1400, height=50, relief=RIDGE, bg="black")
TopFrame2.grid(row=1, column=0, sticky=W)
TopFrame3 = Frame(MainFrame, bd=5, width=1400, height=800, relief=RIDGE, bg="black")
TopFrame3.grid(row=2, column=0, sticky=W)

InnerTopFrame1 = Frame(TopFrame1, bd=5, width=1300, height=190, bg='black', relief=RIDGE)
InnerTopFrame1.grid(row=0, column=0, sticky=W)
InnerTopFrame2 = Frame(TopFrame2, bd=5, width=1300, height=48, bg="gray", relief=RIDGE)
InnerTopFrame2.grid(row=1, column=0, sticky=W)
InnerTopFrame3 = Frame(TopFrame3, bd=5, width=1300, height=800, relief=RIDGE)
InnerTopFrame3.grid(row=2, column=0, sticky=W)


def Reset():
    Team.set("")
    Name.set("")
    Salary.set("")
    ERTQual.set("")
    EMTQual.set("")
    for rowx in range(4, InitInput_shape[0]+4):
        my_entries[rowx-4].delete(0, END)
        my_entries[rowx-4].insert(0, "")


def iExit():
    rootbase.destroy()
    runpy.run_path(path_name="OptimizerMain.py")


def addData():
    if ERTQual.get() == "":
        ERTInput = "N"
    else:
        ERTInput = ERTQual.get().upper()
    if EMTQual.get() == "":
        EMTInput = "N"
    else:
        EMTInput = EMTQual.get().upper()
    if Name.get() == "":
        messagebox.showerror("Data Entry Form", "Enter Name")
    elif Team.get() == "":
        messagebox.showerror("Data Entry Form", "Enter Team")
    else:
        valueenter = 'Team.get(), Name.get(), ERTInput, EMTInput'
        entry_list = []
        for entries in my_entries:
            if entries.get() == "":
                inputentry = 0
            else:
                inputentry = entries.get()
            entry_list += [str(inputentry)]
        for rowx in range(0, InitInput_shape[0]):
            valueenter = valueenter + ', entry_list[' + str(rowx) + ']'
        if eval(valueenter[0:10]) == "A":
            tree_records.insert('', index=1, values=(eval(valueenter)), tags=('Ateam',))
        elif eval(valueenter[0:10]) == "B":
            tree_records.insert('', index=Ateam+1, values=(eval(valueenter)), tags=('Bteam',))
        elif eval(valueenter[0:10]) == "C":
            tree_records.insert('', index=Ateam+Bteam+1, values=(eval(valueenter)), tags=('Cteam',))
        elif eval(valueenter[0:10]) == "D":
            tree_records.insert('', index=Ateam+Bteam+Cteam+1, values=(eval(valueenter)), tags=('Dteam',))

        messagebox.showinfo("Data Entry Form", "Record Entered Successfully")


def DisplayData():
    global BaseCAS_matrix
    count = 0
    for record in BaseCAS_matrix:
        record_stuff1 = str(record).replace("'", '')
        record_stuff = str(record_stuff1).strip("[]")
        if record[0] == "A":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Ateam',))
        elif record[0] == "B":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Bteam',))
        elif record[0] == "C":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Cteam',))
        elif record[0] == "D":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Dteam',))
        count += 1


def LearnersInfo(ev):
    global entry_list
    viewInfo = tree_records.selection()
    learnerData = tree_records.item(viewInfo)
    row = learnerData['values']
    Team.set(row[0])
    Name.set(row[1])
    ERTQual.set(row[2])
    EMTQual.set(row[3])
    for rowx in range(4, InitInput_shape[0]+4):
        my_entries[rowx-4].delete(0, END)
        my_entries[rowx-4].insert(0, str(row[rowx]))


def update():
    global entry_list
    selected = tree_records.focus()
    position = int(tree_records.focus())
    ERTInput = ERTQual.get().upper()
    EMTInput = EMTQual.get().upper()
    valueenter = 'Team.get(), Name.get(), ERTInput, EMTInput'
    entry_list = []
    for entries in my_entries:
        for entries in my_entries:
            if entries.get() == "":
                inputentry = 0
            else:
                inputentry = entries.get()
            entry_list += [str(inputentry)]
    for rowx in range(0, InitInput_shape[0]):
        valueenter = valueenter + ', entry_list[' + str(rowx) + ']'
    tree_records.item(selected, text="", values=eval(valueenter))


def deleteDB():
    selected = tree_records.focus()
    position = int(tree_records.focus())
    tree_records.delete(selected)
    messagebox.showinfo("Data Entry Form", "Record Successfully Deleted")


def saveDB():
    global fileBase_matrix1, foundflag
    erase_base = open("Base Data CAS Manual.txt", "w+")
    erase_base.close()
    PostHeader1 = ["Team", "Name", "ERT Qual", "EMT Qual"]
    PostHeader2 = np.hstack((PostHeader1, Post_Headerinfo))
    with open("Base Data CAS Manual.txt", 'a') as appFile:
        writer = csv.writer(appFile)
        writer.writerow(PostHeader2)
        appFile.close()
    for base_items in tree_records.get_children():
        with open("Base Data CAS Manual.txt", 'a') as appFile:
            writer = csv.writer(appFile)
            writer.writerow(tree_records.item(base_items)['values'])
            appFile.close()
    if foundflag == 0:
        messagebox.showinfo("Data Entry Form", "Record Successfully Saved")
    else:
        foundflag = 0


    #============================================================================================================

lblTeam = Label(InnerTopFrame1, font="Arial 14 bold", text="Team", bd=10, bg='black', fg='white')
lblTeam.grid(row=0, column=0, sticky=W)
comboTeam = ttk.Combobox(InnerTopFrame1, font="Arial 14 bold", width=8, justify="center", textvariable=Team)
comboTeam['value'] = ('', 'A', 'B', 'C', 'D')
comboTeam.current(0)
comboTeam.grid(row=0, column=1)

lblExistingName = Label(InnerTopFrame1, font="Arial 14 bold", text="Name", bd=10, bg='black', fg='white')
lblExistingName.grid(row=1, column=0, sticky=W)
txtExistingName = Entry(InnerTopFrame1, font="Arial 14 bold", justify="center", bd=3, width=32, textvariable=Name)
txtExistingName.grid(row=1, column=1)

lblERT = Label(InnerTopFrame1, font="Arial 14 bold", text="ERT Qualified (Y/N)", bd=10, bg='black', fg='white')
lblERT.grid(row=2, column=0, sticky=W)
txtERT = Entry(InnerTopFrame1, font="Arial 14 bold", bd=3, justify="center", width=8, textvariable=ERTQual)
txtERT.grid(row=2, column=1)

lblEMT = Label(InnerTopFrame1, font="Arial 14 bold", text="EMT Qualified (Y/N)", bd=10, bg='black', fg='white')
lblEMT.grid(row=3, column=0, sticky=W)
txtEMT = Entry(InnerTopFrame1, font="Arial 14 bold", bd=3, justify="center", width=8, textvariable=EMTQual)
txtEMT.grid(row=3, column=1)
lblPostComp = Label(InnerTopFrame1, font="Arial 16 bold", text="Post Competency Information", bd=10, width=22,
                    bg='black', fg='white', justify='left')
lblPostComp.place(x=418, y=0)

#Create Top Post Section....
post_itemsx1 = 0
post_itemsadj = 0
my_entries = []
initialadj = 0
columnrange = int(InitInput_shape[0]/3)
remainder = InitInput_shape[0] % 3
countrows = 0
if InitInput_shape[0] < 3:
    endrows = InitInput_shape[0]
else:
    endrows = 3
addedadj = 0
for ycolumns in range(0, columnrange):
    for post_itemsx in range(0+initialadj, endrows+initialadj):
        my_labelx = f"lblPost{post_itemsx + addedadj}"
        my_labelx = Label(InnerTopFrame1, font="Arial 14 bold", text=InitInput_matrix[post_itemsx+addedadj, 0],
                          bg='black', fg='white')
        my_labelx.grid(row=post_itemsx-initialadj+1, column=ycolumns+initialadj+2, stick=E)
        my_txtentry = Entry(InnerTopFrame1, font="Arial 14 bold", bd=3, justify="center", width=5)
        my_txtentry.grid(row=post_itemsx-initialadj+1, column=ycolumns+initialadj+3)
        my_entries.append(my_txtentry)
        countrows += 1
    if InitInput_shape[0] - countrows < 3 and remainder != 0:
        endrows = remainder
        initialadj += 1
        addedadj = 2
    else:
        initialadj += 3
#=============================================================================================
scroll_x = Scrollbar(InnerTopFrame3, orient=HORIZONTAL)
scroll_y = Scrollbar(InnerTopFrame3, orient=VERTICAL)
scroll_x.pack(side=BOTTOM, fill=X)
scroll_y.pack(side=RIGHT, fill=Y)
post_headersData = []
for post_headers in BaseCAS_sdf:
    post_headersData.append(post_headers)
tree_records = ttk.Treeview(InnerTopFrame3, height=24, columns=post_headersData,
                            xscrollcommand=scroll_x.set, yscrollcommand=scroll_y.set)
tree_records.tag_configure('Ateam', background='#9DD0F3', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Bteam', background='#F7CDAB', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Cteam', background='#DCBBF6', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Dteam', background='#E5F7AB', font=('Arial', 14, 'bold'))
scroll_x.config(command=tree_records.xview)
scroll_y.config(command=tree_records.yview)
for tree_items in range(np.shape(post_headersData)[0]):
    tree_records.heading(tree_items, text=post_headersData[tree_items])
    tree_records['show'] = 'headings'
    if tree_items == 1:
        col_width = 150
    else:
        col_width = 75
    tree_records.column(tree_items, width=col_width, anchor=CENTER)
tree_records.pack(fill="both", expand="yes")
tree_records.bind("<ButtonRelease-1>", LearnersInfo)
#=============================================================================================
button_width = 12

btnUpdate = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Update List", cursor="circle", command=update)
btnUpdate.configure(fg='blue')
btnUpdate.grid(row=0, column=0, padx=3)

btnAddNew = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Add New", cursor="circle", command=addData)
btnAddNew.configure(fg='blue')
btnAddNew.grid(row=0, column=1, padx=3)

btnDelete = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Delete", cursor="circle", command=deleteDB)
btnDelete.configure(fg='blue')
btnDelete.grid(row=0, column=2, padx=3)

btnReset = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                  text="Reset", cursor="circle", command=Reset)
btnReset.configure(fg='blue')
btnReset.grid(row=0, column=3, padx=3)

btnSearch = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Save", cursor="circle", command=saveDB)
btnSearch.configure(fg='blue')
btnSearch.grid(row=0, column=4, padx=3)

btnExit = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                 text="Exit", cursor="circle", command=iExit)
btnExit.configure(fg='blue')
btnExit.grid(row=0, column=5, padx=3)

DisplayData()

if foundflag == 1:
    messagebox.showinfo("Alert", 'At least 1 "Name" error was detected and Corrrected!')
    saveDB()

rootbase.mainloop()
