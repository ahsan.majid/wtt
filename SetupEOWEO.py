from typing import List, Any
import numpy as np
from datetime import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import csv
import datetime
import pandas as pd
from PIL import ImageTk, Image
import runpy

screen2 = Toplevel()
screen2.geometry("700x730+350+0")
screen2.title("")
screen2.configure(bg="black", bd=10, width=700, height=730, relief=RIDGE)
style = ttk.Style()
style.theme_use('clam')
entry_input = ''
my_DateA = ''
my_DateB = ''
my_DateC = ''
my_DateD = ''
my_DateAA = ''
my_DateBB = ''
my_DateCC = ''
my_DateDD = ''
button_knowA = 0
button_knowB = 0
button_knowC = 0
button_knowD = 0
Sched_Message = 'Press to Create Schedule'
shift_ready = ''
Set_Height = 60
global Temp_Shift

def exit_setsched():
    screen2.destroy()
    #runpy.run_path(path_name="PostScheduleMain.py")


def Dupont_Select():
    screen2.destroy()
    runpy.run_path(path_name="SetDuPont.py")


def FourFour_Select():
    screen2.destroy()
    runpy.run_path(path_name="SetupFour.py")


def Seven_Select():
    screen2.destroy()
    runpy.run_path(path_name="SetupSeven.py")


def FourFive_Select():
    screen2.destroy()
    runpy.run_path(path_name="SetupFourFive.py")


yearrange = 400

EOWEOOT_table = pd.read_table('ShiftEOWEOOTTable.txt', delimiter=',')
sdf_EOWEOOTTable = pd.DataFrame(EOWEOOT_table)
EOWEOOT_matrix = np.array(sdf_EOWEOOTTable)
EOWEOOT_shape = np.shape(EOWEOOT_matrix)[0]
schedmain_label = Label(screen2, text="EOWEO Schedule", width=20, font="Arial 20 bold")
schedmain_label.place(x=230, y=10)
first_team = "A"
second_team = ""
third_team = ""
fourth_team = ""

def Select_DateA(event):
    global my_DateAA, first_team, SecondTeam_var, Selected_DateA, SecondTeam_label, SecondTeam_combo
    my_DateAA = ScdCal.selection_get()
    my_WeekdayA = my_DateAA.weekday()
    #first_team ="A"
    if not my_WeekdayA == 0:
        messagebox.showwarning('Error', 'Monday Date not Chosen!')
        my_DateAA = ''
    elif my_DateAA == my_DateBB or my_DateAA == my_DateCC or my_DateAA == my_DateDD:
        messagebox.showwarning('Error', 'This Data is already Chosen!')
        my_DateAA = ''
    else:
        my_DateA = ScdCal.get_date()
        Selected_DateA = Label(screen2, text=my_DateA, font=("Arial", 14), height=1)
        Selected_DateA.place(x=270, y=Set_Height)

    if my_DateAA != "":
        SecondTeam_label = Label(screen2, text="Select Night Shift Team with Team A", font=("Arial", 14),
                                 height=1)
        SecondTeam_label.place(x=20, y=Set_Height+50)
        SecondTeam_var = StringVar()
        SecondTeam_combo = ttk.Combobox(screen2, font="Arial 14 bold", width=8, justify="center",
                                        textvariable=SecondTeam_var)
        SecondTeam_combo['value'] = ('B', 'C', 'D')
        SecondTeam_combo.place(x=270, y=Set_Height+50)
        SecondTeam_combo.bind("<<ComboboxSelected>>", Select_third)
    return my_DateAA, first_team, SecondTeam_var


def Select_third(event):
    global first_team, second_team, ThirdTeam_var, ThirdTeam_label, ThirdTeam_combo
    second_team = SecondTeam_var.get()
    ThirdTeam_label = Label(screen2, text="Select Next Rotation Day Shift Team", font=("Arial", 14),
                            height=1)
    ThirdTeam_label.place(x=20, y=Set_Height+100)
    ThirdTeam_var = StringVar()
    ThirdTeam_combo = ttk.Combobox(screen2, font="Arial 14 bold", width=8, justify="center",
                                   textvariable=ThirdTeam_var)
    ThirdTeam_combo['value'] = ('', 'B', 'C', 'D')
    ThirdTeam_combo.current(0)
    ThirdTeam_combo.place(x=270, y=Set_Height+100)
    ThirdTeam_combo.bind("<<ComboboxSelected>>", Select_FinalTeams)
    if second_team == "B":
        ThirdTeam_combo['value'] = ('C', 'D')
    if second_team == "C":
        ThirdTeam_combo['value'] = ('B', 'D')
    if second_team == "D":
        ThirdTeam_combo['value'] = ('B', 'C')
    return second_team, ThirdTeam_var


def Select_FinalTeams(event):
    global first_team, second_team, third_team, fourth_team
    third_team = ThirdTeam_var.get()
    if (second_team == "B" or second_team == "C") and (third_team == "B" or third_team == "C"):
        fourth_team = "D"
    if (second_team == "B" or second_team == "D") and (third_team == "B" or third_team == "D"):
        fourth_team = "C"
    if (second_team == "C" or second_team == "D") and (third_team == "C" or third_team == "D"):
        fourth_team = "B"
    Set_OTCriteria()
    #shift_ready = Button(screen2, text=Sched_Message, font=("Arial", 20), height=1, command=Shift_EOWEO)
    #shift_ready.place(x=60, y=300)
    return my_DateAA, first_team, second_team, third_team, fourth_team


def Set_OTCriteria():
    global OTCrit_var, label_OTCrit, combo_OTCrit
    OTCrit_var = StringVar()
    label_OTCrit = Label(screen2, text="Set OT Selection Criteria:", font=("Arial", 14), height=1)
    label_OTCrit.place(x=75, y=Set_Height+150)
    combo_OTCrit = ttk.Combobox(screen2, font="Arial 14 bold", width=8, justify="center",
                                textvariable=OTCrit_var)
    combo_OTCrit['value'] = ("OT Hours", "Seniority")
    combo_OTCrit.place(x=270, y=Set_Height+150)
    combo_OTCrit.bind("<<ComboboxSelected>>", Set_SlideTol)


def Set_SlideTol(event):
    global tol_var, label_tol, combo_tol
    tol_var = IntVar()
    label_tol = Label(screen2, text="Set Slider Tolerance:", font=("Arial", 14), height=1)
    label_tol.place(x=105, y=Set_Height+200)
    combo_tol = ttk.Combobox(screen2, font="Arial 14 bold", width=8, justify="center",
                             textvariable=tol_var)
    combo_tol['value'] = (1, 2)
    combo_tol.place(x=270, y=Set_Height+200)
    combo_tol.bind("<<ComboboxSelected>>", Set_DoubleDay)


def Set_DoubleDay(event):
    global double_var, label_double, combo_double
    double_var = IntVar()
    label_double = Label(screen2, text="Set Double Time Day:", font=("Arial", 14), height=1)
    label_double.place(x=100, y=Set_Height+250)
    combo_double = ttk.Combobox(screen2, font="Arial 14 bold", width=8, justify="center",
                                textvariable=double_var)
    combo_double['value'] = (0, 7, 8, 9, 10, 11, 12, 13)
    combo_double.place(x=270, y=Set_Height+250)
    combo_double.bind("<<ComboboxSelected>>", Confirm_Sched)


def Confirm_Sched(event):
    global shift_ready
    shift_ready = Button(screen2, text=Sched_Message, font=("Arial", 20), height=1, cursor="circle",
                         fg='blue', command=Shift_EOWEO)
    shift_ready.place(x=60, y=Set_Height+300)


def Shift_EOWEO():
    global first_team, second_team, third_team, fourth_team, my_DateAA, Temp_Shift
    file_sched = open("ShiftEOWEO.txt", "w+")
    file_sched.close()
    #Set up date matrix
    Shift_Four = ["Date"]
    add_dates: int

    #Insert Shifts into matrix
    Temp_ShiftTitle = []
    Temp_Shift = [[0 for x in range(8)] for y in range(28)]
    Temp_ShiftTitle = [[0 for x in range(8)] for y in range(1)]
    Temp_ShiftTitle[0][0] = "Days"
    Temp_ShiftTitle[0][1] = "Nights"
    Temp_ShiftTitle[0][2] = "1stOTD"
    Temp_ShiftTitle[0][3] = "2ndOTD"
    Temp_ShiftTitle[0][4] = "3rdOTD"
    Temp_ShiftTitle[0][5] = "1stOTN"
    Temp_ShiftTitle[0][6] = "2ndOTN"
    Temp_ShiftTitle[0][7] = "3rdOTN"
    nil = 'nil'
    for add_dates in range(0, 28*yearrange):
        next_date = my_DateAA + timedelta(add_dates)
        Shift_Four += [datetime.datetime.strftime(next_date, '%Y-%m-%d')]
    Shift_Four = np.reshape(Shift_Four, (1+((29-1)*yearrange), 1))
    cycle_adder = 0
    for xx_row in range(0, 28):
        if xx_row >= 0 and xx_row <= 1:
            Temp_Shift[xx_row][0] = first_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = second_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 2 and xx_row <= 3:
            Temp_Shift[xx_row][0] = third_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = fourth_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 4 and xx_row <= 6:
            Temp_Shift[xx_row][0] = second_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = first_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 7 and xx_row <= 8:
            Temp_Shift[xx_row][0] = fourth_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = third_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 9 and xx_row <= 10:
            Temp_Shift[xx_row][0] = first_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = second_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 11 and xx_row <= 13:
            Temp_Shift[xx_row][0] = third_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = fourth_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 14 and xx_row <= 15:
            Temp_Shift[xx_row][0] = second_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = first_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 16 and xx_row <= 17:
            Temp_Shift[xx_row][0] = fourth_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = third_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 18 and xx_row <= 20:
            Temp_Shift[xx_row][0] = first_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = second_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])

        if xx_row >= 21 and xx_row <= 22:
            Temp_Shift[xx_row][0] = third_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = fourth_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 23 and xx_row <= 24:
            Temp_Shift[xx_row][0] = second_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = first_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
        if xx_row >= 25 and xx_row <= 27:
            Temp_Shift[xx_row][0] = fourth_team
            Temp_Shift[xx_row][2] = eval(EOWEOOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(EOWEOOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(EOWEOOT_matrix[xx_row][3])
            Temp_Shift[xx_row][1] = third_team
            Temp_Shift[xx_row][5] = eval(EOWEOOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(EOWEOOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(EOWEOOT_matrix[xx_row][7])
    Temp_Shift1 = []
    for temp_stack in range(1, yearrange+1):
        Temp_Shift1 += Temp_Shift
    Temp_ShiftFinal = np.vstack((Temp_ShiftTitle, Temp_Shift1))
    Shift_FourFinal = np.hstack((Shift_Four, Temp_ShiftFinal))
    with open('ShiftEOWEO.txt', 'a') as appFile:
        writer = csv.writer(appFile)
        writer.writerows(Shift_FourFinal)
        appFile.close()
    erase_Setup = open('SetupScheduleInfo.txt', 'w+')
    erase_Setup.close()
    SetupInfo_header = (["Schedule", "EB Days", "Double Days", "OT Selection"])
    with open("SetupScheduleInfo.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInfo_header)
        appFile1.close()
    with open("SetupScheduleInfo.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(["EOWEO Schedule", tol_var.get(), double_var.get(), OTCrit_var.get()])
        appFile1.close()
    erase_appoints = open('Shift Appointments.txt', 'w+')
    erase_appoints.close()
    Appoints_header = ['Date', 'Name', 'Team', 'Appointment']
    with open("Shift Appointments.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(Appoints_header)
        appFile1.close()
    messagebox.showinfo("Success!", "EOWEO Schedule Successfully Created!")
    Selected_DateA.destroy()
    SecondTeam_combo.destroy()
    SecondTeam_label.destroy()
    ThirdTeam_label.destroy()
    ThirdTeam_combo.destroy()
    label_OTCrit.destroy()
    combo_OTCrit.destroy()
    label_tol.destroy()
    combo_tol.destroy()
    label_double.destroy()
    combo_double.destroy()
    shift_ready.destroy()


ScdCal = Calendar(screen2, setmode="day", date_pattern="mm/dd/yyyy", selectforeground="red", showweeknumbers=False)
ScdCal.configure(font="Arial 14", foreground="black", background="Yellow", cursor="circle")
ScdCal.place(x=400, y=Set_Height)
ScdCal.bind("<<CalendarSelected>>", Select_DateA)

open_CalA = Label(screen2, text="Select A-Team 1st Monday Day Shift:", font=("Arial", 14), height=1)
open_CalA.place(x=20, y=Set_Height)
blank_ASel = Label(screen2, text='                  ', font=("Arial", 14), height=1)
blank_ASel.place(x=270, y=Set_Height)

EOWEO_tag = Label(screen2, text="EOWEO Schedule Example", font="Arial 20 bold", fg='white', bg='black')
EOWEO_tag.place(x=205, y=470)

EOWEO_image = Image.open('EOWEO Schedule Image.png')
EOWEO_resize = EOWEO_image.resize((600, 90), Image.ANTIALIAS)
EOWEO_new = ImageTk.PhotoImage(EOWEO_resize)
EOWEOImage_label = Label(screen2, image=EOWEO_new)
EOWEOImage_label.place(x=50, y=500)

Dupont_selectbutton = Button(screen2, text="DuPont Schedule", width=17, font="Arial 14 bold", cursor="circle",
                             fg='blue', command=Dupont_Select)
Dupont_selectbutton.place(x=20, y=640)

Four_selectbutton = Button(screen2, text="4on/4off Schedule", width=17, font="Arial 14 bold", cursor="circle",
                           fg='blue', command=FourFour_Select)
Four_selectbutton.place(x=170, y=640)

Seven_selectbutton = Button(screen2, text="7on/7off Schedule", width=17, font="Arial 14 bold", cursor="circle",
                            fg='blue', command=Seven_Select)
Seven_selectbutton.place(x=470, y=640)

FourFive_selectbutton = Button(screen2, text="Four/Five Schedule", width=17, font="Arial 14 bold", cursor="circle",
                               fg='blue', command=FourFive_Select)
FourFive_selectbutton.place(x=320, y=640)

exitschedset_button = Button(screen2, text="Exit", width=7, font="Arial 16 bold", cursor="circle", fg='blue',
                             command=exit_setsched)
exitschedset_button.place(x=600, y=680)

screen2.mainloop()
