import pandas as pd
import numpy as np
import csv
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import runpy
import subprocess

global weeks_entry, sched_date

rootmain = Tk()
x = 1.0
rootmain.attributes('-alpha', x)
weeks_entry = 0
sched_date = ''
style = ttk.Style()
style.theme_use('clam')
border = 8

TeamName_table = pd.read_table('TeamName.txt')
sdf_TeamNameTable = pd.DataFrame(TeamName_table)
TeamName_matrix = np.array(sdf_TeamNameTable)

SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)
InfoTable_matrixshape = np.shape(InfoTable_matrix)
Unit_Name = TeamName_matrix[0][0]

Schedule_table = pd.read_csv('Total Schedule.txt', delimiter=',')
sdf_ScheduleTable = pd.DataFrame(Schedule_table)
ScheduleTable_matrix = np.array(sdf_ScheduleTable)
ScheduleTable_matrixshape = np.shape(ScheduleTable_matrix)

Initial_table = pd.read_csv('Initial Input.txt', delimiter=',')
sdf_InitialTable = pd.DataFrame(Initial_table)
Initial_matrix = np.array(sdf_InitialTable)
#Unit_Name = InfoTable_matrixshape[0, 4]

Post_CountDays = 0
Post_CountNights = 0
for items in range(np.shape(Initial_matrix)[0]):
    if Initial_matrix[items, 1] == "Y" or Initial_matrix[items, 2] == "Y":
        Post_CountDays += 1
    if Initial_matrix[items, 1] == "Y" and Initial_matrix[items, 2] != "Y":
        Post_CountNights += 1

Post_count = np.shape(Initial_matrix)[0]

Schedule_table = pd.read_csv('Total Schedule.txt', delimiter=',')
sdf_ScheduleTable = pd.DataFrame(Schedule_table)
ScheduleTable_matrix = np.array(sdf_ScheduleTable)

Event_table = pd.read_table('Shift Appointments.txt', delimiter=',')
sdf_EventTable = pd.DataFrame(Event_table)
sdf_EventTablesort = sdf_EventTable.sort_values(by=(['Date', 'Team', 'Name']))
Event_matrix = np.array(sdf_EventTablesort)
Event_shape = np.shape(Event_matrix)[0]

BaseCAS_Data = pd.read_csv("Base Data CAS.txt", delimiter=',')
BaseCAS_sdf = pd.DataFrame(BaseCAS_Data)
BaseCAS_matrix = np.array(BaseCAS_sdf)
BaseCAS_shape = np.shape(BaseCAS_matrix)

A_peoplecount = 0
B_peoplecount = 0
C_peoplecount = 0
D_peoplecount = 0

for count_people in range(np.shape(BaseCAS_matrix)[0]):
    if BaseCAS_matrix[count_people, 0] == "A":
        A_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "B":
        B_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "C":
        C_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "D":
        D_peoplecount += 1


input_table = pd.read_table('PassDate.txt', delimiter=',')
sdf_inputtable = pd.DataFrame(input_table)
Pass_matrix = np.array(sdf_inputtable)
Pass_shape = np.shape(Pass_matrix)[0]


def Run_Schedule():
    var.set("")
    label42.destroy()
    label43.destroy()
    label46.destroy()
    label47.destroy()
    button4.destroy()
    button5.destroy()
    runpy.run_path(path_name="PostSchedulerV20.py")
    UpdateInfo_chart()


def abort_sched():
    var.set("")
    label42.destroy()
    label43.destroy()
    label46.destroy()
    label47.destroy()
    button4.destroy()
    button5.destroy()


def UpdateInfo_chart():
    rootmain.destroy()
    runpy.run_path(path_name="PostScheduleMain.py")
    Update_on_exit = 0


def Open_pdf():
    file_name = '/Users/kelliecascarelli/PycharmProjects/PostScheduleMVC/test.pdf'
    subprocess.call(['open', file_name])


def OpenOpt_pdf():
    runpy.run_path(path_name="OptimizerMain.py")
    #runpy.run_path(path_name="OptimizerV7.py")


def OpenEB_pdf():
    runpy.run_path(path_name="ExtraBoardAnalysis.py")


def YOS():
    runpy.run_path(path_name="YearsofService.py")


def exit_main():
    rootmain.destroy()


def Setup_Schedule():
    runpy.run_path(path_name="SetupScheduleV2.py")


def Initial_Input():
    runpy.run_path(path_name="InitialInput.py")


def Update_Posts():
    runpy.run_path(path_name="UpdatePostData.py")


def Update_Off():
    runpy.run_path(path_name="OffTimeSetup.py")


def Event_Input():
    runpy.run_path(path_name="EventCalendarV4.py")


def edit_basedata():
    runpy.run_path(path_name="BaseDataInput4.py")


def edit_basecomp():
    runpy.run_path(path_name="BaseDataCAS.py")


def Sched_dateentry(event):
    global sched_date, weeks_entry, label43, label46, label47, button4, button5
    if weeks_entry == 0:
        messagebox.showwarning('Error', 'Enter Duration (weeks)!')
    else:
        sched_date = Cal_Schedule.selection_get()
        sched_dateformatted = datetime.strftime(sched_date, "%b %d, %Y")
        day_chosen = datetime.weekday(sched_date)

        if day_chosen != 0:
            messagebox.showwarning('Error', 'Monday Date not Chosen!')
            sched_date = ''
        else:
            label43 = Label(label4, text="Start Date Selected:", bg="white", font="Arial 14", width=20, relief="solid", borderwidth=1)
            label43.place(x=20, y=150)
            label46 = Label(label4, text=sched_dateformatted, bg="white", fg="black", font="Arial 14", width=14,
                            relief="solid", borderwidth=1)
            label46.place(x=189, y=150)
            sched_finaldate = sched_date + timedelta(weeks_entry)*7 - timedelta(1)
            sched_finaldateformat = datetime.strftime(sched_finaldate, "%b %d, %Y")
            label47 = Label(label4, text=("Schedule Period: " + sched_dateformatted + " - " + sched_finaldateformat), bg="#57E4D7", \
                            fg="black", font="Arial 20 bold", width=50, relief="solid", borderwidth=1)
            label47.place(x=20, y=260)
            button4 = tk.Button(wrap_main4, text="Press to Create Schedule", font="Arial 14 bold", width=25, cursor="circle", command=Run_Schedule)
            button4.place(x=20, y=310)
            button4.configure(fg="blue", bg="white")
            button5 = tk.Button(wrap_main4, text="Press to Cancel", font="Arial 14 bold", width=25, cursor="circle", command=abort_sched)
            button5.place(x=250, y=310)
            button5.configure(fg="blue")
            erase_pass = open("PassDate.txt", "w+")
            erase_pass.close()
            PassHeader = ["Date", "Duration", "PostFlag", "PassBypass"]
            with open("PassDate.txt", 'a') as appFile:
                writer = csv.writer(appFile)
                writeList = PassHeader
                writer.writerow(writeList)
                appFile.close()
            with open("PassDate.txt", 'a', newline="") as appFile:
                writer = csv.writer(appFile)
                writeList = [sched_date, weeks_entry, 0, 1]
                writer.writerow(writeList)
                appFile.close()
    return sched_date


def Select_weeks():
    global weeks_entry, label42
    weeks_entry = var.get()
    label42 = Label(label4, text="Select Monday Start Date from Calendar", bg="white", font="Arial 14",
                    width=35, relief="solid", borderwidth=1)
    label42.place(x=20, y=100)
    return weeks_entry


label_width = 78
label_height = 100
frame_start = 0
frame_position = 260
frame_xpos = 10
right_frames = frame_xpos + 709
the_color = 'black'

wrap_main1 = LabelFrame(rootmain, relief=RIDGE, bd=1)
wrap_main1.place(x=frame_xpos, y=frame_start)
wrap_main1.configure(bg=the_color)
label1 = Label(wrap_main1, width=label_width, height=label_height, bg=the_color, fg='white')
label1.configure(bg="black", bd=border, width=77, height=130, relief=RIDGE)
label1.pack()
label110 = Label(label1, text="Team:", font="Arial 16 bold", bg=the_color, fg='white', width=6, anchor=E)
label110.place(x=361, y=12)
label111 = Label(label1, text=Unit_Name, font="Arial 20 bold", bg=the_color, fg='white', width=10, anchor=W)
label111.place(x=435, y=10)
label12 = Label(label1, text="Current Schedule:", font="Arial 16 bold", bg=the_color, fg='white', width=25, anchor=E)
label12.place(x=191, y=42)
label13 = Label(label1, text=InfoTable_matrix[0, 0], font="Arial 20 bold", bg=the_color, fg="white", width=20, anchor=W)
label13.place(x=435, y=40)
label141 = Label(label1, text="OT Criteria:", font="Arial 16 bold", bg=the_color, fg='white', width=9)
label141.place(x=335, y=72)
label151 = Label(label1, text=str(InfoTable_matrix[0, 3]), font="Arial 20 bold", bg=the_color,
                 fg="white", width=8, anchor=W)
label151.place(x=435, y=70)
label14 = Label(label1, text="Slider Tolerance:", font="Arial 16 bold", bg=the_color, fg='white', width=30)
label14.place(x=222, y=102)
TextInfo = "Shifts"
Posx = 460
if InfoTable_matrix[0, 1] == 1:
    TextInfo = "Shift"
    Posx = 454
label15 = Label(label1, text=str(InfoTable_matrix[0, 1]) + " " + TextInfo, font="Arial 20 bold", bg=the_color,
                fg="white", width=6, anchor=W)
label15.place(x=435, y=100)
label16 = Label(label1, text="Double Time Day:", font="Arial 16 bold", bg=the_color, fg='white', width=30)
label16.place(x=218, y=132)
if InfoTable_matrix[0, 2] == 0:
    Doubleday = "None"
    label17 = Label(label1, text=Doubleday, font="Arial 20 bold", bg=the_color, fg="white", width=4, anchor=W)
    label17.place(x=435, y=130)
else:
    Doubleday = InfoTable_matrix[0, 2]
    label17 = Label(label1, text=Doubleday, font="Arial 20 bold", bg=the_color, fg="white", width=1)
    label17.place(x=459, y=130)

Button_11 = tk.Button(wrap_main1, text="Set the Work Schedule", font="Arial 14 bold", width=20, cursor="circle", command=Setup_Schedule)
Button_11.configure(fg='blue')
Button_11.place(x=10, y=15)

Button_18 = tk.Button(wrap_main1, text="Set Up Post Information", font="Arial 14 bold", width=20, cursor="circle", command=Initial_Input)
Button_18.configure(fg='blue')
Button_18.place(x=10, y=65)

Button_181 = tk.Button(wrap_main1, text="Update Post Information", font="Arial 14 bold", width=20, cursor="circle", command=Update_Posts)
Button_181.configure(fg='blue')
Button_181.place(x=10, y=115)

Button_182 = tk.Button(wrap_main1, text="Set Off Time Information", font="Arial 14 bold", width=20, cursor="circle", command=Update_Off)
Button_182.configure(fg='blue')
Button_182.place(x=10, y=165)

wrap_main2 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main2.place(x=frame_xpos, y=frame_start+frame_position-45)
wrap_main2.configure(bg=the_color)
label2 = Label(wrap_main2)
label2.configure(bg="black", bd=border, width=77, height=100, relief=RIDGE)
label2.pack()
Button_21 = tk.Button(wrap_main2, text="OT & Work Post Data", font="Arial 14 bold", width=19, cursor="circle", command=edit_basedata)
Button_21.configure(fg="blue")
Button_21.place(x=10, y=15)
Button_22 = tk.Button(wrap_main2, text="Competency & ER Data", font="Arial 14 bold", width=19, cursor="circle", command=edit_basecomp)
Button_22.configure(fg="blue")
Button_22.place(x=10, y=65)
Button_23 = tk.Button(wrap_main2, text="Team Balance Analysis", font="Arial 14 bold", width=19, cursor="circle", command=OpenOpt_pdf)
Button_23.configure(fg="blue")
Button_23.place(x=10, y=115)
Button_24 = tk.Button(wrap_main2, text="Years of Service", font="Arial 14 bold", width=19, cursor="circle", command=YOS)
Button_24.configure(fg="blue")
Button_24.place(x=10, y=165)


if Pass_matrix[0, 3] != 0:
    plot_posts = []
    BaseCAS_sdf.drop(["Team", "Name", "ERT Qual", "EMT Qual"],  inplace=True, axis=1)
    plot_posts = np.array(BaseCAS_sdf.columns)

    plot_yACASavg = []
    plot_yBCASavg = []
    plot_yCCASavg = []
    plot_yDCASavg = []
    for CAS_itemscols in range(np.shape(BaseCAS_matrix)[1]-4):
        countA = 0
        countB = 0
        countC = 0
        countD = 0
        plot_yACASsum = 0
        plot_yBCASsum = 0
        plot_yCCASsum = 0
        plot_yDCASsum = 0
        for CAS_itemsrows in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4] != 0:
                if BaseCAS_matrix[CAS_itemsrows][0] == "A":
                    plot_yACASsum = plot_yACASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countA += 1
                if BaseCAS_matrix[CAS_itemsrows][0] == "B":
                    plot_yBCASsum = plot_yBCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countB += 1
                if BaseCAS_matrix[CAS_itemsrows][0] == "C":
                    plot_yCCASsum = plot_yCCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countC += 1
                if BaseCAS_matrix[CAS_itemsrows][0] == "D":
                    plot_yDCASsum = plot_yDCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countD += 1
        plot_yACASavg += [round(plot_yACASsum/countA, 1)]
        plot_yBCASavg += [round(plot_yBCASsum/countB, 1)]
        plot_yCCASavg += [round(plot_yCCASsum/countC, 1)]
        plot_yDCASavg += [round(plot_yDCASsum/countD, 1)]

    plot_xCAS = []

    for xplot_values in range(np.shape(plot_posts)[0]):
        plot_xCAS += [xplot_values]

    plot_x3 = np.array(plot_xCAS)

    figure4 = plt.Figure(figsize=(4/6*np.shape(plot_posts)[0]*1.1, 3), dpi=90, facecolor='black')
    ax3 = figure4.add_subplot(111)
    figure4.subplots_adjust(top=0.75)
    rects10 = ax3.bar(plot_x3 - .23, plot_yACASavg, color='blue', width=.12, label='A', align='center')
    rects11 = ax3.bar(plot_x3 - .09, plot_yBCASavg, color='orange', width=.1, label="B", align='center')
    rects12 = ax3.bar(plot_x3 + .09, plot_yCCASavg, color='purple', width=.1, label="C", align='center')
    rects13 = ax3.bar(plot_x3 + .23, plot_yDCASavg, color='green', width=.12, label="D", align='center')
    ax3.set_xticks(np.arange(0, np.shape(plot_posts)[0], 1))
    ax3.set_title("Avg Post Competency", color="white", pad=25, fontsize=13)
    ax3.set_xticklabels(plot_posts, rotation=0)
    ax3.tick_params(labelsize=12, colors='white', pad=1)
    ax3.patch.set_color('black')
    ax3.set_ylim(0, 4)
    ax3.legend(['A', 'B', 'C', 'D'], facecolor='black', labelcolor='white', fontsize="small",
               frameon=False, ncol=4, bbox_to_anchor=(.95, 1.16))
    ax3.spines['bottom'].set_color('white')
    ax3.spines['left'].set_color('white')
    #figure4.tight_layout()
    chart4 = FigureCanvasTkAgg(figure4, wrap_main2)
    chart4.get_tk_widget().place(x=198, y=20)
    plt.show()

wrap_main3 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main3.place(x=frame_xpos, y=frame_start+frame_position*2-70+10)
wrap_main3.configure(bg=the_color)
label3 = Label(wrap_main3, width=label_width, height=label_height-20, bg=the_color, fg='white')
label3.configure(bg="black", bd=border, width=77, height=19, relief=RIDGE)
label3.pack()
Button_31 = tk.Button(wrap_main3, text="Input Calendar Events", font="Arial 14 bold", width=18, cursor="circle", command=Event_Input)
Button_31.configure(fg="blue")
Button_31.place(x=10, y=15)

if Pass_matrix[0, 3] != 0:
    #Start_Date3 = date.today() - timedelta(date.today().weekday()) + timedelta(7)
    #Start_Date2 = datetime.strftime(date.today() - timedelta(date.today().weekday()) + timedelta(7), "%b %d, %Y")
    #End_Date3 = date.today() - timedelta(date.today().weekday()) + timedelta(20)
    #End_Date2 = datetime.strftime(date.today() - timedelta(date.today().weekday()) + timedelta(20), "%b %d, %Y")
    Start_Date3 = date.today() + timedelta(1)
    Start_Date2 = datetime.strftime(date.today() + timedelta(1), "%b %d, %Y")
    End_Date3 = date.today() + timedelta(14)
    End_Date2 = datetime.strftime(date.today() + timedelta(14), "%b %d, %Y")
    plot_xeventdates = []
    plot_shortdates = []
    for dateitems in range(14):
        if dateitems == 0:
            plot_xeventdates += [datetime.strftime(Start_Date3, "%Y-%m-%d")]
            plot_shortdates += [datetime.strftime(Start_Date3, "%m/%d")]
        elif dateitems == 13:
            plot_xeventdates += [datetime.strftime(End_Date3, "%Y-%m-%d")]
            plot_shortdates += [datetime.strftime(End_Date3, "%m/%d")]
        else:
            plot_xeventdates += [datetime.strftime(Start_Date3 + timedelta(dateitems), "%Y-%m-%d")]
            plot_shortdates += [datetime.strftime(Start_Date3 + timedelta(dateitems), "%m/%d")]

    plot_yAVac = []
    plot_yBVac = []
    plot_yCVac = []
    plot_yDVac = []

    for event_dates in range(14):
        plot_yAVacsum = 0
        plot_yBVacsum = 0
        plot_yCVacsum = 0
        plot_yDVacsum = 0
        for event_items in range(np.shape(Event_matrix)[0]):
            if plot_xeventdates[event_dates] == Event_matrix[event_items][0]:
                if Event_matrix[event_items][2] == "A":
                    if Event_matrix[event_items][3] == "Vacation" or Event_matrix[event_items][3] == "Off" or \
                            Event_matrix[event_items][3] == "Train/SpecialAssign":
                        plot_yAVacsum = 1 + plot_yAVacsum
                if Event_matrix[event_items][2] == "B":
                    if Event_matrix[event_items][3] == "Vacation" or Event_matrix[event_items][3] == "Off" or \
                            Event_matrix[event_items][3] == "Train/SpecialAssign":
                        plot_yBVacsum = 1 + plot_yBVacsum
                if Event_matrix[event_items][2] == "C":
                    if Event_matrix[event_items][3] == "Vacation" or Event_matrix[event_items][3] == "Off" or \
                            Event_matrix[event_items][3] == "Train/SpecialAssign":
                        plot_yCVacsum = 1 + plot_yCVacsum
                if Event_matrix[event_items][2] == "D":
                    if Event_matrix[event_items][3] == "Vacation" or Event_matrix[event_items][3] == "Off" or \
                            Event_matrix[event_items][3] == "Train/SpecialAssign":
                        plot_yDVacsum = 1 + plot_yDVacsum
        plot_yAVac += [plot_yAVacsum]
        plot_yBVac += [plot_yBVacsum]
        plot_yCVac += [plot_yCVacsum]
        plot_yDVac += [plot_yDVacsum]
    plot_x1 = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])
    figure3 = plt.Figure(figsize=(8.2, 4), dpi=90, facecolor='black')
    ax2 = figure3.add_subplot(111)
    Vacplot_width = 0.15
    figure3.subplots_adjust(bottom=0.2)
    rects2 = ax2.bar(plot_x1 - .8, plot_yAVac, color='blue', width=Vacplot_width, label='A', align='center')
    rects3 = ax2.bar(plot_x1 - .6, plot_yBVac, color='orange', width=Vacplot_width, label="B", align='center')
    rects4 = ax2.bar(plot_x1 - .4, plot_yCVac, color='purple', width=Vacplot_width, label="C", align='center')
    rects5 = ax2.bar(plot_x1 - .2, plot_yDVac, color='green', width=Vacplot_width, label="D", align='center')
    ax2.xaxis.grid(True, linestyle='dotted')
    ax2.margins(x=0.01)
    ax2.tick_params(axis='x', width=0)
    ax2.set_xticks(np.arange(0, 14, 1))
    ax2.set_title("Next 2 Weeks Events",
                  loc='center', color="white", pad=25, fontsize=13)
    ax2.set_xticklabels(plot_shortdates, rotation=45, ha='right')
    ax2.tick_params(labelsize=12, colors='white', pad=0, length=5)
    ax2.patch.set_color('black')
    ax2.set_ylim(0, 5)
    ax2.legend(['A', 'B', 'C', 'D'], facecolor='black', labelcolor='white', fontsize="small",
               frameon=False, ncol=4, bbox_to_anchor=(.72, 1.12))
    ax2.spines['bottom'].set_color('white')
    ax2.spines['left'].set_color('white')
    ax2.spines['top'].set_color('white')
    ax2.spines['top'].set_linestyle('dotted')
    ax2.spines['right'].set_color('white')
    ax2.spines['right'].set_linestyle('dotted')
    chart2 = FigureCanvasTkAgg(figure3, wrap_main3)
    chart2.get_tk_widget().place(x=144, y=40)

    plt.show()

wrap_main4 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main4.place(x=right_frames, y=frame_start)
wrap_main4.configure(bg=the_color)
label4 = Label(wrap_main4)
label4.pack(padx=0)
label4.configure(bg="black", bd=border, width=77, height=26, relief=RIDGE)
label41 = Label(label4, text="Create Shift Schedule", bg=the_color, fg='white', font="Arial 18 bold", width=17)
label41.place(x=15, y=10)
label44 = Label(label4, text="Enter Duration (weeks):", bg="white", font="Arial 14", width=19, relief="solid", borderwidth=1)
label44.place(x=20, y=50)
labelcr = Label(label4, text="Copyright 2022 [Keith Cascarelli]", bg=the_color, fg='white', font="Arial 12 bold", width=35)
labelcr.place(x=450, y=10)

var = IntVar()
var.set("")
Entry44 = Entry(wrap_main4, textvariable=var)
Entry44.configure(font="Arial 13", width=5, borderwidth=1, justify="center")
Entry44.place(x=190, y=48)
Button_44 = tk.Button(wrap_main4, text="Press to Select", font="Arial 14 bold", width=13, cursor="circle", command=Select_weeks)
Button_44.configure(fg='blue')
Button_44.place(x=250, y=50)

Cal_Schedule = Calendar(label4, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                        showweeknumbers=False)
Cal_Schedule.configure(font="Arial 16", foreground="black", background="yellow", cursor="circle")
Cal_Schedule.place(x=400, y=50)
Cal_Schedule.bind("<<CalendarSelected>>", Sched_dateentry)

if Pass_matrix[0, 3] != 0:
    Start_Date = datetime.strptime(ScheduleTable_matrix[0][0], "%Y-%m-%d")
    Start_Date = datetime.strftime(Start_Date, "%b %d, %Y")

    End_Date = datetime.strptime(ScheduleTable_matrix[ScheduleTable_matrixshape[0]-1][0], "%Y-%m-%d")
    End_Date = datetime.strftime(End_Date, "%b %d, %Y")


wrap_main6 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main6.place(x=right_frames, y=frame_start+frame_position*2-93)
wrap_main6.configure(bg=the_color)
label6 = Label(wrap_main6, width=label_width, height=label_height+4, bg=the_color, fg='white')
label6.configure(bg="black", bd=border, width=77, height=21, relief=RIDGE)
label6.pack()
label61 = Label(label6, text="Last Schedule Run Information: ", bg=the_color, font="Arial 18 bold", fg='white', width=27)
label61.place(x=8, y=14)
label62 = Label(label6, text=Start_Date + ' - ' + End_Date, bg=the_color, font="Arial 18 bold", fg='white', width=22)
label62.place(x=295, y=14)

if Pass_matrix[0, 3] != 0:
    plot_y = []
    plot_yA = 0
    plot_yB = 0
    plot_yC = 0
    plot_yD = 0
    plot_x = ['A', 'B', 'C', 'D']

    Start_Date = datetime.strptime(ScheduleTable_matrix[0][0], "%Y-%m-%d")
    Start_Date = datetime.strftime(Start_Date, "%b %d, %Y")

    End_Date = datetime.strptime(ScheduleTable_matrix[ScheduleTable_matrixshape[0]-1][0], "%Y-%m-%d")
    End_Date = datetime.strftime(End_Date, "%b %d, %Y")

    Comp_Aavg = []
    Comp_Bavg = []
    Comp_Cavg = []
    Comp_Davg = []
    A_sum = 0
    B_sum = 0
    C_sum = 0
    D_sum = 0
    AOT_sum = 0
    BOT_sum = 0
    COT_sum = 0
    DOT_sum = 0
    for sched_itemsrows in range(np.shape(ScheduleTable_matrix)[0]):
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A" and ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yA += 1
            else:
                plot_yA += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yA += 1
            else:
                plot_yA += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
            A_sum = A_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                        plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                        plot_yD -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                     Post_CountNights*2+8].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find("/", 0, 20)+1:20] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                        plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                        plot_yD -= 1
        if plot_yA < 0:
            plot_yA = 0
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yB += 1
            else:
                plot_yB += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yB += 1
            else:
                plot_yB += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
            B_sum = B_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + 2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                        plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                        plot_yD -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                     Post_CountNights*2+8].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find("/", 0, 20)+1:20] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                        plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                        plot_yD -= 1
        if plot_yB < 0:
            plot_yB = 0
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C" and ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yC += 1
            else:
                plot_yC += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yC += 1
            else:
                plot_yC += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
            C_sum = C_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                        plot_yD -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                     Post_CountNights*2+8].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find("/", 0, 20)+1:20] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                        plot_yD -= 1
        if plot_yC < 0:
            plot_yC = 0
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D" and ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yD += 1
            else:
                plot_yD += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yD += 1
            else:
                plot_yD += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
            D_sum = D_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                        [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "-Slide" \
                        == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                        plot_yC -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                        plot_yC -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find(":", 0, 40) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find(":", 0, 40)+1:40] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                        [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                               Post_CountNights*2+8].find("/", 0, 20)+1:20] + "-Slide" \
                        == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                        plot_yC -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                        plot_yC -= 1
        if plot_yD < 0:
            plot_yD = 0
        for sched_itemscols in range(0, Post_CountDays):
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team A" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                AOT_sum = AOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team B" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                BOT_sum = BOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team C" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                COT_sum = COT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team D" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                DOT_sum = DOT_sum + 1
        for sched_itemscols in range(0, Post_CountNights):
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                AOT_sum = AOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                BOT_sum = BOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                COT_sum = COT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                DOT_sum = DOT_sum + 1

    #shift_utilA = ((Post_CountDays+Post_CountNights)/2*A_sum)/((Post_CountDays+Post_CountNights)/2*A_sum+plot_yA)
    #shift_utilB = ((Post_CountDays+Post_CountNights)/2*B_sum)/((Post_CountDays+Post_CountNights)/2*B_sum+plot_yB)
    #shift_utilC = ((Post_CountDays+Post_CountNights)/2*C_sum)/((Post_CountDays+Post_CountNights)/2*C_sum+plot_yC)
    #shift_utilD = ((Post_CountDays+Post_CountNights)/2*D_sum)/((Post_CountDays+Post_CountNights)/2*D_sum+plot_yD)
    if plot_yA < 0:
        plot_yA = 0
    if plot_yB < 0:
        plot_yB = 0
    if plot_yC < 0:
        plot_yC = 0
    if plot_yD < 0:
        plot_yD = 0
    shift_utilA = (A_peoplecount*A_sum-plot_yA)/(A_peoplecount*A_sum)
    shift_utilB = (B_peoplecount*B_sum-plot_yB)/(B_peoplecount*B_sum)
    shift_utilC = (C_peoplecount*C_sum-plot_yC)/(C_peoplecount*C_sum)
    shift_utilD = (D_peoplecount*D_sum-plot_yD)/(D_peoplecount*D_sum)
    #print(B_sum, plot_yB, shift_utilB)
    plot_y = [shift_utilA, shift_utilB, shift_utilC, shift_utilD]
    AOT_avg = (AOT_sum/((Post_CountDays+Post_CountNights)/2*A_sum))
    BOT_avg = (BOT_sum/((Post_CountDays+Post_CountNights)/2*B_sum))
    COT_avg = (COT_sum/((Post_CountDays+Post_CountNights)/2*C_sum))
    DOT_avg = (DOT_sum/((Post_CountDays+Post_CountNights)/2*D_sum))
    plot_y1 = [AOT_avg, BOT_avg, COT_avg, DOT_avg]

    AOT_avg1 = (AOT_sum/(A_peoplecount*A_sum))
    BOT_avg1 = (BOT_sum/(B_peoplecount*B_sum))
    COT_avg1 = (COT_sum/(C_peoplecount*C_sum))
    DOT_avg1 = (DOT_sum/(D_peoplecount*D_sum))
    plot_y3 = [AOT_avg1, BOT_avg1, COT_avg1, DOT_avg1]
    plot_y2 = []
    colorUt = []
    for y_value in range(np.shape(plot_y)[0]):
        plot_y2.append(plot_y[y_value]-plot_y3[y_value]*1.5)
    for y2_value in plot_y2:
        if y2_value < 6/7:  # 6/7 represents typical shift/vacation relief optimum ratio, this assumes 4-5 weeks vacation allotment per person
            colorUt.append('red')
        if y2_value == 6/7:
            colorUt.append('gold')
        if y2_value > 6/7:
            colorUt.append('green')

    figure = plt.Figure(figsize=(5, 4), dpi=90, facecolor='black')
    ax = figure.add_subplot(111)
    figure.subplots_adjust(bottom=0.2)
    rects1 = ax.bar(plot_x, plot_y2, align='center', color=colorUt, width=.4)
    ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
    ax.set_title("Shift Efficiency %", loc='center', color="white", pad=25, fontsize=13)
    ax.tick_params(labelsize=12, colors='white', pad=1)
    ax.patch.set_color('black')
    ax.set_ylim(.5, 1)
    plt.rcParams['font.size'] = 12
    for rect in rects1:
        height = rect.get_height()
        ax.annotate('{:0.0%}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom',
                    color='white')

    ax.spines['bottom'].set_color('white')
    ax.spines['left'].set_color('white')
    chart = FigureCanvasTkAgg(figure, label6)
    chart.get_tk_widget().place(x=27, y=50)
    plt.show()

    figure2 = plt.Figure(figsize=(5, 4), dpi=90, facecolor='black')
    ax2 = figure2.add_subplot(111)
    figure2.subplots_adjust(bottom=0.2)
    rects2 = ax2.bar(plot_x, plot_y3, align='center', color='red', width=.4)
    ax2.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
    ax2.set_title("Overtime %", color="white", pad=25, fontsize=13)
    ax2.tick_params(labelsize=12, colors='white', pad=1)
    ax2.patch.set_color('black')
    ax2.set_ylim(0, .3)
    for rect in rects2:
        height = rect.get_height()
        ax2.annotate('{:0.0%}'.format(height),
                     xy=(rect.get_x() + rect.get_width() / 2, height),
                     xytext=(0, 3),  # 3 points vertical offset
                     textcoords="offset points",
                     ha='center', va='bottom',
                     color='white')

    ax2.spines['bottom'].set_color('white')
    ax2.spines['left'].set_color('white')
    chart2 = FigureCanvasTkAgg(figure2, label6)
    chart2.get_tk_widget().place(x=366, y=50)
    plt.show()


UpdateInfo_button = Button(wrap_main6, text="Update Charts", width=13, font="Arial 14 bold", cursor="circle", command=UpdateInfo_chart)
UpdateInfo_button.place(x=20, y=310)
UpdateInfo_button.configure(bg='white', fg='blue')

CurrSchd_button = Button(wrap_main6, text="Show Last Schedule", width=17, font="Arial 14 bold", cursor="circle", command=Open_pdf)
CurrSchd_button.place(x=155, y=310)
CurrSchd_button.configure(bg='white', fg='blue')

EB_button = Button(wrap_main6, text="Extra Board Analysis", width=18, font="Arial 14 bold", cursor="circle", command=OpenEB_pdf)
EB_button.place(x=326, y=310)
EB_button.configure(bg='white', fg='blue')

wrap_main7 = LabelFrame(rootmain)
wrap_main7.place(x=0, y=783)
wrap_main7.configure(bg='whitesmoke', height=10, width=1441)

exitmain_button = Button(wrap_main6, text="Exit", width=7, font="Arial 14 bold", cursor="circle", command=exit_main)
exitmain_button.place(x=620, y=310)
exitmain_button.configure(bg='white', fg='blue')

rootmain.title("")
#rootmain.attributes('-fullscreen', True)
rootmain.geometry("1599x790+0+20")
rootmain.mainloop()
