from typing import List, Any
import numpy as np
from datetime import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import csv
import datetime
import pandas as pd
import importlib
import runpy

screen = Toplevel()
screen.geometry("400x400+500+0")
screen.title("")
screen.configure(bg="black", bd=10, width=400, height=400, relief=RIDGE)
#style = ttk.Style()
#style.theme_use('clam')
global Temp_Shift


def exit_setsched():
    screen.destroy()


def Dupont_Select():
    screen.destroy()
    runpy.run_path(path_name="SetDuPont.py")


def FourFour_Select():
    screen.destroy()
    runpy.run_path(path_name="SetupFour.py")


def EOWEO_Select():
    screen.destroy()
    runpy.run_path(path_name="SetupEOWEO.py")


def Seven_Select():
    screen.destroy()
    runpy.run_path(path_name="SetupSeven.py")


def FourFive_Select():
    screen.destroy()
    runpy.run_path(path_name="SetupFourFive.py")


schedmain_label = Label(screen, text="Set the Work Schedules", width=20, font="Arial 20 bold")
schedmain_label.pack(pady=10)

Dupont_selectbutton = Button(screen, text="DuPont Schedule", width=17, font="Arial 14 bold", command=Dupont_Select)
Dupont_selectbutton.configure(fg='blue', cursor="circle")
Dupont_selectbutton.pack(pady=10)

Four_selectbutton = Button(screen, text="4on/4off Schedule", width=17, font="Arial 14 bold", command=FourFour_Select)
Four_selectbutton.configure(fg='blue', cursor="circle")
Four_selectbutton.pack(pady=10)

EOWEO_selectbutton = Button(screen, text="EOWEO Schedule", width=17, font="Arial 14 bold", command=EOWEO_Select)
EOWEO_selectbutton.configure(fg='blue', cursor="circle")
EOWEO_selectbutton.pack(pady=10)

FourFive_selectbutton = Button(screen, text="Four/Five Schedule", width=17, font="Arial 14 bold", command=FourFive_Select)
FourFive_selectbutton.configure(fg='blue', cursor="circle")
FourFive_selectbutton.pack(pady=10)

Seven_selectbutton = Button(screen, text="7on/7off Schedule", width=17, font="Arial 14 bold", command=Seven_Select)
Seven_selectbutton.configure(fg='blue', cursor="circle")
Seven_selectbutton.pack(pady=10)

exitschedset_button = Button(screen, text="Exit", width=7, font="Arial 16 bold", command=exit_setsched)
exitschedset_button.configure(fg='blue', cursor="circle")
exitschedset_button.place(x=165, y=350)

screen.mainloop()
