import pandas as pd
from pandas import *
import numpy as np
from numpy import *
import csv
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import pymysql
import random
import time
import runpy

fileYOS_Data = pd.read_csv("Vacation.txt", delimiter=',')
fileYOS_sdf = pd.DataFrame(fileYOS_Data)
fileYOS_matrix = np.array(fileYOS_sdf.sort_values(by=['Team', 'Salary/Wage'], ascending=[True, True]))
fileYOS_shape = np.shape(fileYOS_matrix)
pd.set_option("display.max_rows", None, "display.max_columns", None)

Base_Data = pd.read_csv("Base Data.txt", delimiter=',')
Base_sdf = pd.DataFrame(Base_Data)
Base_matrix = np.array(Base_sdf.sort_values(by=['Team', 'Salary'], ascending=[True, True]))
Base_shape = np.shape(Base_matrix)

newYOS_matrix = []
for Base_items in range(np.shape(Base_matrix)[0]):
    newYOS_matrix2 = []
    if Base_matrix[Base_items, 1] in fileYOS_matrix:
        for YOS_items in range(np.shape(fileYOS_matrix)[0]):
            if Base_matrix[Base_items, 1] == fileYOS_matrix[YOS_items, 1]:
                if np.shape(newYOS_matrix)[0] == 0:
                    newYOS_matrix2 = [Base_matrix[Base_items, 0], Base_matrix[Base_items, 1], Base_matrix[Base_items, 2], \
                                      fileYOS_matrix[YOS_items, 3]]
                else:
                    newYOS_matrix2 += [Base_matrix[Base_items, 0], Base_matrix[Base_items, 1], Base_matrix[Base_items, 2], \
                                       fileYOS_matrix[YOS_items, 3]]
    else:
        if np.shape(newYOS_matrix)[0] == 0:
            newYOS_matrix2 = [Base_matrix[Base_items, 0], Base_matrix[Base_items, 1], Base_matrix[Base_items, 2], 0]
        else:
            newYOS_matrix2 += [Base_matrix[Base_items, 0], Base_matrix[Base_items, 1], Base_matrix[Base_items, 2], 0]

    newYOS_matrix += [newYOS_matrix2]

fileYOS_matrix = np.array(newYOS_matrix)

Ateam = 0
Bteam = 0
Cteam = 0
Dteam = 0

for teams in range(np.shape(fileYOS_matrix)[0]):
    if fileYOS_matrix[teams, 0] == "A":
        Ateam += 1
    elif fileYOS_matrix[teams, 0] == "B":
        Bteam += 1
    elif fileYOS_matrix[teams, 0] == "C":
        Cteam += 1
    elif fileYOS_matrix[teams, 0] == "D":
        Dteam += 1



rootbase = Toplevel()
rootbase.title = ('')
rootbase.maxsize(1500, 1300)
rootbase.geometry("+400+0")
rootbase.configure(bg="black")
adddata_count = 0
Team = StringVar()
Name = StringVar()
Salary = StringVar()
YOS = IntVar()
Team.set("")
Name.set("")
Salary.set("")
YOS.set("")
Post_Headerinfo = []
post_nameslist = {}

MainFrame = Frame(rootbase, bd=10, width=1400, height=1000, relief=RIDGE, bg="black")
MainFrame.grid()

style = ttk.Style()
style.theme_use('clam')

TopFrame1 = Frame(MainFrame, bd=5, width=1400, height=200, relief=RIDGE, bg="black")
TopFrame1.grid(row=0, column=0, sticky=W)
TopFrame2 = Frame(MainFrame, bd=5, width=1400, height=50, relief=RIDGE, bg="black")
TopFrame2.grid(row=1, column=0, sticky=W)
TopFrame3 = Frame(MainFrame, bd=5, width=1400, height=800, relief=RIDGE, bg="black")
TopFrame3.grid(row=2, column=0, sticky=W)

InnerTopFrame1 = Frame(TopFrame1, bd=5, width=1300, height=190, bg='black', relief=RIDGE)
InnerTopFrame1.grid(row=0, column=0, sticky=W)
InnerTopFrame2 = Frame(TopFrame2, bd=5, width=1300, height=48, bg="black", relief=RIDGE)
InnerTopFrame2.grid(row=1, column=0, sticky=W)
InnerTopFrame3 = Frame(TopFrame3, bd=5, width=1300, height=1000, relief=RIDGE)
InnerTopFrame3.grid(row=2, column=0, sticky=W)


def Reset():
    Team.set("")
    Name.set("")
    Salary.set("")
    YOS.set("")


def iExit():
    rootbase.destroy()


def DisplayData():
    count = 0
    for record in fileYOS_matrix:
        record_stuff1 = str(record).replace("'", '')
        record_stuff = str(record_stuff1).strip("[]")
        if record[0] == "A":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Ateam',))
        elif record[0] == "B":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Bteam',))
        elif record[0] == "C":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Cteam',))
        elif record[0] == "D":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Dteam',))
        count += 1


def LearnersInfo(ev):
    global entry_list
    viewInfo = tree_records.focus()
    learnerData = tree_records.item(viewInfo)
    row = learnerData['values']
    Team.set(row[0])
    Name.set(row[1])
    Salary.set(row[2])
    YOS.set(row[3])


def update():
    global entry_list
    selected = tree_records.focus()
    position = int(tree_records.focus())
    valueenter = 'Team.get(), Name.get(), Salary.get(), YOS.get()'
    tree_records.item(selected, text="", values=eval(valueenter))


def saveDB():
    global fileYOS_matrix
    erase_Vac = open("Vacation.txt", "w+")
    erase_Vac.close()
    PostHeader1 = ["Team", "Name", "Salary/Wage", "YearsofService"]
    with open("Vacation.txt", 'a') as appFile:
        writer = csv.writer(appFile)
        writer.writerow(PostHeader1)
        appFile.close()
    for base_items in tree_records.get_children():
        with open("Vacation.txt", 'a', newline="") as appFile:
            writer = csv.writer(appFile)
            writer.writerow(tree_records.item(base_items)['values'])
            appFile.close()
    messagebox.showinfo("Data Entry Form", "Record Successfully Saved")


    #============================================================================================================
paddingxtop = 2

lblTeam = Label(InnerTopFrame1, font="Arial 14 bold", text="Team", bg='black', fg='white', bd=10, width=31)
lblTeam.grid(row=0, column=0, sticky=W)
comboTeam = ttk.Combobox(InnerTopFrame1, font="Arial 14 bold", width=8, justify="center", textvariable=Team)
comboTeam['value'] = ('', 'A', 'B', 'C', 'D')
comboTeam.current(0)
comboTeam.grid(row=0, column=1, padx=paddingxtop)

lblExistingName = Label(InnerTopFrame1, font="Arial 14 bold", text="Name", bg='black', fg='white', bd=10, width=31)
lblExistingName.grid(row=1, column=0, sticky=W)
txtExistingName = Entry(InnerTopFrame1, font="Arial 14 bold", justify="center", bd=3, width=32, textvariable=Name)
txtExistingName.grid(row=1, column=1, padx=paddingxtop)

lblSalary = Label(InnerTopFrame1, font="Arial 14 bold", text="Salary/Wage", bg='black', fg='white', bd=10, width=31)
lblSalary.grid(row=2, column=0, sticky=W)
comboSalary = ttk.Combobox(InnerTopFrame1, font="Arial 14 bold", width=8, justify="center", textvariable=Salary)
comboSalary['value'] = ('', 'Salary', 'Wage')
comboSalary.current(0)
comboSalary.grid(row=2, column=1, padx=paddingxtop)

lblYOS = Label(InnerTopFrame1, font="Arial 14 bold", text="Years of Service", bg='black', fg='white', bd=10, width=31)
lblYOS.grid(row=3, column=0, sticky=W)
txtYOS = Entry(InnerTopFrame1, font="Arial 14 bold", bd=3, justify="center", width=8, textvariable=YOS)
txtYOS.grid(row=3, column=1, padx=paddingxtop)

#=============================================================================================
scroll_x = Scrollbar(InnerTopFrame3, orient=HORIZONTAL)
scroll_y = Scrollbar(InnerTopFrame3, orient=VERTICAL)

scroll_x.pack(side=BOTTOM, fill=X)
scroll_y.pack(side=RIGHT, fill=Y)

post_headersData = []
for post_headers in fileYOS_sdf:
    post_headersData.append(post_headers)
tree_records = ttk.Treeview(InnerTopFrame3, height=24, columns=post_headersData,
                            xscrollcommand=scroll_x.set, yscrollcommand=scroll_y.set)

tree_records.tag_configure('Ateam', background='#9DD0F3', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Bteam', background='#F7CDAB', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Cteam', background='#DCBBF6', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Dteam', background='#E5F7AB', font=('Arial', 14, 'bold'))

scroll_x.config(command=tree_records.xview)
scroll_y.config(command=tree_records.yview)

for tree_items in range(np.shape(post_headersData)[0]):
    tree_records.heading(tree_items, text=post_headersData[tree_items])
    tree_records['show'] = 'headings'
    if tree_items == 0:
        col_width = 75
    else:
        col_width = 150
    tree_records.column(tree_items, width=col_width, anchor=CENTER)
tree_records.pack(fill="both", expand="yes")
tree_records.bind("<ButtonRelease-1>", LearnersInfo)
#=============================================================================================
button_width = 14
paddingx = 10

btnUpdate = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Update List", cursor="circle", command=update)
btnUpdate.configure(fg='blue')
btnUpdate.grid(row=0, column=0, padx=paddingx)

btnReset = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                  text="Reset", cursor="circle", command=Reset)
btnReset.configure(fg='blue')
btnReset.grid(row=0, column=3, padx=paddingx)

btnSave = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Save", cursor="circle", command=saveDB)
btnSave.configure(fg='blue')
btnSave.grid(row=0, column=4, padx=paddingx)

btnExit = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                 text="Exit", cursor="circle", command=iExit)
btnExit.configure(fg='blue')
btnExit.grid(row=0, column=5, padx=paddingx)

DisplayData()

rootbase.mainloop()
