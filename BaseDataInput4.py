import pandas as pd
from pandas import *
import numpy as np
from numpy import *
import csv
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import pymysql
import random
import time
import runpy

SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)
InfoTable_matrixshape = np.shape(InfoTable_matrix)

fileBase_Data = pd.read_csv("Base Data.txt", delimiter=',')
fileBase_sdf = pd.DataFrame(fileBase_Data)
fileBase_sdf.rename(columns={'Seniority':InfoTable_matrix[0, 3]}, inplace=True)
if InfoTable_matrix[0, 3] == "Seniority":
    fileBase_matrix = np.array(fileBase_sdf.sort_values(by=['Team', 'Salary', str(fileBase_sdf.columns[3])],
                                                        ascending=[True, True, False]))
else:
    fileBase_matrix = np.array(fileBase_sdf.sort_values(by=['Team', 'Salary', str(fileBase_sdf.columns[3])],
                                                        ascending=[True, True, True]))
fileBase_shape = np.shape(fileBase_matrix)
pd.set_option("display.max_rows", None, "display.max_columns", None)

Ateam = 0
Bteam = 0
Cteam = 0
Dteam = 0

for teams in range(np.shape(fileBase_matrix)[0]):
    if fileBase_matrix[teams, 0] == "A":
        Ateam += 1
    elif fileBase_matrix[teams, 0] == "B":
        Bteam += 1
    elif fileBase_matrix[teams, 0] == "C":
        Cteam += 1
    elif fileBase_matrix[teams, 0] == "D":
        Dteam += 1

InitInput_Data = pd.read_csv("Initial Input.txt", delimiter=',')
InitInput_sdf = pd.DataFrame(InitInput_Data)
InitInput_matrix = np.array(InitInput_sdf)
InitInput_shape = np.shape(InitInput_matrix)

rootbase = Toplevel()
rootbase.title = ('')
rootbase.maxsize(1500, 1300)
rootbase.geometry("+0+0")
rootbase.configure(bg="black")
adddata_count = 0
Team = StringVar()
Name = StringVar()
Salary = StringVar()
OTHours = IntVar()
Team.set("")
Name.set("")
Salary.set("")
OTHours.set("")
Post_Headerinfo = []
post_nameslist = {}
for post_items in range(InitInput_shape[0]):
    post_nameslist["Post" + str(post_items+1)] = InitInput_matrix[post_items,0]
    Post_Headerinfo += [InitInput_matrix[post_items, 0]]
MainFrame = Frame(rootbase, bd=10, width=1400, height=1000, relief=RIDGE, bg="black")
MainFrame.grid()

style = ttk.Style()
style.theme_use('clam')

TopFrame1 = Frame(MainFrame, bd=5, width=1400, height=200, relief=RIDGE, bg="black")
TopFrame1.grid(row=0, column=0, sticky=W)
TopFrame2 = Frame(MainFrame, bd=5, width=1400, height=50, relief=RIDGE, bg="black")
TopFrame2.grid(row=1, column=0, sticky=W)
TopFrame3 = Frame(MainFrame, bd=5, width=1400, height=800, relief=RIDGE, bg="black")
TopFrame3.grid(row=2, column=0, sticky=W)

InnerTopFrame1 = Frame(TopFrame1, bd=5, width=1300, height=190, bg='black', relief=RIDGE)
InnerTopFrame1.grid(row=0, column=0, sticky=W)
InnerTopFrame2 = Frame(TopFrame2, bd=5, width=1300, height=48, bg="black", relief=RIDGE)
InnerTopFrame2.grid(row=1, column=0, sticky=W)
InnerTopFrame3 = Frame(TopFrame3, bd=5, width=1300, height=1000, relief=RIDGE)
InnerTopFrame3.grid(row=2, column=0, sticky=W)


def Reset():
    Team.set("")
    Name.set("")
    Salary.set("")
    OTHours.set("")
    for rowx in range(4, InitInput_shape[0]+4):
        my_entries[rowx-4].delete(0, END)
        my_entries[rowx-4].insert(0, "")


def iExit():
    rootbase.destroy()


def addData():
    if Salary.get() == "":
        SalaryInput = "Wage"
    else:
        SalaryInput1 = Salary.get()[0:1].upper()
        SalaryInput2 = Salary.get()[1:10].lower()
        SalaryInput = SalaryInput1 + SalaryInput2
    if OTHours.get() == "":
        OTHoursInput = 0
    else:
        OTHoursInput = OTHours.get()
    if Name.get() == "":
        messagebox.showerror("Data Entry Form", "Enter Name")
    elif Team.get() == "":
        messagebox.showerror("Data Entry Form", "Enter Team")
    else:
        valueenter = 'Team.get(), Name.get(), SalaryInput, OTHoursInput'
        entry_list = []
        for entries in my_entries:
            if entries.get() == "":
                inputentry = 0
            else:
                inputentry = entries.get()
            entry_list += [str(inputentry)]
        for rowx in range(0, InitInput_shape[0]):
            valueenter = valueenter + ', entry_list[' + str(rowx) + ']'
        if eval(valueenter[0:10]) == "A":
            tree_records.insert('', index=1, values=(eval(valueenter)), tags=('Ateam',))
        elif eval(valueenter[0:10]) == "B":
            tree_records.insert('', index=Ateam+1, values=(eval(valueenter)), tags=('Bteam',))
        elif eval(valueenter[0:10]) == "C":
            tree_records.insert('', index=Ateam+Bteam+1, values=(eval(valueenter)), tags=('Cteam',))
        elif eval(valueenter[0:10]) == "D":
            tree_records.insert('', index=Bteam+Cteam+1, values=(eval(valueenter)), tags=('Dteam',))

        messagebox.showinfo("Data Entry Form", "Record Entered Successfully" + '\n' +
                            + "Press Save to Confirm" + '\n' + "Please Add New Person in Competency Information")


def DisplayData():
    count = 0
    for record in fileBase_matrix:
        record_stuff1 = str(record).replace("'", '')
        record_stuff = str(record_stuff1).strip("[]")
        if record[0] == "A":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                    values=record_stuff, tags=('Ateam',))
        elif record[0] == "B":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Bteam',))
        elif record[0] == "C":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Cteam',))
        elif record[0] == "D":
            tree_records.insert('', index='end', iid=count, text="Nothing",
                                values=record_stuff, tags=('Dteam',))
        count += 1


def LearnersInfo(ev):
    global entry_list
    viewInfo = tree_records.focus()
    learnerData = tree_records.item(viewInfo)
    row = learnerData['values']
    Team.set(row[0])
    Name.set(row[1])
    Salary.set(row[2])
    OTHours.set(row[3])
    for rowx in range(4, InitInput_shape[0]+4):
        my_entries[rowx-4].delete(0, END)
        my_entries[rowx-4].insert(0, str(row[rowx]))


def update():
    global entry_list
    selected = tree_records.focus()
    position = int(tree_records.focus())
    if Salary.get() == "":
        SalaryInput = "Wage"
    else:
        SalaryInput1 = Salary.get()[0:1].upper()
        SalaryInput2 = Salary.get()[1:10].lower()
        SalaryInput = SalaryInput1 + SalaryInput2
    valueenter = 'Team.get(), Name.get(), SalaryInput, OTHours.get()'
    entry_list = []
    for entries in my_entries:
        if entries.get() == "":
            inputentry = 0
        else:
            inputentry = entries.get()
        entry_list += [str(inputentry)]
    for rowx in range(0, InitInput_shape[0]):
        valueenter = valueenter + ', entry_list[' + str(rowx) + ']'
    tree_records.item(selected, text="", values=eval(valueenter))


def deleteDB():
    selected = tree_records.focus()
    position = int(tree_records.focus())
    tree_records.delete(selected)
    messagebox.showinfo("Data Entry Form", "Record Successfully Deleted")


def saveDB():
    global fileBase_matrix1
    erase_base = open("Base Data.txt", "w+")
    erase_base.close()
    PostHeader1 = ["Team", "Name", "Salary", lblOTHourstext]
    PostHeader2 = np.hstack((PostHeader1, Post_Headerinfo))
    with open("Base Data.txt", 'a') as appFile:
        writer = csv.writer(appFile)
        writer.writerow(PostHeader2)
        appFile.close()
    for base_items in tree_records.get_children():
        with open("Base Data.txt", 'a', newline="") as appFile:
            writer = csv.writer(appFile)
            writer.writerow(tree_records.item(base_items)['values'])
            appFile.close()
    messagebox.showinfo("Data Entry Form", "Record Successfully Saved")


    #============================================================================================================

lblTeam = Label(InnerTopFrame1, font="Arial 14 bold", text="Team", bg='black', fg='white', bd=10)
lblTeam.grid(row=0, column=0, sticky=W)
comboTeam = ttk.Combobox(InnerTopFrame1, font="Arial 14 bold", width=8, justify="center", textvariable=Team)
comboTeam['value'] = ('', 'A', 'B', 'C', 'D')
comboTeam.current(0)
comboTeam.grid(row=0, column=1)

lblExistingName = Label(InnerTopFrame1, font="Arial 14 bold", text="Name", bg='black', fg='white', bd=10)
lblExistingName.grid(row=1, column=0, sticky=W)
txtExistingName = Entry(InnerTopFrame1, font="Arial 14 bold", justify="center", bd=3, width=32, textvariable=Name)
txtExistingName.grid(row=1, column=1)

lblSalary = Label(InnerTopFrame1, font="Arial 14 bold", text="Salary/Wage", bg='black', fg='white', bd=10)
lblSalary.grid(row=2, column=0, sticky=W)
txtSalary = Entry(InnerTopFrame1, font="Arial 14 bold", bd=3, justify="center", width=8, textvariable=Salary)
txtSalary.grid(row=2, column=1)

if InfoTable_matrix[0,3] == "OT Hours":
    lblOTHourstext = "OT Hours"
else:
    lblOTHourstext = InfoTable_matrix[0,3]

lblOTHours = Label(InnerTopFrame1, font="Arial 14 bold", text=lblOTHourstext, bg='black', fg='white', bd=10)
lblOTHours.grid(row=3, column=0, sticky=W)
txtOTHours = Entry(InnerTopFrame1, font="Arial 14 bold", bd=3, justify="center", width=8, textvariable=OTHours)
txtOTHours.grid(row=3, column=1)

lblPostComp = Label(InnerTopFrame1, font="Arial 16 bold", text="Post Order Information", bd=10, width=20,
                    bg='black', fg='white', justify='left')
lblPostComp.place(x=390, y=0)

#Create Top Post Section....
post_itemsx1 = 0
post_itemsadj = 0
my_entries = []
initialadj = 0
columnrange = int(InitInput_shape[0] / 3)
remainder = InitInput_shape[0] % 3
countrows = 0
if InitInput_shape[0] < 3:
    endrows = InitInput_shape[0]
else:
    endrows = 3
addedadj = 0
for ycolumns in range(0, columnrange):
    for post_itemsx in range(0+initialadj, endrows+initialadj):
        my_labelx = f"lblPost{post_itemsx + addedadj}"
        my_labelx = Label(InnerTopFrame1, font="Arial 14 bold", bg='black', fg='white', text=InitInput_matrix[post_itemsx+addedadj, 0])
        my_labelx.grid(row=post_itemsx-initialadj+1, column=ycolumns+initialadj+2, stick=W)
        my_txtentry = Entry(InnerTopFrame1, font="Arial 14 bold", bd=3, justify="center", width=5)
        my_txtentry.grid(row=post_itemsx-initialadj+1, column=ycolumns+initialadj+3)
        my_entries.append(my_txtentry)
        countrows += 1
    if InitInput_shape[0] - countrows < 3 and remainder != 0:
        endrows = remainder
        initialadj += 1
        addedadj = 2
    else:
        initialadj += 3


#=============================================================================================
scroll_x = Scrollbar(InnerTopFrame3, orient=HORIZONTAL)
scroll_y = Scrollbar(InnerTopFrame3, orient=VERTICAL)

scroll_x.pack(side=BOTTOM, fill=X)
scroll_y.pack(side=RIGHT, fill=Y)

post_headersData = []
for post_headers in fileBase_sdf:
    post_headersData.append(post_headers)
tree_records = ttk.Treeview(InnerTopFrame3, height=24, columns=post_headersData,
                            xscrollcommand=scroll_x.set, yscrollcommand=scroll_y.set)

tree_records.tag_configure('Ateam', background='#9DD0F3', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Bteam', background='#F7CDAB', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Cteam', background='#DCBBF6', font=('Arial', 14, 'bold'))
tree_records.tag_configure('Dteam', background='#E5F7AB', font=('Arial', 14, 'bold'))

scroll_x.config(command=tree_records.xview)
scroll_y.config(command=tree_records.yview)

for tree_items in range(np.shape(post_headersData)[0]):
    tree_records.heading(tree_items, text=post_headersData[tree_items])
    tree_records['show'] = 'headings'
    if tree_items == 1:
        col_width = 150
    else:
        col_width = 75
    tree_records.column(tree_items, width=col_width, anchor=CENTER)
tree_records.pack(fill="both", expand="yes")
tree_records.bind("<ButtonRelease-1>", LearnersInfo)
#=============================================================================================
button_width = 12

btnUpdate = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Update List", cursor="circle", command=update)
btnUpdate.configure(fg='blue')
btnUpdate.grid(row=0, column=0, padx=3)

btnAddNew = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Add New", cursor="circle", command=addData)
btnAddNew.configure(fg='blue')
btnAddNew.grid(row=0, column=1, padx=3)

btnDelete = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Delete", cursor="circle", command=deleteDB)
btnDelete.configure(fg='blue')
btnDelete.grid(row=0, column=2, padx=3)

btnReset = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                  text="Reset", cursor="circle", command=Reset)
btnReset.configure(fg='blue')
btnReset.grid(row=0, column=3, padx=3)

btnSearch = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                   text="Save", cursor="circle", command=saveDB)
btnSearch.configure(fg='blue')
btnSearch.grid(row=0, column=4, padx=3)

btnExit = Button(InnerTopFrame2, pady=1, bd=4, font="Arial 14 bold", width=button_width,
                 text="Exit", cursor="circle", command=iExit)
btnExit.configure(fg='blue')
btnExit.grid(row=0, column=5, padx=3)

DisplayData()

rootbase.mainloop()
