import pandas as pd
import numpy as np
import csv
from scipy.optimize import linear_sum_assignment
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfgen import canvas
from reportlab.graphics import renderPDF
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import VerticalBarChart
from reportlab.graphics.charts.lineplots import LinePlot
import runpy
import subprocess


BaseCAS_Data = pd.read_csv("Base Data CAS.txt", delimiter=',')
BaseCAS_sdf = pd.DataFrame(BaseCAS_Data)
BaseCAS_matrix = np.array(BaseCAS_sdf)
plot_posts = list(BaseCAS_sdf.columns[4:np.shape(BaseCAS_matrix)[1]])
ExtraBoard_adder = 0
ExtraBoard_current = np.shape(BaseCAS_matrix)[0]/4 - np.shape(plot_posts)[0] + ExtraBoard_adder/4
if int(ExtraBoard_current) == ExtraBoard_current:
    ExtraBoard_new = ExtraBoard_current - ExtraBoard_current/2
else:
    ExtraBoard_new = round(ExtraBoard_current - ExtraBoard_current/2+.05, 0)

TeamName_table = pd.read_table('TeamName.txt')
sdf_TeamNameTable = pd.DataFrame(TeamName_table)
TeamName_matrix = np.array(sdf_TeamNameTable)

SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)

SetupEBTemplate_table = pd.read_csv('ExtraBoardTemplate.txt', delimiter=',')
sdf_EBTemplateTable = pd.DataFrame(SetupEBTemplate_table)
EBTemplate_matrix = np.array(sdf_EBTemplateTable)

SetupVacTemplate_table = pd.read_csv('VacationTemplate.txt', delimiter=',')
sdf_VacTemplateTable = pd.DataFrame(SetupVacTemplate_table)
VacTemplate_matrix = np.array(sdf_VacTemplateTable)

SetupVacation_table = pd.read_csv('Vacation.txt', delimiter=',')
sdf_VacationTable = pd.DataFrame(SetupVacation_table)
Vacation_matrix = np.array(sdf_VacationTable)

Initial_table = pd.read_csv('Initial Input.txt', delimiter=',')
sdf_InitialTable = pd.DataFrame(Initial_table)
Initial_matrix = np.array(sdf_InitialTable)
Post_CountDays = 0
Post_CountNights = 0
for items in range(np.shape(Initial_matrix)[0]):
    if Initial_matrix[items, 1] == "Y" or Initial_matrix[items, 2] == "Y":
        Post_CountDays += 1
    if Initial_matrix[items, 1] == "Y" and Initial_matrix[items, 2] != "Y":
        Post_CountNights += 1
#Determine How Many People in Each Team:
A_count = 0
B_count = 0
C_count = 0
D_count = 0
for x_rows in range(np.shape(Vacation_matrix)[0]):
    if Vacation_matrix[x_rows][0] == "A":
        A_count += 1
    if Vacation_matrix[x_rows][0] == "B":
        B_count += 1
    if Vacation_matrix[x_rows][0] == "C":
        C_count += 1
    if Vacation_matrix[x_rows][0] == "D":
        D_count += 1
#Let's Determine Team Time Off:
A_off = 0
B_off = 0
C_off = 0
D_off = 0
A_salaryboost = 0
B_salaryboost = 0
C_salaryboost = 0
D_salaryboost = 0
for x_rows in range(np.shape(Vacation_matrix)[0]):
    if Vacation_matrix[x_rows][3] < 5:
        if Vacation_matrix[x_rows][0] == "A":
            A_off += 2 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                A_salaryboost += 2 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "B":
            B_off += 2 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                B_salaryboost += 2 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "C":
            C_off += 2 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                C_salaryboost += 2 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "D":
            D_off += 2 *3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                D_salaryboost += 2 * 3.5 * EBTemplate_matrix[5][1]/100
    if Vacation_matrix[x_rows][3] >= 5 and Vacation_matrix[x_rows][3] < 10:
        if Vacation_matrix[x_rows][0] == "A":
            A_off += 3 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                A_salaryboost += 3 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "B":
            B_off += 3 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                B_salaryboost += 3 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "C":
            C_off += 3 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                C_salaryboost += 3 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "D":
            D_off += 3 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                D_salaryboost += 3 * 3.5 * EBTemplate_matrix[5][1]/100
    if Vacation_matrix[x_rows][3] >= 10 and Vacation_matrix[x_rows][3] < 20:
        if Vacation_matrix[x_rows][0] == "A":
            A_off += 4 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                A_salaryboost += 4 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "B":
            B_off += 4 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                B_salaryboost += 4 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "C":
            C_off += 4 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                C_salaryboost += 4 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "D":
            D_off += 4 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                D_salaryboost += 4 * 3.5 * EBTemplate_matrix[5][1]/100
    if Vacation_matrix[x_rows][3] >= 20 and Vacation_matrix[x_rows][3] < 30:
        if Vacation_matrix[x_rows][0] == "A":
            A_off += 5 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                A_salaryboost += 5 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "B":
            B_off += 5 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                B_salaryboost += 5 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "C":
            C_off += 5 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                C_salaryboost += 5 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "D":
            D_off += 5 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                D_salaryboost += 5 * 3.5 * EBTemplate_matrix[5][1]/100
    if Vacation_matrix[x_rows][3] >= 30:
        if Vacation_matrix[x_rows][0] == "A":
            A_off += 6 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                A_salaryboost += 6 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "B":
            B_off += 6 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                B_salaryboost += 6 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "C":
            C_off += 6 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                C_salaryboost += 6 * 3.5 * EBTemplate_matrix[5][1]/100
        if Vacation_matrix[x_rows][0] == "D":
            D_off += 6 * 3.5
            if Vacation_matrix[x_rows][2] == "Salary":
                D_salaryboost += 6 * 3.5 * EBTemplate_matrix[5][1]/100
#Floating Holiday Addition---------------------
A_off += 2 * A_count
B_off += 2 * B_count
C_off += 2 * C_count
D_off += 2 * D_count

#Add in the Other Off Time---------------------
Illness = EBTemplate_matrix[0][1] / 100 * 365.25/2 / 4
TrainOffShift = EBTemplate_matrix[1][1] / 100 * 365.25/2 / 4
Special_Assign = EBTemplate_matrix[2][1] / 100 * 365.25/2 / 4
OtherOff = EBTemplate_matrix[3][1] / 100 * 365.25/2 / 4
Parental = EBTemplate_matrix[4][1] / 100 * 365.25/2 / 4

A_off += Illness + TrainOffShift + Special_Assign + OtherOff + Parental + A_salaryboost
B_off += Illness + TrainOffShift + Special_Assign + OtherOff + Parental + B_salaryboost
C_off += Illness + TrainOffShift + Special_Assign + OtherOff + Parental + C_salaryboost
D_off += Illness + TrainOffShift + Special_Assign + OtherOff + Parental + D_salaryboost

iris = pd.read_table('Base Data.txt', delimiter=',')
df = pd.DataFrame(iris)
dfOT = pd.DataFrame(iris)
matrix_BasePosts = np.array(df)
Sal_matrix = []
for Sal_items in range(np.shape(matrix_BasePosts)[0]):
    if matrix_BasePosts[Sal_items, 2] == "Salary":
        Sal_matrix += [matrix_BasePosts[Sal_items, 1]]


def Create_pdf():
    global A_off, B_off, C_off, D_off, Sal_matrix, InfoTable_matrix, ExtraBoard_current, BaseCAS_matrix, TeamName_matrix
    c = canvas.Canvas('EBAnalysis.pdf', pagesize=landscape(letter))
    c.setFont("Helvetica", 8, leading=None)
    styles = getSampleStyleSheet()
    c.setFillColor(colors.black)
    Names_Spread = 104
    Names_top = 525
    Graph_scoot = -10
    people_per_team = (np.shape(BaseCAS_matrix)[0] + ExtraBoard_adder) / 4
    #Page Title
    start_date = datetime.now()
    end_date = start_date + timedelta(6)
    pdfstart_datefinal = datetime.strftime(start_date, "%B %d, %Y")
    pdfend_datefinal = datetime.strftime((end_date), "%B %d, %Y")
    Team_Header = "Team Name: " + TeamName_matrix[0][0] + "  Extra Board Analysis Run Date: "
    Header_title = Team_Header + str(pdfstart_datefinal)
    c.setFillColor(colors.firebrick)
    c.setFont("Helvetica", 16, leading=None)
    c.drawCentredString(400, Names_top+55, Header_title)
    Page1_subheader = "Current Team Roster Information - " + str(InfoTable_matrix[0][0])
    c.setFillColor(colors.blue)
    c.setFont("Helvetica", 14, leading=None)
    c.drawCentredString(400, Names_top+55-20, Page1_subheader)
    #keith=c.acroForm
    #keith.textfield(value='Exit', fillColor=colors.yellow, borderColor=colors.black, textColor=colors.blue, borderWidth=1, \
    #                borderStyle='solid', width=50, height=30, x=500, y=10)

    plot_x = [i for i in range(0, 21, 5)]
    Max_yearsofservice = 35
    plot_yCurrent = []
    plot_yNew = []
    OT_daysCurrent = []
    OT_daysNew = []
    OT_Base_per_Team_Current = []
    OT_Base_per_Team_New = []
    Vacation_matrixtemp = Vacation_matrix
    count_new = 1
    Avg_vac_days = 3.5 * 3
    Vacation_onlyCurrent = []
    Vacation_onlyNew = []
    Coverage_per_Team_Current = []
    Coverage_per_Team_New = []
    ExtraOT_aboveshift_Current = []
    ExtraOT_aboveshift_New = []
    TotalOT_perTeam_Current = []
    TotalOT_perTeam_New = []
    people_per_teamnew = people_per_team - (ExtraBoard_current-ExtraBoard_new)
    for y_cols in range(np.shape(plot_x)[0]):
        plot_ytemp = 0
        for x_rows in range(np.shape(Vacation_matrixtemp)[0]):
            if Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] > Max_yearsofservice:
                plot_ytemp += 2 * 3.5 + 2
                Vacation_matrixtemp[x_rows][0] = "New" + str(count_new)
                Vacation_matrixtemp[x_rows][1] = "New" + str(count_new)
                Vacation_matrixtemp[x_rows][2] = "Wage"
                Vacation_matrixtemp[x_rows][3] = 1
                count_new += 1
            elif Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] <= Max_yearsofservice and \
                    Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] < 5:
                plot_ytemp += 2 * 3.5 + 2
            elif Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] <= Max_yearsofservice and \
                    Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] < 10:
                plot_ytemp += 3 * 3.5 + 2
            elif Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] <= Max_yearsofservice and \
                    Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] < 20:
                plot_ytemp += 4 * 3.5 + 2
            elif Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] <= Max_yearsofservice and \
                    Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] < 30:
                plot_ytemp += 5 * 3.5 + 2
            elif Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] <= Max_yearsofservice and \
                    Vacation_matrixtemp[x_rows][3] + plot_x[y_cols] >= 30:
                plot_ytemp += 6 * 3.5 + 2
        Vacation_onlyCurrent += [plot_ytemp/4/ExtraBoard_current]
        Vacation_onlyNew += [(plot_ytemp/4 - Avg_vac_days*ExtraBoard_new)/ExtraBoard_new]
        Vacation_temp = plot_ytemp/4
        Illness = EBTemplate_matrix[0][1] / 100 * people_per_team * 365.25/2 * .5
        TrainOffShift = EBTemplate_matrix[1][1] / 100 * people_per_team * 365.25/2
        Special_Assign = EBTemplate_matrix[2][1] / 100 * people_per_team * 365.25/2
        OtherOff = EBTemplate_matrix[3][1] / 100 * 365.25/2 * people_per_team * .5
        Parental = EBTemplate_matrix[4][1] / 100 * 365.25/2 * people_per_team
        Supv_Boost = np.shape(Sal_matrix)[0] / 4 * EBTemplate_matrix[5][1] / 100 * 4.5 * 3.5
        plot_ytemp += (Illness + TrainOffShift + Special_Assign + OtherOff + Parental - Supv_Boost) / \
                      people_per_team * (np.shape(BaseCAS_matrix)[0] + ExtraBoard_adder)
        extra_exSupv = Illness + TrainOffShift + Special_Assign + OtherOff + Parental
        OT_absorbCurrent = Illness + TrainOffShift + Special_Assign + OtherOff + Parental
        OT_absorbNew = OT_absorbCurrent * people_per_teamnew / people_per_team
        OT_notabsorbCurrent = (Illness + OtherOff)
        OT_notabsorbNew = ((OT_notabsorbCurrent * people_per_teamnew/people_per_team))
        plot_yCurrent += [round(plot_ytemp/(4 * ExtraBoard_current), 0)]
        plot_yNew += [round(Vacation_temp - Avg_vac_days*ExtraBoard_new + extra_exSupv*people_per_teamnew/people_per_team - Supv_Boost, 0)]
        Coverage_per_Team_Current += [round((Vacation_onlyCurrent[y_cols] + OT_absorbCurrent), 1)]
        Coverage_per_Team_New += [round((Vacation_onlyNew[y_cols] + OT_absorbNew), 1)]
        #OT_BaseperTeam_Current =
        Dupont_OTAdd = 0
        if InfoTable_matrix[0][0] == "DuPont Schedule" and ExtraBoard_new < 1:
            Dupont_OTAdd = (365.25 / 7 + 365.25 * 6 / 28)/4
        OT_daysCurrent += [round((Illness + OtherOff + Supv_Boost), 1)]
        OT_Base_per_Team_Current += [round(Illness + OtherOff + Supv_Boost, 1)]
        OT_daysNew += [round(((Illness + OtherOff)*(people_per_teamnew/people_per_team) + Supv_Boost), 1)]
        OT_Base_per_Team_New += [round(((Illness + OtherOff)*(people_per_teamnew/people_per_team) + Supv_Boost), 1)]
        if Coverage_per_Team_Current[y_cols] > (365.25/2):
            ExtraOT_aboveshift_Current += [round(Coverage_per_Team_Current[y_cols]-(365.25/2), 1)]
        else:
            ExtraOT_aboveshift_Current += [0]
        if Coverage_per_Team_New[y_cols] > (365.25/2):
            ExtraOT_aboveshift_New += [round(Coverage_per_Team_New[y_cols]-(365.25/2), 1)]
        else:
            ExtraOT_aboveshift_New += [0]
        if ExtraBoard_current < 1:
            TotalOT_perTeam_Current += [round(ExtraOT_aboveshift_Current[y_cols]+OT_Base_per_Team_Current[y_cols]+\
                                        Dupont_OTAdd+OT_notabsorbCurrent, 1)]
        else:
            TotalOT_perTeam_Current += [round(ExtraOT_aboveshift_Current[y_cols]+OT_Base_per_Team_Current[y_cols]+\
                                        OT_notabsorbCurrent, 1)]
        if ExtraBoard_new < 1:
            TotalOT_perTeam_New += [round(ExtraOT_aboveshift_New[y_cols]+OT_Base_per_Team_New[y_cols]+ \
                                              Dupont_OTAdd+OT_notabsorbNew, 1)]
        else:
            TotalOT_perTeam_New += [round(ExtraOT_aboveshift_New[y_cols]+OT_Base_per_Team_New[y_cols]+ \
                                          OT_notabsorbNew, 1)]
    #Coverage FTE----------------------------------------------------------------
    Cover_FTE_Total_Current = []
    Cover_FTE_Total_New = []
    OT_FTE_Current = []
    OT_FTE_New = []
    FTE_Final_Current = []
    FTE_Final_New = []
    EBCost_Current = 4 * ExtraBoard_current
    EBCost_New = 4 * ExtraBoard_new
    for y_cols in range(np.shape(Coverage_per_Team_Current)[0]):
        if Coverage_per_Team_Current[y_cols] > (365.25/2):
            Cover_FTE_Total_Current += [round((365.25/2)*4*1.5/(365.25/2), 2)]
            OT_FTE_Current += [round(TotalOT_perTeam_Current[y_cols]/(365.25/2)*1.5*4, 2)]
            FTE_Final_Current += [round(EBCost_Current-Cover_FTE_Total_Current[y_cols]+OT_FTE_Current[y_cols], 1)]
        else:
            Cover_FTE_Total_Current += [round(Coverage_per_Team_Current[y_cols]*1.5*4/(365.25/2), 2)]
            OT_FTE_Current += [round(TotalOT_perTeam_Current[y_cols]/(365.25/2)*1.5*4, 2)]
            FTE_Final_Current += [round(EBCost_Current-Cover_FTE_Total_Current[y_cols]+OT_FTE_Current[y_cols], 1)]
        if Coverage_per_Team_New[y_cols] > (365.25/2):
            Cover_FTE_Total_New += [round((365.25/2)*4*1.5/(365.25/2), 2)]
            OT_FTE_New += [round(TotalOT_perTeam_New[y_cols]/(365.25/2)*1.5*4, 2)]
            FTE_Final_New += [round(EBCost_New-Cover_FTE_Total_New[y_cols]+OT_FTE_New[y_cols], 1)]
        else:
            Cover_FTE_Total_New += [round(Coverage_per_Team_New[y_cols]*1.5*4/(365.25/2), 1)]
            OT_FTE_New += [round(TotalOT_perTeam_New[y_cols]/(365.25/2)*1.5*4, 1)]
            FTE_Final_New += [round(EBCost_New-Cover_FTE_Total_New[y_cols]+OT_FTE_New[y_cols], 1)]
    '''print(people_per_team, people_per_teamnew)
    print(ExtraBoard_current, ExtraBoard_new)
    print('Vacation')
    print('curr', Vacation_onlyCurrent)
    print('new', Vacation_onlyNew)
    print('OT abs')
    print('curr', OT_absorbCurrent)
    print('new', OT_absorbNew)
    print('Coverage per team')
    print('Curr', Coverage_per_Team_Current)
    print('new', Coverage_per_Team_New)
    print('EB Cost')
    print('curr', EBCost_Current)
    print('new', EBCost_New)
    print('Dupont Add', Dupont_OTAdd)
    print('Ext above shift')
    print('curr', ExtraOT_aboveshift_Current)
    print('new', ExtraOT_aboveshift_New)
    print('TotalOT_perTeam')
    print('curr', TotalOT_perTeam_Current)
    print('new', TotalOT_perTeam_New)

    print("OT")
    print(OT_FTE_New)'''
    #DONE WITH THE FTE MATH FOR FIRST CHART---------------------------------------------------------
    #print('New')
    #print('OT Base New', OT_Base_per_Team_New)
    for y_cols in range(np.shape(plot_yCurrent)[0]):
        if plot_yNew[y_cols] > 365.25/2:
            OT_daysNew[y_cols] = round((OT_daysNew[y_cols] + Dupont_OTAdd / 4 + (Illness + OtherOff) * \
                                         people_per_teamnew/people_per_team + \
                                  plot_yNew[y_cols] - 365.25/2)/ (people_per_teamnew*365.25/200), 1)
        elif 365.25/2 - plot_yNew[y_cols] <= (Illness + OtherOff) * people_per_teamnew/people_per_team:
            OT_daysNew[y_cols] = round((OT_daysNew[y_cols] + Dupont_OTAdd / 4 + plot_yNew[y_cols] - 365.25/2 + \
                                        (Illness + OtherOff) * people_per_teamnew/people_per_team \
                                        + plot_yNew[y_cols] - 365.25/2)/ (people_per_teamnew*365.25/200), 1)
        else:
            OT_daysNew[y_cols] = round((OT_daysNew[y_cols] + Dupont_OTAdd / 4)/ \
                            (people_per_teamnew*365.25/200), 1)
        if plot_yCurrent[y_cols] > 365.25/2:
            OT_daysCurrent[y_cols] = round((OT_daysCurrent[y_cols] + plot_yCurrent[y_cols] - 365.25/2)/ \
                                           (people_per_team*365.25/200) / ExtraBoard_current, 1)
        elif 365.25/2 - plot_yCurrent[y_cols] <= Illness + OtherOff:
            OT_daysCurrent[y_cols] = round((OT_daysCurrent[y_cols] + plot_yCurrent[y_cols] - 365.25/2 + plot_yCurrent[y_cols] \
                                            - 365.25/2 + Illness + OtherOff)/ (people_per_team*365.25/200) / ExtraBoard_current, 1)
        else:
            OT_daysCurrent[y_cols] = round(OT_daysCurrent[y_cols]/(people_per_team*365.25/200)/ExtraBoard_current, 1)

    #Calculate Cost Using Current and New Extra Board Coverage
    Cost_Current = 1
    EBCost_Current = Cost_Current * 4 * ExtraBoard_current
    EBCost_New = Cost_Current * 4 * ExtraBoard_new
    EBCurrent_Net = []
    EBNew_Net = []
    for y_cols in range(np.shape(plot_yCurrent)[0]):
        EBCurrent_Net += [round(EBCost_Current - (plot_yCurrent[y_cols]/(365.25/2) * 1.5 * 4), 1)]
        if plot_yNew[y_cols] > 365.25/2:
            EBNew_Net += [round(EBCost_New + Dupont_OTAdd/(365.25/2)*1.5 - 4*1.5 + \
                                (365.25/2 - plot_yNew[y_cols])*4*1.5/(365.25/2), 1)]
        if 365.25/2 - plot_yNew[y_cols] <= (Illness + OtherOff)*people_per_teamnew/people_per_team:
            EBNew_Net += [round(EBCost_New + Dupont_OTAdd/(365.25/2)*1.5 - (365.25/2 - plot_yNew[y_cols])/(365.25/2)*4*1.5 - plot_yNew[y_cols] * 4 / \
                          (365.25/2) * 1.5 + ((Illness+OtherOff)*people_per_teamnew/people_per_team - \
                                              (365.25/2 - plot_yNew[y_cols]))*4*1.5/(365.25/2), 1)]
        else:
            EBNew_Net += [round(EBCost_New - plot_yNew[y_cols] * 1.5 * 4 / (365.25/2), 1)]
    if np.max(FTE_Final_Current) >= np.max(FTE_Final_New):
        y_plotmax = int(np.max(FTE_Final_Current)) + 1
    else:
        y_plotmax = int(np.max(FTE_Final_New)) + 1
    if np.min(FTE_Final_Current) <= np.min(FTE_Final_New):
        y_plotmin = int(np.min(FTE_Final_Current)) - 1
    else:
        y_plotmin = int(np.min(FTE_Final_New)) - 1
    y_plotinc = []
    for y_inc in range(y_plotmin - 1, y_plotmax + 2):
        y_plotinc += [y_inc]
    plot_yfinalCurrent = []
    plot_yfinalNew = []
    plot_yfinaldelta = []
    plot_yOTCurrent = []
    plot_yOTNew = []
    plot_yFlexCurrent = []
    plot_yFlexNew = []
    plot_yFlexCurrenttemp = []
    plot_yFlexNewtemp = []
    for y_cols in range(np.shape(FTE_Final_Current)[0]):
        if Vacation_onlyCurrent[y_cols] < 365.25/2:
            plot_yFlexCurrenttemp += [int(round(100 - Vacation_onlyCurrent[y_cols]/(365.25/200)/ExtraBoard_current, 0))]
        else:
            plot_yFlexCurrenttemp += [round(0, 0)]
        if Vacation_onlyNew[y_cols] < 365.25/2:
            plot_yFlexNewtemp += [int(round(100 - Vacation_onlyNew[y_cols]/(365.25/200), 0))]
            #print(plot_yCurrent[y_cols]*ExtraBoard_current/ExtraBoard_new-Avg_vac_days)
        else:
            plot_yFlexNewtemp += [round(0, 0)]
    Low_FlexCurrentFlag = -1
    Low_FlexNewFlag = -1
    for y_cols in range(np.shape(plot_yFlexCurrenttemp)[0]):
        if plot_yFlexCurrenttemp[y_cols] < 10:
            Low_FlexCurrentFlagFlag = y_cols
            break
    for y_cols in range(np.shape(plot_yFlexNewtemp)[0]):
        if plot_yFlexNewtemp[y_cols] < 10:
            Low_FlexNewFlag = y_cols
            break
    #if np.min(plot_yFlexNewtemp) < 10:
    #    Low_FlexNewFlag = np.where(plot_yFlexNewtemp == np.min(plot_yFlexNewtemp))

    for y_cols in range(np.shape(TotalOT_perTeam_Current)[0]):
        plot_yfinalCurrent += [(plot_x[y_cols], FTE_Final_Current[y_cols])]
        plot_yfinalNew += [(plot_x[y_cols], FTE_Final_New[y_cols])]
        plot_yfinaldelta += [round(FTE_Final_Current[y_cols] - FTE_Final_New[y_cols], 1)]
        plot_yOTCurrent += [(plot_x[y_cols], round((TotalOT_perTeam_Current[y_cols])/(365.25/200*people_per_team), 1))]
        plot_yOTNew += [(plot_x[y_cols], round(TotalOT_perTeam_New[y_cols]/(365.25/200*people_per_teamnew), 1))]
        if 100 - Vacation_onlyCurrent[y_cols]/(365.25/200) > 0:
            plot_yFlexCurrent += [(plot_x[y_cols], int(round(100 - Vacation_onlyCurrent[y_cols]/(365.25/200), 0)))]
        else:
            plot_yFlexCurrent += [(plot_x[y_cols], round(0, 0))]
        if 100 - Vacation_onlyNew[y_cols]/(365.25/200) > 0:
            plot_yFlexNew += [(plot_x[y_cols], int(round(100 - Vacation_onlyNew[y_cols]/(365.25/200), 0)))]
        else:
            plot_yFlexNew += [(plot_x[y_cols], round(0, 0))]
    if np.max(TotalOT_perTeam_Current) >= np.max(TotalOT_perTeam_New):
        y_plotmax2 = int(np.max(TotalOT_perTeam_Current)/((365.25/200)*people_per_team))
    else:
        y_plotmax2 = int(np.max(TotalOT_perTeam_New)/((365.25/200)*people_per_teamnew))
    y_plotinc2 = []
    y_plotincrange2 = 5
    if y_plotmax2 < 10:
        y_plotincrange2 = 2
        y_plotmax2 = 10
    for y_inc in range(0, int(y_plotmax2/10)*10 + y_plotincrange2 + 10, y_plotincrange2):
        y_plotinc2 += [y_inc]
    if np.max(Vacation_onlyCurrent)/ExtraBoard_current <= np.max(Vacation_onlyNew)/ExtraBoard_new:
        y_plotmax3 = int(100 - np.min(Vacation_onlyCurrent)/ExtraBoard_current/(365.25/200))
    else:
        y_plotmax3 = int(100 - np.min(Vacation_onlyNew)/ExtraBoard_new/(365.25/200))
    y_plotinc3 = []
    for y_inc in range(0, y_plotmax3 + 10, 10):
        y_plotinc3 += [y_inc]
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(150, 525, 'Extra Board FTE Cost')
    c.drawCentredString(150, 370, 'Years from Today')
    c.drawCentredString(150, 315, 'Minimum Estimated Overtime %')
    c.drawCentredString(150, 210, 'Years from Today')
    c.drawCentredString(150, 152, 'Schedule Flexibility %')
    c.drawCentredString(150, 47, 'Years from Today')
    c.setFillColor(colors.red)
    c.rect(x=60, y=347, width=10, height=5, stroke=0, fill=1)
    c.rect(x=60, y=187, width=10, height=5, stroke=0, fill=1)
    c.setFillColor(colors.blue)
    c.rect(x=60, y=335, width=10, height=5, stroke=0, fill=1)
    c.rect(x=60, y=175, width=10, height=5, stroke=0, fill=1)
    c.setFillColor(colors.black)
    c.setFont("Helvetica", 10, leading=None)
    c.drawString(75, 347, str(ExtraBoard_current) + ' Extra Board per Team')
    c.drawString(75, 187, str(ExtraBoard_current) + ' Extra Board per Team')
    EB_commentadd = ""
    if ExtraBoard_new < 1:
        EB_commentadd = ' (Shared Extra Board)'
    c.drawString(75, 335, str(ExtraBoard_new) + ' Extra Board per Team' + EB_commentadd)
    c.drawString(75, 175, str(ExtraBoard_new) + ' Extra Board per Team' + EB_commentadd)
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.drawString(300, 525, 'Information:')
    c.setFont("Helvetica", 10, leading=None)
    c.drawString(310, 510, '- Coverage Needed Per Team in Year 0 with Current Extra Board:  Total: ' \
                 + str(int(Vacation_onlyCurrent[0] + OT_absorbCurrent)) \
                 + ' days ,  Vacation: ' + str(int(Vacation_onlyCurrent[0])) + ' days')
    c.drawString(310, 495, '- Maximum Years of Service Used in Projection:  ' + str(Max_yearsofservice) + ' years')
    c.drawString(310, 480, '- % Illness:  ' + str(EBTemplate_matrix[0][1]) + '%')
    c.drawString(310, 465, '- % Off Shift Training:  ' + str(EBTemplate_matrix[1][1]) + '%')
    c.drawString(310, 450, '- % Special Assignment:  ' + str(EBTemplate_matrix[2][1]) + '%')
    c.drawString(310, 435, '- % Other (Jury/Personal):  ' + str(EBTemplate_matrix[3][1]) + '%')
    c.drawString(310, 420, '- % Parental Time Off:  ' + str(EBTemplate_matrix[4][1]) + '%')
    c.drawString(310, 405, '- % Supervisor Vacation Scheduled Without Extra Board Available:  ' + str(EBTemplate_matrix[5][1]) + '%')
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.drawString(300, 385, 'Analysis: (Set Extra Board Based on Lowest FTE Cost)')
    c.setFont("Helvetica", 10, leading=None)
    plot_yfinaldelta2 = []
    for y_cols in range(np.shape(plot_yfinaldelta)[0] - 1):
            plot_yfinaldelta2 += [round(plot_yfinaldelta[y_cols+1] + plot_yfinaldelta[y_cols], 1)]
    cross_year1 = 0
    cross_year2 = 0
    cross_year_add = 5
    for y_cols in range(np.shape(plot_yfinaldelta)[0]-1):
        if plot_yfinaldelta[y_cols] > 0 and plot_yfinaldelta[y_cols+1] < 0:
            if cross_year1 == 0:
                cross_year1 = plot_yfinaldelta[y_cols] / ((plot_yfinaldelta[y_cols] - plot_yfinaldelta[y_cols+1]) / 5) + y_cols * cross_year_add
            else:
                cross_year2 = plot_yfinaldelta[y_cols] / ((plot_yfinaldelta[y_cols] - plot_yfinaldelta[y_cols+1]) / 5)
        elif plot_yfinaldelta[y_cols] < 0 and plot_yfinaldelta[y_cols+1] > 0:
            if cross_year1 == 0:
                cross_year1 = plot_yfinaldelta[y_cols] / ((plot_yfinaldelta[y_cols] - plot_yfinaldelta[y_cols+1]) / 5) + y_cols * cross_year_add
            else:
                cross_year2 = plot_yfinaldelta[y_cols] / ((plot_yfinaldelta[y_cols] - plot_yfinaldelta[y_cols+1]) / 5)
    c.drawString(310, 370, '- Current Extra Board: ' + str(ExtraBoard_current) + ' per Team')
    analysis_mover = 15
    if np.max(plot_yfinaldelta2) < 0:
        c.drawString(310, 370 - analysis_mover, '- ' + str(ExtraBoard_current) + ' Extra Board per Team is recommended')
        analysis_mover += 15
    if np.min(plot_yfinaldelta2) > 0:
        c.drawString(310, 370 - analysis_mover, '- ' + str(ExtraBoard_new) + ' Extra Board per Team will save ' + \
                     str(round(np.average(plot_yfinaldelta2), 1)) + ' FTE/year')
        analysis_mover += 15
    if cross_year1 <= 5 and cross_year1 > 0:
        if plot_yfinaldelta[0] > 0:
            Extra_ExtraBoard = ExtraBoard_new
            Extra_ExtraBoard2 = ExtraBoard_current
        else:
            Extra_ExtraBoard = ExtraBoard_current
            Extra_ExtraBoard2 = ExtraBoard_new
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard) + ' Extra Board per Team will save an average of ' + \
                     str(round(plot_yfinaldelta2[0]/5*abs(cross_year1)/2, 1)) + ' FTE/year in first ' + \
                     str(round(abs(cross_year1), 0)) + ' years')
        analysis_mover += 15
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard2) + ' Extra Board per Team should be considered after ' + \
                     str(round(abs(cross_year1), 0)) + ' years')
        analysis_mover += 15
    elif cross_year1 >= 5 and cross_year1 > 0:
        if plot_yfinaldelta[1] > 0:
            Extra_ExtraBoard = ExtraBoard_new
            Extra_ExtraBoard2 = ExtraBoard_current
        else:
            Extra_ExtraBoard = ExtraBoard_current
            Extra_ExtraBoard2 = ExtraBoard_new
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard) + ' Extra Board per Team will save an average of ' + \
                     str(round((plot_yfinaldelta2[0]/2*5+plot_yfinaldelta[1]/2*(abs(cross_year1)-5))/abs(cross_year1), 1)) + \
                     ' FTE/year in first ' + str(round(abs(cross_year1), 0)) + ' years')
        analysis_mover += 15
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard2) + ' Extra Board per Team should be considered after ' + \
                     str(round(abs(cross_year1), 0)) + ' years')
        analysis_mover += 15
    elif cross_year1 >= 10 and cross_year1 > 0:
        if plot_yfinaldelta[2] > 0:
            Extra_ExtraBoard = ExtraBoard_new
            Extra_ExtraBoard2 = ExtraBoard_current
        else:
            Extra_ExtraBoard = ExtraBoard_current
            Extra_ExtraBoard2 = ExtraBoard_new
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard) + ' Extra Board per Team will save an average of ' + \
                     str(round((plot_yfinaldelta2[0]/2*5+plot_yfinaldelta2[1]/2*5+plot_yfinaldelta[2]/2 * \
                                (abs(cross_year1)-5))/abs(cross_year1), 1)) + ' FTE/year in first 10 years')
        analysis_mover += 15
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard2) + ' Extra Board per Team should be considered after ' + \
                     str(round(abs(cross_year1), 0)) + ' years')
        analysis_mover += 15
    elif cross_year1 >= 15 and cross_year1 > 0 and plot_yfinaldelta[3] > 0 and plot_yfinaldelta2[3] > 0:
        if plot_yfinaldelta[2] > 0:
            Extra_ExtraBoard = ExtraBoard_new
            Extra_ExtraBoard2 = ExtraBoard_current
        else:
            Extra_ExtraBoard = ExtraBoard_current
            Extra_ExtraBoard2 = ExtraBoard_new
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard) + ' Extra Board per Team will save an average of ' + \
                     str(round((plot_yfinaldelta2[0]/2*5+plot_yfinaldelta2[1]/2*5+plot_yfinaldelta2[2]/2*5+plot_yfinaldelta[3]/2 * \
                                (abs(cross_year1)-5))/abs(cross_year1), 1)) + \
                     ' FTE/year in first ' + str(round(abs(cross_year1), 0)) + ' years')
        analysis_mover += 15
        c.drawString(310, 370 - analysis_mover, '- ' + str(Extra_ExtraBoard2) + ' Extra Board per Team should be considered after ' + \
                     str(round(abs(cross_year1), 0)) + ' years')
        analysis_mover += 15
    if Low_FlexCurrentFlag != -1:
        c.drawString(310, 370 - analysis_mover, '- Schedule Flexibility is less than 10% for ' + str(ExtraBoard_current) + \
                     ' Extra Board in year ' + str(plot_x[int(Low_FlexCurrentFlag[0][0])]))
        analysis_mover += 15
    if Low_FlexNewFlag != -1:
        c.drawString(310, 370 - analysis_mover, '- Schedule Flexibility is less than 10% for ' + str(ExtraBoard_new) + \
                     ' Extra Board in year ' + str(plot_x[int(Low_FlexNewFlag)]))
        analysis_mover += 15
    #EB Chart------------------------------------------------------------------------------------
    drawing = Drawing(300, 200)
    data = [plot_yfinalCurrent]
    lp = LinePlot()
    lp.x = 50
    lp.y = 50
    lp.height = 125
    lp.width = 200
    lp.data = data
    lp.lines.strokeWidth = 4
    lp.joinedLines = 1
    #lp.lines[0].symbol = makeMarker('FilledCircle')
    #lp.lines[1].symbol = makeMarker('Circle')
    #lp.lineLabelFormat = '%2.1f'
    #lp.strokeColor = colors.black
    lp.xValueAxis.valueMin = 0
    lp.xValueAxis.valueMax = 20
    lp.xValueAxis.valueSteps = [0, 5, 10, 15, 20]
    lp.xValueAxis.labelTextFormat = '%2.0f'
    lp.yValueAxis.valueMin = y_plotmin - 1
    lp.yValueAxis.valueMax = y_plotmax + 1
    lp.yValueAxis.valueSteps = y_plotinc
    drawing.add(lp)
    #drawing.save()
    drawing.save(formats=['pdf'],outDir='.', fnRoot=None)
    x1, y1 = 0, 350
    renderPDF.draw(drawing, c, x1, y1, showBoundary=False)
    #c.save()
    drawing2 = Drawing(300, 200)
    data2 = [plot_yfinalNew]
    lp2 = LinePlot()
    lp2.x = 50
    lp2.y = 50
    lp2.height = 125
    lp2.width = 200
    lp2.data = data2
    lp2.lines.strokeWidth = 4
    lp2.joinedLines = 1
    lp2.lines[0].strokeColor = colors.blue
    lp2.xValueAxis.valueMin = 0
    lp2.xValueAxis.valueMax = 20
    lp2.xValueAxis.valueSteps = [0, 5, 10, 15, 20]
    lp2.xValueAxis.labelTextFormat = '%2.0f'
    lp2.yValueAxis.valueMin = y_plotmin - 1
    lp2.yValueAxis.valueMax = y_plotmax + 1
    lp2.yValueAxis.valueSteps = y_plotinc
    drawing2.add(lp2)
    drawing2.save()
    drawing2.save(formats=['pdf'],outDir='.', fnRoot=None)
    x1, y1 = 0, 350
    renderPDF.draw(drawing2, c, x1, y1, showBoundary=False)
    drawing3 = Drawing(300, 200)
    data3 = [plot_yOTCurrent]
    lp3 = LinePlot()
    lp3.x = 50
    lp3.y = 50
    lp3.height = 75
    lp3.width = 200
    lp3.data = data3
    lp3.lines.strokeWidth = 4
    lp3.joinedLines = 1
    #lp.lines[0].symbol = makeMarker('FilledCircle')
    #lp.lines[1].symbol = makeMarker('Circle')
    #lp2.lineLabelFormat = '%2.1f'
    lp3.lines[0].strokeColor = colors.red
    lp3.xValueAxis.valueMin = 0
    lp3.xValueAxis.valueMax = 20
    lp3.xValueAxis.valueSteps = [0, 5, 10, 15, 20]
    lp3.xValueAxis.labelTextFormat = '%2.0f'
    lp3.yValueAxis.valueMin = 0
    lp3.yValueAxis.valueMax = int(y_plotmax2/10)*10 + 10
    lp3.yValueAxis.valueSteps = y_plotinc2
    drawing3.add(lp3)
    drawing3.save()
    drawing3.save(formats=['pdf'],outDir='.', fnRoot=None)
    x1, y1 = 0, 190
    renderPDF.draw(drawing3, c, x1, y1, showBoundary=False)
    drawing4 = Drawing(300, 200)
    data4 = [plot_yOTNew]
    lp4 = LinePlot()
    lp4.x = 50
    lp4.y = 50
    lp4.height = 75
    lp4.width = 200
    lp4.data = data4
    lp4.lines.strokeWidth = 4
    lp4.joinedLines = 1
    #lp.lines[0].symbol = makeMarker('FilledCircle')
    #lp.lines[1].symbol = makeMarker('Circle')
    #lp2.lineLabelFormat = '%2.1f'
    lp4.lines[0].strokeColor = colors.blue
    lp4.xValueAxis.valueMin = 0
    lp4.xValueAxis.valueMax = 20
    lp4.xValueAxis.valueSteps = [0, 5, 10, 15, 20]
    lp4.xValueAxis.labelTextFormat = '%2.0f'
    lp4.yValueAxis.valueMin = 0
    lp4.yValueAxis.valueMax = int(y_plotmax2/10)*10 + 10 # + y_plotincrange2*2
    lp4.yValueAxis.valueSteps = y_plotinc2
    drawing4.add(lp4)
    drawing4.save()
    drawing4.save(formats=['pdf'],outDir='.', fnRoot=None)
    x1, y1 = 0, 190
    renderPDF.draw(drawing4, c, x1, y1, showBoundary=False)
    drawing5 = Drawing(300, 200)
    data5 = [plot_yFlexCurrent]
    lp5 = LinePlot()
    lp5.x = 50
    lp5.y = 50
    lp5.height = 75
    lp5.width = 200
    lp5.data = data5
    lp5.lines.strokeWidth = 4
    lp5.joinedLines = 1
    #lp.lines[0].symbol = makeMarker('FilledCircle')
    #lp.lines[1].symbol = makeMarker('Circle')
    #lp2.lineLabelFormat = '%2.1f'
    lp5.lines[0].strokeColor = colors.red
    lp5.xValueAxis.valueMin = 0
    lp5.xValueAxis.valueMax = 20
    lp5.xValueAxis.valueSteps = [0, 5, 10, 15, 20]
    lp5.xValueAxis.labelTextFormat = '%2.0f'
    lp5.yValueAxis.valueMin = 0
    lp5.yValueAxis.valueMax = int((y_plotmax3 + 10)/10)*10
    lp5.yValueAxis.valueSteps = y_plotinc3
    drawing5.add(lp5)
    drawing5.save()
    drawing5.save(formats=['pdf'],outDir='.', fnRoot=None)
    x1, y1 = 0, 27
    renderPDF.draw(drawing5, c, x1, y1, showBoundary=False)
    drawing6 = Drawing(300, 200)
    data6 = [plot_yFlexNew]
    lp6 = LinePlot()
    lp6.x = 50
    lp6.y = 50
    lp6.height = 75
    lp6.width = 200
    lp6.data = data6
    lp6.lines.strokeWidth = 4
    lp6.joinedLines = 1
    #lp.lines[0].symbol = makeMarker('FilledCircle')
    #lp.lines[1].symbol = makeMarker('Circle')
    #lp2.lineLabelFormat = '%2.1f'
    lp6.lines[0].strokeColor = colors.blue
    lp6.xValueAxis.valueMin = 0
    lp6.xValueAxis.valueMax = 20
    lp6.xValueAxis.valueSteps = [0, 5, 10, 15, 20]
    lp6.xValueAxis.labelTextFormat = '%2.0f'
    lp6.yValueAxis.valueMin = 0
    lp6.yValueAxis.valueMax = int((y_plotmax3 + 10)/10)*10
    lp6.yValueAxis.valueSteps = y_plotinc3
    drawing6.add(lp6)
    drawing6.save()
    drawing6.save(formats=['pdf'],outDir='.', fnRoot=None)
    x1, y1 = 0, 27
    renderPDF.draw(drawing6, c, x1, y1, showBoundary=False)
    c.save()
    file_name = '/Users/kelliecascarelli/PycharmProjects/PostScheduleMVC/EBAnalysis.pdf'
    subprocess.call(['open', file_name])

Create_pdf()

A_peoplecount = 0
B_peoplecount = 0
C_peoplecount = 0
D_peoplecount = 0

count_team = 1
xstart = 4
BadName_Number = []
temp_matrix = []

input_table = pd.read_table('PassDate.txt', delimiter=',')
sdf_inputtable = pd.DataFrame(input_table)
Pass_matrix = np.array(sdf_inputtable)
Pass_shape = np.shape(Pass_matrix)[0]
