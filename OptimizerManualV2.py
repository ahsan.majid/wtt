import pandas as pd
import numpy as np
import csv
from scipy.optimize import linear_sum_assignment
from tkinter import *
import itertools
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfgen import canvas
from reportlab.graphics import renderPDF
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import VerticalBarChart
import runpy
import subprocess
import sys
sys.setrecursionlimit(500000)

'''Algorithm Framework
Read Tables: SetupScheduleInfo.txt, Initial Input.txt, Base Data CAS Manual.txt

def possible_Salary(y, n) & def solve_Salary()  (solve the salaries first)
def possible_FillSTLPost(y, n) & def solve_FillSTLPost()   (fill in the balance of 1st and 2nd posts)
def lowTeam_afterSTLfill()   (Team stat stuff)
def Fill_AllPosts()  (fill in the rest of qualified 2nd Posts)
def Shave_to_Ten()   (reduces the undefined to 10 people)
def Shave_to_Seven()  (reduces the undefined to 7 people)
def Fill_possible(y, n) & def Fill_solve()  (solve for the rest)
def last_tuning()   (final touches)
def Next_Checks()   (some final checks)
def final_print()   (do the last stats)
Create_pdf()
'''

TeamName_table = pd.read_table('TeamName.txt')
sdf_TeamNameTable = pd.DataFrame(TeamName_table)
TeamName_matrix = np.array(sdf_TeamNameTable)

SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)
InfoTable_matrixshape = np.shape(InfoTable_matrix)

if InfoTable_matrix[0, 0] == "DuPont Schedule":
    shift_table = pd.read_csv('ShiftDupont.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "4on/4off Schedule":
    shift_table = pd.read_csv('ShiftFourFour.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "EOWEO Schedule":
    shift_table = pd.read_csv('ShiftEOWEO.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "7on/7off Schedule":
    shift_table = pd.read_csv('ShiftSeven.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "Four/Five Schedule":
    shift_table = pd.read_csv('ShiftFourFive.txt', delimiter=',')
sdf = pd.DataFrame(shift_table)
sdf_matrix = np.array(sdf)
Team_Pairing_Schd = []
DuPont_Monday_Teams = []
for x_rows in range(0, 28):
    if sdf_matrix[x_rows][1] + sdf_matrix[x_rows][2] not in Team_Pairing_Schd:
        Team_Pairing_Schd += [sdf_matrix[x_rows][1] + sdf_matrix[x_rows][2]]
    if datetime.strptime(sdf_matrix[x_rows][0], "%Y-%m-%d").weekday() == 0 and InfoTable_matrix[0][0] == "DuPont Schedule" \
            and sdf_matrix[x_rows][1] + sdf_matrix[x_rows][2] not in DuPont_Monday_Teams:
        DuPont_Monday_Teams += [sdf_matrix[x_rows][1] + sdf_matrix[x_rows][2]]
Initial_table = pd.read_csv('Initial Input.txt', delimiter=',')
sdf_InitialTable = pd.DataFrame(Initial_table)
Initial_matrix = np.array(sdf_InitialTable)
Post_CountDays = 0
Post_CountNights = 0
for items in range(np.shape(Initial_matrix)[0]):
    if Initial_matrix[items, 1] == "Y" or Initial_matrix[items, 2] == "Y":
        Post_CountDays += 1
    if Initial_matrix[items, 1] == "Y" and Initial_matrix[items, 2] != "Y":
        Post_CountNights += 1
Post_Headerinfo = []
post_nameslist = {}
for post_items in range(np.shape(Initial_matrix)[0]):
    post_nameslist["Post" + str(post_items+1)] = Initial_matrix[post_items,0]
    Post_Headerinfo += [Initial_matrix[post_items, 0]]

Post_count = np.shape(Initial_matrix)[0]

#Initial Perm Matrix Stuff-----------------------------------------
Index_list = []
'''for x_rows in range(0, 7):
    Index_list += [x_rows]
erase_base = open("Permutation7.txt", "w+")
erase_base.close()
PostHeader1 = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six']
with open("Permutation7.txt", 'a') as appFile:
    writer = csv.writer(appFile)
    writer.writerow(PostHeader1)
    appFile.close()
for comb in itertools.permutations(Index_list, 7):
    with open("Permutation7.txt", 'a') as appFile:
        writer = csv.writer(appFile)
        writer.writerow(comb)
        appFile.close()'''
Perm_table = pd.read_csv('Permutation7.txt', delimiter=',')
sdf_PermTable = pd.DataFrame(Perm_table)
Perm_matrix = np.array(sdf_PermTable)

#Schedule_table = pd.read_csv('Total Schedule.txt', delimiter=',')
#sdf_ScheduleTable = pd.DataFrame(Schedule_table)
#ScheduleTable_matrix = np.array(sdf_ScheduleTable)

BaseCAS_Data = pd.read_csv("Base Data CAS Manual.txt", delimiter=',')
#BaseCAS_Data = pd.read_csv("Base Data CAS.txt", delimiter=',')
BaseCAS_sdf = pd.DataFrame(BaseCAS_Data)
#BaseCAS_sdf = pd.DataFrame(BaseCAS_Data.sort_values(by=['Team', 'Name']))
BaseCAS_matrix = np.array(BaseCAS_sdf)
#BaseCAS_matrix = np.array(BaseCAS_sdf.sort_values(by=['Team']))
BaseCAS_shaperows = np.shape(BaseCAS_matrix)[0]
BaseCAS_shapecols = np.shape(BaseCAS_matrix)[1]
#BaseCAS_matrixBase = np.array((BaseCAS_sdf))
BaseCAS_matrixBase = np.array(BaseCAS_sdf.sort_values(by=['Team', str(BaseCAS_sdf.columns[4]), \
                                                          str(BaseCAS_sdf.columns[5]), str(BaseCAS_sdf.columns[6]), \
                                                          str(BaseCAS_sdf.columns[7]), str(BaseCAS_sdf.columns[8]), str(BaseCAS_sdf.columns[9])], \
                                                      ascending=[True, False, False, False, False, False, False]))
Base_Base = np.array(BaseCAS_sdf.sort_values(by=['Team', str(BaseCAS_sdf.columns[4]), \
                                                 str(BaseCAS_sdf.columns[5]), str(BaseCAS_sdf.columns[6]), \
                                                 str(BaseCAS_sdf.columns[7]), str(BaseCAS_sdf.columns[8]), str(BaseCAS_sdf.columns[9])], \
                                             ascending=[True, False, False, False, False, False, False]))
perm_count = 0
perm_count_final = 0
perm_count_max = 500
Flag_cannot = 0
Stop_this = 0
Last_Ditch_Effort = 0
Post_avgCheck = 0
Team1_avg = 0
Team2_avg = 0
Team3_avg = 0
Team4_avg = 0
Team1_count = 0
Team2_count = 0
Team3_count = 0
Team4_count = 0
Team1_sum = 0
Team2_sum = 0
Team3_sum = 0
Team4_sum = 0
Team1_postcount = 0
Team1_postsum = 0
Team_postcount = np.zeros(shape=(4, np.shape(BaseCAS_matrix)[1]-4))
Team_postsum = np.zeros(shape=(4, np.shape(BaseCAS_matrix)[1]-4))
BaseTeam_postavg = np.zeros(shape=(4, np.shape(BaseCAS_matrix)[1]-4))
for x_rows in range(np.shape(BaseCAS_matrix)[0]):
    for y_cols in range(4, np.shape(BaseCAS_matrix)[1]):
        if BaseCAS_matrix[x_rows][0] == "A" and BaseCAS_matrix[x_rows][y_cols] > 0:
            Team1_count += 1
            Team1_sum += BaseCAS_matrix[x_rows][y_cols]
            Team_postcount[0][y_cols-4] += 1
            Team_postsum[0][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
        if BaseCAS_matrix[x_rows][0] == "B" and BaseCAS_matrix[x_rows][y_cols] > 0:
            Team2_count += 1
            Team2_sum += BaseCAS_matrix[x_rows][y_cols]
            Team_postcount[1][y_cols-4] += 1
            Team_postsum[1][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
        if BaseCAS_matrix[x_rows][0] == "C" and BaseCAS_matrix[x_rows][y_cols] > 0:
            Team3_count += 1
            Team3_sum += BaseCAS_matrix[x_rows][y_cols]
            Team_postcount[2][y_cols-4] += 1
            Team_postsum[2][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
        if BaseCAS_matrix[x_rows][0] == "D" and BaseCAS_matrix[x_rows][y_cols] > 0:
            Team4_count += 1
            Team4_sum += BaseCAS_matrix[x_rows][y_cols]
            Team_postcount[3][y_cols-4] += 1
            Team_postsum[3][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
if Team1_count > 0:
    Team1_avg = Team1_sum / Team1_count
else:
    Team1_avg = 0
if Team2_count > 0:
    Team2_avg = Team2_sum / Team2_count
else:
    Team2_avg = 0
if Team3_count > 0:
    Team3_avg = Team3_sum / Team3_count
else:
    Team3_avg = 0
if Team4_count > 0:
    Team4_avg = Team4_sum / Team4_count
else:
    Team4_avg = 0
Team_avgBase = np.hstack((round(Team1_avg, 1), round(Team2_avg, 1), round(Team3_avg, 1), round(Team4_avg, 1)))
for x_rows in range(np.shape(Team_postcount)[0]):
    for y_cols in range(np.shape(Team_postcount)[1]):
        if Team_postcount[x_rows][y_cols] > 0:
            BaseTeam_postavg[x_rows][y_cols] = round(Team_postsum[x_rows][y_cols] / Team_postcount[x_rows][y_cols], 1)
        else:
            BaseTeam_postavg[x_rows][y_cols] = 0
    kwcitems_count = 0
    kwcitems_team = ""
    kwcitemsBase_matrix = []
    for kwcitems_cols in range(4, 10):
        for kwcitems_n in range(1, 5):
            if kwcitems_n == 1:
                kwcitems_team = "A"
            if kwcitems_n == 2:
                kwcitems_team = "B"
            if kwcitems_n == 3:
                kwcitems_team = "C"
            if kwcitems_n == 4:
                kwcitems_team = "D"
            kwcitems_count = 0
            for kwcitems_rows in range(np.shape(BaseCAS_matrixBase)[0]):
                if BaseCAS_matrixBase[kwcitems_rows][0] == kwcitems_team and \
                        BaseCAS_matrixBase[kwcitems_rows][kwcitems_cols] != 0:
                    kwcitems_count = kwcitems_count + 1
            kwcitemsBase_matrix += [kwcitems_team, kwcitems_count]
    kwcitemsBase_matrix1 = []
    kwcitemsBase_matrix2 = []
    kwcitemsBase_matrix3 = []
    kwcitemsBase_matrix4 = []
    kwcitemsBase_matrix5 = []
    Count_1 = 0
    Count_2 = 0
    Count_3 = 0
    Count_4 = 0
    Count1_ERT = 0
    Count2_ERT = 0
    Count3_ERT = 0
    Count4_ERT = 0
    Count1_EMT = 0
    Count2_EMT = 0
    Count3_EMT = 0
    Count4_EMT = 0
    for team_rows in range(np.shape(BaseCAS_matrixBase)[0]):
        if BaseCAS_matrixBase[team_rows][0] == "A":
            Count_1 += 1
            if BaseCAS_matrixBase[team_rows][2] == "Y":
                Count1_ERT += 1
            if BaseCAS_matrixBase[team_rows][3] == "Y":
                Count1_EMT += 1
        if BaseCAS_matrixBase[team_rows][0] == "B":
            Count_2 += 1
            if BaseCAS_matrixBase[team_rows][2] == "Y":
                Count2_ERT += 1
            if BaseCAS_matrixBase[team_rows][3] == "Y":
                Count2_EMT += 1
        if BaseCAS_matrixBase[team_rows][0] == "C":
            Count_3 += 1
            if BaseCAS_matrixBase[team_rows][2] == "Y":
                Count3_ERT += 1
            if BaseCAS_matrixBase[team_rows][3] == "Y":
                Count3_EMT += 1
        if BaseCAS_matrixBase[team_rows][0] == "D":
            Count_4 += 1
            if BaseCAS_matrixBase[team_rows][2] == "Y":
                Count4_ERT += 1
            if BaseCAS_matrixBase[team_rows][3] == "Y":
                Count4_EMT += 1
    TeamBase_CountTotal = np.hstack((Count_1, Count_2, Count_3, Count_4))
    TeamBase_ERTTotal = np.hstack((Count1_ERT, Count2_ERT, Count3_ERT, Count4_ERT))
    TeamBase_EMTTotal = np.hstack((Count1_EMT, Count2_EMT, Count3_EMT, Count4_EMT))
    for kwcitems_cols2 in range(0, np.shape(kwcitemsBase_matrix)[0], 2):
        if kwcitemsBase_matrix[kwcitems_cols2] == "A":
            kwcitemsBase_matrix1 += [kwcitemsBase_matrix[kwcitems_cols2+1]]
        if kwcitemsBase_matrix[kwcitems_cols2] == "B":
            kwcitemsBase_matrix2 += [kwcitemsBase_matrix[kwcitems_cols2+1]]
        if kwcitemsBase_matrix[kwcitems_cols2] == "C":
            kwcitemsBase_matrix3 += [kwcitemsBase_matrix[kwcitems_cols2+1]]
        if kwcitemsBase_matrix[kwcitems_cols2] == "D":
            kwcitemsBase_matrix4 += [kwcitemsBase_matrix[kwcitems_cols2+1]]
    kwcitemsBase_matrix5 = np.vstack((kwcitemsBase_matrix1, kwcitemsBase_matrix2, kwcitemsBase_matrix3, kwcitemsBase_matrix4))

#Establish First Time Checks Against Optimized Solution
Team_avgtemp = Team_avgBase
kwcitems_matrix5temp = kwcitemsBase_matrix5
Team_ERTTotaltemp = TeamBase_ERTTotal
Team_EMTTotaltemp = TeamBase_EMTTotal
Team_postavgtemp = BaseTeam_postavg
BaseCAS_matrixtemp = BaseCAS_matrix
Team_postavgstddevtemp = np.sum(np.std(BaseTeam_postavg, axis=0))**.5
Team_postavg_mintemp = np.min(BaseTeam_postavg)
Team_postavg_maxtemp = np.max(BaseTeam_postavg)

iris = pd.read_table('Base Data.txt', delimiter=',')
df = pd.DataFrame(iris)
dfOT = pd.DataFrame(iris)
matrix_BasePosts = np.array(df)
Sal_matrix = []
for Sal_items in range(np.shape(matrix_BasePosts)[0]):
    if matrix_BasePosts[Sal_items, 2] == "Salary":
        Sal_matrix += [matrix_BasePosts[Sal_items, 1]]

#Who has less than 3 posts and no Salary in Current------------------------------------------------
A_TeamLTA3 = []
B_TeamLTA3 = []
C_TeamLTA3 = []
D_TeamLTA3 = []



def Create_pdf():
    global BaseCAS_matrixBase, BaseCAS_matrix, kwcitems_matrix5, Team_CountTotal, PostCAS_dataavg, PostCAS_dataavail, Post_availmin, \
        Post_availmax, PostERT_avail, PostERT_availmin, PostERT_availmax, PostEMT_avail, PostEMT_availmin, \
        PostEMT_availmax, Team_postavg, Team_avg, plot_posts, kwcitemsBase_matrix5, TeamBase_ERTTotal, \
        TeamBase_EMTTotal, InfoTable_matrix, Team_avgBase, Sal_matrix, Post_Headerinfo, Base_Base, Flag_cannot, \
        Team_ERTTotal, Team_EMTTotal, Team_Pairing_Schd, DuPont_Monday_Teams
    c = canvas.Canvas('Optimizer.pdf', pagesize=landscape(letter))
    c.setFont("Helvetica", 8, leading=None)
    styles = getSampleStyleSheet()
    c.setFillColor(colors.black)
    Names_Spread = 104
    Names_top = 525
    Graph_scoot = 60
    #Page Title
    start_date = datetime.now()
    end_date = start_date + timedelta(6)
    pdfstart_datefinal = datetime.strftime(start_date, "%B %d, %Y")
    pdfend_datefinal = datetime.strftime((end_date), "%B %d, %Y")
    Team_Header = "Team Name: " + TeamName_matrix[0][0] + "  Optimizer Run Date: "
    Header_title = Team_Header + str(pdfstart_datefinal)
    c.setFillColor(colors.firebrick)
    c.setFont("Helvetica", 16, leading=None)
    c.drawCentredString(400, Names_top+55, Header_title)
    Page1_subheader = "Current Team Roster Information - " + str(InfoTable_matrix[0][0])
    c.setFillColor(colors.blue)
    c.setFont("Helvetica", 14, leading=None)
    c.drawCentredString(400, Names_top+55-20, Page1_subheader)
    Page1_Teamhdrspace = 25
    Move_Page1_Teamhdr = 0
    Adj_TeamHdrwidth = 0
    plot_posts = list(BaseCAS_sdf.columns[4:np.shape(BaseCAS_matrix)[1]])
    c.setFont("Helvetica-Bold", 14, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(140, Names_top+10, 'Current Team Roster and Competency')
    c.setFillColor(colors.green)
    c.rect(x=10+Move_Page1_Teamhdr, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-67, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(28, Names_top+35-15-Page1_Teamhdrspace+2, "Team")
    c.setFillColor(colors.green)
    c.rect(x=46, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-67+50, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawString(28+22, Names_top+35-15-Page1_Teamhdrspace+2, "Name")
    c.setFillColor(colors.green)
    c.rect(x=133, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-79, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(145, Names_top+35-15-Page1_Teamhdrspace+2, "S/W")
    c.setFillColor(colors.green)
    c.rect(x=133+25, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-79, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(145+25, Names_top+35-15-Page1_Teamhdrspace+2, "ERT")
    c.setFillColor(colors.green)
    c.rect(x=133+25+25, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-79, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(145+25+25, Names_top+35-15-Page1_Teamhdrspace+2, "EMT")
    Move_Page1_Teamhdr = 75
    for Teamhdr in range(np.shape(plot_posts)[0]):
        c.setFillColor(colors.green)
        c.rect(x=133+Move_Page1_Teamhdr, y=Names_top+35-15-Page1_Teamhdrspace-1, width=25, height=12, stroke=1, fill=1)
        c.setFont("Helvetica", 8, leading=None)
        c.setFillColor(colors.white)
        c.drawCentredString(146+Move_Page1_Teamhdr, Names_top+35-15-Page1_Teamhdrspace+2, plot_posts[Teamhdr])
        Move_Page1_Teamhdr += 25
    c.setFont("Helvetica", 8, leading=None)
    count_BaseCAS = 0
    for x_rows in range(np.shape(BaseCAS_matrixBase)[0]):
        if BaseCAS_matrixBase[x_rows][0] == "A":
            c.setFillColorRGB(157/255, 208/255, 243/255)
        if BaseCAS_matrixBase[x_rows][0] == "B":
            c.setFillColorRGB(247/255, 205/255, 171/255)
        if BaseCAS_matrixBase[x_rows][0] == "C":
            c.setFillColorRGB(220/255, 187/255, 246/255)
        if BaseCAS_matrixBase[x_rows][0] == "D":
            c.setFillColorRGB(229/255, 247/255, 171/255)
        c.rect(x=10, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=Names_Spread-68, height=12, stroke=1, fill=1)
        c.setFillColor(colors.black)
        c.drawCentredString(28, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, BaseCAS_matrixBase[x_rows][0])
        count_BaseCAS += 12
    count_BaseCAS = 0
    for x_rows in range(np.shape(BaseCAS_matrixBase)[0]):
        if BaseCAS_matrixBase[x_rows][0] == "A":
            c.setFillColorRGB(157/255, 208/255, 243/255)
        if BaseCAS_matrixBase[x_rows][0] == "B":
            c.setFillColorRGB(247/255, 205/255, 171/255)
        if BaseCAS_matrixBase[x_rows][0] == "C":
            c.setFillColorRGB(220/255, 187/255, 246/255)
        if BaseCAS_matrixBase[x_rows][0] == "D":
            c.setFillColorRGB(229/255, 247/255, 171/255)
        c.rect(x=10+36, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=Names_Spread-88+71, height=12, stroke=1, fill=1)
        c.rect(x=10+36+87, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=25, height=12, stroke=1, fill=1)
        c.setFillColor(colors.black)
        c.drawString(28+22, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, BaseCAS_matrixBase[x_rows][1])
        c.setFillColor(colors.black)
        if BaseCAS_matrixBase[x_rows][1] in Sal_matrix:
            Sal_Wage = "S"
        else:
            Sal_Wage = "W"
        c.drawCentredString(28+22+87+8, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, Sal_Wage)
        count_BaseCAS += 12
    count_BaseCAS = 0
    for x_rows in range(np.shape(BaseCAS_matrixBase)[0]):
        colcount_BaseCAS = 87
        for y_cols in range(2, np.shape(BaseCAS_matrixBase)[1]):
            if BaseCAS_matrixBase[x_rows][0] == "A":
                c.setFillColorRGB(157/255, 208/255, 243/255)
            if BaseCAS_matrixBase[x_rows][0] == "B":
                c.setFillColorRGB(247/255, 205/255, 171/255)
            if BaseCAS_matrixBase[x_rows][0] == "C":
                c.setFillColorRGB(220/255, 187/255, 246/255)
            if BaseCAS_matrixBase[x_rows][0] == "D":
                c.setFillColorRGB(229/255, 247/255, 171/255)
            c.rect(x=10+36+colcount_BaseCAS+25, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=25, height=12, stroke=1, fill=1)
            c.setFillColor(colors.black)
            if y_cols == 2 or y_cols == 3:
                if BaseCAS_matrixBase[x_rows][y_cols] == "Y":
                    BaseCAS_matrixBase[x_rows][y_cols] = "X"
                elif BaseCAS_matrixBase[x_rows][y_cols] == "N":
                    BaseCAS_matrixBase[x_rows][y_cols] = ""
            if y_cols >= 4 and BaseCAS_matrixBase[x_rows][y_cols] == 0:
                BaseCAS_matrixBase[x_rows][y_cols] = ""
            c.drawCentredString(22+37+colcount_BaseCAS+25, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, str(BaseCAS_matrixBase[x_rows][y_cols]))
            colcount_BaseCAS += 25
        count_BaseCAS += 12
    Base_stddev = np.std(BaseTeam_postavg, axis=0)
    Sum_stddev = np.sum(Base_stddev)
    Avg_stddev = Sum_stddev**.5
    c.setFont("Helvetica-Bold", 11, leading=None)
    c.setFillColor(colors.black)
    c.drawString(452+Graph_scoot, 420, 'Competency Analysis:')
    c.setFont("Helvetica", 10, leading=None)
    Base_min = np.min(BaseTeam_postavg)
    c.drawString(454+Graph_scoot, 405, 'Minimum Post Avg Competency: ' + str(round(Base_min, 1)))
    Base_max = np.max(BaseTeam_postavg)
    c.drawString(454+Graph_scoot, 390, 'Maximum Post Avg Competency: ' + str(round(Base_max, 1)))
    c.drawString(454+Graph_scoot, 375, 'Overall Competency Standard Deviation: ' + str(round(Avg_stddev, 1)))
    New_stddev = np.std(Team_postavg, axis=0)
    New_Sum_stddev = np.sum(New_stddev)
    New_avg_stddev = New_Sum_stddev**.5
    #Comptency Chart Current Roster
    drawing = Drawing(300, 150)
    data = BaseTeam_postavg
    bc = VerticalBarChart()
    bc.x = 50
    bc.y = 50
    bc.height = 75
    bc.width = 200
    bc.data = data
    #bc.strokeColor = colors.black
    bc.valueAxis.valueMin = 0
    bc.valueAxis.valueMax = 3
    bc.valueAxis.valueStep = 1
    bc.categoryAxis.labels.boxAnchor = 'ne'
    bc.categoryAxis.labels.dx = 8
    bc.categoryAxis.labels.dy = -2
    bc.categoryAxis.labels.angle = 30
    bc.categoryAxis.categoryNames = plot_posts
    bc.bars[0].fillColor = colors.HexColor("#9DD0F3")
    bc.bars[1].fillColor = colors.HexColor("#F7CDAB")
    bc.bars[2].fillColor = colors.HexColor("#DCBBF6")
    bc.bars[3].fillColor = colors.HexColor("#E5F7AB")
    drawing.add(bc)
    drawing.save()
    drawing_title = "Current Competency Distribution"
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(560+Graph_scoot, 530+5, drawing_title)
    x, y = 410+Graph_scoot, 405
    renderPDF.draw(drawing, c, x, y, showBoundary=False)
    #Post Distribution Chart Current Roster
    drawing1 = Drawing(300, 150)
    data1 = kwcitemsBase_matrix5
    bc1 = VerticalBarChart()
    bc1.x = 50
    bc1.y = 50
    bc1.height = 75
    bc1.width = 200
    bc1.data = data1
    bc1.valueAxis.valueMin = 0
    bc1.valueAxis.valueMax = 5
    bc1.valueAxis.valueStep = 1
    bc1.categoryAxis.labels.boxAnchor = 'ne'
    bc1.categoryAxis.labels.dx = 8
    bc1.categoryAxis.labels.dy = -2
    bc1.categoryAxis.labels.angle = 30
    bc1.categoryAxis.categoryNames = plot_posts
    bc1.bars[0].fillColor = colors.HexColor("#9DD0F3")
    bc1.bars[1].fillColor = colors.HexColor("#F7CDAB")
    bc1.bars[2].fillColor = colors.HexColor("#DCBBF6")
    bc1.bars[3].fillColor = colors.HexColor("#E5F7AB")
    drawing1.add(bc1)
    drawing1.save()
    drawing_title1 = "Current Post Distribution"
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(560+Graph_scoot, 345+5, drawing_title1)
    x1, y1 = 410+Graph_scoot, 220
    renderPDF.draw(drawing1, c, x1, y1, showBoundary=False)
    c.setFont("Helvetica-Bold", 11, leading=None)
    c.setFillColor(colors.black)
    c.drawString(452+Graph_scoot, 230, 'Post Distribution Analysis:')
    c.setFont("Helvetica", 10, leading=None)
    Postavail_flagBase = []
    maxcolBase = kwcitemsBase_matrix5.max(axis=0, keepdims=True)
    mincolBase = kwcitemsBase_matrix5.min(axis=0, keepdims=True)
    for y_cols in range(np.shape(maxcolBase)[1]):
        if maxcolBase[0][y_cols] - mincolBase[0][y_cols] == 2:
            Postavail_flagBase += [plot_posts[y_cols]]
    c.drawString(454+Graph_scoot, 200, 'Posts That Are Not Balanced: ')
    Post_mover = 454+Graph_scoot + 135
    if np.shape(Postavail_flagBase)[0] != 0:
        for y_cols in range(np.shape(Postavail_flagBase)[0]):
            if np.shape(Postavail_flagBase)[0] == 1:
                c.drawString(Post_mover, 200, Postavail_flagBase[y_cols])
            elif np.shape(Postavail_flagBase)[0] > 1 and y_cols < np.shape(Postavail_flagBase)[0] - 1:
                c.drawString(Post_mover, 200, Postavail_flagBase[y_cols] + ', ')
            elif np.shape(Postavail_flagBase)[0] > 1 and y_cols == np.shape(Postavail_flagBase)[0] - 1:
                c.drawString(Post_mover, 200, Postavail_flagBase[y_cols])
            Post_mover += 30
    else:
        c.drawString(Post_mover, 200, 'None')
    LowPost_availsumBase = []
    for y_cols in range(2, np.shape(kwcitemsBase_matrix5)[1]):
        LowPost_availsumBasetemp = 0
        for x_rows in range(np.shape(kwcitemsBase_matrix5)[0]):
            LowPost_availsumBasetemp += kwcitemsBase_matrix5[x_rows][y_cols]
        LowPost_availsumBase += [LowPost_availsumBasetemp]
    LowPost_availBase = plot_posts[int(np.where(LowPost_availsumBase == np.min(LowPost_availsumBase))[0])+2]
    c.drawString(454+Graph_scoot, 215, 'Posts With Lowest Qualifications: ' + LowPost_availBase)
    #ERT and EMT Stuff------------------------------------------------------------------------------------
    drawing2 = Drawing(300, 150)
    data2 = kwcitemsBase_matrix5
    bc2 = VerticalBarChart()
    bc2.x = 50
    bc2.y = 50
    bc2.height = 75
    bc2.width = 200
    data2 = np.vstack((TeamBase_ERTTotal, TeamBase_EMTTotal))
    bc2.data = data2
    bc2.valueAxis.valueMin = 0
    bc2.valueAxis.valueMax = 5
    bc2.valueAxis.valueStep = 1
    bc2.categoryAxis.labels.boxAnchor = 'ne'
    bc2.categoryAxis.labels.dx = 8
    bc2.categoryAxis.labels.dy = -2
    bc2.categoryAxis.labels.angle = 0
    bc2.categoryAxis.categoryNames = ['A', 'B', 'C', 'D']
    bc2.bars[0].fillColor = colors.red
    bc2.bars[1].fillColor = colors.fidblue
    drawing2.add(bc2)
    drawing2.save()
    drawing_title2 = "Current ERT and EMT Distribution"
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(560+Graph_scoot, 170+5, drawing_title2)
    x1, y1 = 410+Graph_scoot, 45
    renderPDF.draw(drawing2, c, x1, y1, showBoundary=False)
    c.setFont("Helvetica-Bold", 11, leading=None)
    c.setFillColor(colors.black)
    c.drawString(452+Graph_scoot, 65, 'ERT and EMT Distribution Analysis:')
    if np.min(TeamBase_ERTTotal) < PostERT_availmin:
        ERT_comment = "Not Balanced"
        ERT_nudge = 2*len(ERT_comment) + 11
    elif np.min(TeamBase_ERTTotal) <= PostERT_availmin and np.max(TeamBase_ERTTotal) > PostERT_availmax:
        ERT_comment = "Not Balanced, But Okay"
        ERT_nudge = 2*len(ERT_comment) + 11
    else:
        ERT_comment = "Balanced"
        ERT_nudge = 2*len(ERT_comment) + 11
    c.setFont("Helvetica", 10, leading=None)
    c.drawString(454+Graph_scoot, 50, 'ERT ' + ERT_comment)
    if np.min(TeamBase_EMTTotal) < PostEMT_availmin:
        EMT_comment = "Not Balanced"
        EMT_nudge = 2*len(EMT_comment) + 11
    elif np.min(TeamBase_EMTTotal) <= PostEMT_availmin and np.max(TeamBase_EMTTotal) > PostEMT_availmax:
        EMT_comment = "Not Balanced, But Okay"
        EMT_nudge = 2*len(EMT_comment) + 11
    else:
        EMT_comment = "Balanced"
        EMT_nudge = 2*len(EMT_comment) + 11
    c.setFont("Helvetica", 10, leading=None)
    c.drawString(454+Graph_scoot, 35, 'EMT ' + EMT_comment)
    c.drawAlignedString(780, 10, 'Page 1')
    c.showPage()
    #pdf Page 2-----------------------------------------------------------------------------------
    Team_Header = "Team Name: " + TeamName_matrix[0][0] + "  Optimizer Run Date: "
    Header_title = Team_Header + str(pdfstart_datefinal)
    c.setFillColor(colors.firebrick)
    c.setFont("Helvetica", 16, leading=None)
    c.drawCentredString(400, Names_top+55, Header_title)
    OptTitle_Comment = 'Optimized Team Roster Information - '
    if np.sum(Team_CountTotal) < np.shape(BaseCAS_matrix)[0] or Flag_cannot == 1:
        OptTitle_Comment = 'Optimized Team Roster Same as Current Team Roster - '
    Page1_subheader = OptTitle_Comment + str(InfoTable_matrix[0][0])
    c.setFillColor(colors.blue)
    c.setFont("Helvetica", 14, leading=None)
    c.drawCentredString(400, Names_top+55-20, Page1_subheader)
    Page1_Teamhdrspace = 25
    Move_Page1_Teamhdr = 0
    Adj_TeamHdrwidth = 0
    plot_posts = list(BaseCAS_sdf.columns[4:np.shape(BaseCAS_matrix)[1]])
    c.setFont("Helvetica-Bold", 14, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(149, Names_top+10, 'Optimized Team Roster and Competency')
    c.setFillColor(colors.green)
    c.rect(x=10+Move_Page1_Teamhdr, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-67, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(28, Names_top+35-15-Page1_Teamhdrspace+2, "Team")
    c.setFillColor(colors.green)
    c.rect(x=46, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-67+50, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawString(28+22, Names_top+35-15-Page1_Teamhdrspace+2, "Name")
    c.setFillColor(colors.green)
    c.rect(x=133, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-79, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(145, Names_top+35-15-Page1_Teamhdrspace+2, "S/W")
    c.setFillColor(colors.green)
    c.rect(x=133+25, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-79, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(145+25, Names_top+35-15-Page1_Teamhdrspace+2, "ERT")
    c.setFillColor(colors.green)
    c.rect(x=133+25+25, y=Names_top+35-15-Page1_Teamhdrspace-1, width=Names_Spread-79, height=12, stroke=1, fill=1)
    c.setFont("Helvetica", 8, leading=None)
    c.setFillColor(colors.white)
    c.drawCentredString(145+25+25, Names_top+35-15-Page1_Teamhdrspace+2, "EMT")
    Move_Page1_Teamhdr = 75
    for Teamhdr in range(np.shape(plot_posts)[0]):
        c.setFillColor(colors.green)
        c.rect(x=133+Move_Page1_Teamhdr, y=Names_top+35-15-Page1_Teamhdrspace-1, width=25, height=12, stroke=1, fill=1)
        c.setFont("Helvetica", 8, leading=None)
        c.setFillColor(colors.white)
        c.drawCentredString(146+Move_Page1_Teamhdr, Names_top+35-15-Page1_Teamhdrspace+2, plot_posts[Teamhdr])
        Move_Page1_Teamhdr += 25
    c.setFont("Helvetica", 8, leading=None)
    count_BaseCAS = 0
    BaseCAS_matrix = BaseCAS_matrix[np.lexsort((BaseCAS_matrix[:, 5][::-1], BaseCAS_matrix[:, 4][::-1], BaseCAS_matrix[:, 0]))]
    BaseCAS_matrix = sorted(BaseCAS_matrix, key=lambda x:(x[5]), reverse=True)
    BaseCAS_matrix = sorted(BaseCAS_matrix, key=lambda x:(x[4]), reverse=True)
    BaseCAS_matrix = sorted(BaseCAS_matrix, key=lambda x:(x[0]))
    BaseCAS_matrix = np.array(BaseCAS_matrix)
    #Save the Optimized Matrix---------------------------------------------------------------------------
    erase_base = open("Base Data CAS Optimized.txt", "w+")
    erase_base.close()
    PostHeader1 = ["Team", "Name", "ERT Qual", "EMT Qual"]
    PostHeader2 = np.hstack((PostHeader1, Post_Headerinfo))
    with open("Base Data CAS Optimized.txt", 'a') as appFile:
        writer = csv.writer(appFile)
        writer.writerow(PostHeader2)
        appFile.close()
    for base_items in BaseCAS_matrix:
        if base_items[0] == 1:
            base_items[0] = "A"
        elif base_items[0] == 2:
            base_items[0] = "B"
        elif base_items[0] == 3:
            base_items[0] = "C"
        else:
            base_items[0] = "D"
        with open("Base Data CAS Optimized.txt", 'a') as appFile:
            writer = csv.writer(appFile)
            writer.writerow(base_items)
            appFile.close()
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][0] == 1:
            BaseCAS_matrix[x_rows][0] = "A"
        if BaseCAS_matrix[x_rows][0] == 2:
            BaseCAS_matrix[x_rows][0] = "B"
        if BaseCAS_matrix[x_rows][0] == 3:
            BaseCAS_matrix[x_rows][0] = "C"
        if BaseCAS_matrix[x_rows][0] == 4:
            BaseCAS_matrix[x_rows][0] = "D"
    if np.sum(Team_CountTotal) < np.shape(BaseCAS_matrix)[0] or Flag_cannot == 1:
        BaseCAS_matrix = np.array(Base_Base)
        kwcitems_matrix5 = kwcitemsBase_matrix5
        Team_CountTotal = TeamBase_CountTotal
        Team_postavg = BaseTeam_postavg
        Team_avg = Team_avgBase
        Team_ERTTotal = TeamBase_ERTTotal
        Team_EMTTotal = TeamBase_EMTTotal
        Flag_cannot = 1
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][0] == "A":
            c.setFillColorRGB(157/255, 208/255, 243/255)
        if BaseCAS_matrix[x_rows][0] == "B":
            c.setFillColorRGB(247/255, 205/255, 171/255)
        if BaseCAS_matrix[x_rows][0] == "C":
            c.setFillColorRGB(220/255, 187/255, 246/255)
        if BaseCAS_matrix[x_rows][0] == "D":
            c.setFillColorRGB(229/255, 247/255, 171/255)
        c.rect(x=10, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=Names_Spread-68, height=12, stroke=1, fill=1)
        c.setFillColor(colors.black)
        c.drawCentredString(28, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, BaseCAS_matrix[x_rows][0])
        count_BaseCAS += 12
    count_BaseCAS = 0
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][0] == "A":
            c.setFillColorRGB(157/255, 208/255, 243/255)
        if BaseCAS_matrix[x_rows][0] == "B":
            c.setFillColorRGB(247/255, 205/255, 171/255)
        if BaseCAS_matrix[x_rows][0] == "C":
            c.setFillColorRGB(220/255, 187/255, 246/255)
        if BaseCAS_matrix[x_rows][0] == "D":
            c.setFillColorRGB(229/255, 247/255, 171/255)
        c.rect(x=10+36, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=Names_Spread-88+71, height=12, stroke=1, fill=1)
        c.rect(x=10+36+87, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=25, height=12, stroke=1, fill=1)
        c.setFillColor(colors.black)
        c.drawString(28+22, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, BaseCAS_matrix[x_rows][1])
        if BaseCAS_matrix[x_rows][1] in Sal_matrix:
            Sal_Wage = "S"
        else:
            Sal_Wage = "W"
        c.drawCentredString(28+22+87+8, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, Sal_Wage)
        count_BaseCAS += 12
    count_BaseCAS = 0
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        colcount_BaseCAS = 87
        for y_cols in range(2, np.shape(BaseCAS_matrix)[1]):
            if BaseCAS_matrix[x_rows][0] == "A":
                c.setFillColorRGB(157/255, 208/255, 243/255)
            if BaseCAS_matrix[x_rows][0] == "B":
                c.setFillColorRGB(247/255, 205/255, 171/255)
            if BaseCAS_matrix[x_rows][0] == "C":
                c.setFillColorRGB(220/255, 187/255, 246/255)
            if BaseCAS_matrix[x_rows][0] == "D":
                c.setFillColorRGB(229/255, 247/255, 171/255)
            c.rect(x=10+36+colcount_BaseCAS+25, y=Names_top-15-count_BaseCAS-Page1_Teamhdrspace+23, width=25, height=12, stroke=1, fill=1)
            c.setFillColor(colors.black)
            if y_cols == 2 or y_cols == 3:
                if BaseCAS_matrix[x_rows][y_cols] == "Y":
                    BaseCAS_matrix[x_rows][y_cols] = "X"
                elif BaseCAS_matrix[x_rows][y_cols] == "N":
                    BaseCAS_matrix[x_rows][y_cols] = ""
            if y_cols >= 4 and BaseCAS_matrix[x_rows][y_cols] == 0:
                BaseCAS_matrix[x_rows][y_cols] = ""
            c.drawCentredString(22+37+colcount_BaseCAS+25, Names_top-15-count_BaseCAS+1-Page1_Teamhdrspace+24, str(BaseCAS_matrix[x_rows][y_cols]))
            colcount_BaseCAS += 25
        count_BaseCAS += 12
    c.setFont("Helvetica-Bold", 11, leading=None)
    c.setFillColor(colors.black)
    c.drawString(452+Graph_scoot, 420, 'Competency Analysis:')
    c.setFont("Helvetica", 10, leading=None)
    New_min = np.min(Team_postavg)
    if round(New_min, 1) > round(Base_min, 1):
        Min_comment = " (Improved)"
        Nudge_min = 2*len(" (Improved)") + 3
    elif round(New_min, 1) == round(Base_min, 1):
        Min_comment = " (No Change)"
        Nudge_min = 2*len(" (No Change)") + 6
    elif round(New_min, 1) < round(Base_min, 1):
        Min_comment = " (Worse)"
        Nudge_min = 2*len(" (Worse)") + 3
    c.drawString(454+Graph_scoot, 405, 'Minimum Post Avg Competency: ' + str(round(New_min, 1)) + Min_comment)
    New_max = np.max(Team_postavg)
    if round(New_max, 1) < round(Base_max, 1):
        Max_comment = " (Improved)"
        Nudge_max = 2*len(" (Improved)") + 3
    elif round(New_max, 1) == round(Base_max, 1):
        Max_comment = " (No Change)"
        Nudge_max = 2*len(" (No Change)") + 6
    elif round(New_max, 1) > round(Base_max, 1):
        Max_comment = " (Worse)"
        Nudge_max = 2*len(" (Worse)") + 3
    c.drawString(454+Graph_scoot, 390, 'Maximum Post Avg Competency: ' + str(round(New_max, 1))+ Max_comment)
    New_stddev = np.std(Team_postavg, axis=0)
    New_Sum_stddev = np.sum(New_stddev)
    New_avg_stddev = New_Sum_stddev**.5
    if round(New_avg_stddev, 1) < round(Avg_stddev, 1):
        Std_comment = " (Improved)"
        Nudge_std = 2*len(" (Improved)") + 3
    elif round(New_avg_stddev, 1) == round(Avg_stddev, 1):
        Std_comment = " (No Change)"
        Nudge_std = 2*len(" (No Change)") + 6
    elif round(New_avg_stddev, 1) > round(Avg_stddev, 1):
        Std_comment = " (Worse)"
        Nudge_std = 2*len(" (Worse)") + 3
    c.drawString(454+Graph_scoot, 375, 'Overall Competency Standard Deviation: ' + str(round(New_avg_stddev, 1)) \
                 + Std_comment)
    #Comptency Chart Current Roster
    drawing = Drawing(300, 150)
    data = Team_postavg
    bc = VerticalBarChart()
    bc.x = 50
    bc.y = 50
    bc.height = 75
    bc.width = 200
    bc.data = data
    #bc.strokeColor = colors.black
    bc.valueAxis.valueMin = 0
    bc.valueAxis.valueMax = 3
    bc.valueAxis.valueStep = 1
    bc.categoryAxis.labels.boxAnchor = 'ne'
    bc.categoryAxis.labels.dx = 8
    bc.categoryAxis.labels.dy = -2
    bc.categoryAxis.labels.angle = 30
    bc.categoryAxis.categoryNames = plot_posts
    bc.bars[0].fillColor = colors.HexColor("#9DD0F3")
    bc.bars[1].fillColor = colors.HexColor("#F7CDAB")
    bc.bars[2].fillColor = colors.HexColor("#DCBBF6")
    bc.bars[3].fillColor = colors.HexColor("#E5F7AB")
    drawing.add(bc)
    drawing.save()
    drawing_title = "Optimized Competency Distribution"
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(560+Graph_scoot, 530+5, drawing_title)
    x, y = 410+Graph_scoot, 405
    renderPDF.draw(drawing, c, x, y, showBoundary=False)
    #Post Distribution Chart Current Roster
    drawing1 = Drawing(300, 150)
    data1 = kwcitems_matrix5
    bc1 = VerticalBarChart()
    bc1.x = 50
    bc1.y = 50
    bc1.height = 75
    bc1.width = 200
    bc1.data = data1
    #bc.strokeColor = colors.black
    bc1.valueAxis.valueMin = 0
    bc1.valueAxis.valueMax = 5
    bc1.valueAxis.valueStep = 1
    bc1.categoryAxis.labels.boxAnchor = 'ne'
    bc1.categoryAxis.labels.dx = 8
    bc1.categoryAxis.labels.dy = -2
    bc1.categoryAxis.labels.angle = 30
    bc1.categoryAxis.categoryNames = plot_posts
    bc1.bars[0].fillColor = colors.HexColor("#9DD0F3")
    bc1.bars[1].fillColor = colors.HexColor("#F7CDAB")
    bc1.bars[2].fillColor = colors.HexColor("#DCBBF6")
    bc1.bars[3].fillColor = colors.HexColor("#E5F7AB")
    drawing1.add(bc1)
    drawing1.save()
    drawing_title1 = "Optimized Post Distribution"
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(560+Graph_scoot, 345+5, drawing_title1)
    x1, y1 = 410+Graph_scoot, 220
    renderPDF.draw(drawing1, c, x1, y1, showBoundary=False)
    c.setFont("Helvetica-Bold", 11, leading=None)
    c.setFillColor(colors.black)
    c.drawString(452+Graph_scoot, 230, 'Post Distribution Analysis:')
    c.setFont("Helvetica", 10, leading=None)
    Postavail_flagNew = []
    maxcolBase = kwcitems_matrix5.max(axis=0, keepdims=True)
    mincolBase = kwcitems_matrix5.min(axis=0, keepdims=True)
    for y_cols in range(np.shape(maxcolBase)[1]):
        if maxcolBase[0][y_cols] - mincolBase[0][y_cols] >= 2:
            Postavail_flagNew += [plot_posts[y_cols]]
    c.drawString(454+Graph_scoot, 200, 'Posts That Are Not Balanced: ')
    Post_mover = 454+Graph_scoot + 135
    if np.shape(Postavail_flagNew)[0] != 0:
        for y_cols in range(np.shape(Postavail_flagNew)[0]):
            if np.shape(Postavail_flagNew)[0] == 1:
                c.drawString(Post_mover, 200, Postavail_flagNew[y_cols])
            elif np.shape(Postavail_flagNew)[0] > 1 and y_cols < np.shape(Postavail_flagNew)[0] - 1:
                c.drawString(Post_mover, 200, Postavail_flagNew[y_cols] + ', ')
            elif np.shape(Postavail_flagNew)[0] > 1 and y_cols == np.shape(Postavail_flagNew)[0] - 1:
                c.drawString(Post_mover, 200, Postavail_flagNew[y_cols])
            Post_mover += 30
    else:
        c.drawString(Post_mover, 200, 'None')
    LowPost_availsum = []
    for y_cols in range(2, np.shape(kwcitems_matrix5)[1]):
        LowPost_availsumtemp = 0
        for x_rows in range(np.shape(kwcitems_matrix5)[0]):
            LowPost_availsumtemp += kwcitems_matrix5[x_rows][y_cols]
        LowPost_availsum += [LowPost_availsumtemp]
    LowPost_avail = plot_posts[int(np.where(LowPost_availsum == np.min(LowPost_availsum))[0])+2]
    c.drawString(454+Graph_scoot, 215, 'Posts With Lowest Qualifications: ' + LowPost_avail)
    #ERT and EMT Stuff------------------------------------------------------------------------------------
    drawing2 = Drawing(300, 150)
    data2 = kwcitemsBase_matrix5
    bc2 = VerticalBarChart()
    bc2.x = 50
    bc2.y = 50
    bc2.height = 75
    bc2.width = 200
    data2 = np.vstack((Team_ERTTotal, Team_EMTTotal))
    bc2.data = data2
    bc2.valueAxis.valueMin = 0
    bc2.valueAxis.valueMax = 5
    bc2.valueAxis.valueStep = 1
    bc2.categoryAxis.labels.boxAnchor = 'ne'
    bc2.categoryAxis.labels.dx = 8
    bc2.categoryAxis.labels.dy = -2
    bc2.categoryAxis.labels.angle = 0
    bc2.categoryAxis.categoryNames = ['A', 'B', 'C', 'D']
    bc2.bars[0].fillColor = colors.red
    bc2.bars[1].fillColor = colors.fidblue
    drawing2.add(bc2)
    drawing2.save()
    drawing_title2 = "Optimized ERT and EMT Distribution"
    c.setFont("Helvetica-Bold", 10, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(560+Graph_scoot, 170+5, drawing_title2)
    x1, y1 = 410+Graph_scoot, 45
    renderPDF.draw(drawing2, c, x1, y1, showBoundary=False)
    c.setFont("Helvetica-Bold", 11, leading=None)
    c.setFillColor(colors.black)
    c.drawString(452+Graph_scoot, 65, 'ERT and EMT Distribution Analysis:')
    if np.min(Team_ERTTotal) < PostERT_availmin:
        ERT_comment = "Not Balanced"
        ERT_nudge = 2*len(ERT_comment) + 11
    elif np.min(Team_ERTTotal) <= PostERT_availmin and np.max(Team_ERTTotal) > PostERT_availmax:
        ERT_comment = "Not Balanced, But Okay"
        ERT_nudge = 2*len(ERT_comment) + 11
    else:
        ERT_comment = "Balanced"
        ERT_nudge = 2*len(ERT_comment) + 11
    c.setFont("Helvetica", 10, leading=None)
    c.drawString(454+Graph_scoot, 50, 'ERT ' + ERT_comment)
    if np.min(Team_EMTTotal) < PostEMT_availmin:
        EMT_comment = "Not Balanced"
        EMT_nudge = 2*len(EMT_comment) + 11
    elif np.min(Team_EMTTotal) <= PostEMT_availmin and np.max(Team_EMTTotal) > PostEMT_availmax:
        EMT_comment = "Not Balanced, But Okay"
        EMT_nudge = 2*len(EMT_comment) + 11
    else:
        EMT_comment = "Balanced"
        EMT_nudge = 2*len(EMT_comment) + 11
    c.setFont("Helvetica", 10, leading=None)
    c.drawString(454+Graph_scoot, 35, 'EMT ' + EMT_comment)
    c.drawAlignedString(780, 10, 'Page 2')
    c.showPage()
    #pdf Page 3-----------------------------------------------------------------------------------
    Team_Header = "Team Name: " + TeamName_matrix[0][0] + "  Optimizer Run Date: "
    Header_title = Team_Header + str(pdfstart_datefinal)
    c.setFillColor(colors.firebrick)
    c.setFont("Helvetica", 16, leading=None)
    c.drawCentredString(400, Names_top+55, Header_title)
    Page1_subheader = "Overall Anaysis and Recommendations - " + str(InfoTable_matrix[0][0])
    c.setFillColor(colors.blue)
    c.setFont("Helvetica", 14, leading=None)
    c.drawCentredString(400, Names_top+55-20, Page1_subheader)
    c.setFont("Helvetica-Bold", 14, leading=None)
    c.setFillColor(colors.black)
    c.drawCentredString(80, Names_top+10, 'Current Team Roster')
    c.setFillColor(colors.black)
    c.rect(x=10, y=30, width=770/2, height=500, stroke=1, fill=0)
    c.setFont("Helvetica-Bold", 12, leading=None)
    c.drawString(15, Names_top-10, 'Team Pairing Analysis:')
    #Analyze Team Pairs------------------------------------------------
    if InfoTable_matrix[0, 0] == "DuPont Schedule":
        shift_table = pd.read_csv('ShiftDupont.txt', delimiter=',')
    elif InfoTable_matrix[0, 0] == "4on/4off Schedule":
        shift_table = pd.read_csv('ShiftFourFour.txt', delimiter=',')
    elif InfoTable_matrix[0, 0] == "EOWEO Schedule":
        shift_table = pd.read_csv('ShiftEOWEO.txt', delimiter=',')
    elif InfoTable_matrix[0, 0] == "7on/7off Schedule":
        shift_table = pd.read_csv('7on/7off Schedule', delimiter=',')
    elif InfoTable_matrix[0, 0] == "Four/Five Schedule":
        shift_table = pd.read_csv('ShiftFourFive.txt', delimiter=',')
    sdf = pd.DataFrame(shift_table)
    sdf_matrix = np.array(sdf)
    if InfoTable_matrix[0, 0] != "": # "DuPont Schedule":
        AB_shifts = 0
        AC_shifts = 0
        AD_shifts = 0
        BC_shifts = 0
        BD_shifts = 0
        CD_shifts = 0
        for x_rows in range(14):
            if (sdf_matrix[x_rows][1] == "A" and sdf_matrix[x_rows][2] == "B") or \
                    (sdf_matrix[x_rows][2] == "A" and sdf_matrix[x_rows][1] == "B"):
                AB_shifts += 1
            elif (sdf_matrix[x_rows][1] == "A" and sdf_matrix[x_rows][2] == "C") or \
                    (sdf_matrix[x_rows][2] == "A" and sdf_matrix[x_rows][1] == "C"):
                AC_shifts += 1
            elif (sdf_matrix[x_rows][1] == "A" and sdf_matrix[x_rows][2] == "D") or \
                    (sdf_matrix[x_rows][2] == "A" and sdf_matrix[x_rows][1] == "D"):
                AD_shifts += 1
            elif (sdf_matrix[x_rows][1] == "B" and sdf_matrix[x_rows][2] == "C") or \
                    (sdf_matrix[x_rows][2] == "B" and sdf_matrix[x_rows][1] == "C"):
                BC_shifts += 1
            elif (sdf_matrix[x_rows][1] == "B" and sdf_matrix[x_rows][2] == "D") or \
                    (sdf_matrix[x_rows][2] == "B" and sdf_matrix[x_rows][1] == "D"):
                BD_shifts += 1
            elif (sdf_matrix[x_rows][1] == "C" and sdf_matrix[x_rows][2] == "D") or \
                    (sdf_matrix[x_rows][2] == "C" and sdf_matrix[x_rows][1] == "D"):
                CD_shifts += 1
        Pair_list = np.hstack((AB_shifts, AC_shifts, AD_shifts, BC_shifts, BD_shifts, CD_shifts))
        Pair_avail = []
        Pair_compavg = []
        for a_loop in range(1, 4):
            Pair_availtemp = []
            Pair_compavgtemp = []
            for y_cols in range(np.shape(kwcitemsBase_matrix5)[1]):
                Pair_availtemp += [np.sum(kwcitemsBase_matrix5[0][y_cols]) + np.sum(kwcitemsBase_matrix5[a_loop][y_cols])]
                #Pair_compavgtemp += [round((np.sum(kwcitemsBase_matrix5[0][y_cols]) * BaseTeam_postavg[0][y_cols] \
                #                            + np.sum(kwcitemsBase_matrix5[a_loop][y_cols]) * BaseTeam_postavg[a_loop][y_cols]) \
                #                           / (np.sum(kwcitemsBase_matrix5[0][y_cols]) + np.sum(kwcitemsBase_matrix5[a_loop][y_cols])), 2)]
                Pair_compavgtemp += [(Team_avg[0] + Team_avg[a_loop]) / 2]
            if np.shape(Pair_avail)[0] == 0:
                Pair_avail = Pair_availtemp
                Pair_compavg = Pair_compavgtemp
            else:
                Pair_avail = np.vstack((Pair_avail, Pair_availtemp))
                Pair_compavg = np.vstack((Pair_compavg, Pair_compavgtemp))
        for b_loop in range(2, 4):
            Pair_availtemp = []
            Pair_compavgtemp = []
            for y_cols in range(np.shape(kwcitemsBase_matrix5)[1]):
                Pair_availtemp += [np.sum(kwcitemsBase_matrix5[1][y_cols]) + np.sum(kwcitemsBase_matrix5[b_loop][y_cols])]
                #Pair_compavgtemp += [round((np.sum(kwcitemsBase_matrix5[1][y_cols]) * BaseTeam_postavg[1][y_cols] \
                #                            + np.sum(kwcitemsBase_matrix5[b_loop][y_cols]) * BaseTeam_postavg[b_loop][y_cols]) \
                #                           / (np.sum(kwcitemsBase_matrix5[1][y_cols]) + np.sum(kwcitemsBase_matrix5[b_loop][y_cols])), 2)]
                Pair_compavgtemp += [(Team_avg[1] + Team_avg[b_loop]) / 2]
            Pair_avail = np.vstack((Pair_avail, Pair_availtemp))
            Pair_compavg = np.vstack((Pair_compavg, Pair_compavgtemp))
        for c_loop in range(3, 4):
            Pair_availtemp = []
            Pair_compavgtemp = []
            for y_cols in range(np.shape(kwcitemsBase_matrix5)[1]):
                Pair_availtemp += [np.sum(kwcitemsBase_matrix5[2][y_cols]) + np.sum(kwcitemsBase_matrix5[c_loop][y_cols])]
                #Pair_compavgtemp += [round((np.sum(kwcitemsBase_matrix5[2][y_cols]) * BaseTeam_postavg[2][y_cols] \
                #                            + np.sum(kwcitemsBase_matrix5[c_loop][y_cols]) * BaseTeam_postavg[c_loop][y_cols]) \
                #                           / (np.sum(kwcitemsBase_matrix5[2][y_cols]) + np.sum(kwcitemsBase_matrix5[c_loop][y_cols])), 2)]
                Pair_compavgtemp += [(Team_avg[2] + Team_avg[c_loop]) / 2]
            Pair_avail = np.vstack((Pair_avail, Pair_availtemp))
            Pair_compavg = np.vstack((Pair_compavg, Pair_compavgtemp))
    Pair_availsum = []
    Pair_compsum = []
    PairTeam_list = ["AB", "AC", "AD", "BC", "BD", "CD"]
    for x_rows in range(np.shape(Pair_avail)[0]):
        Pair_availsum += [np.sum(Pair_avail[x_rows])]
        Pair_compsum += [round(np.average(Pair_compavg[x_rows]), 3)]
    LowPair_avail = np.where(Pair_availsum == np.min(Pair_availsum))
    if np.shape(LowPair_avail)[1] == 1:
        LowPair_team = PairTeam_list[int(LowPair_avail[0])]
        LowPair_team2 = PairTeam_list[int(LowPair_avail[0])]
        PairTeam_end = ""
        LowPair_comp = np.where(Pair_compsum == np.min(Pair_compsum))
        LowPair_teamcomp = PairTeam_list[int(LowPair_comp[0][0])]
        if Pair_list[int(LowPair_avail[0])] == 1:
            PairTeam_flag = "Minimizes"
            PairTeam_end = " (Good!)"
        else:
            PairTeam_flag = "Does Not Mimimize"
    else:
        LowPair_team = " >1 Team"
        PairTeam_flag = "Minimizes"
        PairTeam_end = ' (Good!)'
        LowPair_teamcomp = ' >1 Team'
        LowPair_team2 = "Low"
    c.setFillColor(colors.black)
    c.setFont("Helvetica", 12, leading=None)
    c.drawString(35, Names_top-10-15, 'Lowest Team Pair Qualifications: ' + LowPair_team)
    c.drawString(35, Names_top-10-30, 'Current Schedule ' + PairTeam_flag + ' ' + LowPair_team2 + ' Team Pairing' + PairTeam_end)
    c.drawString(35, Names_top-10-45, 'Lowest Team Pair Competency Avg: ' + LowPair_teamcomp + ' (' + \
                 str(round(np.min(Pair_compsum), 1)) + ')')
    HighPair_comp = np.where(Pair_compsum == np.max(Pair_compsum))
    HighPair_teamcomp = PairTeam_list[int(HighPair_comp[0][0])]
    c.drawString(35, Names_top-10-60, 'Highest Team Pair Competency Avg: ' + HighPair_teamcomp + ' (' + \
                 str(round(np.max(Pair_compsum), 1)) + ')')
    c.setFont("Helvetica-Bold", 12, leading=None)
    c.drawString(35, Names_top-10-75, 'Suggestions:')
    c.setFont("Helvetica", 12, leading=None)
    PairTeam_sugg = "None"
    if PairTeam_end != " (Good!)":
        if InfoTable_matrix[0][0] == "DuPont Schedule":
            PairTeam_sugg = "Rearrange Team Schedule: " + LowPair_team + " should be paired on Monday"
        elif LowPair_team in Team_Pairing_Schd:
            PairTeam_sugg = "Rearrange Team Schedule: " + LowPair_team + " should not be paired together"
        else:
            PairTeam_sugg = "None"
    c.drawString(55, Names_top-10-90, PairTeam_sugg)
    c.setFont("Helvetica-Bold", 12, leading=None)
    #Analyze Team Specific---------------------------------------------------------------------
    c.drawString(15, Names_top-10-120, 'Team Specific Analysis:')
    c.setFillColor(colors.black)
    Base_belowminavail = np.where(kwcitemsBase_matrix5 < 2)
    if np.shape(Base_belowminavail)[1] == 0:
        Team_availstuff = "All teams meet minimum qualifications for each post (Good!)"
    else:
        Team_availstuff = "Not all teams meet minimum qualificaitons for each post"
    c.setFont("Helvetica", 12, leading=None)
    c.drawString(35, Names_top-10-135, Team_availstuff)
    c.drawString(35, Names_top-10-150, "A Team Competency Avg: " + str(Team_avgBase[0]))
    c.drawString(35, Names_top-10-165, "B Team Competency Avg: " + str(Team_avgBase[1]))
    c.drawString(35, Names_top-10-180, "C Team Competency Avg: " + str(Team_avgBase[2]))
    c.drawString(35, Names_top-10-195, "D Team Competency Avg: " + str(Team_avgBase[3]))
    Outside_LTA3Base = []
    Outside_LowCompBase = []
    LowPostavail_number = int(np.where(LowPost_availsumBase == np.min(LowPost_availsumBase))[0])+6
    UnBalPost_Base = 0
    for y_cols in range(np.shape(plot_posts)[0]):
        if np.shape(Postavail_flagBase)[0] != 0:
            if plot_posts[y_cols] == Postavail_flagBase[0]:
                UnBalPost_Base = y_cols
    for x_rows in range(np.shape(BaseCAS_matrixBase)[0]):
        LTA_qual = 0
        LTA_compsum = 0
        LTA_compcount = 0
        Outside_LTA3Basetemp = []
        Outside_LowCompBasetemp = []
        if BaseCAS_matrixBase[x_rows][1] not in Sal_matrix and BaseCAS_matrixBase[x_rows][5] == "":
            for y_cols in range(4, np.shape(BaseCAS_matrixBase)[1]):
                if BaseCAS_matrixBase[x_rows][y_cols] != "":
                    LTA_qual += 1
                    LTA_compsum += BaseCAS_matrixBase[x_rows][y_cols]
                    LTA_compcount += 1
            if LTA_qual < 3:
                if BaseCAS_matrixBase[x_rows][LowPostavail_number] == "":
                    Outside_LTA3Basetemp = [BaseCAS_matrixBase[x_rows][0], BaseCAS_matrixBase[x_rows][1], \
                                            plot_posts[LowPostavail_number-4]]
                elif BaseCAS_matrixBase[x_rows][UnBalPost_Base+4] == "":
                    Outside_LTA3Basetemp = [BaseCAS_matrixBase[x_rows][0], BaseCAS_matrixBase[x_rows][1], \
                                            plot_posts[UnBalPost_Base]]
                else:
                    Outside_LTA3Basetemp = [BaseCAS_matrixBase[x_rows][0], BaseCAS_matrixBase[x_rows][1], '']
                if np.shape(Outside_LTA3Base)[0] == 0:
                    Outside_LTA3Base = Outside_LTA3Basetemp
                elif Outside_LTA3Basetemp not in Outside_LTA3Base:
                    Outside_LTA3Base = np.vstack((Outside_LTA3Base, Outside_LTA3Basetemp))
            if LTA_compsum/LTA_compcount < 1.5:
                Outside_LowCompBasetemp = [BaseCAS_matrixBase[x_rows][0], BaseCAS_matrixBase[x_rows][1]]
                if np.shape(Outside_LowCompBase)[0] == 0:
                    Outside_LowCompBase = Outside_LowCompBasetemp
                elif Outside_LowCompBasetemp not in Outside_LowCompBase:
                    Outside_LowCompBase = np.vstack((Outside_LowCompBase, Outside_LowCompBasetemp))
    c.drawString(35, Names_top-10-210, 'Personnel With Less Than 3 Qualifications: ')
    LTA3_nudge = 0
    for x_rows in range(np.shape(Outside_LTA3Base)[0]):
        c.drawString(35+20, Names_top-10-225-LTA3_nudge, Outside_LTA3Base[x_rows][0] + " Team: " + Outside_LTA3Base[x_rows][1])
        LTA3_nudge += 15
    c.drawString(35, Names_top-10-225-LTA3_nudge, 'Personnel With Competency Less Than 1.5: ')
    for x_rows in range(np.shape(Outside_LowCompBase)[0]):
        c.drawString(35+20, Names_top-10-240-LTA3_nudge, Outside_LowCompBase[x_rows][0] + " Team: " + Outside_LowCompBase[x_rows][1])
        LTA3_nudge += 15
    c.drawString(35, Names_top-10-240-LTA3_nudge, 'Posts With Lowest Qualifications: ' + LowPost_availBase)
    c.setFont("Helvetica-Bold", 12, leading=None)
    c.drawString(35, Names_top-10-255-LTA3_nudge, 'Suggestions: ')
    c.setFont("Helvetica", 12, leading=None)
    if np.shape(Outside_LowCompBase)[0] > 0:
        for x_rows in range(np.shape(Outside_LowCompBase)[0]):
            c.drawString(55, Names_top-10-270-LTA3_nudge, 'Improve ' + Outside_LowCompBase[x_rows][1] + ' Competency')
            LTA3_nudge += 15
    if np.shape(Outside_LTA3Base)[0] > 0:
        for x_rows in range(np.shape(Outside_LTA3Base)[0]):
            c.drawString(55, Names_top-10-270-LTA3_nudge, 'Train ' + Outside_LTA3Base[x_rows][1] + ' on Post: ' + Outside_LTA3Base[x_rows][2])
            LTA3_nudge += 15
    if np.shape(Outside_LowCompBase)[0] == 0 and np.shape(Outside_LTA3Base)[0] == 0:
        c.drawString(55, Names_top-10-270-LTA3_nudge, 'None')
    #Page 3 Right Hand Side of Page-----------------------------------------------
    c.setFont("Helvetica-Bold", 14, leading=None)
    c.setFillColor(colors.black)
    c.drawString(400, Names_top+10, 'Optimized Team Roster')
    c.setFillColor(colors.black)
    c.rect(x=770/2+10, y=30, width=770/2, height=500, stroke=1, fill=0)
    c.setFont("Helvetica-Bold", 12, leading=None)
    c.drawString(770/2+15, Names_top-10, 'Team Pairing Analysis:')
    #Analyze Team Pairs------------------------------------------------
    if InfoTable_matrix[0, 0] == "DuPont Schedule":
        shift_table = pd.read_csv('ShiftDupont.txt', delimiter=',')
    elif InfoTable_matrix[0, 0] == "4on/4off Schedule":
        shift_table = pd.read_csv('ShiftFourFour.txt', delimiter=',')
    elif InfoTable_matrix[0, 0] == "EOWEO Schedule":
        shift_table = pd.read_csv('ShiftEOWEO.txt', delimiter=',')
    elif InfoTable_matrix[0, 0] == "7on/7off Schedule":
        shift_table = pd.read_csv('7on/7off Schedule', delimiter=',')
    elif InfoTable_matrix[0, 0] == "Four/Five Schedule":
        shift_table = pd.read_csv('ShiftFourFive.txt', delimiter=',')
    sdf = pd.DataFrame(shift_table)
    sdf_matrix = np.array(sdf)
    if InfoTable_matrix[0, 0] != "": # "DuPont Schedule":
        AB_shifts = 0
        AC_shifts = 0
        AD_shifts = 0
        BC_shifts = 0
        BD_shifts = 0
        CD_shifts = 0
        for x_rows in range(14):
            if (sdf_matrix[x_rows][1] == "A" and sdf_matrix[x_rows][2] == "B") or \
                    (sdf_matrix[x_rows][2] == "A" and sdf_matrix[x_rows][1] == "B"):
                AB_shifts += 1
            elif (sdf_matrix[x_rows][1] == "A" and sdf_matrix[x_rows][2] == "C") or \
                    (sdf_matrix[x_rows][2] == "A" and sdf_matrix[x_rows][1] == "C"):
                AC_shifts += 1
            elif (sdf_matrix[x_rows][1] == "A" and sdf_matrix[x_rows][2] == "D") or \
                    (sdf_matrix[x_rows][2] == "A" and sdf_matrix[x_rows][1] == "D"):
                AD_shifts += 1
            elif (sdf_matrix[x_rows][1] == "B" and sdf_matrix[x_rows][2] == "C") or \
                    (sdf_matrix[x_rows][2] == "B" and sdf_matrix[x_rows][1] == "C"):
                BC_shifts += 1
            elif (sdf_matrix[x_rows][1] == "B" and sdf_matrix[x_rows][2] == "D") or \
                    (sdf_matrix[x_rows][2] == "B" and sdf_matrix[x_rows][1] == "D"):
                BD_shifts += 1
            elif (sdf_matrix[x_rows][1] == "C" and sdf_matrix[x_rows][2] == "D") or \
                    (sdf_matrix[x_rows][2] == "C" and sdf_matrix[x_rows][1] == "D"):
                CD_shifts += 1
        Pair_list = np.hstack((AB_shifts, AC_shifts, AD_shifts, BC_shifts, BD_shifts, CD_shifts))
        Pair_avail = []
        Pair_compavg = []
        for a_loop in range(1, 4):
            Pair_availtemp = []
            Pair_compavgtemp = []
            for y_cols in range(np.shape(kwcitems_matrix5)[1]):
                Pair_availtemp += [np.sum(kwcitems_matrix5[0][y_cols]) + np.sum(kwcitems_matrix5[a_loop][y_cols])]
                #Pair_compavgtemp += [round((np.sum(kwcitems_matrix5[0][y_cols]) * Team_postavg[0][y_cols] \
                #                            + np.sum(kwcitems_matrix5[a_loop][y_cols]) * Team_postavg[a_loop][y_cols]) \
                #                           / (np.sum(kwcitems_matrix5[0][y_cols]) + np.sum(kwcitems_matrix5[a_loop][y_cols])), 2)]
                Pair_compavgtemp += [(Team_avg[0] + Team_avg[a_loop]) / 2]
            if np.shape(Pair_avail)[0] == 0:
                Pair_avail = Pair_availtemp
                Pair_compavg = Pair_compavgtemp
            else:
                Pair_avail = np.vstack((Pair_avail, Pair_availtemp))
                Pair_compavg = np.vstack((Pair_compavg, Pair_compavgtemp))
        for b_loop in range(2, 4):
            Pair_availtemp = []
            Pair_compavgtemp = []
            for y_cols in range(np.shape(kwcitems_matrix5)[1]):
                Pair_availtemp += [np.sum(kwcitems_matrix5[1][y_cols]) + np.sum(kwcitems_matrix5[b_loop][y_cols])]
                #Pair_compavgtemp += [round((np.sum(kwcitems_matrix5[1][y_cols]) * Team_postavg[1][y_cols] \
                #                            + np.sum(kwcitems_matrix5[b_loop][y_cols]) * Team_postavg[b_loop][y_cols]) \
                #                           / (np.sum(kwcitems_matrix5[1][y_cols]) + np.sum(kwcitems_matrix5[b_loop][y_cols])), 2)]
                Pair_compavgtemp += [(Team_avg[1] + Team_avg[b_loop]) / 2]
            Pair_avail = np.vstack((Pair_avail, Pair_availtemp))
            Pair_compavg = np.vstack((Pair_compavg, Pair_compavgtemp))
        for c_loop in range(3, 4):
            Pair_availtemp = []
            Pair_compavgtemp = []
            for y_cols in range(np.shape(kwcitems_matrix5)[1]):
                Pair_availtemp += [np.sum(kwcitems_matrix5[2][y_cols]) + np.sum(kwcitems_matrix5[c_loop][y_cols])]
                #Pair_compavgtemp += [round((np.sum(kwcitems_matrix5[2][y_cols]) * Team_postavg[2][y_cols] \
                #                            + np.sum(kwcitems_matrix5[c_loop][y_cols]) * Team_postavg[c_loop][y_cols]) \
                #                           / (np.sum(kwcitems_matrix5[2][y_cols]) + np.sum(kwcitems_matrix5[c_loop][y_cols])), 2)]
                Pair_compavgtemp += [(Team_avg[2] + Team_avg[c_loop]) / 2]
            Pair_avail = np.vstack((Pair_avail, Pair_availtemp))
            Pair_compavg = np.vstack((Pair_compavg, Pair_compavgtemp))
    Pair_availsum = []
    Pair_compsum = []
    PairTeam_list = ["AB", "AC", "AD", "BC", "BD", "CD"]
    for x_rows in range(np.shape(Pair_avail)[0]):
        Pair_availsum += [np.sum(Pair_avail[x_rows])]
        Pair_compsum += [round(np.average(Pair_compavg[x_rows]), 3)]
    LowPair_avail = np.where(Pair_availsum == np.min(Pair_availsum))
    if np.shape(LowPair_avail)[1] > 1:
        LowPair_avail = [LowPair_avail[0][1]]
    LowPair_team = PairTeam_list[int(LowPair_avail[0])]
    PairTeam_end = ""
    if Pair_list[int(LowPair_avail[0])] == 1:
        PairTeam_flag = "Minimizes"
        PairTeam_end = " (Good!)"
    else:
        PairTeam_flag = "Does Not Mimimize"
    c.setFillColor(colors.black)
    c.setFont("Helvetica", 12, leading=None)
    c.drawString(770/2 + 35, Names_top-10-15, 'Lowest Team Pair Qualifications: ' + LowPair_team)
    c.drawString(770/2 + 35, Names_top-10-30, 'Current Schedule ' + PairTeam_flag + ' ' + LowPair_team + ' Team Pairing' + PairTeam_end)
    LowPair_comp = np.where(Pair_compsum == np.min(Pair_compsum))
    LowPair_teamcomp = PairTeam_list[int(LowPair_comp[0][0])]
    c.drawString(770/2+35, Names_top-10-45, 'Lowest Team Pair Competency Avg: ' + LowPair_teamcomp + ' (' + \
                 str(round(np.min(Pair_compsum), 1)) + ')')
    HighPair_comp = np.where(Pair_compsum == np.max(Pair_compsum))
    HighPair_teamcomp = PairTeam_list[int(HighPair_comp[0][0])]
    c.drawString(770/2+35, Names_top-10-60, 'Highest Team Pair Competency Avg: ' + HighPair_teamcomp + ' (' + \
                 str(round(np.max(Pair_compsum), 1)) + ')')
    c.setFont("Helvetica-Bold", 12, leading=None)
    c.drawString(770/2+35, Names_top-10-75, 'Suggestions:')
    c.setFont("Helvetica", 12, leading=None)
    PairTeam_sugg = "None"
    if PairTeam_end != " (Good!)":
        if InfoTable_matrix[0][0] == "DuPont Schedule":
            PairTeam_sugg = "Rearrange Team Schedule: " + LowPair_team + " should be paired on Monday"
        elif LowPair_team in Team_Pairing_Schd:
            PairTeam_sugg = "Rearrange Team Schedule: " + LowPair_team + " should not be paired together"
        else:
            PairTeam_sugg = "None"
    c.drawString(770/2+55, Names_top-10-90, PairTeam_sugg)
    c.setFont("Helvetica-Bold", 12, leading=None)
    #Analyze Team Specific---------------------------------------------------------------------
    c.drawString(770/2+15, Names_top-10-120, 'Team Specific Analysis:')
    c.setFillColor(colors.black)
    Base_belowminavail = np.where(kwcitems_matrix5 < 2)
    if np.shape(Base_belowminavail)[1] == 0:
        Team_availstuff = "All teams meet minimum qualifications for each post (Good!)"
    else:
        Team_availstuff = "Not all teams meet minimum qualificaitons for each post"
    c.setFont("Helvetica", 12, leading=None)
    c.drawString(770/2+35, Names_top-10-135, Team_availstuff)
    c.drawString(770/2+35, Names_top-10-150, "A Team Competency Avg: " + str(round(Team_avg[0], 1)))
    c.drawString(770/2+35, Names_top-10-165, "B Team Competency Avg: " + str(round(Team_avg[1], 1)))
    c.drawString(770/2+35, Names_top-10-180, "C Team Competency Avg: " + str(round(Team_avg[2], 1)))
    c.drawString(770/2+35, Names_top-10-195, "D Team Competency Avg: " + str(round(Team_avg[3], 1)))
    Outside_LTA3 = []
    Outside_LowComp = []
    LowPostavail_number = int(np.where(LowPost_availsum == np.min(LowPost_availsum))[0])+6
    UnBalPost = 0
    for y_cols in range(np.shape(plot_posts)[0]):
        if np.shape(Postavail_flagNew)[0] != 0:
            if plot_posts[y_cols] == Postavail_flagNew[0]:
                UnBalPost = y_cols
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        LTA_qual = 0
        LTA_compsum = 0
        LTA_compcount = 0
        Outside_LTA3temp = []
        Outside_LowComptemp = []
        if BaseCAS_matrix[x_rows][1] not in Sal_matrix and BaseCAS_matrix[x_rows][5] == "":
            for y_cols in range(4, np.shape(BaseCAS_matrix)[1]):
                if BaseCAS_matrix[x_rows][y_cols] != "":
                    LTA_qual += 1
                    LTA_compsum += BaseCAS_matrix[x_rows][y_cols]
                    LTA_compcount += 1
            if LTA_qual < 3:
                if BaseCAS_matrix[x_rows][LowPostavail_number] == "":
                    Outside_LTA3temp = [BaseCAS_matrix[x_rows][0], BaseCAS_matrix[x_rows][1], \
                                        plot_posts[LowPostavail_number-4]]
                elif BaseCAS_matrix[x_rows][UnBalPost+4] == "":
                    Outside_LTA3temp = [BaseCAS_matrix[x_rows][0], BaseCAS_matrix[x_rows][1], \
                                        plot_posts[UnBalPost]]
                else:
                    Outside_LTA3temp = [BaseCAS_matrix[x_rows][0], BaseCAS_matrix[x_rows][1], '']
                if np.shape(Outside_LTA3)[0] == 0:
                    Outside_LTA3 = Outside_LTA3temp
                elif Outside_LTA3temp not in Outside_LTA3:
                    Outside_LTA3 = np.vstack((Outside_LTA3, Outside_LTA3temp))
            if LTA_compsum/LTA_compcount < 1.5:
                Outside_LowComptemp = [BaseCAS_matrix[x_rows][0], BaseCAS_matrix[x_rows][1]]
                if np.shape(Outside_LowComp)[0] == 0:
                    Outside_LowComp = Outside_LowComptemp
                elif Outside_LowComptemp not in Outside_LowComp:
                    Outside_LowComp = np.vstack((Outside_LowComp, Outside_LowComptemp))
    c.drawString(770/2+35, Names_top-10-210, 'Personnel With Less Than 3 Qualifications: ')
    LTA3_nudge = 0
    for x_rows in range(np.shape(Outside_LTA3Base)[0]):
        c.drawString(770/2+35+20, Names_top-10-225-LTA3_nudge, Outside_LTA3[x_rows][0] + " Team: " + Outside_LTA3[x_rows][1])
        LTA3_nudge += 15
    c.drawString(770/2+35, Names_top-10-225-LTA3_nudge, 'Personnel With Competency Less Than 1.5: ')
    for x_rows in range(np.shape(Outside_LowComp)[0]):
        c.drawString(770/2+35+20, Names_top-10-240-LTA3_nudge, Outside_LowComp[x_rows][0] + " Team: " + Outside_LowComp[x_rows][1])
        LTA3_nudge += 15
    c.drawString(770/2+35, Names_top-10-240-LTA3_nudge, 'Posts With Lowest Qualifications: ' + LowPost_avail)
    c.setFont("Helvetica-Bold", 12, leading=None)
    c.drawString(770/2+35, Names_top-10-255-LTA3_nudge, 'Suggestions: ')
    c.setFont("Helvetica", 12, leading=None)
    if np.shape(Outside_LowComp)[0] > 0:
        for x_rows in range(np.shape(Outside_LowComp)[0]):
            c.drawString(770/2+55, Names_top-10-270-LTA3_nudge, 'Improve ' + Outside_LowComp[x_rows][1] + ' Competency')
            LTA3_nudge += 15
    if np.shape(Outside_LTA3Base)[0] > 0:
        for x_rows in range(np.shape(Outside_LTA3Base)[0]):
            c.drawString(770/2+55, Names_top-10-270-LTA3_nudge, 'Train ' + Outside_LTA3[x_rows][1] + ' on Post: ' + Outside_LTA3[x_rows][2])
            LTA3_nudge += 15
    if np.shape(Outside_LowComp)[0] == 0 and np.shape(Outside_LTA3)[0] == 0:
        c.drawString(770/2+55, Names_top-10-270-LTA3_nudge, 'None')
    c.setFont("Helvetica", 10, leading=None)
    c.drawAlignedString(780, 10, 'Page 3')
    c.showPage()
    c.save()
    file_name = '/Users/kelliecascarelli/PycharmProjects/PostScheduleMVC/Optimizer.pdf'
    subprocess.call(['open', file_name])


#Place all other Salaries
count_team = 0
def possible_Salary(y, n):
    global BaseCAS_matrix, count_team, kwcitems_matrix5, xstart, Post_availmin, Post_availmax, PostCAS_dataavail, PostCAS_dataavg
    cert_data = []
    #countj = 0
    #count_team = 1
    count_ER = 0
    count_EM = 0
    cert_count = 0
    cert_sum = 0
    for i in range(0, np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[i][0] == n and count_team >= round(np.shape(BaseCAS_matrix)[0]/4):
            count_team = 1
            count_ER = 0
            count_EM = 0
            cert_count = 0
            cert_sum = 0
            return False
        elif BaseCAS_matrix[i][0] == n and count_team < round(np.shape(BaseCAS_matrix)[0]/4):
            count_team = count_team + 1
        Post_countstuff = 0
        count_team2 = 1
    xx_flag = 0
    Post_avgtemp = []
    Post_counttemp = 0
    Post_sumtemp = 0
    for xx in range(np.shape(BaseCAS_matrix)[1]-4):
        if count_team >= int(round(np.shape(BaseCAS_matrix)[0]/8)):
            Post_counttemp = 0
            Post_sumtemp = 0
            for yy in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[yy][0] == n and BaseCAS_matrix[yy][xx+4] > 0:
                    Post_counttemp += 1
                    Post_sumtemp += BaseCAS_matrix[yy][xx+4]
        if Post_counttemp > 0:
            Post_avgtemp += [round(Post_sumtemp/Post_counttemp, 1)]
        else:
            Post_avgtemp += [0]
    for xx in range(np.shape(BaseCAS_matrix)[1]-4):
        if BaseCAS_matrix[y][xx+4] > 0:
            if kwcitems_matrix5[n-1][xx] + 1 > 1 or \
                    (kwcitems_matrix5[n-1][xx] + 1 < 1 and
                     count_team > round(np.shape(BaseCAS_matrix)[0]/4)):
                xx_flag = 1
                break
    if xx_flag == 1:
        return False
    return True


def solve_Salary():
    global BaseCAS_matrix, kwcitems_matrix5, solution_find, temp_matrix, Post_availmin, \
        Post_availmax, PostCAS_dataavail, PostCAS_dataavg, Team_CountTotal
    solution_find = 1
    #for x in range(4, 10):
    for y in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[y][0] == 0 and BaseCAS_matrix[y][1] in Sal_matrix:
            for n in range(1, 5):
                #for x in range(4, np.shape(BaseCAS_matrix)[1]):
                if possible_Salary(y, n):
                    BaseCAS_matrix[y][0] = n
                    #kwcitems_matrix5 = temp_matrix
                    for kwcmatrix5_items in range(np.shape(kwcitems_matrix5)[1]):
                        if BaseCAS_matrix[y][kwcmatrix5_items+4] > 0:
                            kwcitems_matrix5[n-1][kwcmatrix5_items] = kwcitems_matrix5[n-1][kwcmatrix5_items] + 1
                    solve_Salary()
                    BaseCAS_matrix[y][0] = 0
                    return
    solution_find = 100
    Team_Stats()
    TeamCasAvg()
    solve_FillSTLPost()


#Fill in and balance the STL and second post:
def possible_FillSTLPost(y, n):
    global BaseCAS_matrix, count_team, kwcitems_matrix5, Post_availmin, Post_availmax, PostCAS_dataavail, \
        PostCAS_dataavg, Team_CountTotal
    cert_data = []
    #countj = 0
    count_team = 0
    count_ER = 0
    count_EM = 0
    cert_count = 0
    cert_sum = 0
    for i in range(0, np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[i][0] == n and count_team >= round(np.shape(BaseCAS_matrix)[0]/4):
            count_team = 1
            count_ER = 0
            count_EM = 0
            cert_count = 0
            cert_sum = 0
            return False
        elif BaseCAS_matrix[i][0] == n and count_team < round(np.shape(BaseCAS_matrix)[0]/4):
            count_team = count_team + 1
        Post_countstuff = 0
        count_team2 = 1
    xx_flag = 0
    Post_avgtemp = []
    Post_counttemp = 0
    Post_sumtemp = 0
    for xx in range(np.shape(BaseCAS_matrix)[1]-4):
        if count_team >= int(round(np.shape(BaseCAS_matrix)[0]/8)):
            Post_counttemp = 0
            Post_sumtemp = 0
            for yy in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[yy][0] == n and BaseCAS_matrix[yy][xx+4] > 0:
                    Post_counttemp += 1
                    Post_sumtemp += BaseCAS_matrix[yy][xx+4]
        if Post_counttemp > 0:
            Post_avgtemp += [round(Post_sumtemp/Post_counttemp, 1)]
        else:
            Post_avgtemp += [0]
    for xx in range(np.shape(BaseCAS_matrix)[1]-4):
        if BaseCAS_matrix[y][xx+4] > 0:
            if Post_availmax[xx] == int(Post_availmax[xx]):
                MaxOffset = 0
            else:
                MaxOffset = 1
            if kwcitems_matrix5[n-1][xx] + 1 > Post_availmax[xx] - MaxOffset or \
                    (kwcitems_matrix5[n-1][xx] + 1 < Post_availmin[xx] and \
                     count_team > round(np.shape(BaseCAS_matrix)[0]/4)):
                xx_flag = 1
                break
    if xx_flag == 1:
        return False
    return True




def solve_FillSTLPost():
    global BaseCAS_matrix, kwcitems_matrix5, solution_find, temp_matrix, Post_availmin, Post_availmax, \
        PostCAS_dataavail, PostCAS_dataavg, Team_CountTotal, Team_ERTTotal, Team_EMTTotal
    solution_find += 1
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        for n in range(1, 5):
            if BaseCAS_matrix[x_rows][4] > 0 and kwcitems_matrix5[n-1][0] == 1 and BaseCAS_matrix[x_rows][0] == 0:
                BaseCAS_matrix[x_rows][0] = n
                kwcitems_matrix5[n-1][0] += 1
                break
    kwcitems_count = 0
    kwcitems_matrix = []
    for kwcitems_cols in range(4, 10):
        for kwcitems_n in range(1, 5):
            kwcitems_count = 0
            for kwcitems_rows in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[kwcitems_rows][0] == kwcitems_n and \
                        BaseCAS_matrix[kwcitems_rows][kwcitems_cols] != 0:
                    kwcitems_count = kwcitems_count + 1
            kwcitems_matrix += [kwcitems_n, kwcitems_count]
    kwcitems_matrix1 = []
    kwcitems_matrix2 = []
    kwcitems_matrix3 = []
    kwcitems_matrix4 = []
    kwcitems_matrix5 = []
    Count_1 = 0
    Count_2 = 0
    Count_3 = 0
    Count_4 = 0
    for team_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[team_rows][0] == 1:
            Count_1 += 1
        if BaseCAS_matrix[team_rows][0] == 2:
            Count_2 += 1
        if BaseCAS_matrix[team_rows][0] == 3:
            Count_3 += 1
        if BaseCAS_matrix[team_rows][0] == 4:
            Count_4 += 1
    Team_CountTotal = np.hstack((Count_1, Count_2, Count_3, Count_4))
    for kwcitems_cols2 in range(0, np.shape(kwcitems_matrix)[0], 2):
        if kwcitems_matrix[kwcitems_cols2] == 1:
            kwcitems_matrix1 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 2:
            kwcitems_matrix2 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 3:
            kwcitems_matrix3 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 4:
            kwcitems_matrix4 += [kwcitems_matrix[kwcitems_cols2+1]]
    kwcitems_matrix5 = np.vstack((kwcitems_matrix1, kwcitems_matrix2, kwcitems_matrix3, kwcitems_matrix4))
    Team_Stats()
    TeamCasAvg()
    lowTeam_afterSTLfill()


def lowTeam_afterSTLfill():
    global BaseCAS_matrix, kwcitems_matrix5, Team_CountTotal
    low = np.where(Team_CountTotal == np.min(Team_CountTotal))
    for low_value in range(np.shape(low)[0]):
        for x_rows in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[x_rows][0] == 0 and BaseCAS_matrix[x_rows][5] > 0:
                BaseCAS_matrix[x_rows][0] = int(low[low_value]) + 1
                break
    kwcitems_count = 0
    kwcitems_matrix = []
    for kwcitems_cols in range(4, 10):
        for kwcitems_n in range(1, 5):
            kwcitems_count = 0
            for kwcitems_rows in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[kwcitems_rows][0] == kwcitems_n and \
                        BaseCAS_matrix[kwcitems_rows][kwcitems_cols] != 0:
                    kwcitems_count = kwcitems_count + 1
            kwcitems_matrix += [kwcitems_n, kwcitems_count]
    kwcitems_matrix1 = []
    kwcitems_matrix2 = []
    kwcitems_matrix3 = []
    kwcitems_matrix4 = []
    kwcitems_matrix5 = []
    Count_1 = 0
    Count_2 = 0
    Count_3 = 0
    Count_4 = 0
    for team_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[team_rows][0] == 1:
            Count_1 += 1
        if BaseCAS_matrix[team_rows][0] == 2:
            Count_2 += 1
        if BaseCAS_matrix[team_rows][0] == 3:
            Count_3 += 1
        if BaseCAS_matrix[team_rows][0] == 4:
            Count_4 += 1
    Team_CountTotal = np.hstack((Count_1, Count_2, Count_3, Count_4))
    for kwcitems_cols2 in range(0, np.shape(kwcitems_matrix)[0], 2):
        if kwcitems_matrix[kwcitems_cols2] == 1:
            kwcitems_matrix1 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 2:
            kwcitems_matrix2 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 3:
            kwcitems_matrix3 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 4:
            kwcitems_matrix4 += [kwcitems_matrix[kwcitems_cols2+1]]
    kwcitems_matrix5 = np.vstack((kwcitems_matrix1, kwcitems_matrix2, kwcitems_matrix3, kwcitems_matrix4))
    Fill_AllPosts()


def TeamCasAvg():
    global BaseCAS_matrix, Team_postavg, Team_avg
    Team1_avg = 0
    Team2_avg = 0
    Team3_avg = 0
    Team4_avg = 0
    Team1_count = 0
    Team2_count = 0
    Team3_count = 0
    Team4_count = 0
    Team1_sum = 0
    Team2_sum = 0
    Team3_sum = 0
    Team4_sum = 0
    Team1_postcount = 0
    Team1_postsum = 0
    Team_postcount = np.zeros(shape=(4, np.shape(BaseCAS_matrix)[1]-4))
    Team_postsum = np.zeros(shape=(4, np.shape(BaseCAS_matrix)[1]-4))
    Team_postavg = np.zeros(shape=(4, np.shape(BaseCAS_matrix)[1]-4))
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        for y_cols in range(4, np.shape(BaseCAS_matrix)[1]):
            if BaseCAS_matrix[x_rows][0] == 1 and BaseCAS_matrix[x_rows][y_cols] > 0:
                Team1_count += 1
                Team1_sum += BaseCAS_matrix[x_rows][y_cols]
                Team_postcount[0][y_cols-4] += 1
                Team_postsum[0][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
            if BaseCAS_matrix[x_rows][0] == 2 and BaseCAS_matrix[x_rows][y_cols] > 0:
                Team2_count += 1
                Team2_sum += BaseCAS_matrix[x_rows][y_cols]
                Team_postcount[1][y_cols-4] += 1
                Team_postsum[1][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
            if BaseCAS_matrix[x_rows][0] == 3 and BaseCAS_matrix[x_rows][y_cols] > 0:
                Team3_count += 1
                Team3_sum += BaseCAS_matrix[x_rows][y_cols]
                Team_postcount[2][y_cols-4] += 1
                Team_postsum[2][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
            if BaseCAS_matrix[x_rows][0] == 4 and BaseCAS_matrix[x_rows][y_cols] > 0:
                Team4_count += 1
                Team4_sum += BaseCAS_matrix[x_rows][y_cols]
                Team_postcount[3][y_cols-4] += 1
                Team_postsum[3][y_cols-4] += BaseCAS_matrix[x_rows][y_cols]
    if Team1_count > 0:
        Team1_avg = Team1_sum / Team1_count
    else:
        Team1_avg = 0
    if Team2_count > 0:
        Team2_avg = Team2_sum / Team2_count
    else:
        Team2_avg = 0
    if Team3_count > 0:
        Team3_avg = Team3_sum / Team3_count
    else:
        Team3_avg = 0
    if Team4_count > 0:
        Team4_avg = Team4_sum / Team4_count
    else:
        Team4_avg = 0
    Team_avg = np.hstack((round(Team1_avg, 2), round(Team2_avg, 2), round(Team3_avg, 2), round(Team4_avg, 2)))
    for x_rows in range(np.shape(Team_postcount)[0]):
        for y_cols in range(np.shape(Team_postcount)[1]):
            if Team_postcount[x_rows][y_cols] > 0:
                Team_postavg[x_rows][y_cols] = round(Team_postsum[x_rows][y_cols] / Team_postcount[x_rows][y_cols], 1)
            else:
                Team_postavg[x_rows][y_cols] = 0
    return


def Perm_Table_Set():
    global BaseCAS_matrix, Zero_BaseCAS_matrix, NonZero_BaseCAS_matrix, perm_count
    Perm_table_use = Perm_matrix[perm_count]
    Zero_BaseCAS_matrix_perm = []
    for x_perm in Perm_table_use:
        count = 0
        for x_rows in Zero_BaseCAS_matrix:
            if count == x_perm:
                if np.shape(Zero_BaseCAS_matrix_perm)[0] == 0:
                    Zero_BaseCAS_matrix_perm = [x_rows]
                    break
                else:
                    Zero_BaseCAS_matrix_perm = np.vstack((Zero_BaseCAS_matrix_perm, [x_rows]))
                    break
            else:
                count += 1
    if perm_count < np.shape(Perm_matrix)[0]:
        perm_count += 1
    BaseCAS_matrix = np.vstack((Zero_BaseCAS_matrix_perm, NonZero_BaseCAS_matrix))
    return


def Team_Stats():
    global BaseCAS_matrix, kwcitems_matrix5, Team_CountTotal, Team_ERTTotal, Team_EMTTotal
    kwcitems_count = 0
    kwcitems_matrix = []
    for kwcitems_cols in range(4, 10):
        for kwcitems_n in range(1, 5):
            kwcitems_count = 0
            for kwcitems_rows in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[kwcitems_rows][0] == kwcitems_n and \
                        BaseCAS_matrix[kwcitems_rows][kwcitems_cols] != 0:
                    kwcitems_count = kwcitems_count + 1
            kwcitems_matrix += [kwcitems_n, kwcitems_count]
    kwcitems_matrix1 = []
    kwcitems_matrix2 = []
    kwcitems_matrix3 = []
    kwcitems_matrix4 = []
    kwcitems_matrix5 = []
    Count_1 = 0
    Count_2 = 0
    Count_3 = 0
    Count_4 = 0
    Count1_ERT = 0
    Count2_ERT = 0
    Count3_ERT = 0
    Count4_ERT = 0
    Count1_EMT = 0
    Count2_EMT = 0
    Count3_EMT = 0
    Count4_EMT = 0
    for team_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[team_rows][0] == 1:
            Count_1 += 1
            if BaseCAS_matrix[team_rows][2] == "Y":
                Count1_ERT += 1
            if BaseCAS_matrix[team_rows][3] == "Y":
                Count1_EMT += 1
        if BaseCAS_matrix[team_rows][0] == 2:
            Count_2 += 1
            if BaseCAS_matrix[team_rows][2] == "Y":
                Count2_ERT += 1
            if BaseCAS_matrix[team_rows][3] == "Y":
                Count2_EMT += 1
        if BaseCAS_matrix[team_rows][0] == 3:
            Count_3 += 1
            if BaseCAS_matrix[team_rows][2] == "Y":
                Count3_ERT += 1
            if BaseCAS_matrix[team_rows][3] == "Y":
                Count3_EMT += 1
        if BaseCAS_matrix[team_rows][0] == 4:
            Count_4 += 1
            if BaseCAS_matrix[team_rows][2] == "Y":
                Count4_ERT += 1
            if BaseCAS_matrix[team_rows][3] == "Y":
                Count4_EMT += 1
    Team_CountTotal = np.hstack((Count_1, Count_2, Count_3, Count_4))
    Team_ERTTotal = np.hstack((Count1_ERT, Count2_ERT, Count3_ERT, Count4_ERT))
    Team_EMTTotal = np.hstack((Count1_EMT, Count2_EMT, Count3_EMT, Count4_EMT))
    for kwcitems_cols2 in range(0, np.shape(kwcitems_matrix)[0], 2):
        if kwcitems_matrix[kwcitems_cols2] == 1:
            kwcitems_matrix1 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 2:
            kwcitems_matrix2 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 3:
            kwcitems_matrix3 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 4:
            kwcitems_matrix4 += [kwcitems_matrix[kwcitems_cols2+1]]
    kwcitems_matrix5 = np.vstack((kwcitems_matrix1, kwcitems_matrix2, kwcitems_matrix3, kwcitems_matrix4))


def Fill_AllPosts():
    global BaseCAS_matrix, kwcitems_matrix5, Team_CountTotal, PostCAS_dataavg, PostCAS_dataavail, PostEMT_availmax, \
        PostEMT_availmin, Team_postavg, PostERT_avail, PostERT_availmax, PostERT_availmin, PostEMT_avail, PostEMT_availmax, \
        PostEMT_availmin, Fill_list, Team_avg, PostOut_compavg, Team_lowavg
    TeamCasAvg()
    Team_Stats()
    #Let's Fill the Second Post with Balanced CAS across multiple posts
    find_secondpost = []
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][5] > 0 and BaseCAS_matrix[x_rows][0] == 0:
            find_secondpost += [BaseCAS_matrix[x_rows][1]]
    lowsecondvalue = 10
    lowsecond = 0
    for x_rows in range(np.shape(Team_postavg)[0]):
        if Team_postavg[x_rows][1] < lowsecondvalue:
            lowsecondvalue = Team_postavg[x_rows][1]
            lowsecond = x_rows
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][5] > lowsecondvalue and BaseCAS_matrix[x_rows][0] == 0:
            BaseCAS_matrix[x_rows][0] = lowsecond + 1
            Team_CountTotal[lowsecond] += 1
            break
    Post2_compavg = []
    PostOut_compavg = []
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        Post2_compsum_temp = 0
        Post2_compcount_temp = 0
        PostOut_compsum_temp = 0
        PostOut_compcount_temp = 0
        Post2_compname_temp = ""
        Post2_compavg_temp = []
        PostOut_compname_temp = ""
        PostOut_compavg_temp = []
        if BaseCAS_matrix[x_rows][5] > 0 and BaseCAS_matrix[x_rows][0] == 0:
            for y_cols in range(5, np.shape(BaseCAS_matrix)[1]):
                if BaseCAS_matrix[x_rows][y_cols] != 0:
                    Post2_compsum_temp += BaseCAS_matrix[x_rows][y_cols]
                    Post2_compcount_temp += 1
            Post2_compname_temp = BaseCAS_matrix[x_rows][1]
            Post2_compavg_temp = [Post2_compname_temp, Post2_compsum_temp/Post2_compcount_temp]
            if np.shape(Post2_compavg)[0] == 0:
                Post2_compavg = [Post2_compavg_temp]
            else:
                Post2_compavg = np.vstack(([Post2_compavg, Post2_compavg_temp]))
        if BaseCAS_matrix[x_rows][5] == 0 and BaseCAS_matrix[x_rows][0] == 0:
            for y_cols in range(5, np.shape(BaseCAS_matrix)[1]):
                if BaseCAS_matrix[x_rows][y_cols] != 0:
                    PostOut_compsum_temp += BaseCAS_matrix[x_rows][y_cols]
                    PostOut_compcount_temp += 1
            PostOut_compname_temp = BaseCAS_matrix[x_rows][1]
            PostOut_compavg_temp = [PostOut_compname_temp, PostOut_compsum_temp/PostOut_compcount_temp]
            if np.shape(PostOut_compavg)[0] == 0:
                PostOut_compavg = [PostOut_compavg_temp]
            else:
                PostOut_compavg = np.vstack(([PostOut_compavg, PostOut_compavg_temp]))
    #Place the last of Post 2 Qualified-------------------------------------------
    TeamCasAvg()
    Team_Stats()
    Post2_lowqual = np.where(kwcitems_matrix5[:, 1] == 1)
    for y_cols in range(np.shape(Post2_lowqual)[1]):
        max_Comp_Post2 = 0
        max_rowPost2 = 0
        for x_rows in range(np.shape(Post2_compavg)[0]):
            if float(Post2_compavg[x_rows][1]) > max_Comp_Post2:
                max_Comp_Post2 = float(Post2_compavg[x_rows][1])
                max_rowPost2 = x_rows
    Team_lowavg = np.where(Team_avg == np.min(Team_avg))
    Comb_lowqualavg = 0
    for x_rows in range(np.shape(Post2_lowqual)[1]):
        for y_cols in range(np.shape(Team_lowavg)[1]):
            if Post2_lowqual[0][x_rows] == Team_lowavg[0][y_cols]:
                Comb_lowqualavg = Post2_lowqual[0][x_rows]
            break
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][1] == Post2_compavg[max_rowPost2][0]:
            BaseCAS_matrix[x_rows][0] = Comb_lowqualavg + 1
            break
    Post2_compavg = np.delete(Post2_compavg, max_rowPost2, 0)
    Team_Stats()
    TeamCasAvg()
    Post2_lowqual = np.where(kwcitems_matrix5[:, 1] == 1)
    Stop_adder = 0
    Added_Name = 0
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][1] in Post2_compavg and Stop_adder == 0:
            for y_cols in range(np.shape(Post2_lowqual)[1]):
                BaseCAS_matrix[x_rows][0] = int(Post2_lowqual[y_cols]) + 1
                Stop_adder += 1
                Added_Name = y_cols
                break
    Post2_compavg = np.delete(Post2_compavg, Added_Name, 0)
    Team_Stats()
    TeamCasAvg()
    Post2_lowqual = np.where(kwcitems_matrix5[:, 1] == np.min(kwcitems_matrix5[:, 1]))
    Low_Team_Count = np.where(Team_CountTotal == np.min(Team_CountTotal))
    Stop_adder = 0
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][1] in Post2_compavg and Stop_adder == 0:
            for y_cols in range(np.shape(Low_Team_Count)[1]):
                BaseCAS_matrix[x_rows][0] = int(Low_Team_Count[y_cols]) + 1
                Stop_adder += 1
                Added_Name = y_cols
                break
    Post2_compavg = np.delete(Post2_compavg, Added_Name, 0)
    Team_Stats()
    TeamCasAvg()
    Fill_list = []
    Fill_count = 0
    for x_rows in BaseCAS_matrix:
        Fill_listtemp = []
        if x_rows[0] == 0:
            Fill_listtemp = [x_rows]
            Fill_count += 1
            if np.shape(Fill_list)[0] == 0:
                Fill_list += Fill_listtemp
            else:
                Fill_list = np.vstack((Fill_list, Fill_listtemp))
    if np.shape(Fill_list)[0] <= 7:
        Fill_solve()
    else:
        Shave_to_Ten()


#Shave to 10 Unknowns then Shave 2 more------------------------------------------------------
def Shave_to_Ten():
    global BaseCAS_matrix, kwcitems_matrix5, Post_availmax, Post_availmin, PostCAS_dataavail, \
        PostCAS_dataavg, Team_CountTotal, Team_ERTTotal, Team_EMTTotal, Post_availmaxmax, Fill_list, Team_avg, \
        PostOut_compavg, LowPostOut_list, HighPostOut_list
    TeamCasAvg()
    Team_Stats()
    Low_qual_post = np.where(kwcitems_matrix5 == np.min(kwcitems_matrix5))
    Min_kwc_colposts = []
    for y_cols in range(np.shape(kwcitems_matrix5)[1]):
        Min_kwc_colposts += [0]
    for y_cols in range(np.shape(kwcitems_matrix5)[1]):
        for yy_cols in range(np.shape(Low_qual_post)[1]):
            if y_cols == Low_qual_post[1][yy_cols]:
                Min_kwc_colposts[y_cols] += 1
    Min_Min_kwc_colposts = np.where(Min_kwc_colposts == np.max(Min_kwc_colposts))
    Find_low_quals = []
    Min_qual_count0 = 0
    Min_qual_count1 = 0
    Min_qual_count2 = 0
    Min_qual_count3 = 0
    for y_cols in range(np.shape(Low_qual_post)[1]):
        if Low_qual_post[0][y_cols] == 0:
            Min_qual_count0 += 1
        if Low_qual_post[0][y_cols] == 1:
            Min_qual_count1 += 1
        if Low_qual_post[0][y_cols] == 2:
            Min_qual_count2 += 1
        if Low_qual_post[0][y_cols] == 3:
            Min_qual_count3 += 1
    Min_kwcmatrix5_teamslist = np.hstack((Min_qual_count0, Min_qual_count1, Min_qual_count2, Min_qual_count3))
    Min_kwcmatrix5_teams = np.where(Min_kwcmatrix5_teamslist == np.max(Min_kwcmatrix5_teamslist))
    max_Comp_PostOut = 0
    max_rowPostOut = 0
    min_Comp_PostOut = 4
    min_rowPostOut = 0
    avg_PostOutsum = 0
    for x_rows in range(np.shape(PostOut_compavg)[0]):
        if float(PostOut_compavg[x_rows][1]) > max_Comp_PostOut:
            max_Comp_PostOut = float(PostOut_compavg[x_rows][1])
            max_rowPostOut = x_rows
        if float(PostOut_compavg[x_rows][1]) < min_Comp_PostOut:
            min_Comp_PostOut = float(PostOut_compavg[x_rows][1])
            min_rowPostOut = x_rows
        avg_PostOutsum += float(PostOut_compavg[x_rows][1])
    avg_PostOut = round(avg_PostOutsum / np.shape(PostOut_compavg)[0], 3)
    Team_lowavg = np.min(Team_avg)
    Team_highavg = np.max(Team_avg)
    Team_lowavg_where = int(np.where(Team_avg == Team_lowavg)[0][0])
    Team_highavg_where = int(np.where(Team_avg == Team_highavg)[0][0])
    # Get Below Avg and Above Avg List from PostOut_avg---------------------------------
    LowPostOut_list = []
    HighPostOut_list = []
    LowPostOut_listtemp = ""
    HighPostOut_listtemp = ""
    for x_rows in PostOut_compavg:
        if float(x_rows[1]) <= avg_PostOut:
            LowPostOut_listtemp = x_rows
            if np.shape(LowPostOut_list)[0] == 0:
                LowPostOut_list = [LowPostOut_listtemp]
            else:
                LowPostOut_list = np.vstack((LowPostOut_list, LowPostOut_listtemp))
        if float(x_rows[1]) > avg_PostOut:
            HighPostOut_listtemp = x_rows
            if np.shape(HighPostOut_list)[0] == 0:
                HighPostOut_list = [HighPostOut_listtemp]
            else:
                HighPostOut_list = np.vstack((HighPostOut_list, HighPostOut_listtemp))
    for yy_cols in range(np.shape(Min_kwcmatrix5_teams)[1]):
        if np.shape(Fill_list)[0] <= 10:
            break
        for x_rows in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[x_rows][0] == 0:
                #for High Team Avg need a Low Person Avg---------------------------------
                if BaseCAS_matrix[x_rows][int(Min_Min_kwc_colposts[0][0])+4] != 0 and \
                        Team_avg[yy_cols] >= avg_PostOut and \
                        BaseCAS_matrix[x_rows][1] in LowPostOut_list:
                    BaseCAS_matrix[x_rows][0] = int(Min_kwcmatrix5_teams[0][yy_cols]) + 1
                    Low_row_out = np.where(LowPostOut_list[:, 0] == BaseCAS_matrix[x_rows][1])[0][0]
                    LowPostOut_list = np.delete(LowPostOut_list, Low_row_out, 0)
                    Low_row_avg = np.where(PostOut_compavg[:, 0] == BaseCAS_matrix[x_rows][1])[0][0]
                    PostOut_compavg = np.delete(PostOut_compavg, Low_row_avg, 0)
                    Fill_list = []
                    Fill_count = 0
                    for xx_rows in BaseCAS_matrix:
                        Fill_listtemp = []
                        if xx_rows[0] == 0:
                            Fill_listtemp = [xx_rows]
                            Fill_count += 1
                            if np.shape(Fill_list)[0] == 0:
                                Fill_list += Fill_listtemp
                            else:
                                Fill_list = np.vstack((Fill_list, Fill_listtemp))
                    if np.shape(Fill_list)[0] <= 10:
                        Shave_to_Seven()
                    else:
                        Shave_to_Ten()
                    #for Low Team Avg need a High Person Avg---------------------------------
                    break


def Shave_to_Seven():
    global BaseCAS_matrix, kwcitems_matrix5, Post_availmax, Post_availmin, PostCAS_dataavail, \
        PostCAS_dataavg, Team_CountTotal, Team_ERTTotal, Team_EMTTotal, Post_availmaxmax, Fill_list, Team_avg, \
        PostOut_compavg, LowPostOut_list, HighPostOut_list, Fill_list
    TeamCasAvg()
    Team_Stats()
    Final_LowTeam = 0
    Final_LowPostwhere = 0
    Final_LowPost = 0
    Final_HighTeam = 0
    Final_HighPostwhere = 0
    Final_HighPost = 0
    #Think max to prevent getting too close to post max
    if np.where(Team_avg == np.min(Team_avg))[0][0] in np.where(kwcitems_matrix5 == 0)[0]:
        Final_LowTeam = np.where(Team_avg == np.min(Team_avg))[0][0]
        Final_LowPostwhere = np.where(np.where(kwcitems_matrix5 == 0)[0] == Final_LowTeam)[0][0]
        Final_LowPost = np.where(kwcitems_matrix5 == 0)[1][Final_LowPostwhere]
    elif np.where(Team_avg == np.min(Team_avg))[0][0] in np.where(kwcitems_matrix5 == 1)[0]:
        Final_LowTeam = np.where(Team_avg == np.min(Team_avg))[0][0]
        Final_LowPostwhere = np.where(np.where(kwcitems_matrix5 == 1)[0] == Final_LowTeam)[0][0]
        Final_LowPost = np.where(kwcitems_matrix5 == 1)[1][Final_LowPostwhere]
    if np.where(Team_avg == np.max(Team_avg))[0][0] in np.where(kwcitems_matrix5 == 0)[0]:
        Final_HighTeam = np.where(Team_avg == np.max(Team_avg))[0][0]
        Final_HighPostwhere = np.where(np.where(kwcitems_matrix5 == 0)[0] == Final_HighTeam)[0][0]
        Final_HighPost = np.where(kwcitems_matrix5 == 0)[1][Final_HighPostwhere]
    elif np.where(Team_avg == np.max(Team_avg))[0][0] in np.where(kwcitems_matrix5 == 1)[0]:
        Final_HighTeam = np.where(Team_avg == np.max(Team_avg))[0][0]
        Final_HighPostwhere = np.where(np.where(kwcitems_matrix5 == 1)[0] == Final_HighTeam)[0][0]
        Final_HighPost = np.where(kwcitems_matrix5 == 1)[1][Final_HighPostwhere]
    for xxx_rows in range(np.shape(BaseCAS_matrix)[0]):
        Tapout_High = 0
        for watch_items in range(np.shape(Post_availmax_watch)[1]):
            if kwcitems_matrix5[Final_LowTeam][Post_availmax_watch[0][watch_items]] == \
                    Post_availmax[Post_availmax_watch[0][watch_items]] - 1:
                Tapout_High = 1
        if BaseCAS_matrix[xxx_rows][4+Final_LowPost] != 0 and BaseCAS_matrix[xxx_rows][0] == 0 and \
                BaseCAS_matrix[xxx_rows][1] in HighPostOut_list and Tapout_High == 0:
            BaseCAS_matrix[xxx_rows][0] = Final_LowTeam + 1
            Team_Stats()
            TeamCasAvg()
            High_row_out = np.where(HighPostOut_list[:, 0] == BaseCAS_matrix[xxx_rows][1])[0][0]
            HighPostOut_list = np.delete(HighPostOut_list, High_row_out, 0)
            High_row_avg = np.where(PostOut_compavg[:, 0] == BaseCAS_matrix[xxx_rows][1])[0][0]
            PostOut_compavg = np.delete(PostOut_compavg, High_row_avg, 0)
            if np.where(Team_avg == np.min(Team_avg))[0][0] in np.where(kwcitems_matrix5 == 0)[0]:
                Final_LowTeam = np.where(Team_avg == np.min(Team_avg))[0][0]
                Final_LowPostwhere = np.where(np.where(kwcitems_matrix5 == 0)[0] == Final_LowTeam)[0][0]
                Final_LowPost = np.where(kwcitems_matrix5 == 0)[1][Final_LowPostwhere]
            elif np.where(Team_avg == np.min(Team_avg))[0][0] in np.where(kwcitems_matrix5 == 1)[0]:
                Final_LowTeam = np.where(Team_avg == np.min(Team_avg))[0][0]
                Final_LowPostwhere = np.where(np.where(kwcitems_matrix5 == 1)[0] == Final_LowTeam)[0][0]
                Final_LowPost = np.where(kwcitems_matrix5 == 1)[1][Final_LowPostwhere]
            Fill_list = []
            Fill_count = 0
            for xx_rows in BaseCAS_matrix:
                Fill_listtemp = []
                if xx_rows[0] == 0:
                    Fill_listtemp = [xx_rows]
                    Fill_count += 1
                    if np.shape(Fill_list)[0] == 0:
                        Fill_list += Fill_listtemp
                    else:
                        Fill_list = np.vstack((Fill_list, Fill_listtemp))
            if np.shape(Fill_list)[0] == 7:
                break
                Stop_it_now()
        elif BaseCAS_matrix[xxx_rows][4+Final_LowPost] != 0 and BaseCAS_matrix[xxx_rows][0] == 0 and \
                BaseCAS_matrix[xxx_rows][1] in LowPostOut_list and Tapout_High == 1 and np.shape(Fill_list)[0] > 7:
            BaseCAS_matrix[xxx_rows][0] = Final_HighTeam + 1
            Team_Stats()
            TeamCasAvg()
            Low_row_out = np.where(LowPostOut_list[:, 0] == BaseCAS_matrix[xxx_rows][1])[0][0]
            LowPostOut_list = np.delete(LowPostOut_list, Low_row_out, 0)
            Low_row_avg = np.where(PostOut_compavg[:, 0] == BaseCAS_matrix[xxx_rows][1])[0][0]
            PostOut_compavg = np.delete(PostOut_compavg, Low_row_avg, 0)
            Fill_list = []
            Fill_count = 0
            for xx_rows in BaseCAS_matrix:
                Fill_listtemp = []
                if xx_rows[0] == 0:
                    Fill_listtemp = [xx_rows]
                    Fill_count += 1
                    if np.shape(Fill_list)[0] == 0:
                        Fill_list += Fill_listtemp
                    else:
                        Fill_list = np.vstack((Fill_list, Fill_listtemp))
            if np.shape(Fill_list)[0] == 7:
                break
                Stop_it_now()
            break
    if np.shape(Fill_list)[0] > 7:
        Shave_to_Seven()
    elif np.shape(Fill_list)[0] == 7:
        Stop_it_now()
    Team_Stats()
    TeamCasAvg()


def Stop_it_now():
    global Fill_list, kwcitems_matrix5, BaseCAS_matrix, Team_CountTotal, Index_list, Zero_BaseCAS_matrix, \
        NonZero_BaseCAS_matrix, comb_count, Team_avg, Team_postavg, LowPostOut_list, HighPostOut_list, Perm_matrix, \
        xx_flag, count_perm_table, perm_count, Fill_solve_counter
    Team_Stats()
    TeamCasAvg()
    Fill_list = []
    Fill_count = 0
    for xx_rows in BaseCAS_matrix:
        Fill_listtemp = []
        if xx_rows[0] == 0:
            Fill_listtemp = [xx_rows]
            Fill_count += 1
            if np.shape(Fill_list)[0] == 0:
                Fill_list += Fill_listtemp
            else:
                Fill_list = np.vstack((Fill_list, Fill_listtemp))
    '''column_countmax = 5
    Low_qual_Name = ""
    for x_rows in range(np.shape(Fill_list)[0]):
        column_count = 0
        for y_cols in range(6, np.shape(Fill_list)[1]):
            if Fill_list[x_rows][y_cols] != 0:
                column_count += 1
        if column_count < column_countmax:
            Low_qual_Name = Fill_list[x_rows][1]
            column_countmax = column_count
    #TeamCount_lowwhere = np.where(Team_CountTotal == np.min(Team_CountTotal))[0][0]
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][1] == Low_qual_Name:
            BaseCAS_matrix[x_rows][0] = TeamCount_lowwhere + 1'''
    Fill_list = []
    Fill_count = 0
    for xx_rows in BaseCAS_matrix:
        Fill_listtemp = []
        if xx_rows[0] == 0:
            Fill_listtemp = [xx_rows]
            Fill_count += 1
            if np.shape(Fill_list)[0] == 0:
                Fill_list += Fill_listtemp
            else:
                Fill_list = np.vstack((Fill_list, Fill_listtemp))
    Team_Stats()
    TeamCasAvg()
    #Set up the permutation matrix------------------------------------
    Zero_BaseCAS_matrix = []
    NonZero_BaseCAS_matrix = []
    for x_rows in BaseCAS_matrix:
        if x_rows[0] == 0:
            if np.shape(Zero_BaseCAS_matrix)[0] == 0:
                Zero_BaseCAS_matrix = [x_rows]
            else:
                Zero_BaseCAS_matrix = np.vstack((Zero_BaseCAS_matrix, [x_rows]))
        else:
            if np.shape(NonZero_BaseCAS_matrix)[0] == 0:
                NonZero_BaseCAS_matrix = [x_rows]
            else:
                NonZero_BaseCAS_matrix = np.vstack((NonZero_BaseCAS_matrix, [x_rows]))
    BaseCAS_matrix = np.vstack((Zero_BaseCAS_matrix, NonZero_BaseCAS_matrix))
    comb_count = 0
    perm_count += 1
    xx_flag = 1
    Fill_solve_counter = -1
    Fill_solve()



#Place all others-----------------------------------------------------------------------------------
count_team = 0
def Fill_possible(y, n):
    global BaseCAS_matrix, count_team, kwcitems_matrix5, xstart, Post_availmin, Post_availmax, PostCAS_dataavail, \
        PostCAS_dataavg, Team_CountTotal, Team_ERTTotal, Team_EMTTotal, Post_availmaxmax, Perm_matrix, LowPostOut_list, \
        HighPostOut_list
    cert_data = []
    #countj = 0
    #count_team = 1
    count_ER = 0
    count_EM = 0
    cert_count = 0
    cert_sum = 0
    for i in range(0, np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[i][0] == n and count_team >= round(np.shape(BaseCAS_matrix)[0]/4):
            count_team = 1
            count_ER = 0
            count_EM = 0
            cert_count = 0
            cert_sum = 0
            return False
        elif BaseCAS_matrix[i][0] == n and count_team < round(np.shape(BaseCAS_matrix)[0]/4):
            count_team = count_team + 1
        Post_countstuff = 0
        count_team2 = 1
    xx_flag = 0
    Post_avgtemp = []
    Post_counttemp = 0
    Post_sumtemp = 0
    min_Post_Count_Seq = 0
    for x_rows in range(np.shape(kwcitems_matrix5)[0]):
        for y_cols in range(np.shape(kwcitems_matrix5)[1]):
            pass
    for xx in range(np.shape(BaseCAS_matrix)[1]-4):
        if count_team >= int(round(np.shape(BaseCAS_matrix)[0]/8)):
            Post_counttemp = 0
            Post_sumtemp = 0
            for yy in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[yy][0] == n and BaseCAS_matrix[yy][xx+4] > 0:
                    Post_counttemp += 1
                    Post_sumtemp += BaseCAS_matrix[yy][xx+4]
        if Post_counttemp > 0:
            Post_avgtemp += [round(Post_sumtemp/Post_counttemp, 1)]
        else:
            Post_avgtemp += [0]
    Team_Stats()
    TeamCasAvg()
    #print(Team_avg, np.max(Team_avg), Team_CountTotal)
    for xx in range(np.shape(BaseCAS_matrix)[1]-4):
        if BaseCAS_matrix[y][xx+4] > 0:
            if kwcitems_matrix5[n-1][xx] + 1 > Post_availmax[xx] and \
                    (kwcitems_matrix5[n-1][xx] + 1 < Post_availmin[xx] and \
                     count_team > round(np.shape(BaseCAS_matrix)[0]/4), 0):
                xx_flag = 1
                break
            if count_team >= int(np.shape(BaseCAS_matrix)[0]/8) and xx > 2:
                if BaseCAS_matrix[y][xx+4] < Post_avgtemp[xx]*.2:
                    xx_flag = 1
                    break
            #if BaseCAS_matrix[y][2] == "Y" and count_team > np.shape(BaseCAS_matrix)[0]/4:
            #    if Team_ERTTotal[n-1] + 1 > PostERT_availmax:
            #        xx_flag = 1
            #        break
    if xx_flag == 1:
        return False
    return True


def Fill_solve():
    global BaseCAS_matrix, kwcitems_matrix5, solution_find, temp_matrix, Post_availmin, Index_list, count_perm_table, \
        Post_availmax, PostCAS_dataavail, PostCAS_dataavg, Team_CountTotal, Team_postavg, Team_avg, Fill_list, \
        Team_ERTTotal, Team_EMTTotal, Zero_BaseCAS_matrix, NonZero_BaseCAS_matrix, perm_count, Perm_matrix, xx_flag, \
        Fill_solve_counter, LowPostOut_list, HighPostOut_list
    Team_Stats()
    Fill_solve_counter += 1
    #print(Fill_solve_counter)
    if np.sum(Team_CountTotal) + 1 == np.shape(BaseCAS_matrix)[0]:
        lowteam = np.where(Team_CountTotal == np.min(Team_CountTotal))
        for x_rows in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[x_rows][0] == 0:
                BaseCAS_matrix[x_rows][0] = int(lowteam[0]) + 1
                #print(BaseCAS_matrix[x_rows][0], BaseCAS_matrix[x_rows][1])
    else:
        for y in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[y][0] == 0:
                for n in range(1, 5):
                    if Fill_possible(y, n):
                        BaseCAS_matrix[y][0] = n
                        Team_Stats()
                        Fill_solve()
                        BaseCAS_matrix[y][0] = 0
                        return
    #print(HighPostOut_list, LowPostOut_list)
    last_tuning()


def last_tuning():
    global BaseCAS_matrix, kwcitems_matrix5, Team_CountTotal, Team_postavg, Team_avg, Team_ERTTotal, Team_EMTTotal, \
        solution_find, perm_count, Team_postavg_mintemp, Team_postavg_maxtemp, Team_postavgstddevtemp, \
        Team_postavgtemp, kwcitems_matrix5temp, Team_ERTTotaltemp, Team_EMTTotaltemp, BaseCAS_matrixtemp, Team_avgtemp, \
        perm_count_final, Post_avgCheck, perm_count_max, Last_Ditch_Effort, Stop_this, Fill_solve_counter
    TeamCasAvg()
    Team_Stats()
    #Post_avgCheck = 0
    if np.sum(Team_CountTotal) != np.shape(BaseCAS_matrix)[0]:
        #perm_count += 1
        Fill_solve_counter = 0
        #print('Hello Team Count')
        Perm_Table_Set()
        Fill_solve()
    elif round(np.min(Team_avg), 1) < round(np.min(Team_avgBase), 1):
        #perm_count += 1
        Fill_solve_counter = 0
        #print('Hello Team Avg')
        Perm_Table_Set()
        Fill_solve()
    elif np.min(kwcitems_matrix5) == 1:
        #perm_count += 1
        Fill_solve_counter = 0
        #print("Hello Post Problem")
        Perm_Table_Set()
        Fill_solve()
    elif np.min(Team_postavg) >= Team_postavg_mintemp and np.max(Team_postavg) <= Team_postavg_maxtemp \
            and round(np.sum(np.std(Team_postavg, axis=0))**.5, 1) < round(Team_postavgstddevtemp, 1):
        Team_postavg_mintemp = np.min(Team_postavg)
        Team_postavg_maxtemp = np.max(Team_postavg)
        Team_postavgstddevtemp = np.sum(np.std(Team_postavg, axis=0))**.5
        Team_postavgtemp = Team_postavg
        kwcitems_matrix5temp = kwcitems_matrix5
        BaseCAS_matrixtemp = BaseCAS_matrix
        Team_avgtemp = Team_avg
        Team_ERTTotaltemp = Team_ERTTotal
        Team_EMTTotaltemp = Team_EMTTotal
        perm_count_final = perm_count
        Post_avgCheck = 1
        #if Post_avgCheck == 1 and np.min(Team_ERTTotal) > 0:
        #    Team_ERTTotaltemp = Team_ERTTotal
        #    Team_EMTTotaltemp = Team_EMTTotal

        #if Post_avgCheck == 0:
        #if Post_avgCheck != 1:
    if perm_count > 3380:
        Last_Ditch_Effort = 1
        BaseCAS_matrix = BaseCAS_matrixtemp
        kwcitems_matrix5 = kwcitems_matrix5temp
        Team_postavg = Team_postavgtemp
        Team_avg = Team_avgtemp
        Team_postavg_min = Team_postavg_mintemp
        Team_postavg_max = Team_postavg_maxtemp
        Team_postavgstddev = Team_postavgstddevtemp
        Flag_cannot = 1
        Next_Checks()
    elif Post_avgCheck != 1:
        #perm_count += 1
        Fill_solve_counter = 0
        #print("Hello Post Avail Problem")
        Perm_Table_Set()
        Fill_solve()

    if Post_avgCheck == 1 and Stop_this != 1:
        Stop_this = 1
        BaseCAS_matrix = BaseCAS_matrixtemp
        kwcitems_matrix5 = kwcitems_matrix5temp
        Team_postavg = Team_postavgtemp
        Team_avg = Team_avgtemp
        Team_postavg_min = Team_postavg_mintemp
        Team_postavg_max = Team_postavg_maxtemp
        Team_postavgstddev = Team_postavgstddevtemp
        if np.min(Team_ERTTotal) < PostERT_availmin - 2:
            lowERT = np.where(Team_ERTTotal == np.min(Team_ERTTotal))
            #Check if STL Post can be moved with affecting 2nd Post
            STL_flaglowERT = "N"
            STL_flagswapERT = "N"
            STL_flagteamERT = 0
            for x_rows in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[x_rows][1] not in Sal_matrix and BaseCAS_matrix[x_rows][4] > 0 and \
                        BaseCAS_matrix[x_rows][5] == 0:
                    if BaseCAS_matrix[x_rows][0] == int(lowERT[0]) + 1 and BaseCAS_matrix[x_rows][2] == "Y":
                        STL_flaglowERT = "N"
                    elif BaseCAS_matrix[x_rows][0] == int(lowERT[0]) + 1 and BaseCAS_matrix[x_rows][2] == "N":
                        STL_flaglowERT = BaseCAS_matrix[x_rows][1]
                        break
            for x_rows in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[x_rows][1] not in Sal_matrix and BaseCAS_matrix[x_rows][4] > 0 and \
                        BaseCAS_matrix[x_rows][5] == 0 and BaseCAS_matrix[x_rows][2] == "Y":
                    if STL_flaglowERT != "N" and BaseCAS_matrix[x_rows][0] != int(lowERT[0]) + 1 and \
                            STL_flagswapERT == "N" and STL_flagteamERT == 0:
                        STL_flagswapERT = BaseCAS_matrix[x_rows][1]
                        STL_flagteamERT = BaseCAS_matrix[x_rows][0]
            if STL_flagteamERT != 0 and solution_find < 10000:
                for x_rows in range(np.shape(BaseCAS_matrix)[0]):
                    if BaseCAS_matrix[x_rows][1] == STL_flaglowERT:
                        BaseCAS_matrix[x_rows][0] = STL_flagteamERT
                    if BaseCAS_matrix[x_rows][1] == STL_flagswapERT:
                        BaseCAS_matrix[x_rows][0] = int(lowERT[0]) + 1
                    if BaseCAS_matrix[x_rows][1] not in Sal_matrix and BaseCAS_matrix[x_rows][4] == 0:
                        BaseCAS_matrix[x_rows][0] = 0
                Team_Stats()
                TeamCasAvg()
                #solve_FillSTLPost()
        else:
            Next_Checks()



def Next_Checks():
    global BaseCAS_matrix, kwcitems_matrix5, Team_CountTotal, Team_postavg, Team_avg, Team_ERTTotal, Team_EMTTotal, \
        PostCAS_dataavg, PostCAS_dataavail, PostERT_availmax, Post_availmin, PostERT_avail, PostEMT_avail, PostERT_availmin, \
        PostERT_availmax, PostEMT_availmin, PostEMT_availmax
    Team_Stats()
    TeamCasAvg()
    #Check low Comp Avg Scores
    lowCAS = np.where(Team_postavg == np.min(Team_postavg))
    final_print()


def final_print():
    global BaseCAS_matrix, kwcitems_matrix5, Team_CountTotal, Team_postavg, Team_avg, Team_ERTTotal, Team_EMTTotal, \
        PostCAS_dataavg, PostCAS_dataavail, PostERT_availmax, Post_availmin, PostERT_avail, PostEMT_avail, PostERT_availmin, \
        PostERT_availmax, PostEMT_availmin, PostEMT_availmax, Post_Headerinfo, Flag_cannot
    TeamCasAvg()
    Team_Stats()
    if Flag_cannot != 1:
        Create_pdf()


#Get some data from BaseCAS_Data
PostCAS_dataavail = []
PostCAS_dataavg = []
PostERT_avail = 0
PostEMT_avail = 0
solution_find = 0
for colitems in range(BaseCAS_shapecols):
    PostCAS_sum = 0
    PostCAS_count = 0
    PostCAS_avg = 0
    PostCAS_avail = 0
    PostERT_count = 0
    PostEMT_count = 0
    for rowitems in range(BaseCAS_shaperows):
        if colitems > 3 and BaseCAS_matrix[rowitems, colitems] > 0:
            PostCAS_sum = PostCAS_sum + BaseCAS_matrix[rowitems, colitems]
            PostCAS_count = PostCAS_count + 1
        if colitems == 2 and BaseCAS_matrix[rowitems, colitems] == 'Y':
            PostERT_count = PostERT_count + 1
        if colitems == 3 and BaseCAS_matrix[rowitems, colitems] == 'Y':
            PostEMT_count = PostEMT_count + 1
    if colitems == 2:
        PostERT_avail = PostERT_count / 4
    if colitems == 3:
        PostEMT_avail = PostEMT_count / 4
    if PostCAS_count > 0:
        PostCAS_avg = round(PostCAS_sum / PostCAS_count, 1)
        PostCAS_avail = PostCAS_count / 4
        PostCAS_dataavail += [PostCAS_avail]
        PostCAS_dataavg += [PostCAS_avg]
if int(PostERT_avail) == PostERT_avail:
    PostERT_availmax = int(PostERT_avail)
    if PostERT_avail > 2:
        PostERT_availmin = int(PostERT_avail) - 1
    else:
        PostERT_availmin = int(PostERT_avail)
else:
    PostERT_availmax = int(PostERT_avail) + 1
    if PostERT_avail > 2:
        PostERT_availmin = int(PostERT_avail) - 1
if int(PostEMT_avail) == PostEMT_avail:
    PostEMT_availmax = int(PostEMT_avail)
    PostEMT_availmin = int(PostEMT_avail)
else:
    PostEMT_availmax = int(PostEMT_avail) + 1
    PostEMT_availmin = int(PostEMT_avail)
Post_availmin = []
Post_availmax = []
Post_availmaxmax = []
for gggg in range(np.shape(PostCAS_dataavail)[0]):
    if int(PostCAS_dataavail[gggg]) == PostCAS_dataavail[gggg]:
        Post_availmin += [PostCAS_dataavail[gggg]]
        Post_availmax += [PostCAS_dataavail[gggg]]
        Post_availmaxmax += [4]
    if int(PostCAS_dataavail[gggg]) < PostCAS_dataavail[gggg]:
        Post_availmin += [int(PostCAS_dataavail[gggg])]
        Post_availmax += [int(PostCAS_dataavail[gggg]) + 1]
        Post_availmaxmax += [(PostCAS_dataavail[gggg] - int(PostCAS_dataavail[gggg])) * 4]
Post_availmax_watch = np.where(Post_availmax == np.max(Post_availmax))
#Change Teams to Team Numbers and zero out all but 1st Post Salary
for x_rows in range(np.shape(BaseCAS_matrix)[0]):
    if BaseCAS_matrix[x_rows][0] == "A":
        if BaseCAS_matrix[x_rows][1] in Sal_matrix and BaseCAS_matrix[x_rows][4] > 0:
            BaseCAS_matrix[x_rows][0] = 1
        else:
            BaseCAS_matrix[x_rows][0] = 0
    if BaseCAS_matrix[x_rows][0] == "B":
        if BaseCAS_matrix[x_rows][1] in Sal_matrix and BaseCAS_matrix[x_rows][4] > 0:
            BaseCAS_matrix[x_rows][0] = 2
        else:
            BaseCAS_matrix[x_rows][0] = 0
    if BaseCAS_matrix[x_rows][0] == "C":
        if BaseCAS_matrix[x_rows][1] in Sal_matrix and BaseCAS_matrix[x_rows][4] > 0:
            BaseCAS_matrix[x_rows][0] = 3
        else:
            BaseCAS_matrix[x_rows][0] = 0
    if BaseCAS_matrix[x_rows][0] == "D":
        if BaseCAS_matrix[x_rows][1] in Sal_matrix and BaseCAS_matrix[x_rows][4] > 0:
            BaseCAS_matrix[x_rows][0] = 4
        else:
            BaseCAS_matrix[x_rows][0] = 0


def STL_PostSolve():
    global BaseCAS_matrix, kwcitems_matrix5, STL_PostFlag
    for x_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[x_rows][1] in Sal_matrix:
            BaseCAS_matrix[x_rows][0] = STL_PostFlag
            solve_Salary()


#Function to analyze team roster progress
def solution_matrix():
    global BaseCAS_matrix, Team_CountTotal, solution_find, kwcitems_matrix5, STL_PostFlag
    solution_find += 1
    kwcitems_count = 0
    kwcitems_matrix = []
    for kwcitems_cols in range(4, 10):
        for kwcitems_n in range(1, 5):
            kwcitems_count = 0
            for kwcitems_rows in range(np.shape(BaseCAS_matrix)[0]):
                if BaseCAS_matrix[kwcitems_rows][0] == kwcitems_n and \
                        BaseCAS_matrix[kwcitems_rows][kwcitems_cols] != 0:
                    kwcitems_count = kwcitems_count + 1
            kwcitems_matrix += [kwcitems_n, kwcitems_count]
    kwcitems_matrix1 = []
    kwcitems_matrix2 = []
    kwcitems_matrix3 = []
    kwcitems_matrix4 = []
    kwcitems_matrix5 = []
    Count_1 = 0
    Count_2 = 0
    Count_3 = 0
    Count_4 = 0
    for team_rows in range(np.shape(BaseCAS_matrix)[0]):
        if BaseCAS_matrix[team_rows][0] == 1:
            Count_1 += 1
        if BaseCAS_matrix[team_rows][0] == 2:
            Count_2 += 1
        if BaseCAS_matrix[team_rows][0] == 3:
            Count_3 += 1
        if BaseCAS_matrix[team_rows][0] == 4:
            Count_4 += 1
    Team_CountTotal = np.hstack((Count_1, Count_2, Count_3, Count_4))
    for kwcitems_cols2 in range(0, np.shape(kwcitems_matrix)[0], 2):
        if kwcitems_matrix[kwcitems_cols2] == 1:
            kwcitems_matrix1 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 2:
            kwcitems_matrix2 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 3:
            kwcitems_matrix3 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 4:
            kwcitems_matrix4 += [kwcitems_matrix[kwcitems_cols2+1]]
    kwcitems_matrix5 = np.vstack((kwcitems_matrix1, kwcitems_matrix2, kwcitems_matrix3, kwcitems_matrix4))
    STL_PostFlag = 0
    for yyy in range(np.shape(kwcitems_matrix5)[0]):
        if kwcitems_matrix5[yyy][0] == 0:
            STL_PostFlag = yyy + 1
            break
    if STL_PostFlag > 0:
        STL_PostSolve()
    #if solution_find > 1:
    if solution_find >= 100:
        solve_FillSTLPost()
        return
    else:
        solve_Salary()


if solution_find == 0:
    solution_matrix()

A_peoplecount = 0
B_peoplecount = 0
C_peoplecount = 0
D_peoplecount = 0
for count_people in range(np.shape(BaseCAS_matrix)[0]):
    if BaseCAS_matrix[count_people, 0] == "A":
        A_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "B":
        B_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "C":
        C_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "D":
        D_peoplecount += 1
count_team = 1
xstart = 4
BadName_Number = []
temp_matrix = []

input_table = pd.read_table('PassDate.txt', delimiter=',')
sdf_inputtable = pd.DataFrame(input_table)
Pass_matrix = np.array(sdf_inputtable)
Pass_shape = np.shape(Pass_matrix)[0]

if Pass_matrix[0, 3] != 0 and solution_find == 0:
    plot_posts = []
    #BaseCAS_sdf.drop(["Team", "Name", "ERT Qual", "EMT Qual"],  inplace=True, axis=1)
    plot_posts = np.array(BaseCAS_sdf.columns)
    newmatrix_final = []
    newmatrix_final = BaseCAS_matrix
    #Convert Team Names to Numbers
    #People Competency
    comp_people = []
    comp_peoplenames = []
    comp_peopleavg = []
    for comp_peoplerows in range(np.shape(newmatrix_final)[0]):
        comp_peoplesum = 0
        comp_peoplecount = 0
        comp_avg = 0
        for comp_peoplecols in range(4, np.shape(newmatrix_final)[1]):
            if newmatrix_final[comp_peoplerows][comp_peoplecols] > 0:
                comp_peoplesum = comp_peoplesum + newmatrix_final[comp_peoplerows][comp_peoplecols]
                comp_peoplecount = comp_peoplecount + 1
                comp_avg = comp_peoplesum/comp_peoplecount
        comp_peopleavg += [comp_avg]
        comp_peoplenames += [newmatrix_final[comp_peoplerows][1]]

    #Zero Out Team Designations for Selected Personnel
    for rowitems in range(np.shape(newmatrix_final)[0]):
        for colitems in range(4, np.shape(newmatrix_final)[1]):
            if newmatrix_final[rowitems][1] not in Sal_matrix:
                newmatrix_final[rowitems][0] = 0

    PostCAS_availsort = PostCAS_dataavail
    PostCAS_availsort.sort(reverse=True)
    PostCAS_sortfinal = []
    kwcitems_count = 0
    kwcitems_matrix = []
    for kwcitems_cols in range(4, np.shape(BaseCAS_matrix)[1]):
        for kwcitems_n in range(1, 5):
            kwcitems_count = 0
            for kwcitems_rows in range(np.shape(newmatrix_final)[0]):
                if newmatrix_final[kwcitems_rows][0] == kwcitems_n and \
                        newmatrix_final[kwcitems_rows][kwcitems_cols] != 0:
                    kwcitems_count = kwcitems_count + 1
            kwcitems_matrix += [kwcitems_n, kwcitems_count]
    kwcitems_matrix1 = []
    kwcitems_matrix2 = []
    kwcitems_matrix3 = []
    kwcitems_matrix4 = []
    kwcitems_matrix5 = []
    for kwcitems_cols2 in range(0, np.shape(kwcitems_matrix)[0], 2):
        if kwcitems_matrix[kwcitems_cols2] == 1:
            kwcitems_matrix1 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 2:
            kwcitems_matrix2 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 3:
            kwcitems_matrix3 += [kwcitems_matrix[kwcitems_cols2+1]]
        if kwcitems_matrix[kwcitems_cols2] == 4:
            kwcitems_matrix4 += [kwcitems_matrix[kwcitems_cols2+1]]
    kwcitems_matrix5 = np.vstack((kwcitems_matrix1, kwcitems_matrix2, kwcitems_matrix3, kwcitems_matrix4))
    temp_matrix = kwcitems_matrix5
