import pandas as pd
import numpy as np
import csv
from scipy.optimize import linear_sum_assignment
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import runpy
import subprocess


global weeks_entry, sched_date

rootmain = Tk()
weeks_entry = 0
sched_date = ''
style = ttk.Style()
style.theme_use('clam')

SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)
InfoTable_matrixshape = np.shape(InfoTable_matrix)

Schedule_table = pd.read_csv('Total Schedule.txt', delimiter=',')
sdf_ScheduleTable = pd.DataFrame(Schedule_table)
ScheduleTable_matrix = np.array(sdf_ScheduleTable)
ScheduleTable_matrixshape = np.shape(ScheduleTable_matrix)

Initial_table = pd.read_csv('Initial Input.txt', delimiter=',')
sdf_InitialTable = pd.DataFrame(Initial_table)
Initial_matrix = np.array(sdf_InitialTable)
Post_CountDays = 0
Post_CountNights = 0
for items in range(np.shape(Initial_matrix)[0]):
    if Initial_matrix[items, 1] == "Y" or Initial_matrix[items, 2] == "Y":
        Post_CountDays += 1
    if Initial_matrix[items, 1] == "Y" and Initial_matrix[items, 2] != "Y":
        Post_CountNights += 1

Post_count = np.shape(Initial_matrix)[0]

Schedule_table = pd.read_csv('Total Schedule.txt', delimiter=',')
sdf_ScheduleTable = pd.DataFrame(Schedule_table)
ScheduleTable_matrix = np.array(sdf_ScheduleTable)

BaseCAS_Data = pd.read_csv("Base Data CAS.txt", delimiter=',')
BaseCAS_sdf = pd.DataFrame(BaseCAS_Data)
BaseCAS_matrix = np.array(BaseCAS_sdf)
BaseCAS_shaperows = np.shape(BaseCAS_matrix)[0]
BaseCAS_shapecols = np.shape(BaseCAS_matrix)[1]

#Get some data from BaseCAS_Data
PostCAS_data = []
PostERT_avail = 0
PostEMT_avail = 0
for colitems in range(BaseCAS_shapecols):
    PostCAS_sum = 0
    PostCAS_count = 0
    PostCAS_avg = 0
    PostCAS_avail = 0
    PostERT_count = 0
    PostEMT_count = 0
    for rowitems in range(BaseCAS_shaperows):
        if colitems > 3 and BaseCAS_matrix[rowitems, colitems] > 0:
            PostCAS_sum = PostCAS_sum + BaseCAS_matrix[rowitems, colitems]
            PostCAS_count = PostCAS_count + 1
        if colitems == 2 and BaseCAS_matrix[rowitems, colitems] == 'Y':
            PostERT_count = PostERT_count + 1
        if colitems == 3 and BaseCAS_matrix[rowitems, colitems] == 'Y':
            PostEMT_count = PostEMT_count + 1
    if colitems == 2:
        PostERT_avail = PostERT_count / 4
    if colitems == 3:
        PostEMT_avail = PostEMT_count / 4
    if PostCAS_count > 0:
        PostCAS_avg = PostCAS_sum / PostCAS_count
        PostCAS_avail = PostCAS_count / 4
        if np.shape(PostCAS_data)[0] == 0:
            PostCAS_data += [PostCAS_avail, PostCAS_avg]
        else:
            PostCAS_data = np.vstack((PostCAS_data, [PostCAS_avail, PostCAS_avg]))
PostCAS_data = np.vstack((PostCAS_data))
#print(PostCAS_data, PostERT_avail, PostEMT_avail)


#print(BaseCAS_sdf.columns.values[6])


A_peoplecount = 0
B_peoplecount = 0
C_peoplecount = 0
D_peoplecount = 0

for count_people in range(np.shape(BaseCAS_matrix)[0]):
    if BaseCAS_matrix[count_people, 0] == "A":
        A_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "B":
        B_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "C":
        C_peoplecount += 1
    if BaseCAS_matrix[count_people, 0] == "D":
        D_peoplecount += 1

#Setup Base Data (matrix_try) to usable matrix
BaseCAS_sdf_try = BaseCAS_sdf
BaseCAS_matrix_try = BaseCAS_matrix
BaseCAS_matrix_try = np.delete(BaseCAS_matrix_try, 0, 1)
BaseCAS_matrix_try = np.delete(BaseCAS_matrix_try, 0, 1)
BaseCAS_matrix_try = np.delete(BaseCAS_matrix_try, 0, 1)
BaseCAS_matrix_try = np.delete(BaseCAS_matrix_try, 0, 1)
BaseCAS_matrix_try1 = np.delete(BaseCAS_matrix_try, 0, 1)
CAS_avg = []
for CAS_rows in range(np.shape(BaseCAS_matrix_try1)[0]):
    CAS_avg += [np.mean(BaseCAS_matrix_try1[CAS_rows], axis=0)]

BaseCAS_matrix_try = np.where(BaseCAS_matrix_try == 0, 10000, BaseCAS_matrix_try)
BaseCAS_matrix_try = np.where(BaseCAS_matrix_try <= 4, -BaseCAS_matrix_try, BaseCAS_matrix_try)
BaseCAS_matrix_try = np.hstack((BaseCAS_matrix_try, BaseCAS_matrix_try))
BaseCAS_matrix_try = np.hstack((BaseCAS_matrix_try, BaseCAS_matrix_try))



#print(BaseCAS_matrix_try)
#Extract the Table Data from the Matrix and put into another Matrix
ncol_size = np.shape(BaseCAS_matrix_try)[1]
matrix_kwc = []
for n_row in range(np.shape(BaseCAS_matrix_try)[0]):
    matrix_kwc2 = []
    for n_col in range(ncol_size):
        matrix_kwc2 += [BaseCAS_matrix_try[n_row, n_col]]
    matrix_kwc += [matrix_kwc2]

#print(BaseCAS_matrix_try)
#print(BaseCAS_sdf_try)

#Extract the Names from the Matrix and put into another Matrix
matrix_names = []
for m_row in range(np.shape(BaseCAS_matrix)[0]):
    matrix_names += [BaseCAS_matrix[m_row, 1]]

#Setup Post and Team Names
InitInput_table = pd.read_table('Initial Input.txt', delimiter=',')
sdf_InitInputTable = pd.DataFrame(InitInput_table)
InitInput_matrix = np.array(sdf_InitInputTable)
InitInput_shape = np.shape(InitInput_matrix)[0]

keithDayPosts = []
keithNightPosts = []
for post_items in range(InitInput_shape):
    if InitInput_matrix[post_items, 1] == "Y" or InitInput_matrix[post_items, 2] == "Y":
        keithDayPosts += [InitInput_matrix[post_items, 0]]
    if InitInput_matrix[post_items, 1] == "Y":
        keithNightPosts += [InitInput_matrix[post_items, 0]]

keithPosts = np.hstack((keithDayPosts, keithNightPosts))
keithPosts = np.hstack((keithPosts, keithPosts))
keithPostsize = np.shape(keithPosts)[0]

keithPosts2 = []
for post_items in range(InitInput_shape * 4 + 4):
    if post_items < InitInput_shape:
        keithPosts2 += [keithPosts[post_items] + "A"]
    elif InitInput_shape <= post_items < InitInput_shape * 2:
        keithPosts2 += [keithPosts[post_items] + "B"]
    elif InitInput_shape * 2 <= post_items < InitInput_shape * 3:
        keithPosts2 += [keithPosts[post_items] + "C"]
    elif InitInput_shape * 3 <= post_items < InitInput_shape * 4:
        keithPosts2 += [keithPosts[post_items] + "D"]
    elif InitInput_shape * 4 <= post_items < InitInput_shape * 4 + 1:
        keithPosts2 += ['VR_A']
    elif InitInput_shape * 4 + 1 <= post_items < InitInput_shape * 4 + 2:
        keithPosts2 += ['VR_B']
    elif InitInput_shape * 4 + 2 <= post_items < InitInput_shape * 4 + 3:
        keithPosts2 += ['VR_C']
    else:
        keithPosts2 += ['VR_D']

post_size = np.shape(keithPosts)[0]
keith_vertposts = []
keith_vertposts = np.reshape(keithPosts2, (post_size + 4, 1))

#Create 'Dummy' columns to balance the Matrix
CASmatrix_dum = []
for row_dum in range(np.shape(BaseCAS_matrix_try)[0]):
    cost_row = []
    for col_dum in range(np.shape(BaseCAS_matrix_try)[0]-np.shape(BaseCAS_matrix_try)[1]):
        cost_row += [float(round(10 + CAS_avg[row_dum], 2))]
    CASmatrix_dum += [cost_row]
CASmatrix_kwc = np.hstack((BaseCAS_matrix_try, CASmatrix_dum))

#Extract the Table Data from the Matrix and put into another Matrix
ncol_size = np.shape(CASmatrix_kwc)[1]
nmatrix_kwc = []
for n_row in range(np.shape(CASmatrix_kwc)[0]):
    matrix_kwc2 = []
    for n_col in range(ncol_size):
        matrix_kwc2 += [CASmatrix_kwc[n_row, n_col]]
    nmatrix_kwc += [matrix_kwc2]

#Perform 2DAP on the table data matrix
row_ind, col_ind = linear_sum_assignment(nmatrix_kwc)
opt_assignc = col_ind
opt_assignr = row_ind
#tc = CASnmatrix_kwc[row_ind, col_ind].sum()

#print(opt_assignr)
#print(opt_assignc)
#print(np.shape(matrix_names))
#print(post_size)

#Align the assignments and print
matrix_final1 = []
matrix_finalOTSched = []
matrix_finalcount = []
for item_posts in keith_vertposts:
    row_kwckwc = 0
    for xcol_kwc in range(np.shape(nmatrix_kwc)[1]):
        if col_ind[xcol_kwc] < np.shape(nmatrix_kwc)[1] and item_posts == keith_vertposts[col_ind[xcol_kwc]]:
            matrix_final1.append(matrix_names[row_kwckwc])
            matrix_finalOTSched.append(0)
            matrix_finalcount.append(1)
        row_kwckwc = row_kwckwc + 1

matrix_final = []
matrix_final = np.reshape(matrix_final1, (post_size + 4, 1))
matrix_finalP = np.concatenate((keith_vertposts, matrix_final), 1)
matrix_finalPP = np.reshape(matrix_finalP, (post_size + 4, 2))

input_table = pd.read_table('PassDate.txt', delimiter=',')
sdf_inputtable = pd.DataFrame(input_table)
Pass_matrix = np.array(sdf_inputtable)
Pass_shape = np.shape(Pass_matrix)[0]

def UpdateInfo_chart():
    rootmain.destroy()
    runpy.run_path(path_name="PostScheduleMain.py")


def Open_pdf():
    file_name = '/Users/kelliecascarelli/PycharmProjects/PostScheduleMVC/test.pdf'
    subprocess.call(['open', file_name])


def exit_main():
    rootmain.destroy()


def Setup_Schedule():
    rootmain.destroy()
    runpy.run_path(path_name="SetupScheduleV2.py")


def Initial_Input():
    rootmain.destroy()
    runpy.run_path(path_name="InitialInput.py")


def Update_Posts():
    rootmain.destroy()
    runpy.run_path(path_name="UpdatePostData.py")


def Event_Input():
    rootmain.destroy()
    runpy.run_path(path_name="EventCalendarV3.py")


def edit_basedata():
    rootmain.destroy()
    runpy.run_path(path_name="BaseDataInput4.py")


def edit_basecomp():
    rootmain.destroy()
    runpy.run_path(path_name="BaseDataCAS.py")

label_width = 78
label_height = 100
frame_start = 0
frame_position = 260
frame_xpos = 10
right_frames = frame_xpos + 709
the_color = 'black'

wrap_main1 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main1.place(x=frame_xpos, y=frame_start)
wrap_main1.configure(bg=the_color)
label1 = Label(wrap_main1, width=label_width, height=label_height, bg=the_color, fg='white')
label1.pack()
label11 = Label(label1, text="Set Up the Schedule", font="Arial 20 bold", bg=the_color,
                fg='white', width=18)
label11.place(x=0, y=10)
label12 = Label(label1, text="Current Schedule:", font="Arial 16 bold", bg=the_color, fg='white', width=25)
label12.place(x=240, y=12)
label13 = Label(label1, text=InfoTable_matrix[0,0], font="Arial 20 bold", bg=the_color, fg="white", width=20)
label13.place(x=420, y=10)
label141 = Label(label1, text="OT Criteria:", font="Arial 16 bold", bg=the_color, fg='white', width=9)
label141.place(x=335, y=42)
label151 = Label(label1, text=str(InfoTable_matrix[0,3]), font="Arial 20 bold", bg=the_color,
                 fg="white", width=8)
label151.place(x=454, y=40)
label14 = Label(label1, text="Slider Tolerance:", font="Arial 16 bold", bg=the_color, fg='white', width=30)
label14.place(x=222, y=72)
TextInfo = "Shifts"
Posx = 460
if InfoTable_matrix[0, 1] == 1:
    TextInfo = "Shift"
    Posx = 454
label15 = Label(label1, text=str(InfoTable_matrix[0, 1]) + " " + TextInfo, font="Arial 20 bold", bg=the_color,
                fg="white", width=6)
label15.place(x=Posx, y=70)
label16 = Label(label1, text="Double Time Day:", font="Arial 16 bold", bg=the_color, fg='white', width=30)
label16.place(x=218, y=102)
if InfoTable_matrix[0, 2] == 0:
    Doubleday = "None"
    label17 = Label(label1, text=Doubleday, font="Arial 20 bold", bg=the_color, fg="white", width=4)
    label17.place(x=459, y=100)
else:
    Doubleday = InfoTable_matrix[0, 2]
    label17 = Label(label1, text=Doubleday, font="Arial 20 bold", bg=the_color, fg="white", width=1)
    label17.place(x=459, y=100)

label18 = Label(label1, text="Initialize Post Information", font="Arial 20 bold", bg=the_color, fg='white', width=20)
label18.place(x=13, y=80)

label181 = Label(label1, text="Update Post Information", font="Arial 20 bold", bg=the_color,
                 fg='white', width=20)
label181.place(x=7, y=150)

Button_11 = tk.Button(wrap_main1, text="Press to Select", font="Arial 14 bold", width=13, command=Setup_Schedule)
Button_11.configure(fg='red')
Button_11.place(x=35, y=45)

Button_18 = tk.Button(wrap_main1, text="Press to Select", font="Arial 14 bold", width=13, command=Initial_Input)
Button_18.configure(fg='red')
Button_18.place(x=35, y=115)

Button_181 = tk.Button(wrap_main1, text="Press to Select", font="Arial 14 bold", width=13, command=Update_Posts)
Button_181.configure(fg='red')
Button_181.place(x=35, y=185)

wrap_main2 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main2.place(x=frame_xpos, y=frame_start+frame_position-40)
wrap_main2.configure(bg=the_color)
label2 = Label(wrap_main2, width=label_width, height=label_height, bg=the_color)
label2.pack()
label21 = Label(label2, text="View/Edit Post Data", bg=the_color, fg='white', font="Arial 20 bold", width=15)
label21.place(x=12, y=70)
Button_21 = tk.Button(wrap_main2, text="Press to Select", font="Arial 14 bold", width=13, command=edit_basedata)
Button_21.configure(fg="red")
Button_21.place(x=35, y=105)
label22 = Label(label2, text="View/Edit Competency & ER Information", bg=the_color,
                font="Arial 20 bold", fg='white', width=32)
label22.place(x=10, y=0)
Button_22 = tk.Button(wrap_main2, text="Press to Select", font="Arial 14 bold", width=13, command=edit_basecomp)
Button_22.configure(fg="red")
Button_22.place(x=35, y=35)

if Pass_matrix[0, 3] != 0:
    plot_posts = []
    BaseCAS_sdf.drop(["Team", "Name", "ERT Qual", "EMT Qual"],  inplace=True, axis=1)
    plot_posts = np.array(BaseCAS_sdf.columns)

    plot_yACASavg = []
    plot_yBCASavg = []
    plot_yCCASavg = []
    plot_yDCASavg = []
    for CAS_itemscols in range(np.shape(BaseCAS_matrix)[1]-4):
        countA = 0
        countB = 0
        countC = 0
        countD = 0
        plot_yACASsum = 0
        plot_yBCASsum = 0
        plot_yCCASsum = 0
        plot_yDCASsum = 0
        for CAS_itemsrows in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4] != 0:
                if BaseCAS_matrix[CAS_itemsrows][0] == "A":
                    plot_yACASsum = plot_yACASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countA += 1
                if BaseCAS_matrix[CAS_itemsrows][0] == "B":
                    plot_yBCASsum = plot_yBCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countB += 1
                if BaseCAS_matrix[CAS_itemsrows][0] == "C":
                    plot_yCCASsum = plot_yCCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countC += 1
                if BaseCAS_matrix[CAS_itemsrows][0] == "D":
                    plot_yDCASsum = plot_yDCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                    countD += 1
        plot_yACASavg += [round(plot_yACASsum/countA, 1)]
        plot_yBCASavg += [round(plot_yBCASsum/countB, 1)]
        plot_yCCASavg += [round(plot_yCCASsum/countC, 1)]
        plot_yDCASavg += [round(plot_yDCASsum/countD, 1)]
    print('Old', round(np.mean(plot_yACASavg, axis=0), 1), round(np.mean(plot_yBCASavg, axis=0), 1),
          round(np.mean(plot_yCCASavg, axis=0), 1), round(np.mean(plot_yDCASavg, axis=0), 1))
    plot_xCAS = []

    for xplot_values in range(np.shape(plot_posts)[0]):
        plot_xCAS += [xplot_values]

    plot_x3 = np.array(plot_xCAS)

    figure4 = plt.Figure(figsize=(4/6*np.shape(plot_posts)[0], 2.1), dpi=90, facecolor='black')
    ax3 = figure4.add_subplot(111)
    figure4.subplots_adjust(top=0.75)
    rects10 = ax3.bar(plot_x3 - .23, plot_yACASavg, color='blue', width=.12, label='A', align='center')
    rects11 = ax3.bar(plot_x3 - .09, plot_yBCASavg, color='orange', width=.1, label="B", align='center')
    rects12 = ax3.bar(plot_x3 + .09, plot_yCCASavg, color='purple', width=.1, label="C", align='center')
    rects13 = ax3.bar(plot_x3 + .23, plot_yDCASavg, color='green', width=.12, label="D", align='center')
    ax3.set_xticks(np.arange(0, np.shape(plot_posts)[0], 1))
    ax3.set_title("Avg Post Competency by Team Before", color="white", pad=15, fontsize=11)
    ax3.set_xticklabels(plot_posts, rotation=0)
    ax3.tick_params(labelsize=10, colors='white', pad=1)
    ax3.patch.set_color('black')
    ax3.set_ylim(0, 4)
    ax3.legend(['A', 'B', 'C', 'D'], facecolor='black', labelcolor='white', fontsize="small",
               frameon=False, ncol=4, bbox_to_anchor=(.9, 1.15))
    ax3.spines['bottom'].set_color('white')
    ax3.spines['left'].set_color('white')
    #figure4.tight_layout()
    chart4 = FigureCanvasTkAgg(figure4, wrap_main2)
    chart4.get_tk_widget().place(x=198, y=30)
    plt.show()

wrap_main4 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main4.place(x=right_frames, y=frame_start)
wrap_main4.configure(bg=the_color)
label4 = Label(wrap_main4, width=label_width, height=label_height, fg='white', bg=the_color)
label4.pack()

wrap_main3 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main3.place(x=frame_xpos, y=frame_start+frame_position*2-80)
wrap_main3.configure(bg=the_color)
label3 = Label(wrap_main3, width=label_width, height=label_height, bg=the_color, fg='white')
label3.pack()

if Pass_matrix[0, 3] != 0:
    plot_posts = []
    #BaseCAS_sdf.drop(["Team", "Name", "ERT Qual", "EMT Qual"],  inplace=True, axis=1)
    plot_posts = np.array(BaseCAS_sdf.columns)

    plot_yACASavg = []
    plot_yBCASavg = []
    plot_yCCASavg = []
    plot_yDCASavg = []
    for CAS_itemscols in range(np.shape(BaseCAS_matrix)[1]-4):
        countA = 0
        countB = 0
        countC = 0
        countD = 0
        plot_yACASsum = 0
        plot_yBCASsum = 0
        plot_yCCASsum = 0
        plot_yDCASsum = 0
        for CAS_itemsrows in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4] != 0:
                for PP_rows in range(np.shape(matrix_finalPP)[0]):
                    if (matrix_finalPP[PP_rows, 0][-1] == "A" or matrix_finalPP[PP_rows, 0][0] == "A") \
                            and BaseCAS_matrix[CAS_itemsrows][1] == matrix_finalPP[PP_rows, 1]:
                        plot_yACASsum = plot_yACASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                        countA += 1
                    if (matrix_finalPP[PP_rows, 0][-1] == "B" or matrix_finalPP[PP_rows, 0][0] == "B") \
                            and BaseCAS_matrix[CAS_itemsrows][1] == matrix_finalPP[PP_rows, 1]:
                        plot_yBCASsum = plot_yBCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                        countB += 1
                    if (matrix_finalPP[PP_rows, 0][-1] == "C" or matrix_finalPP[PP_rows, 0][0] == "C") \
                            and BaseCAS_matrix[CAS_itemsrows][1] == matrix_finalPP[PP_rows, 1]:
                        plot_yCCASsum = plot_yCCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                        countC += 1
                    if (matrix_finalPP[PP_rows, 0][-1] == "D" or matrix_finalPP[PP_rows, 0][0] == "D") \
                            and BaseCAS_matrix[CAS_itemsrows][1] == matrix_finalPP[PP_rows, 1]:
                        plot_yDCASsum = plot_yDCASsum + BaseCAS_matrix[CAS_itemsrows][CAS_itemscols+4]
                        countD += 1

        plot_yACASavg += [round(plot_yACASsum/countA, 1)]
        plot_yBCASavg += [round(plot_yBCASsum/countB, 1)]
        plot_yCCASavg += [round(plot_yCCASsum/countC, 1)]
        plot_yDCASavg += [round(plot_yDCASsum/countD, 1)]
    print('New', round(np.mean(plot_yACASavg, axis=0), 1), round(np.mean(plot_yBCASavg, axis=0), 1),
          round(np.mean(plot_yCCASavg, axis=0), 1), round(np.mean(plot_yDCASavg, axis=0), 1))
    #print(BaseCAS_matrix)
    #print(matrix_finalPP[0, 0][-1:])

    newmatrix_final = []
    for PPitems in range(np.shape(matrix_finalPP)[0]):
        newmatrix_temp = np.empty(())
        for BaseCASitems in range(np.shape(BaseCAS_matrix)[0]):
            if BaseCAS_matrix[BaseCASitems, 1] == matrix_finalPP[PPitems, 1]:
                newmatrix_temp = [matrix_finalPP[PPitems, 0][-1:], matrix_finalPP[PPitems, 1], BaseCAS_matrix[BaseCASitems, 2],
                                  BaseCAS_matrix[BaseCASitems, 3], int(BaseCAS_matrix[BaseCASitems, 4]), int(BaseCAS_matrix[BaseCASitems, 5]),
                                  int(BaseCAS_matrix[BaseCASitems, 6]), int(BaseCAS_matrix[BaseCASitems, 7]),
                                  int(BaseCAS_matrix[BaseCASitems, 8]), int(BaseCAS_matrix[BaseCASitems, 9])]
        newmatrix_final += [newmatrix_temp]
        #print(newmatrix_temp)
    #print(np.shape(newmatrix_final))

    plot_xCAS = []

    for xplot_values in range(np.shape(plot_posts)[0]):
        plot_xCAS += [xplot_values]

    plot_x3 = np.array(plot_xCAS)

    figure3 = plt.Figure(figsize=(4/6*np.shape(plot_posts)[0], 2.1), dpi=90, facecolor='black')
    ax3 = figure3.add_subplot(111)
    figure3.subplots_adjust(top=0.75)
    rects10 = ax3.bar(plot_x3 - .23, plot_yACASavg, color='blue', width=.12, label='A', align='center')
    rects11 = ax3.bar(plot_x3 - .09, plot_yBCASavg, color='orange', width=.1, label="B", align='center')
    rects12 = ax3.bar(plot_x3 + .09, plot_yCCASavg, color='purple', width=.1, label="C", align='center')
    rects13 = ax3.bar(plot_x3 + .23, plot_yDCASavg, color='green', width=.12, label="D", align='center')
    ax3.set_xticks(np.arange(0, np.shape(plot_posts)[0], 1))
    ax3.set_title("Avg Post Competency by Team After", color="white", pad=15, fontsize=11)
    ax3.set_xticklabels(plot_posts, rotation=0)
    ax3.tick_params(labelsize=10, colors='white', pad=1)
    ax3.patch.set_color('black')
    ax3.set_ylim(0, 4)
    ax3.legend(['A', 'B', 'C', 'D'], facecolor='black', labelcolor='white', fontsize="small",
               frameon=False, ncol=4, bbox_to_anchor=(.9, 1.15))
    ax3.spines['bottom'].set_color('white')
    ax3.spines['left'].set_color('white')
    #figure4.tight_layout()
    chart3 = FigureCanvasTkAgg(figure3, wrap_main3)
    chart3.get_tk_widget().place(x=198, y=30)
    plt.show()

if Pass_matrix[0, 3] != 0:
    Start_Date3 = date.today() - timedelta(date.today().weekday()) + timedelta(7)
    Start_Date2 = datetime.strftime(date.today() - timedelta(date.today().weekday()) + timedelta(7), "%b %d, %Y")
    End_Date3 = date.today() - timedelta(date.today().weekday()) + timedelta(20)
    End_Date2 = datetime.strftime(date.today() - timedelta(date.today().weekday()) + timedelta(20), "%b %d, %Y")

    plot_xeventdates = []
    plot_shortdates = []
    for dateitems in range(14):
        if dateitems == 0:
            plot_xeventdates += [datetime.strftime(Start_Date3, "%Y-%m-%d")]
            plot_shortdates += [datetime.strftime(Start_Date3, "%m/%d")]
        elif dateitems == 13:
            plot_xeventdates += [datetime.strftime(End_Date3, "%Y-%m-%d")]
            plot_shortdates += [datetime.strftime(End_Date3, "%m/%d")]
        else:
            plot_xeventdates += [datetime.strftime(Start_Date3 + timedelta(dateitems), "%Y-%m-%d")]
            plot_shortdates += [datetime.strftime(Start_Date3 + timedelta(dateitems), "%m/%d")]




if Pass_matrix[0, 3] != 0:
    Start_Date = datetime.strptime(ScheduleTable_matrix[0][0], "%Y-%m-%d")
    Start_Date = datetime.strftime(Start_Date, "%b %d, %Y")

    End_Date = datetime.strptime(ScheduleTable_matrix[ScheduleTable_matrixshape[0]-1][0], "%Y-%m-%d")
    End_Date = datetime.strftime(End_Date, "%b %d, %Y")


wrap_main6 = LabelFrame(rootmain, relief="solid", bd=1)
wrap_main6.place(x=right_frames, y=frame_start+frame_position*2-100)
wrap_main6.configure(bg=the_color)
label6 = Label(wrap_main6, width=label_width, height=label_height, bg=the_color, fg='white')
label6.pack()
label61 = Label(label4, text="Last Schedule Run Information: ", bg=the_color, font="Arial 20 bold", fg='white', width=27)
label61.place(x=1, y=10)
label62 = Label(label4, text=Start_Date + ' - ' + End_Date, bg=the_color, font="Arial 20 bold", fg='white', width=22)
label62.place(x=315, y=10)

if Pass_matrix[0, 3] != 0:
    plot_y = []
    plot_yA = 0
    plot_yB = 0
    plot_yC = 0
    plot_yD = 0
    plot_x = ['A', 'B', 'C', 'D']

    Start_Date = datetime.strptime(ScheduleTable_matrix[0][0], "%Y-%m-%d")
    Start_Date = datetime.strftime(Start_Date, "%b %d, %Y")

    End_Date = datetime.strptime(ScheduleTable_matrix[ScheduleTable_matrixshape[0]-1][0], "%Y-%m-%d")
    End_Date = datetime.strftime(End_Date, "%b %d, %Y")

    Comp_Aavg = []
    Comp_Bavg = []
    Comp_Cavg = []
    Comp_Davg = []
    A_sum = 0
    B_sum = 0
    C_sum = 0
    D_sum = 0
    AOT_sum = 0
    BOT_sum = 0
    COT_sum = 0
    DOT_sum = 0
    for sched_itemsrows in range(np.shape(ScheduleTable_matrix)[0]):
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A" and ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yA += 1
            else:
                plot_yA += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yA += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
            A_sum = A_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                        plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                        plot_yD -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                     Post_CountNights*2+8].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1

                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                        plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                        plot_yD -= 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B" and ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yB += 1
            else:
                plot_yB += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yB += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
            B_sum = B_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + 2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                            ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                     Post_CountNights*2+8].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1


                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                        plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                        plot_yD -= 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C" and ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yC += 1
            else:
                plot_yC += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yC += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
            C_sum = C_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                            plot_yD -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team D":
                        plot_yD -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                     Post_CountNights*2+8].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                            plot_yD -= 1

                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
                        plot_yD -= 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D" and ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find('/', 0, 100) == -1:
                plot_yD += 1
            else:
                plot_yD += 2
        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D" and \
                ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] != " ":
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find('/', 0, 100) == -1:
                plot_yD += 1
        if ScheduleTable_matrix[sched_itemsrows][1] == "Team D" or ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D":
            D_sum = D_sum + 1
            for slidenames in range(Post_CountDays):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                            plot_yC -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+2] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+Post_CountDays*2+8]:
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][1] == "Team C":
                        plot_yC -= 1
            for slidenames in range(Post_CountNights):
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8].find("/", 0, 20) != -1:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [0:ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                     Post_CountNights*2+8].find("/", 0, 20)] + "-Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] \
                            [ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2 + \
                                                                   Post_CountNights*2+8].find("/", 0, 20)+1:20] + "Slide" \
                            == ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                            plot_yA -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                            plot_yB -= 1
                        if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                            plot_yC -= 1
                if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+Post_CountNights*2+8] + "-Slide" == \
                        ScheduleTable_matrix[sched_itemsrows][slidenames+2]:
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A":
                        plot_yA -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B":
                        plot_yB -= 1
                    if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C":
                        plot_yC -= 1
        for sched_itemscols in range(0, Post_CountDays):
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team A" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                AOT_sum = AOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team B" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                BOT_sum = BOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team C" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                COT_sum = COT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][1] == "Team D" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+2].find("-OT", 0, 20) != -1:
                DOT_sum = DOT_sum + 1
        for sched_itemscols in range(0, Post_CountNights):
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team A" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                AOT_sum = AOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team B" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                BOT_sum = BOT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team C" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                COT_sum = COT_sum + 1
            if ScheduleTable_matrix[sched_itemsrows][Post_CountDays*2+7] == "Team D" and \
                    ScheduleTable_matrix[sched_itemsrows][sched_itemscols+Post_CountDays*2+8].find("-OT", 0, 20) != -1:
                DOT_sum = DOT_sum + 1

    shift_utilA = ((Post_CountDays+Post_CountNights)/2*A_sum)/((Post_CountDays+Post_CountNights)/2*A_sum+plot_yA)
    shift_utilB = ((Post_CountDays+Post_CountNights)/2*B_sum)/((Post_CountDays+Post_CountNights)/2*B_sum+plot_yB)
    shift_utilC = ((Post_CountDays+Post_CountNights)/2*C_sum)/((Post_CountDays+Post_CountNights)/2*C_sum+plot_yC)
    shift_utilD = ((Post_CountDays+Post_CountNights)/2*D_sum)/((Post_CountDays+Post_CountNights)/2*D_sum+plot_yD)
    plot_y = [shift_utilA, shift_utilB, shift_utilC, shift_utilD]

    AOT_avg = (AOT_sum/((Post_CountDays+Post_CountNights)/2*A_sum))
    BOT_avg = (BOT_sum/((Post_CountDays+Post_CountNights)/2*B_sum))
    COT_avg = (COT_sum/((Post_CountDays+Post_CountNights)/2*C_sum))
    DOT_avg = (DOT_sum/((Post_CountDays+Post_CountNights)/2*D_sum))
    plot_y1 = [AOT_avg, BOT_avg, COT_avg, DOT_avg]

    AOT_avg1 = (AOT_sum/(A_peoplecount*A_sum))
    BOT_avg1 = (BOT_sum/(B_peoplecount*B_sum))
    COT_avg1 = (COT_sum/(C_peoplecount*C_sum))
    DOT_avg1 = (DOT_sum/(D_peoplecount*D_sum))
    plot_y3 = [AOT_avg1, BOT_avg1, COT_avg1, DOT_avg1]

    plot_y2 = []
    colorUt = []
    for y_value in range(np.shape(plot_y)[0]):
        plot_y2.append(plot_y[y_value]-plot_y1[y_value]*1.5)
    for y2_value in plot_y2:
        if y2_value < 6/7:  # 6/7 represents typical shift/vacation relief optimum ratio, this assumes 4-5 weeks vacation allotment per person
            colorUt.append('red')
        if y2_value == 6/7:
            colorUt.append('gold')
        if y2_value > 6/7:
            colorUt.append('green')

    figure = plt.Figure(figsize=(3.7, 3), dpi=90, facecolor='black')
    ax = figure.add_subplot(111)
    figure.subplots_adjust(bottom=0.2)
    rects1 = ax.bar(plot_x, plot_y2, align='center', color=colorUt, width=.4)
    ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
    ax.set_title("Shift Efficiency %", loc='center', color="white", pad=15, fontsize=11)
    ax.tick_params(labelsize=9, colors='white', pad=1)
    ax.patch.set_color('black')
    ax.set_ylim(.5, 1)
    for rect in rects1:
        height = rect.get_height()
        ax.annotate('{:0.0%}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom',
                    color='white')

    ax.spines['bottom'].set_color('white')
    ax.spines['left'].set_color('white')
    chart = FigureCanvasTkAgg(figure, label4)
    chart.get_tk_widget().place(x=1, y=62)
    plt.show()

    figure2 = plt.Figure(figsize=(3.7, 3), dpi=90, facecolor='black')
    ax2 = figure2.add_subplot(111)
    figure2.subplots_adjust(bottom=0.2)
    rects2 = ax2.bar(plot_x, plot_y3, align='center', color='red', width=.4)
    ax2.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
    ax2.set_title("Overtime %", color="white", pad=15, fontsize=11)
    ax2.tick_params(labelsize=9, colors='white', pad=1)
    ax2.patch.set_color('black')
    ax2.set_ylim(0, .3)
    for rect in rects2:
        height = rect.get_height()
        ax2.annotate('{:0.0%}'.format(height),
                     xy=(rect.get_x() + rect.get_width() / 2, height),
                     xytext=(0, 3),  # 3 points vertical offset
                     textcoords="offset points",
                     ha='center', va='bottom',
                     color='white')

    ax2.spines['bottom'].set_color('white')
    ax2.spines['left'].set_color('white')
    chart2 = FigureCanvasTkAgg(figure2, label4)
    chart2.get_tk_widget().place(x=330, y=62)
    plt.show()

UpdateInfo_button = Button(wrap_main6, text="Update Charts", width=15, font="Arial 16 bold", command=UpdateInfo_chart)
UpdateInfo_button.place(x=50, y=320)
UpdateInfo_button.configure(bg='white', fg='red')

CurrSchd_button = Button(wrap_main6, text="Show Last Schedule", width=18, font="Arial 16 bold", command=Open_pdf)
CurrSchd_button.place(x=200, y=320)
CurrSchd_button.configure(bg='white', fg='red')

wrap_main7 = LabelFrame(rootmain)
wrap_main7.place(x=0, y=783)
wrap_main7.configure(bg='whitesmoke', height=10, width=1441)

exitmain_button = Button(wrap_main6, text="Exit", width=11, font="Arial 16 bold", command=exit_main)
exitmain_button.place(x=590, y=320)
exitmain_button.configure(bg='white', fg='red')

rootmain.title("Scheduling Tool Main")
#rootmain.attributes('-fullscreen', True)
rootmain.geometry("1599x790+0+20")
rootmain.mainloop()



