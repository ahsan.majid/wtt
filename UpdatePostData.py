import pandas as pd
from pandas import *
import numpy as np
from numpy import *
import csv
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import random
import time
import importlib
import runpy

rootinput1 = Toplevel()
rootinput1.title("")
rootinput1.geometry("500x700+450+0")
rootinput1.configure(bg="black", bd=10, width=500, height=550, relief=RIDGE)

PostName = StringVar()
FullPost = StringVar()
HalfPost = StringVar()
TeamName = StringVar()

style = ttk.Style()
style.theme_use('clam')

Title_label = Label(rootinput1, text="Update Post Information", width=20, bg="white", font="Arial 20 bold")
Title_label.pack(pady=20)

TeamName_label = Label(rootinput1, text="Change Team Name?", width=22, bg="light gray")
TeamName_label.pack(pady=0)

TeamName_table = pd.read_table('TeamName.txt')
sdf_TeamNameTable = pd.DataFrame(TeamName_table)
TeamName_matrix = np.array(sdf_TeamNameTable)

TeamName.set(TeamName_matrix[0][0])
EntryTeam_entry = Entry(rootinput1, width=20, justify=CENTER, font="Arial 16", textvariable=TeamName)
EntryTeam_entry.pack(pady=0)

tree_frame1 = Frame(rootinput1)
tree_frame1.pack(pady=20)

tree_scroll = Scrollbar(tree_frame1)
tree_scroll.pack(side=RIGHT, fill=Y)

input_tree = ttk.Treeview(tree_frame1, yscrollcommand=tree_scroll.set)
input_tree.pack()

tree_scroll.config(command=input_tree.yview)

InitInput_table = pd.read_table('Initial Input.txt', delimiter=',')
sdf_InitInputTable = pd.DataFrame(InitInput_table)
InitInput_matrix = np.array(sdf_InitInputTable)
InitInput_shape = np.shape(InitInput_matrix)[0]

fileBase_Data = pd.read_csv("Base Data.txt", delimiter=',')
fileBase_sdf = pd.DataFrame(fileBase_Data)
fileBase_matrix = np.array(fileBase_sdf.sort_values(by=['Team', 'Salary']))
fileBase_shape = np.shape(fileBase_matrix)

BaseCAS_Data = pd.read_csv("Base Data CAS.txt", delimiter=',')
BaseCAS_sdf = pd.DataFrame(BaseCAS_Data)
BaseCAS_matrix = np.array(BaseCAS_sdf.sort_values(by=['Team', 'Name']))
BaseCAS_shape = np.shape(BaseCAS_matrix)

Schedule_table = pd.read_csv('Total Schedule.txt', delimiter=',')
sdf_ScheduleTable = pd.DataFrame(Schedule_table)
ScheduleTable_matrix = np.array(sdf_ScheduleTable)
ScheduleTable_matrixshape = np.shape(ScheduleTable_matrix)
Schedule_Header = np.array(sdf_ScheduleTable.columns)

input_table = pd.read_table('PassDate.txt', delimiter=',')
sdf_inputtable = pd.DataFrame(input_table)
Pass_matrix = np.array(sdf_inputtable)

Flag_Change = 0
global countid
countid = 0
Post_AddFlag = 0
Update_Flag = 0

input_tree['columns'] = ("PostName", "FullPost", "HalfPost")
input_tree.column('#0', width=0, stretch=NO)
input_tree.column('PostName', anchor=W, width=100)
input_tree.column('FullPost', anchor=CENTER, width=100)
input_tree.column('HalfPost', anchor=CENTER, width=100)

input_tree.heading('#0', text='', anchor=W)
input_tree.heading('PostName', text='Post Name', anchor=W)
input_tree.heading('FullPost', text='Full Post', anchor=CENTER)
input_tree.heading('HalfPost', text='Half Post', anchor=CENTER)

for inputdata_record in InitInput_matrix:
    record_stuff1 = str(inputdata_record).replace("'", '')
    record_stuff = str(record_stuff1).strip("[]")
    input_tree.insert(parent='', index='end', iid=countid, text='', values=record_stuff)
    countid += 1

input_frame1 = Frame(rootinput1)
input_frame1.pack(pady=10)
input_frame1.configure(bg="light gray")

inputName_label = Label(input_frame1, text="Post Name", width=10, bg="light gray")
inputName_label.grid(row=0, column=0)

inputFull_label = Label(input_frame1, text="Full Post Y/N", width=10, bg="light gray")
inputFull_label.grid(row=0, column=1)

inputHalf_label = Label(input_frame1, text="Half Post Y/N", width=10, bg="light gray")
inputHalf_label.grid(row=0, column=2)

inputName_entry = Entry(input_frame1, width=10, textvariable=PostName)
inputName_entry.grid(row=1, column=0)

inputFull_entry = Entry(input_frame1, width=10, justify=CENTER, textvariable=FullPost)
inputFull_entry.grid(row=1, column=1)

inputHalf_entry = Entry(input_frame1, width=10, justify=CENTER, textvariable=HalfPost)
inputHalf_entry.grid(row=1, column=2)

def LearnersInfo(ev):
    global entry_list
    viewInfo = input_tree.focus()
    learnerData = input_tree.item(viewInfo)
    row = learnerData['values']
    PostName.set(row[0])
    FullPost.set(row[1])
    HalfPost.set(row[2])


input_tree.bind("<ButtonRelease-1>", LearnersInfo)

def update():
    global entry_list, InitInput_matrix1, fileBase_matrix, BaseCAS_matrix, Post_AddFlag, Update_Flag
    if Post_AddFlag == 1:
        messagebox.showinfo("Alert", "Please Save Before Adding Another Post!")
        inputName_entry.delete(0, END)
        inputFull_entry.delete(0, END)
        inputHalf_entry.delete(0, END)
    if Post_AddFlag == 0:
        selected = input_tree.focus()
        position = int(input_tree.focus())
        valueenter = 'PostName.get(), FullPost.get(), HalfPost.get()'
        input_tree.item(selected, text="", values=eval(valueenter))
        inputName_entry.delete(0, END)
        inputFull_entry.delete(0, END)
        inputHalf_entry.delete(0, END)
        messagebox.showinfo("Alert", "Please Save Before Adding Another Post!")
        Post_AddFlag = 1
        Update_Flag = 1


def add_inputrecord():
    global countid, InitInput_matrix1, fileBase_matrix, BaseCAS_matrix, Post_AddFlag
    if Post_AddFlag == 1:
        messagebox.showinfo("Alert", "Please Save Before Adding Another Post!")
        inputName_entry.delete(0, END)
        inputFull_entry.delete(0, END)
        inputHalf_entry.delete(0, END)
    if Post_AddFlag == 0:
        input_tree.insert('', index='end', iid=countid, values=(inputName_entry.get(), inputFull_entry.get(), inputHalf_entry.get()))
        countid += 1
        Input_List = np.array([[inputName_entry.get(), inputFull_entry.get(), inputHalf_entry.get()]], dtype=object)
        SetupInput_header = ["Post Name", "Full Post", "Half Post"]
        InitInput_matrix1 = np.vstack((SetupInput_header, InitInput_matrix, Input_List))
        fileBase_header = np.hstack((fileBase_sdf.columns.values, inputName_entry.get()))
        Base_holder = []
        CAS_holder = []
        for Baseitems in range(np.shape(fileBase_matrix)[0]):
            Base_holder += [4]
        BaseCAS_header = np.hstack((BaseCAS_sdf.columns.values, inputName_entry.get()))
        for CASitems in range(np.shape(BaseCAS_matrix)[0]):
            CAS_holder += [1]
        Base_holder = np.reshape(Base_holder, (np.shape(Base_holder)[0], 1))
        fileBase_matrix = np.hstack((fileBase_matrix, Base_holder))
        fileBase_matrix = np.vstack((fileBase_header, fileBase_matrix))
        CAS_holder = np.reshape(CAS_holder, (np.shape(CAS_holder)[0], 1))
        BaseCAS_matrix = np.hstack((BaseCAS_matrix, CAS_holder))
        BaseCAS_matrix = np.vstack((BaseCAS_header, BaseCAS_matrix))
        inputName_entry.delete(0, END)
        inputFull_entry.delete(0, END)
        inputHalf_entry.delete(0, END)
        messagebox.showinfo("Alert", "Please Save Before Adding Another Post!")
        Post_AddFlag = 1


def removeall_record():
    for input_record in input_tree.get_children():
        input_tree.delete(input_record)
        InitInput_matrix = []


def removeone_record():
    global BaseCAS_matrix, fileBase_matrix, fileBase_sdf, BaseCAS_sdf, Post_AddFlag
    if Post_AddFlag == 1:
        messagebox.showinfo("Alert", "Please Save Before Removing Another Post!")
        inputName_entry.delete(0, END)
        inputFull_entry.delete(0, END)
        inputHalf_entry.delete(0, END)
    if Post_AddFlag == 0:
        removex = input_tree.selection()[0]
        input_tree.delete(removex)
        fileBase_sdf = fileBase_sdf.drop(fileBase_sdf.columns[[int(removex)+4]], axis=1)
        BaseCAS_sdf = BaseCAS_sdf.drop(BaseCAS_sdf.columns[[int(removex)+4]], axis=1)
        fileBase_matrix = np.array(fileBase_sdf)
        fileBase_matrix = np.vstack((fileBase_sdf.columns.values, fileBase_matrix))
        BaseCAS_matrix = np.array(BaseCAS_sdf)
        BaseCAS_matrix = np.vstack((BaseCAS_sdf.columns.values, BaseCAS_matrix))
        messagebox.showinfo("Alert", "Please Save Before Removing Another Post!")
        Post_AddFlag = 1



def save_input():
    global Post_AddFlag, Update_Flag, EntryTeam_entry
    erase_input = open('TeamName.txt', 'w+')
    erase_input.close()
    SetupTeamName_header = ["TeamName"]
    TeamName1 = [EntryTeam_entry.get()]
    SetupInput_header = ["Post Name", "Full Post", "Half Post"]
    with open("TeamName.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupTeamName_header)
        appFile1.close()
    with open("TeamName.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(TeamName1)
        appFile1.close()
    erase_input = open('Initial Input.txt', 'w+')
    erase_input.close
    with open("Initial Input.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    PostHeader_Input = []
    for init_items in input_tree.get_children():
        with open("Initial Input.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(input_tree.item(init_items)['values'])
            appFile1.close()
        PostHeader_Input += [input_tree.item(init_items)['values'][0]]
    messagebox.showinfo("Success!", "Input Data Saved Successfully!" + '\n' + '\n' + "Update Post and Competency Information")
    if Update_Flag == 1:
        InitInput_table = pd.read_table('Initial Input.txt', delimiter=',')
        sdf_InitInputTable = pd.DataFrame(InitInput_table)
        InitInput_matrix = np.array(sdf_InitInputTable)
        InitInput_shape = np.shape(InitInput_matrix)[0]
        Header_input = ['Team', 'Name', 'Salary', 'OT Hours']
        Header_input2 = ['Team', 'Name', 'ERT Qual', 'EMT Qual']
        New_Header = []
        for init_items in range(InitInput_shape):
            New_Header += [InitInput_matrix[init_items, 0]]
        Header_input = np.hstack((Header_input, New_Header))
        Header_input2 = np.hstack((Header_input2, New_Header))
        erase_Base = open('Base Data.txt', 'w+')
        erase_Base.close
        with open("Base Data.txt", "a+") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(Header_input)
            appFile1.close()
        for Base_items in fileBase_matrix:
            with open("Base Data.txt", "a+") as appFile1:
                writer = csv.writer(appFile1)
                writer.writerow(Base_items)
                appFile1.close()
        erase_CAS = open('Base Data CAS.txt', 'w+')
        erase_CAS.close
        with open("Base Data CAS.txt", "a+") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(Header_input2)
            appFile1.close()
        for CAS_items in BaseCAS_matrix:
            with open("Base Data CAS.txt", "a+") as appFile1:
                writer = csv.writer(appFile1)
                writer.writerow(CAS_items)
                appFile1.close()
    sched_date = Pass_matrix[0, 0]
    weeks_entry = Pass_matrix[0, 1]
    erase_Pass = open('PassDate.txt', 'w+')
    erase_Pass.close
    PassHeader = ["Date", "Duration", "PostFlag", "PassBypass"]
    with open("PassDate.txt", 'a') as appFile:
        writer = csv.writer(appFile)
        writeList = PassHeader
        writer.writerow(writeList)
        appFile.close()
    with open("PassDate.txt", 'a', newline="") as appFile:
        writer = csv.writer(appFile)
        writeList = [sched_date, weeks_entry, 0, 1]
        writer.writerow(writeList)
        appFile.close()
    Post_AddFlag = 0
    Update_Flag = 0


def exit_initinput():
    rootinput1.destroy()


update_input = Button(rootinput1, text="Update", width=15, font="Arial 14 bold", cursor="circle", command=update)
update_input.configure(fg='blue')
update_input.pack(pady=10)

add_input = Button(rootinput1, text="Add Post", width=15, font="Arial 14 bold", cursor="circle", command=add_inputrecord)
add_input.configure(fg='blue')
add_input.pack(pady=10)

remove_selected = Button(rootinput1, text="Remove Selected", width=15, font="Arial 14 bold", cursor="circle", command=removeone_record)
remove_selected.configure(fg='blue')
remove_selected.pack(pady=10)


save_input = Button(rootinput1, text="Save", width=15, font="Arial 14 bold", cursor="circle", command=save_input)
save_input.configure(fg='blue')
save_input.pack(pady=10)

exitinitinput_button = Button(rootinput1, text="Exit", width=7, font="Arial 16 bold", cursor="circle",
                              command=exit_initinput)
exitinitinput_button.configure(fg='blue')
exitinitinput_button.place(x=400, y=650)

rootinput1.mainloop()
