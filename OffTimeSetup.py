import pandas as pd
import numpy as np
import csv
from tkinter import messagebox
from datetime import *
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import runpy
import subprocess

rootinput1 = Toplevel()
rootinput1.title("")
rootinput1.geometry("500x790+450+0")
rootinput1.configure(bg="black", bd=10, width=500, height=750, relief=RIDGE)

Off_Item = StringVar()
Off_Value = StringVar()
Vac_Item = StringVar()
Vac_Value = StringVar()

style = ttk.Style()
style.theme_use('clam')

labeloffTop = Label(rootinput1, text="Set Off Time Information", font="Arial 20 bold", bg='white', fg='black', width=20)
labeloffTop.pack(pady=5)

tree_frame1 = Frame(rootinput1)
tree_frame1.pack(pady=10)

input_tree = ttk.Treeview(tree_frame1, height=6)
input_tree.pack()

InitInput_table = pd.read_table('ExtraBoardTemplate.txt', delimiter=',')
sdf_InitInputTable = pd.DataFrame(InitInput_table)
InitInput_matrix = np.array(sdf_InitInputTable)
InitInput_shape = np.shape(InitInput_matrix)[0]

Vacation_table = pd.read_table('VacationTemplate.txt', delimiter=',')
sdf_VacationTable = pd.DataFrame(Vacation_table)
Vacation_matrix = np.array(sdf_VacationTable)
Vacation_shape = np.shape(Vacation_matrix)[0]

Flag_Change = 0
global countid
countid = 0
Post_AddFlag = 0
Update_Flag = 0
Post_AddFlag2 = 0
Update_Flag2 = 0

input_tree['columns'] = ("Off Item", "Overall % Off")
input_tree.column('#0', width=0, stretch=NO)
input_tree.column('Off Item', anchor=W, width=200)
input_tree.column('Overall % Off', anchor=CENTER, width=100)

input_tree.heading('#0', text='', anchor=W)
input_tree.heading('Off Item', text='Off Item', anchor=W)
input_tree.heading('Overall % Off', text='Overall % Off', anchor=CENTER)

for inputdata_record in InitInput_matrix:
    record_stuff1 = str(inputdata_record).replace("'", '')
    record_stuff = str(record_stuff1).strip("[]")
    input_tree.insert(parent='', index='end', iid=countid, text='', values=record_stuff)
    countid += 1

input_frame1 = Frame(rootinput1, relief='solid', bd=1)
input_frame1.pack(pady=10)
input_frame1.configure(bg="light gray")

inputName_label = Label(input_frame1, text="Off Item", width=20, bg="light gray", anchor=W)
inputName_label.grid(row=0, column=0)

inputFull_label = Label(input_frame1, text="% Off", width=10, bg="light gray")
inputFull_label.grid(row=0, column=1)

inputName_entry = Entry(input_frame1, width=20, textvariable=Off_Item)
inputName_entry.grid(row=1, column=0)

inputFull_entry = Entry(input_frame1, width=10, justify=CENTER, textvariable=Off_Value)
inputFull_entry.grid(row=1, column=1)


def LearnersInfo(ev):
    global entry_list
    viewInfo = input_tree.focus()
    learnerData = input_tree.item(viewInfo)
    row = learnerData['values']
    Off_Item.set(row[0])
    Off_Value.set(row[1])


input_tree.bind("<ButtonRelease-1>", LearnersInfo)

def update():
    global entry_list
    selected = input_tree.focus()
    position = int(input_tree.focus())
    valueenter = 'Off_Item.get(), Off_Value.get()'
    input_tree.item(selected, text="", values=eval(valueenter))
    inputName_entry.delete(0, END)
    inputFull_entry.delete(0, END)
    messagebox.showinfo("", "Press 'Save % Off Info' When You've Completed Updates.")


def update2():
    global entry_list
    selected = input_tree2.focus()
    position = int(input_tree2.focus())
    valueenter2 = 'Vac_Item.get(), Vac_Value.get()'
    input_tree2.item(selected, text="", values=eval(valueenter2))
    inputName_entryvac.delete(0, END)
    inputFull_entryvac.delete(0, END)
    messagebox.showinfo("", "Press 'Save Vacation Info' When You've Completed Updates.")


def save_input():
    global Post_AddFlag
    erase_input = open('ExtraBoardTemplate.txt', 'w+')
    erase_input.close
    SetupInput_header = ["Off Item", "Overall % Off"]
    with open("ExtraBoardTemplate.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    Off_Header_Input = []
    for init_items in input_tree.get_children():
        with open("ExtraBoardTemplate.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(input_tree.item(init_items)['values'])
            appFile1.close()
    messagebox.showinfo("Success!", "Data Saved Successfully!")
    inputName_entry.delete(0, END)
    inputFull_entry.delete(0, END)


def save_input2():
    global Post_AddFlag2
    erase_input = open('VacationTemplate.txt', 'w+')
    erase_input.close
    SetupInput_header = ["Years of Service", "Vacation(wks)"]
    with open("VacationTemplate.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    Off_Header_Input = []
    for init_items in input_tree2.get_children():
        with open("VacationTemplate.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(input_tree2.item(init_items)['values'])
            appFile1.close()
    messagebox.showinfo("Success!", "Data Saved Successfully!")
    inputName_entry.delete(0, END)
    inputFull_entry.delete(0, END)



def exit_initinput():
    rootinput1.destroy()


update_inputoff = Button(rootinput1, text="Update % Off Info", width=18, command=update)
update_inputoff.configure(fg='blue', font="Arial 14 bold", cursor="circle")
update_inputoff.pack(pady=10)

save_inputoff = Button(rootinput1, text="Save % Off Info", width=18, command=save_input)
save_inputoff.configure(fg='blue', font="Arial 14 bold", cursor="circle")
save_inputoff.pack(pady=10)

labelVac = Label(rootinput1, text="Vacation Allotment", font="Arial 20 bold", bg='black', fg='white', width=20)
labelVac.place(x=110, y=380)

tree_frame2 = Frame(rootinput1)
tree_frame2.place(x=90, y=370+55)

input_tree2 = ttk.Treeview(tree_frame2, height=5)
input_tree2.pack()

input_tree2['columns'] = ("Years of Service", "Vacation(wks)")
input_tree2.column('#0', width=0, stretch=NO)
input_tree2.column('Years of Service', anchor=W, width=200)
input_tree2.column('Vacation(wks)', anchor=CENTER, width=100)

input_tree2.heading('#0', text='', anchor=W)
input_tree2.heading('Years of Service', text='Years of Service', anchor=W)
input_tree2.heading('Vacation(wks)', text='Vacation(wks)', anchor=CENTER)

for inputdata_recordvac in Vacation_matrix:
    record_stuff2 = str(inputdata_recordvac).replace("'", '')
    record_stuff2 = str(record_stuff2).strip("[]")
    input_tree2.insert(parent='', index='end', iid=countid, text='', values=record_stuff2)
    countid += 1

input_frame2 = Frame(rootinput1, relief='solid', bd=1)
input_frame2.pack(pady=40)
input_frame2.configure(bg="light gray")

inputName_labelvac = Label(rootinput1, text=" Years of Service                      Vacation(wks)", width=32, bg="light gray", anchor=W)
inputName_labelvac.place(x=96, y=573)

#inputFull_labelvac = Label(rootinput1, text="Vacation(wks)", width=10, bg="light gray")
#inputFull_labelvac.place(x=286, y=565)

inputName_entryvac = Entry(rootinput1, width=20, textvariable=Vac_Item)
inputName_entryvac.place(x=96, y=573+24)

inputFull_entryvac = Entry(rootinput1, width=10, justify=CENTER, textvariable=Vac_Value)
inputFull_entryvac.place(x=288, y=573+24)

def LearnersInfo2(ev):
    global entry_list
    viewInfo = input_tree2.focus()
    learnerData = input_tree2.item(viewInfo)
    row = learnerData['values']
    Vac_Item.set(row[0])
    Vac_Value.set(row[1])


input_tree2.bind("<ButtonRelease-1>", LearnersInfo2)

update_inputvac = Button(rootinput1, text="Update Vacation Info", width=18, command=update2)
update_inputvac.configure(fg='blue', font="Arial 14 bold", cursor="circle")
update_inputvac.place(x=165, y=573+72)

save_inputvac = Button(rootinput1, text="Save Vacation Info", width=18, command=save_input2)
save_inputvac.configure(fg='blue', font="Arial 14 bold", cursor="circle")
save_inputvac.place(x=165, y=572+72+24+20)

exitinitinput_button = Button(rootinput1, text="Exit", width=7, font="Arial 16 bold",
                              command=exit_initinput)
exitinitinput_button.configure(fg='blue', cursor="circle")
exitinitinput_button.place(x=400, y=740)

rootinput1.mainloop()