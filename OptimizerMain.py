from typing import List, Any
import numpy as np
from datetime import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import csv
import datetime
import pandas as pd
import importlib
import runpy

screen = Toplevel()
screen.geometry("600x670+400+0")
screen.title("")
screen.configure(bg="black", bd=10, width=400, height=400, relief=RIDGE)
#style = ttk.Style()
#style.theme_use('clam')
skootover = 100
global Temp_Shift


def exit_setsched():
    screen.destroy()


def Current_Opt():
    runpy.run_path(path_name="OptimizerV7.py")


def Manual_Input():
    screen.destroy()
    runpy.run_path(path_name="BaseDataCASManual.py")


def ManualOpt():
    runpy.run_path(path_name="OptimizerManualV2.py")


def Manual_to_Current():
    pass


def Manual_to_Optimized():
    pass


def Current_to_Manual():
    pass


def Current_to_Optimized():
    pass


Optimizemain_label = Label(screen, text="Team Balance Analysis", width=20+10, font="Arial 20 bold")
Optimizemain_label.pack(pady=10)

Current_label = Label(screen, text="Analyze Current Roster", width=24, bg='black', fg='white', font="Arial 18 bold")
Current_label.place(x=56+skootover, y=60)

Current_selectbutton = Button(screen, text="Run Current Roster Analysis", width=37, font="Arial 14 bold", command=Current_Opt)
Current_selectbutton.configure(fg='blue', cursor="circle")
Current_selectbutton.place(x=50+skootover, y=94)

Line1 = LabelFrame(screen)
Line1.place(x=0, y=140-5)
Line1.configure(bg='white', height=2, width=380+skootover*2)

Manual_label = Label(screen, text="Manually Optimize Roster", width=24, bg='black', fg='white', font="Arial 18 bold")
Manual_label.place(x=56+skootover, y=150)

Manual_selectbutton = Button(screen, text="Manually Change Roster", width=37, font="Arial 14 bold", command=Manual_Input)
Manual_selectbutton.configure(fg='blue', cursor="circle")
Manual_selectbutton.place(x=50+skootover, y=184)

ManualOpt_selectbutton = Button(screen, text="Run Manual Roster Analysis", width=37, font="Arial 14 bold", command=ManualOpt)
ManualOpt_selectbutton.configure(fg='blue', cursor="circle")
ManualOpt_selectbutton.place(x=50+skootover, y=218)

Note_label1 = Label(screen, text="(Manual Input will alter Optimized Result)", width=40, bg='black', fg='white', font="Arial 14 bold")
Note_label1.place(x=30+skootover, y=250)

Line2 = LabelFrame(screen)
Line2.place(x=0, y=290-5)
Line2.configure(bg='white', height=2, width=380+skootover*2)

ResetMan_label = Label(screen, text="Manual Roster Selection Options", width=30, bg='black', fg='white', font="Arial 18 bold")
ResetMan_label.place(x=24+skootover, y=300)

CurrOpt_selectbutton = Button(screen, text="Change Manual Roster to Current Roster", width=37, font="Arial 14 bold", \
                              command=Manual_to_Current)
CurrOpt_selectbutton.configure(fg='blue', cursor="circle")
CurrOpt_selectbutton.place(x=50+skootover, y=334)

ManOptOpt_selectbutton = Button(screen, text="Change Manual Roster to Optimized Roster", width=37, font="Arial 14 bold", \
                                command=Manual_to_Optimized)
ManOptOpt_selectbutton.configure(fg='blue', cursor="circle")
ManOptOpt_selectbutton.place(x=50+skootover, y=368)

Note_label2 = Label(screen, text="(Exit to change none)", width=40, bg='black', fg='white', font="Arial 14 bold")
Note_label2.place(x=30+skootover, y=400)

Line3 = LabelFrame(screen)
Line3.place(x=0, y=418+22-5)
Line3.configure(bg='white', height=2, width=380+skootover*2)

ResetCurr_label = Label(screen, text="Current Roster Selection Options", width=30, bg='black', fg='white', font="Arial 18 bold")
ResetCurr_label.place(x=24+skootover, y=418+32)

ManOpt_selectbutton = Button(screen, text="Change Current Roster to Manual Roster", width=37, font="Arial 14 bold", \
                             command=Current_to_Manual)
ManOpt_selectbutton.configure(fg='blue', cursor="circle")
ManOpt_selectbutton.place(x=50+skootover, y=452+32)

OptOpt_selectbutton = Button(screen, text="Change Current Roster to Optimized Roster", width=37, font="Arial 14 bold", \
                             command=Current_to_Optimized)
OptOpt_selectbutton.configure(fg='blue', cursor="circle")
OptOpt_selectbutton.place(x=50+skootover, y=486+32)

Note_label3 = Label(screen, text="(Exit to change none)", width=40, bg='black', fg='white', font="Arial 14 bold")
Note_label3.place(x=30+skootover, y=518+32)

Line4 = LabelFrame(screen)
Line4.place(x=0, y=518+32+40)
Line4.configure(bg='white', height=2, width=380+skootover*2)

exitschedset_button = Button(screen, text="Exit", width=7, font="Arial 16 bold", command=exit_setsched)
exitschedset_button.configure(fg='blue', cursor="circle")
exitschedset_button.place(x=165+skootover, y=610)

screen.mainloop()
