import pandas as pd
from pandas import *
import numpy as np
from numpy import *
import csv
from tkinter import *
from datetime import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import random
import time
import importlib
import runpy

rootinput = Toplevel()
rootinput.title("")
rootinput.geometry("500x700+450+0")
rootinput.configure(bg="black", bd=10, width=500, height=550, relief=RIDGE)
Pass_Bypass = 0

PostName = StringVar()
FullID = StringVar()
HalfID = StringVar()
TeamName = StringVar()


def message_confirm():
    res = messagebox.askokcancel(title="Warning!", message="This will DELETE Team Roster Data" + "\n" + \
                                                     "\n" + "Do you want to proceed?")  #, **options)
    if res == True:
        save_input()
    else:
        pass


style = ttk.Style()
style.theme_use('clam')

Title_label = Label(rootinput, text="Set Up Post Information", width=20, bg="white", font="Arial 20 bold")
Title_label.pack(pady=20)

TeamName_label = Label(rootinput, text="Input Team Name", width=22, bg="light gray")
TeamName_label.pack(pady=0)

TeamName_table = pd.read_table('TeamName.txt')
sdf_TeamNameTable = pd.DataFrame(TeamName_table)
TeamName_matrix = np.array(sdf_TeamNameTable)

TeamName.set(TeamName_matrix[0][0])
EntryTeam_entry = Entry(rootinput, width=20, justify=CENTER, font="Arial 16", textvariable=TeamName)
EntryTeam_entry.pack(pady=0)

tree_frame1 = Frame(rootinput)
tree_frame1.pack(pady=20)

tree_scroll = Scrollbar(tree_frame1)
tree_scroll.pack(side=RIGHT, fill=Y)

input_tree = ttk.Treeview(tree_frame1, yscrollcommand=tree_scroll.set)
input_tree.pack()

tree_scroll.config(command=input_tree.yview)

InitInput_table = pd.read_table('Initial Input.txt', delimiter=',')
sdf_InitInputTable = pd.DataFrame(InitInput_table)
InitInput_matrix = np.array(sdf_InitInputTable)
InitInput_shape = np.shape(InitInput_matrix)[0]

global countid
countid = 0

input_tree['columns'] = ("Post Name", "Full Post", "Half Post")
input_tree.column('#0', width=0, stretch=NO)
input_tree.column('Post Name', anchor=W, width=100)
input_tree.column('Full Post', anchor=CENTER, width=100)
input_tree.column('Half Post', anchor=CENTER, width=100)

input_tree.heading('#0', text='', anchor=W)
input_tree.heading('Post Name', text='Post Name', anchor=W)
input_tree.heading('Full Post', text='Full Post', anchor=CENTER)
input_tree.heading('Half Post', text='Half Post', anchor=CENTER)

for inputdata_record in InitInput_matrix:
    record_stuff1 = str(inputdata_record).replace("'", '')
    record_stuff = str(record_stuff1).strip("[]")
    input_tree.insert(parent='', index='end', iid=countid, text='', values=(record_stuff))
    countid += 1

input_frame1 = Frame(rootinput)
input_frame1.pack(pady=10)
input_frame1.configure(bg="light gray")

inputName_label = Label(input_frame1, text="Post Name", width=10, bg="light gray")
inputName_label.grid(row=1, column=0)

inputFull_label = Label(input_frame1, text="Full Post Y/N", width=10, bg="light gray")
inputFull_label.grid(row=1, column=1)

inputHalf_label = Label(input_frame1, text="Half Post Y/N", width=10, bg="light gray")
inputHalf_label.grid(row=1, column=2)

inputName_entry = Entry(input_frame1, width=10)
inputName_entry.grid(row=2, column=0)

inputFull_entry = Entry(input_frame1, width=10, justify=CENTER)
inputFull_entry.grid(row=2, column=1)

inputHalf_entry = Entry(input_frame1, width=10, justify=CENTER)
inputHalf_entry.grid(row=2, column=2)

def add_inputrecord():
    global countid, InitInput_matrix1, PostHead_posts
    input_tree.insert('', index='end', iid=countid, values=(inputName_entry.get(), inputFull_entry.get(), inputHalf_entry.get()))
    countid += 1
    Input_List = np.array([[inputName_entry.get(), inputFull_entry.get(), inputHalf_entry.get()]], dtype=object)
    SetupInput_header = ["Post Name", "Full Post", "Half Post"]
    InitInput_matrix1 = np.vstack((SetupInput_header, InitInput_matrix, Input_List))
    inputName_entry.delete(0, END)
    inputFull_entry.delete(0, END)
    inputHalf_entry.delete(0, END)


def removeall_record():
    for input_record in input_tree.get_children():
        input_tree.delete(input_record)
        InitInput_matrix = []


def removeone_record():
    global BaseCAS_matrix, fileBase_matrix, fileBase_sdf, BaseCAS_sdf
    removex = input_tree.selection()[0]
    input_tree.delete(removex)
    fileBase_sdf = fileBase_sdf.drop(fileBase_sdf.columns[[int(removex)+4]], axis=1)
    BaseCAS_sdf = BaseCAS_sdf.drop(BaseCAS_sdf.columns[[int(removex)+4]], axis=1)
    fileBase_matrix = np.array(fileBase_sdf)
    fileBase_matrix = np.vstack((fileBase_sdf.columns.values, fileBase_matrix))
    BaseCAS_matrix = np.array(BaseCAS_sdf)
    BaseCAS_matrix = np.vstack((BaseCAS_sdf.columns.values, BaseCAS_matrix))


def save_input():
    global Pass_Bypass, EntryTeam_entry, TeamName
    erase_input = open('Initial Input.txt', 'w+')
    erase_input.close()
    erase_input = open('TeamName.txt', 'w+')
    erase_input.close()
    SetupInput_header = ["Post Name", "Full Post", "Half Post"]
    SetupTeamName_header = ["TeamName"]
    TeamName1 = [EntryTeam_entry.get()]
    SetupPass_header = ["Date", "Duration", "PostFlag", "PassBypass"]
    with open("TeamName.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupTeamName_header)
        appFile1.close()
    with open("TeamName.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(TeamName1)
        appFile1.close()
    with open("Initial Input.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    Header_input = ['Team', 'Name', 'OT Hours']
    PostHeader_Input = []
    for init_items in input_tree.get_children():
        with open("Initial Input.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            #input_list = (init_items)
            writer.writerow(input_tree.item(init_items)['values'])
            appFile1.close()
        PostHeader_Input += [input_tree.item(init_items)['values'][0]]
    erase_Base = open('Base Data.txt', 'w+')
    erase_Base.close()
    erase_CAS = open('Base Data CAS.txt', 'w+')
    erase_CAS.close()
    Base_header = ['Team', 'Name', 'Salary', 'OT Hours']
    Base_header = np.hstack((Base_header, PostHeader_Input))
    CAS_header = ['Team', 'Name', 'ERT Qual', 'EMT Qual']
    CAS_header = np.hstack((CAS_header, PostHeader_Input))
    with open("Base Data.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(Base_header)
        appFile1.close()
    with open("Base Data CAS.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(CAS_header)
        appFile1.close()
    erase_Pass = open('PassDate.txt', 'w+')
    erase_Pass.close()
    SetupPass_header = ["Date", "Duration", "PostFlag", "PassBypass"]
    with open("PassDate.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupPass_header)
        appFile1.close()
    initialize_PassDate = [0, 0, 0, Pass_Bypass]
    with open("PassDate.txt", "a+") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(initialize_PassDate)
        appFile1.close()
    messagebox.showinfo("Success!", "Input Data Saved Successfully!")


def exit_initinput():
    rootinput.destroy()


add_input = Button(rootinput, text="Add Post", width=15, font="Arial 14 bold", cursor="circle", command=add_inputrecord)
add_input.configure(fg="blue")
add_input.pack(pady=10)

remove_selected = Button(rootinput, text="Remove Selected", width=15, font="Arial 14 bold", cursor="circle", command=removeone_record)
remove_selected.configure(fg="blue")
remove_selected.pack(pady=10)

removeall_input = Button(rootinput, text="Remove All", width=15, font="Arial 14 bold", cursor="circle", command=removeall_record)
removeall_input.configure(fg="blue")
removeall_input.pack(pady=10)

save_input_button = Button(rootinput, text="Save", width=15, font="Arial 14 bold", cursor="circle", command=message_confirm)
save_input_button.configure(fg="blue")
save_input_button.pack(pady=10)

exitinitinput_button = Button(rootinput, text="Exit", width=7, font="Arial 16 bold", cursor="circle", command=exit_initinput)
exitinitinput_button.configure(fg="blue")
exitinitinput_button.place(x=400, y=650)

rootinput.mainloop()
