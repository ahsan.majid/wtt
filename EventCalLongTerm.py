from typing import List, Any
import numpy as np
from datetime import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import csv
import pandas as pd
import importlib
import runpy

rooteventlong = Toplevel()
rooteventlong.geometry("800x790+300+0")
rooteventlong.title("")
color = "black"
rooteventlong.configure(bg="black", bd=10, width=800, height=750, relief=RIDGE)
my_frame = Frame(rooteventlong, highlightbackground='whitesmoke', highlightthickness=1, width=500, height=700, bd=0)
Sched_Message = "Press Me"
font_sizeK = 14
name_selected = ""
time_bypass = 0
extra_ylength = 0

Event1_table = pd.read_table('Shift Appointments.txt', delimiter=',')
sdf_Event1Table = pd.DataFrame(Event1_table)
Event1_matrix = np.array(sdf_Event1Table)

SetupInfo_table = pd.read_csv('SetupScheduleInfo.txt', delimiter=',')
sdf_InfoTable = pd.DataFrame(SetupInfo_table)
InfoTable_matrix = np.array(sdf_InfoTable)
InfoTable_matrixshape = np.shape(InfoTable_matrix)

file_base = pd.read_table('Base Data.txt', delimiter=',')

if InfoTable_matrix[0, 0] == "DuPont Schedule":
    shift_base = pd.read_csv('ShiftDupont.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "4on/4off Schedule":
    shift_base = pd.read_csv('ShiftFourFour.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "EOWEO Schedule":
    shift_base = pd.read_csv('ShiftEOWEO.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "7on/7off Schedule":
    shift_base = pd.read_csv('ShiftSeven.txt', delimiter=',')
elif InfoTable_matrix[0, 0] == "Four/Five Schedule":
    shift_base = pd.read_csv('ShiftFourFive.txt', delimiter=',')
#shift_base = pd.read_table('ShiftDupont.txt', delimiter=',')
sdf_name = pd.DataFrame(file_base)
sdf_basename = np.array(sdf_name.sort_values(by=['Team', 'Salary']))
sdf_shift = pd.DataFrame(shift_base)
sdf_shiftfinal = np.array(sdf_shift)

style = ttk.Style()
style.theme_use('clam')

Title_label = Label(rooteventlong, text="Long Term Calendar Entry", width=25, bg="white", font="Arial 20 bold")
Title_label.pack(pady=5)

tree_frame1 = Frame(rooteventlong)
tree_frame1.place(x=60, y=70)

tree_scroll = Scrollbar(tree_frame1)
tree_scroll.pack(side=RIGHT, fill=Y)

input_tree = ttk.Treeview(tree_frame1, yscrollcommand=tree_scroll.set, height=16)
input_tree.pack()

tree_scroll.config(command=input_tree.yview)

input_tree['columns'] = ("Date", "Name", "Team", "Event")
input_tree.column('#0', anchor=W, width=0, stretch=NO)
input_tree.column('Date', anchor=W, width=150)
input_tree.column('Name', anchor=W, width=250)
input_tree.column('Team', anchor=CENTER, width=50)
input_tree.column('Event', anchor=W, width=200)

input_tree.heading('#0', text='', anchor=W)
input_tree.heading('Date', text='Date', anchor=W)
input_tree.heading('Name', text='Name', anchor=W)
input_tree.heading('Team', text='Team', anchor=CENTER)
input_tree.heading('Event', text='Event', anchor=W)

input_tree.tag_configure('Ateam', background='#9DD0F3')
input_tree.tag_configure('Bteam', background='#F7CDAB')
input_tree.tag_configure('Cteam', background='#DCBBF6')
input_tree.tag_configure('Dteam', background='#E5F7AB')

def display_all():
    global time_bypass
    time_bypass = 1
    display_events()


def display_events():
    global time_bypass, Row1, matrix_children, input_tree
    for input_record in input_tree.get_children():
        input_tree.delete(input_record)
    Event_table = pd.read_table('Shift Appointments.txt', delimiter=',')
    sdf_EventTable = pd.DataFrame(Event_table)
    sdf_EventTablesort = sdf_EventTable.sort_values(by=(['Date', 'Team', 'Name']))
    Event_matrix = np.array(sdf_EventTablesort)
    Event_shape = np.shape(Event_matrix)[0]
    sort_record = ""
    countidevent = 0
    if time_bypass == 1:
        numberofdays = 365
    else:
        numberofdays = 10
    for eventdata_record in Event_matrix:
        #eventdata_first = str(eventdata_record[0])
        eventrecord_stuff1 = str(eventdata_record).replace("'", '')
        eventrecord_stuff = str(eventrecord_stuff1).strip("[]")
        if datetime.strptime(eventdata_record[0], '%Y-%m-%d') >= (datetime.now() - timedelta(numberofdays)):
            eventrecord_child = str(eventdata_record[1:4]).replace(",", '')
            if eventdata_record[2] == "A":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Ateam',))
            elif eventdata_record[2] == "B":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Bteam',))
            elif eventdata_record[2] == "C":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Cteam',))
            elif eventdata_record[2] == "D":
                input_tree.insert('', index='end', text='', values=eventrecord_stuff, tags=('Dteam',))
        countidevent += 1
        sort_record = eventdata_record[0]
        time_bypass = 0


names_names = []
names_shape = np.shape(sdf_basename)[0]
shift_shift = []
shift_shape = np.shape(sdf_shiftfinal)[0]
Sched_select = StringVar()
Sched_select.set("<Select>")
OT_select = StringVar()
OT_select.set("<Select>")

def enter_Name(eventObject):
    global name_selected, type_selected, label_date1event, date_labelevent, blank_dateevent
    name_selected = Name_select.get()
    type_selected = Type_select.get()
    if type_selected == 'Special Assignment':
        spec_assn()
    elif type_selected == "Off Long Term":
        date_labelevent = Label(rooteventlong, text="Select First Date from Calendar:", bg=color, fg='white',
                                font=("Arial", font_sizeK), anchor=W)
        date_labelevent.place(x=40, y=540)
        blank_dateevent = Label(rooteventlong, text='                  ', font=("Arial", font_sizeK), padx=19)
        blank_dateevent.place(x=269, y=540)
    return name_selected, type_selected


def spec_assn():
    global date_labeleventSP, SchedSelect_combo, Sched_select
    date_labeleventSP = Label(rooteventlong, text="Select schedule:", bg=color, fg='white',
                            font=("Arial", font_sizeK), anchor=W)
    date_labeleventSP.place(x=138, y=540)
    sched_sched = ['4 Days: M-Th', '5 Days: M-F', '7 Days']
    Sched_select = StringVar()
    SchedSelect_combo = ttk.Combobox(rooteventlong, font="Arial 14 bold", width=18, justify="center", textvariable=Sched_select)
    SchedSelect_combo['value'] = sched_sched
    SchedSelect_combo.place(x=255, y=540)
    SchedSelect_combo.bind("<<ComboboxSelected>>", spec_assn2)
    return Sched_select


def spec_assn2(event):
    global Sched_select1, name_selected, extra_ylength
    Sched_select1 = SchedSelect_combo.selection_get()
    if Sched_select1 == '7 Days':
        extra_ylength = 40
        spec_assn7days()
    elif Sched_select1 == '4 Days: M-Th' or Sched_select1 == '5 Days: M-F':
        spec_assn2_1()


def spec_assn7days():
    global ScdCal_event2, date_labelevent7, blank_dateevent
    ScdCal_event.destroy()
    ScdCal_event2 = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                             showweeknumbers=False)
    ScdCal_event2.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event2.place(x=440, y=460)
    ScdCal_event2.bind("<<CalendarSelected>>", spec_assn7days1)
    date_labelevent7 = Label(rooteventlong, text="Select First Date from Calendar:", bg=color, fg='white',
                            font=("Arial", font_sizeK), anchor=W)
    date_labelevent7.place(x=40, y=580)
    blank_dateevent = Label(rooteventlong, text='                  ', font=("Arial", font_sizeK), padx=19)
    blank_dateevent.place(x=269, y=580)


def spec_assn7days1(event):
    global date_dateevent7_1, my_firstdate, ScdCal_event2, ScdCal_event3, date_labelevent7_1, blank_dateevent7_1
    my_firstdate = ScdCal_event2.selection_get()
    date_dateevent7_1 = Label(rooteventlong, text=my_firstdate, font=("Arial", font_sizeK), padx=19)
    date_dateevent7_1.place(x=269, y=580)
    ScdCal_event2.destroy()
    ScdCal_event3 = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                             showweeknumbers=False)
    ScdCal_event3.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event3.place(x=440, y=460)
    ScdCal_event3.bind("<<CalendarSelected>>", spec_assn7days2)
    date_labelevent7_1 = Label(rooteventlong, text="Select Last Date from Calendar:", bg=color, fg='white',
                            font=("Arial", font_sizeK), anchor=W)
    date_labelevent7_1.place(x=40, y=620)
    blank_dateevent7_1 = Label(rooteventlong, text='                  ', font=("Arial", font_sizeK), padx=19)
    blank_dateevent7_1.place(x=269, y=620)
    return my_firstdate


def spec_assn7days2(event):
    global my_enddate, date_dateevent7_2, ScdCal_event3, NewConfirm_Button, NewCancel_Button, Event2_matrix
    my_enddate = ScdCal_event3.selection_get()
    date_dateevent7_2 = Label(rooteventlong, text=my_enddate, font=("Arial", font_sizeK), padx=19)
    date_dateevent7_2.place(x=269, y=620)
    EventLongTerm_matrix = []
    Team_Name = ""
    for i_find in range(names_shape):
        if name_selected == sdf_basename[i_find, 1]:
            Team_Name = sdf_basename[i_find, 0]
    shift_status = []
    for z_find in range((my_enddate - my_firstdate).days + 1):
        for j_items in sdf_shiftfinal:
            if str(my_firstdate + timedelta(z_find)) == j_items[0]:
                if j_items[1] == Team_Name or j_items[2] == Team_Name:
                    shift_status += ["Train/SpecialAssign"]
                else:
                    shift_status += ["OT-Block"]
                break
    Event2_matrix = []
    for items in Event1_matrix:
        if np.shape(Event2_matrix)[0] == 0:
            if name_selected != items[1]:
                Event2_matrix = [items]
        else:
            if name_selected != items[1]:
                Event2_matrix = np.vstack((Event2_matrix, items))
            elif name_selected == items[1] and datetime.strptime(items[0], "%Y-%m-%d").date() < my_firstdate and \
                    datetime.strptime(items[0], "%Y-%m-%d").date() < my_enddate:
                Event2_matrix = np.vstack((Event2_matrix, items))
            elif name_selected == items[1] and datetime.strptime(items[0], "%Y-%m-%d").date() > my_enddate:
                Event2_matrix = np.vstack((Event2_matrix, items))
    for itemsadd in range((my_enddate - my_firstdate).days + 1):
        EventLongTerm_matrix1 = []
        EventLongTerm_matrix1 += [str(my_firstdate + timedelta(itemsadd)), name_selected, Team_Name, shift_status[itemsadd]]
        if np.shape(EventLongTerm_matrix)[0] == 0:
            EventLongTerm_matrix = EventLongTerm_matrix1
        else:
            EventLongTerm_matrix = np.vstack((EventLongTerm_matrix, EventLongTerm_matrix1))
    if np.shape(Event2_matrix)[0] != 0:
        Event2_matrix = np.vstack((Event2_matrix, EventLongTerm_matrix))
    else:
        Event2_matrix = EventLongTerm_matrix
    NewConfirm_Button = Button(rooteventlong, text="Press to Confirm", font=("Arial bold", font_sizeK), width=15, cursor="circle",
                               fg='blue', command=save_update7)
    NewConfirm_Button.place(x=100, y=660)
    NewCancel_Button = Button(rooteventlong, text="Press to Cancel", font=("Arial bold", font_sizeK), width=15, cursor="circle",
                              fg='blue', command=reset_specassn)
    NewCancel_Button.place(x=250, y=660)
    return my_enddate


def reset_event7():
    global NewConfirm_Button, NewCancel_Button, date_dateevent7_2, ScdCal_event3, date_dateevent7_1, date_labelevent7, \
        my_firstdate, SchCal_event3, date_labelevent7_1, blank_dateevent7_1, date_labeleventSP, blank_dateevent, SchedSelect_combo
    ScdCal_event3.destroy()
    date_labelevent7.destroy()
    date_dateevent7_2.destroy()
    date_dateevent7_1.destroy()
    date_labelevent7_1.destroy()
    blank_dateevent7_1.destroy()
    date_labelevent.destroy()
    blank_dateevent.destroy()
    SchedSelect_combo.destroy()
    NewConfirm_Button.destroy()
    NewCancel_Button.destroy()
    ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event.place(x=440, y=460)
    ScdCal_event.bind("<<CalendarSelected>>", long_term)
    Name_select.set("<Select>")
    Type_select.set("<Select>")


def save_update7():
    global NewConfirm_Button, NewCancel_Button, date_dateevent7_2, ScdCal_event3, date_dateevent7_1, date_labelevent7, \
        my_firstdate, SchCal_event3, date_labelevent7_1, blank_dateevent7_1, date_labelevent, blank_dateevent, Event2_matrix, \
        OT_select_combo, date_labeleventSP, Sched_select1
    erase_input = open('Shift Appointments.txt', 'w+')
    erase_input.close
    SetupInput_header = ["Date", "Name", "Team", "Appointment"]
    with open("Shift Appointments.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    for save_items in Event2_matrix:
        with open("Shift Appointments.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(save_items)
            appFile1.close()
    messagebox.showinfo("Success!", "Events Saved Successfully!")
    ScdCal_event3.destroy()
    date_labelevent7.destroy()
    date_dateevent7_2.destroy()
    date_dateevent7_1.destroy()
    date_labelevent7_1.destroy()
    blank_dateevent7_1.destroy()
    if Sched_select1 == "7 Days":
        date_labeleventSP.destroy()
    else:
        date_labelevent.destroy()
    blank_dateevent.destroy()
    SchedSelect_combo.destroy()
    NewConfirm_Button.destroy()
    NewCancel_Button.destroy()
    ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event.place(x=440, y=460)
    ScdCal_event.bind("<<CalendarSelected>>", long_term)
    Name_select.set("<Select>")
    Type_select.set("<Select>")


def spec_assn2_1():
    global OT_labelselect, OT_select_combo, OT_select
    OT_labelselect = Label(rooteventlong, text="Can work OT on Weekends?:", bg=color, fg='white',
                           font=("Arial", font_sizeK), anchor=W)
    OT_labelselect.place(x=58, y=580)
    OT_OT = ['Yes', 'No']
    OT_select_combo = ttk.Combobox(rooteventlong, font="Arial 14 bold", width=18, justify="center", textvariable=OT_select)
    OT_select_combo['value'] = OT_OT
    OT_select_combo.place(x=255, y=580)
    OT_select_combo.bind("<<ComboboxSelected>>", spec_assn3)


def spec_assn3(eventObject):
    global OT_select, date_labelevent, blank_dateevent, ScdCal_event2, OT_choice
    OT_choice = OT_select_combo.selection_get()
    ScdCal_event.destroy()
    ScdCal_event2 = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                             showweeknumbers=False)
    ScdCal_event2.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event2.place(x=440, y=460)
    ScdCal_event2.bind("<<CalendarSelected>>", spec_assn4)
    date_labelevent = Label(rooteventlong, text="Select First Date from Calendar:", bg=color, fg='white',
                            font=("Arial", font_sizeK), anchor=W)
    date_labelevent.place(x=40, y=620)
    blank_dateevent = Label(rooteventlong, text='                  ', font=("Arial", font_sizeK), padx=19)
    blank_dateevent.place(x=269, y=620)


def spec_assn4(event):
    global ScdCal_event2, my_firstdate, date_dateevent4, ScdCal_event3, date_labelevent5, blank_dateevent5, OT_choice
    my_firstdate = ScdCal_event2.selection_get()
    ScdCal_event2.destroy()
    date_dateevent4 = Label(rooteventlong, text=my_firstdate, font=("Arial", font_sizeK), padx=19)
    date_dateevent4.place(x=269, y=620)
    date_labelevent5 = Label(rooteventlong, text="Select First Date from Calendar:", bg=color, fg='white',
                            font=("Arial", font_sizeK), anchor=W)
    date_labelevent5.place(x=40, y=660)
    blank_dateevent5 = Label(rooteventlong, text='                  ', font=("Arial", font_sizeK), padx=19)
    blank_dateevent5.place(x=269, y=660)
    ScdCal_event3 = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                             showweeknumbers=False)
    ScdCal_event3.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event3.place(x=440, y=460)
    ScdCal_event3.bind("<<CalendarSelected>>", spec_assn5)
    return my_firstdate


def spec_assn5(event):
    global ScdCal_event3, my_enddate, date_dateevent5, OT_choice, NewConfirm_Button, NewCancel_Button, OT_choice, \
    Event2_matrix, Sched_select1
    my_enddate = ScdCal_event3.selection_get()
    date_dateevent5 = Label(rooteventlong, text=my_enddate, font=("Arial", font_sizeK), padx=19)
    date_dateevent5.place(x=269, y=660)
    Team_Name = ""
    for i_find in range(names_shape):
        if name_selected == sdf_basename[i_find, 1]:
            Team_Name = sdf_basename[i_find, 0]
    shift_status = []
    date_status = []
    if Sched_select1 == '5 Days: M-F':
        wkend_select = 4
    else:
        wkend_select = 3
    for z_find in range((my_enddate - my_firstdate).days + 1):
        for j_items in sdf_shiftfinal:
            if str(my_firstdate + timedelta(z_find)) == j_items[0]:
                if OT_choice == "Yes":
                    if (j_items[1] == Team_Name or j_items[2] == Team_Name) and \
                            0 <= datetime.strptime(str(j_items[0]), "%Y-%m-%d").weekday() <= wkend_select:
                        shift_status += ["Train/SpecialAssign"]
                        date_status += [j_items[0]]
                    elif 0 <= datetime.strptime(str(j_items[0]), "%Y-%m-%d").weekday() <= wkend_select:
                        shift_status += ["OT-Block"]
                        date_status += [j_items[0]]
                    elif j_items[2] == Team_Name and \
                            datetime.strptime(str(j_items[0]), "%Y-%m-%d").weekday() == 6:
                        shift_status += ["Off"]
                        date_status += [j_items[0]]
                    elif j_items[2] != Team_Name and \
                            datetime.strptime(str(j_items[0]), "%Y-%m-%d").weekday() == 6:
                        shift_status += ["OT-Block-Nights"]
                        date_status += [j_items[0]]
                    break
                elif OT_choice == "No":
                    if j_items[1] == Team_Name or j_items[2] == Team_Name:
                        shift_status += ["Train/SpecialAssign"]
                        date_status += [j_items[0]]
                    else:
                        shift_status += ["OT-Block"]
                        date_status += [j_items[0]]
                    break
    Event2_matrix = []
    for items in Event1_matrix:
        if np.shape(Event2_matrix)[0] == 0:
            if name_selected != items[1]:
                Event2_matrix = [items]
        else:
            if name_selected != items[1]:
                Event2_matrix = np.vstack((Event2_matrix, items))
            elif name_selected == items[1] and datetime.strptime(items[0], "%Y-%m-%d").date() < my_firstdate and \
                    datetime.strptime(items[0], "%Y-%m-%d").date() < my_enddate:
                Event2_matrix = np.vstack((Event2_matrix, items))
            elif name_selected == items[1] and datetime.strptime(items[0], "%Y-%m-%d").date() > my_enddate:
                Event2_matrix = np.vstack((Event2_matrix, items))
    EventLongTerm_matrix = []
    for itemsadd in range(np.shape(date_status)[0]):
        EventLongTerm_matrix1 = []
        EventLongTerm_matrix1 += [date_status[itemsadd], name_selected, Team_Name, shift_status[itemsadd]]
        if np.shape(EventLongTerm_matrix)[0] == 0:
            EventLongTerm_matrix = EventLongTerm_matrix1
        else:
            EventLongTerm_matrix = np.vstack((EventLongTerm_matrix, EventLongTerm_matrix1))
    if np.shape(Event2_matrix)[0] != 0:
        Event2_matrix = np.vstack((Event2_matrix, EventLongTerm_matrix))
    else:
        Event2_matrix = EventLongTerm_matrix
    NewConfirm_Button = Button(rooteventlong, text="Press to Confirm", font=("Arial bold", font_sizeK), width=15, cursor="circle",
                               fg='blue', command=save_specassn2)
    NewConfirm_Button.place(x=100, y=700)
    NewCancel_Button = Button(rooteventlong, text="Press to Cancel", font=("Arial bold", font_sizeK), width=15, cursor="circle",
                              fg='blue', command=reset_specassn2)
    NewCancel_Button.place(x=250, y=700)
    return my_enddate


def reset_specassn():
    global NewConfirm_Button, NewCancel_Button, date_dateevent5, ScdCal_event3, date_dateevent4, \
        date_labelevent5, blank_dateevent5, date_labelevent, blank_dateevent, OT_labelselect, OT_select_combo, \
        date_labeleventSP, OT_select
    ScdCal_event3.destroy()
    date_labelevent7.destroy()
    date_dateevent7_2.destroy()
    date_dateevent7_1.destroy()
    date_labelevent7_1.destroy()
    blank_dateevent7_1.destroy()
    date_labeleventSP.destroy()
    blank_dateevent.destroy()
    SchedSelect_combo.destroy()
    NewConfirm_Button.destroy()
    NewCancel_Button.destroy()
    OT_select.set("<Select>")
    ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event.place(x=440, y=460)
    ScdCal_event.bind("<<CalendarSelected>>", long_term)
    Name_select.set("<Select>")
    Type_select.set("<Select>")


def reset_specassn2():
    global NewConfirm_Button, NewCancel_Button, date_dateevent5, ScdCal_event3, date_dateevent4, \
        date_labelevent5, blank_dateevent5, date_labelevent, blank_dateevent, OT_labelselect, OT_select_combo, \
        date_labeleventSP, OT_select
    NewConfirm_Button.destroy()
    NewCancel_Button.destroy()
    date_labelevent5.destroy()
    ScdCal_event3.destroy()
    SchedSelect_combo.destroy()
    date_dateevent4.destroy()
    date_labelevent5.destroy()
    blank_dateevent5.destroy()
    date_dateevent5.destroy()
    date_labeleventSP.destroy()
    date_labelevent.destroy()
    blank_dateevent.destroy()
    OT_labelselect.destroy()
    OT_select_combo.destroy()
    OT_select.set("<Select>")
    ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event.place(x=440, y=460)
    ScdCal_event.bind("<<CalendarSelected>>", long_term)
    Name_select.set("<Select>")
    Type_select.set("<Select>")



def long_term(event):
    global my_firstdate, ScdCal_event1, blank_dateevent1, date_labelevent1, label_date1event, extra_ylength
    my_firstdate = ScdCal_event.selection_get()
    label_date1event = Label(rooteventlong, text=my_firstdate, font=("Arial", font_sizeK), padx=19)
    label_date1event.place(x=269, y=540+extra_ylength)
    date_labelevent1 = Label(rooteventlong, text="Select Last Date from Calendar:", bg=color, fg='white',
                            font=("Arial", font_sizeK), anchor=W)
    date_labelevent1.place(x=40, y=580+extra_ylength)
    blank_dateevent1 = Label(rooteventlong, text='                  ', font=("Arial", font_sizeK), padx=19)
    blank_dateevent1.place(x=269, y=580+extra_ylength)
    ScdCal_event.destroy()
    ScdCal_event1 = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event1.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event1.place(x=440, y=460)
    ScdCal_event1.bind("<<CalendarSelected>>", long_term2)
    return my_firstdate


def long_term2(event):    #Create the Long Term Block
    global my_firstdate, my_enddate, EventLongTerm_matrix, name_selected, Event1_matrix, NewConfirm_Button, \
        NewCancel_Button, label_date2event, Event2_matrix, blank_dateevent, extra_ylength
    EventLongTerm_matrix = []
    my_enddate = ScdCal_event1.selection_get()
    label_date2event = Label(rooteventlong, text=my_enddate, font=("Arial", font_sizeK), padx=19)
    label_date2event.place(x=269, y=580+extra_ylength)
    Team_Name = ""
    for i_find in range(names_shape):
        if name_selected == sdf_basename[i_find, 1]:
            Team_Name = sdf_basename[i_find, 0]
    shift_status = []
    for z_find in range((my_enddate - my_firstdate).days + 1):
        for j_items in sdf_shiftfinal:
            if str(my_firstdate + timedelta(z_find)) == j_items[0]:
                if j_items[1] == Team_Name or j_items[2] == Team_Name:
                    shift_status += ["Off"]
                else:
                    shift_status += ["OT-Block"]
                break
    Event2_matrix = []
    for items in Event1_matrix:
        if np.shape(Event2_matrix)[0] == 0:
            if name_selected != items[1]:
                Event2_matrix = [items]
        else:
            if name_selected != items[1]:
                Event2_matrix = np.vstack((Event2_matrix, items))
            elif name_selected == items[1] and datetime.strptime(items[0], "%Y-%m-%d").date() < my_firstdate and \
                    datetime.strptime(items[0], "%Y-%m-%d").date() < my_enddate:
                Event2_matrix = np.vstack((Event2_matrix, items))
            elif name_selected == items[1] and datetime.strptime(items[0], "%Y-%m-%d").date() > my_enddate:
                Event2_matrix = np.vstack((Event2_matrix, items))
    for itemsadd in range((my_enddate - my_firstdate).days + 1):
        EventLongTerm_matrix1 = []
        EventLongTerm_matrix1 += [str(my_firstdate + timedelta(itemsadd)), name_selected, Team_Name, shift_status[itemsadd]]
        if np.shape(EventLongTerm_matrix)[0] == 0:
            EventLongTerm_matrix = EventLongTerm_matrix1
        else:
            EventLongTerm_matrix = np.vstack((EventLongTerm_matrix, EventLongTerm_matrix1))
    if np.shape(Event2_matrix)[0] != 0:
        Event2_matrix = np.vstack((Event2_matrix, EventLongTerm_matrix))
    else:
        Event2_matrix = EventLongTerm_matrix
    NewConfirm_Button = Button(rooteventlong, text="Press to Confirm", font=("Arial bold", font_sizeK), width=15, cursor="circle",
                               fg='blue', command=save_update)
    NewConfirm_Button.place(x=100, y=620+extra_ylength)
    NewCancel_Button = Button(rooteventlong, text="Press to Cancel", font=("Arial bold", font_sizeK), width=15, cursor="circle",
                              fg='blue', command=reset_event)
    NewCancel_Button.place(x=250, y=620+extra_ylength)
    return my_enddate


def exit_appoint():
    rooteventlong.destroy()


def extra_appoint():
    rooteventlong.destroy()
    runpy.run_path(path_name="EventCalendarV4.py")


def reset_event():
    global NewConfirm_Button, SchCal_event1, blank_dateevent, date_labelevent, label_date1event, label_date2event,\
        NewCancel_Button, blank_dateevent1, date_labelevent1
    ScdCal_event1.destroy()
    blank_dateevent.destroy()
    blank_dateevent1.destroy()
    date_labelevent1.destroy()
    date_labelevent.destroy()
    label_date1event.destroy()
    label_date2event.destroy()
    NewConfirm_Button.destroy()
    NewCancel_Button.destroy()
    ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event.place(x=440, y=460)
    ScdCal_event.bind("<<CalendarSelected>>", long_term)
    Name_select.set("<Select>")
    Type_select.set("<Select>")


def save_update():
    global NewConfirm_Button, NewCancel_Button, label_date2event, ScdCal_event1, blank_dateevent, \
        date_labelevent, label_date1event, Event2_matrix, blank_dateevent1, date_labelevent1
    erase_input = open('Shift Appointments.txt', 'w+')
    erase_input.close
    SetupInput_header = ["Date", "Name", "Team", "Appointment"]
    with open("Shift Appointments.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    for save_items in Event2_matrix:
        with open("Shift Appointments.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(save_items)
            appFile1.close()
    messagebox.showinfo("Success!", "Events Saved Successfully!")
    ScdCal_event1.destroy()
    blank_dateevent1.destroy()
    blank_dateevent.destroy()
    date_labelevent.destroy()
    date_labelevent1.destroy()
    label_date1event.destroy()
    label_date2event.destroy()
    NewConfirm_Button.destroy()
    NewCancel_Button.destroy()
    ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event.place(x=440, y=460)
    ScdCal_event.bind("<<CalendarSelected>>", long_term)
    Name_select.set("<Select>")
    Type_select.set("<Select>")


def save_specassn2():
    global NewConfirm_Button, NewCancel_Button, date_dateevent5, ScdCal_event3, date_dateevent4, \
        date_labelevent5, blank_dateevent5, date_labelevent, blank_dateevent, OT_labelselect, OT_select_combo, \
        date_labeleventSP, OT_select
    erase_input = open('Shift Appointments.txt', 'w+')
    erase_input.close
    SetupInput_header = ["Date", "Name", "Team", "Appointment"]
    with open("Shift Appointments.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInput_header)
        appFile1.close()
    for save_items in Event2_matrix:
        with open("Shift Appointments.txt", "a") as appFile1:
            writer = csv.writer(appFile1)
            writer.writerow(save_items)
            appFile1.close()
    messagebox.showinfo("Success!", "Events Saved Successfully!")

    NewConfirm_Button.destroy()
    NewCancel_Button.destroy()
    date_labelevent5.destroy()
    ScdCal_event3.destroy()
    SchedSelect_combo.destroy()
    date_dateevent4.destroy()
    date_labelevent5.destroy()
    blank_dateevent5.destroy()
    date_dateevent5.destroy()
    date_labeleventSP.destroy()
    date_labelevent.destroy()
    blank_dateevent.destroy()
    OT_labelselect.destroy()
    OT_select_combo.destroy()
    OT_select.set("<Select>")
    ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                            showweeknumbers=False)
    ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
    ScdCal_event.place(x=440, y=460)
    ScdCal_event.bind("<<CalendarSelected>>", long_term)
    Name_select.set("<Select>")
    Type_select.set("<Select>")


#Set up the calendar, selection triggers Sched_Start
ScdCal_event = Calendar(rooteventlong, selectmode="day", date_pattern="mm/dd/yyyy", selectforeground="red",
                        showweeknumbers=False)
ScdCal_event.configure(font="Arial 16", foreground=color, background="Yellow", cursor="circle")
ScdCal_event.place(x=440, y=460)
ScdCal_event.bind("<<CalendarSelected>>", long_term)

#Get the names from the Base Data.txt file
for i_name in range(names_shape):
    names_names += [sdf_basename[i_name, 1]]

#Set up the form
title_labelevent = Label(rooteventlong, text="Add Long Term Event", font='Arial 20 bold', width=55, bg=color, fg='white')
title_labelevent.place(x=60, y=420)
name_labelevent = Label(rooteventlong, text="Select Name from List:", bg=color, fg='white', font=("Arial", font_sizeK))
name_labelevent.place(x=97, y=460)
type_labelevent = Label(rooteventlong, text="Select Long Term Event from List:", bg=color, fg='white',
                        font=("Arial", font_sizeK), anchor=W)
type_labelevent.place(x=30, y=500)

Name_select = StringVar()
Name_select.set("<Select>")
NameSelect_combo = ttk.Combobox(rooteventlong, font="Arial 14 bold", width=18, justify="center", textvariable=Name_select)
NameSelect_combo['value'] = names_names
NameSelect_combo.place(x=255, y=460)
#NameSelect_combo.bind("<<ComboboxSelected>>", enter_Name)

types_types = ['Off Long Term', 'Special Assignment']
Type_select = StringVar()
Type_select.set("<Select>")
TypeSelect_combo = ttk.Combobox(rooteventlong, font="Arial 14 bold", width=18, justify="center", textvariable=Type_select)
TypeSelect_combo['value'] = types_types
TypeSelect_combo.place(x=255, y=500)
TypeSelect_combo.bind("<<ComboboxSelected>>", enter_Name)


Display_button = Button(rooteventlong, text="Display Recent Events", font=("Arial bold", font_sizeK), width=20, cursor="circle",
                        fg='blue', command=display_events)
Display_button.place(x=63, y=45)

Remove_button = Button(rooteventlong, text="Display 12 Months", font=("Arial bold", font_sizeK), width=20, cursor="circle",
                       fg='blue', command=display_all)
Remove_button.place(x=230, y=45)

exitappoint_button = Button(rooteventlong, text="Exit", width=10, font="Arial 16 bold", cursor="circle", command=exit_appoint)
exitappoint_button.configure(fg='blue')
exitappoint_button.place(x=670, y=740)

extraappoint_button = Button(rooteventlong, text="Back", width=10, font="Arial 16 bold", cursor="circle", command=extra_appoint)
extraappoint_button.configure(fg='blue')
extraappoint_button.place(x=20, y=740)

rooteventlong.mainloop()
