from typing import List, Any
import numpy as np
from datetime import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import csv
from datetime import *
import pandas as pd
from PIL import ImageTk, Image
import runpy


screen4 = Toplevel()
screen4.geometry("700x730+350+0")
screen4.title("")
screen4.configure(bg="black", bd=10, width=700, height=730, relief=RIDGE)
style = ttk.Style()
style.theme_use('clam')
entry_input = ''
my_DateA = ''
my_DateB = ''
my_DateC = ''
my_DateD = ''
my_DateAA = ''
my_DateBB = ''
my_DateCC = ''
my_DateDD = ''
button_knowA = 0
button_knowB = 0
button_knowC = 0
button_knowD = 0
Sched_Message = 'Press to Create Schedule'
shift_ready = ''
Set_Height = 60
global Temp_Shift

def exit_setsched():
    screen4.destroy()
    #runpy.run_path(path_name="PostScheduleMain.py")


yearrange = 400

DupontOT_table = pd.read_table('ShiftDupontOTTable.txt', delimiter=',')
sdf_DupontOTTable = pd.DataFrame(DupontOT_table)
DupontOT_matrix = np.array(sdf_DupontOTTable)
DupontOT_shape = np.shape(DupontOT_matrix)[0]
schedmain_label = Label(screen4, text="DuPont Schedule", width=20, font="Arial 20 bold")
schedmain_label.place(x=200, y=10)

def Select_DateA(event):
    global my_DateAA, button_knowA, Selected_DateA, ScdCal, open_CalB, blank_BSel
    my_DateAA = ScdCal.selection_get()
    my_WeekdayA = my_DateAA.weekday()
    if not my_WeekdayA == 0:
        messagebox.showwarning('Error', 'Monday Date not Chosen!')
        my_DateAA = ''
    elif my_DateAA == my_DateBB or my_DateAA == my_DateCC or my_DateAA == my_DateDD:
        messagebox.showwarning('Error', 'This Data is already Chosen!')
        my_DateAA = ''
    else:
        my_DateA = ScdCal.get_date()
        Selected_DateA = Label(screen4, text=my_DateA, font=("Arial", 14), height=1)
        Selected_DateA.place(x=265, y=Set_Height)
        ScdCal.destroy()
        ScdCal = Calendar(screen4, setmode="day", date_pattern="mm/dd/yyyy", selectforeground="red", showweeknumbers=False,
                          month=datetime.strptime(my_DateA, "%m/%d/%Y").month)
        ScdCal.configure(font="Arial 14", foreground="black", background="Yellow", cursor="circle")
        ScdCal.place(x=400, y=Set_Height)
        ScdCal.bind("<<CalendarSelected>>", Select_DateB)

        open_CalB = Label(screen4, text="Select B-Team Monday Days:", font=("Arial", 14), height=1) #, command=Select_DateB)
        open_CalB.place(x=50, y=Set_Height+50)
        blank_BSel = Label(screen4, text='                  ', font=("Arial", 14), height=1)
        blank_BSel.place(x=265, y=Set_Height+50)
    return my_DateAA, button_knowA


def Select_DateB(event):
    global my_DateBB, button_knowB, Selected_DateB, ScdCal, open_CalC, blank_CSel
    my_DateBB = ScdCal.selection_get()
    my_WeekdayB = my_DateBB.weekday()
    if not my_WeekdayB == 0:
        messagebox.showwarning('Error', 'Monday Date not Chosen!')
        my_DateBB = ''
    elif my_DateBB == my_DateAA or my_DateBB == my_DateCC or my_DateBB == my_DateDD:
        messagebox.showwarning('Error', 'This Data is already Chosen!')
        my_DateBB = ''
    else:
        my_DateB = ScdCal.get_date()
        Selected_DateB = Label(screen4, text=my_DateB, font=("Arial", 14), height=1)
        Selected_DateB.place(x=265, y=Set_Height+50)
        ScdCal.destroy()
        ScdCal = Calendar(screen4, setmode="day", date_pattern="mm/dd/yyyy", selectforeground="red", showweeknumbers=False,
                          month=datetime.strptime(my_DateB, "%m/%d/%Y").month)
        ScdCal.configure(font="Arial 14", foreground="black", background="Yellow", cursor="circle")
        ScdCal.place(x=400, y=Set_Height)
        ScdCal.bind("<<CalendarSelected>>", Select_DateC)

        open_CalC = Label(screen4, text="Select C-Team Monday Days:", font=("Arial", 14), height=1) #, command=Select_DateC)
        open_CalC.place(x=50, y=Set_Height+100)
        blank_CSel = Label(screen4, text='                  ', font=("Arial", 14), height=1)
        blank_CSel.place(x=265, y=Set_Height+100)
    return my_DateBB, button_knowB


def Select_DateC(event):
    global my_DateCC, button_knowC, Selected_DateC, ScdCal, open_CalD, blank_DSel
    my_DateCC = ScdCal.selection_get()
    my_WeekdayC = my_DateCC.weekday()
    if not my_WeekdayC == 0:
        messagebox.showwarning('Error', 'Monday Date not Chosen!')
        my_DateCC = ''
    elif my_DateCC == my_DateAA or my_DateCC == my_DateBB or my_DateCC == my_DateDD:
        messagebox.showwarning('Error', 'This Data is already Chosen!')
        my_DateCC = ''
    else:
        my_DateC = ScdCal.get_date()
        Selected_DateC = Label(screen4, text=my_DateC, font=("Arial", 14), height=1)
        Selected_DateC.place(x=265, y=Set_Height+100)
        ScdCal.destroy()
        ScdCal = Calendar(screen4, setmode="day", date_pattern="mm/dd/yyyy", selectforeground="red", showweeknumbers=False,
                          month=datetime.strptime(my_DateC, "%m/%d/%Y").month)
        ScdCal.configure(font="Arial 14", foreground="black", background="Yellow", cursor="circle")
        ScdCal.place(x=400, y=Set_Height)
        ScdCal.bind("<<CalendarSelected>>", Select_DateD)
        open_CalD = Label(screen4, text="Select D-Team Monday Days:", font=("Arial", 14), height=1) #, command=Select_DateD)
        open_CalD.place(x=50, y=Set_Height+150)
        blank_DSel = Label(screen4, text='                  ', font=("Arial", 14), height=1)
        blank_DSel.place(x=265, y=Set_Height+150)
    return my_DateCC, button_knowC


def Select_DateD(event):
    global my_DateDD, button_knowD, Selected_DateD, ScdCal
    my_DateDD = ScdCal.selection_get()
    my_WeekdayD = my_DateDD.weekday()
    if not my_WeekdayD == 0:
        messagebox.showwarning('Error', 'Monday Date not Chosen!')
        my_DateDD = ''
    elif my_DateDD == my_DateAA or my_DateDD == my_DateBB or my_DateDD == my_DateCC:
        messagebox.showwarning('Error', 'This Data is already Chosen!')
        my_DateDD = ''
    else:
        my_DateD = ScdCal.get_date()
        Selected_DateD = Label(screen4, text=my_DateD, font=("Arial", 14), height=1)
        Selected_DateD.place(x=265, y=Set_Height+150)
    Set_OTCriteria()
    return my_DateDD, button_knowD


def Set_OTCriteria():
    global OTCrit_var, label_OTCrit, combo_OTCrit
    OTCrit_var = StringVar()
    label_OTCrit = Label(screen4, text="Set OT Selection Criteria:", font=("Arial", 14), height=1)
    label_OTCrit.place(x=75, y=Set_Height+200)
    combo_OTCrit = ttk.Combobox(screen4, font="Arial 14 bold", width=8, justify="center",
                                textvariable=OTCrit_var)
    combo_OTCrit['value'] = ("OT Hours", "Seniority")
    combo_OTCrit.place(x=265, y=Set_Height+200)
    combo_OTCrit.bind("<<ComboboxSelected>>", Set_SlideTol)


def Set_SlideTol(event):
    global tol_var, label_tol, combo_tol
    tol_var = IntVar()
    label_tol = Label(screen4, text="Set Slider Tolerance:", font=("Arial", 14), height=1)
    label_tol.place(x=105, y=Set_Height+250)
    combo_tol = ttk.Combobox(screen4, font="Arial 14 bold", width=8, justify="center",
                             textvariable=tol_var)
    combo_tol['value'] = (1, 2, 3, 4)
    combo_tol.place(x=265, y=Set_Height+250)
    combo_tol.bind("<<ComboboxSelected>>", Set_DoubleDay)


def Set_DoubleDay(event):
    global double_var, label_double, combo_double
    double_var = IntVar()
    label_double = Label(screen4, text="Set Double Time Day:", font=("Arial", 14), height=1)
    label_double.place(x=106, y=Set_Height+300)
    combo_double = ttk.Combobox(screen4, font="Arial 14 bold", width=8, justify="center",
                                textvariable=double_var)
    combo_double['value'] = (0, 7, 8, 9, 10, 11, 12, 13)
    combo_double.place(x=265, y=Set_Height+300)
    combo_double.bind("<<ComboboxSelected>>", Confirm_Sched)


def Confirm_Sched(event):
    global shift_ready
    shift_ready = Button(screen4, text=Sched_Message, font=("Arial", 20), height=1,
                         cursor="circle", fg='blue', command=Shift_Dupont)
    shift_ready.place(x=60, y=Set_Height+350)


def Shift_Dupont():
    file_sched = open("ShiftDupont.txt", "w+")
    file_sched.close()
    global Temp_Shift, Selected_DateA, Selected_DateB, Selected_DateC, Selected_DateD
    #Setup the date input
    Date_List = np.array([[my_DateAA, my_DateBB, my_DateCC, my_DateDD],
                          ["A", "B", "C", "D"]])
    first_date = min(Date_List[0])
    fourth_date = max(Date_List[0])
    temp_date1 = ''
    temp_date2 = ''
    for x_col in range(np.shape(Date_List)[1]):
        if not Date_List[0, x_col] == first_date:
            if not Date_List[0, x_col] == fourth_date:
                if temp_date1 == '':
                    temp_date1 = Date_List[0, x_col]
                else:
                    temp_date2 = Date_List[0, x_col]
    if temp_date1 < temp_date2:
        second_date = temp_date1
        third_date = temp_date2
    else:
        second_date = temp_date2
        third_date = temp_date1
    #Set up date matrix
    Shift_Dupont = ["Date"]
    add_dates: int

    #Insert Shifts into matrix
    Temp_ShiftTitle = []
    Temp_Shift = [[0 for x in range(8)] for y in range(28)]
    Temp_ShiftTitle = [[0 for x in range(8)] for y in range(1)]
    Temp_ShiftTitle[0][0] = "Days"
    Temp_ShiftTitle[0][1] = "Nights"
    Temp_ShiftTitle[0][2] = "1stOTD"
    Temp_ShiftTitle[0][3] = "2ndOTD"
    Temp_ShiftTitle[0][4] = "3rdOTD"
    Temp_ShiftTitle[0][5] = "1stOTN"
    Temp_ShiftTitle[0][6] = "2ndOTN"
    Temp_ShiftTitle[0][7] = "3rdOTN"
    nil = 'nil'

    for team_col in range(0, 4):
        if Date_List[0, team_col] == first_date:
            first_team = Date_List[1, team_col]
        if Date_List[0, team_col] == second_date:
            second_team = Date_List[1, team_col]
        if Date_List[0, team_col] == third_date:
            third_team = Date_List[1, team_col]
        if Date_List[0, team_col] == fourth_date:
            fourth_team = Date_List[1, team_col]
    for add_dates in range(0, 28*yearrange):
        next_date = first_date + timedelta(add_dates)
        Shift_Dupont += [datetime.strftime(next_date, '%Y-%m-%d')]
    Shift_Dupont = np.reshape(Shift_Dupont, (1+((29-1)*yearrange), 1))
    cycle_adder = 0
    for xx_row in range(0, 28):
        if xx_row >= 0 and xx_row <= 3 or xx_row >= 18 and xx_row <= 20:
            Temp_Shift[xx_row][0] = first_team
            Temp_Shift[xx_row][2] = eval(DupontOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(DupontOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(DupontOT_matrix[xx_row][3])
        if xx_row >= 11 and xx_row <= 14 or xx_row >= 22 and xx_row <= 24:
            Temp_Shift[xx_row][1] = first_team
            Temp_Shift[xx_row][5] = eval(DupontOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(DupontOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(DupontOT_matrix[xx_row][7])
        if xx_row >= 7 and xx_row <= 10 or xx_row >= 25 and xx_row <= 27:
            Temp_Shift[xx_row][0] = second_team
            Temp_Shift[xx_row][2] = eval(DupontOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(DupontOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(DupontOT_matrix[xx_row][3])
        if xx_row >= 1 and xx_row <= 3 or xx_row >= 18 and xx_row <= 21:
            Temp_Shift[xx_row][1] = second_team
            Temp_Shift[xx_row][5] = eval(DupontOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(DupontOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(DupontOT_matrix[xx_row][7])
        if xx_row >= 14 and xx_row <= 17 or xx_row >= 4 and xx_row <= 6:
            Temp_Shift[xx_row][0] = third_team
            Temp_Shift[xx_row][2] = eval(DupontOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(DupontOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(DupontOT_matrix[xx_row][3])
        if xx_row >= 8 and xx_row <= 10 or xx_row >= 25 and xx_row <= 27:
            Temp_Shift[xx_row][1] = third_team
            Temp_Shift[xx_row][5] = eval(DupontOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(DupontOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(DupontOT_matrix[xx_row][7])
        if xx_row == 0:
            Temp_Shift[xx_row][1] = third_team
            Temp_Shift[xx_row][5] = eval(DupontOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(DupontOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(DupontOT_matrix[xx_row][7])
        if xx_row >= 21 and xx_row <= 24 or xx_row >= 11 and xx_row <= 13:
            Temp_Shift[xx_row][0] = fourth_team
            Temp_Shift[xx_row][2] = eval(DupontOT_matrix[xx_row][1])
            Temp_Shift[xx_row][3] = eval(DupontOT_matrix[xx_row][2])
            Temp_Shift[xx_row][4] = eval(DupontOT_matrix[xx_row][3])
        if xx_row >= 4 and xx_row <= 7 or xx_row >= 15 and xx_row <= 17:
            Temp_Shift[xx_row][1] = fourth_team
            Temp_Shift[xx_row][5] = eval(DupontOT_matrix[xx_row][5])
            Temp_Shift[xx_row][6] = eval(DupontOT_matrix[xx_row][6])
            Temp_Shift[xx_row][7] = eval(DupontOT_matrix[xx_row][7])
    Temp_Shift1 = []
    for temp_stack in range(1, yearrange+1):
        Temp_Shift1 += Temp_Shift
    Temp_ShiftFinal = np.vstack((Temp_ShiftTitle, Temp_Shift1))
    Shift_DupontFinal = np.hstack((Shift_Dupont, Temp_ShiftFinal))
    with open('ShiftDupont.txt','a') as appFile:
        writer = csv.writer(appFile)
        writer.writerows(Shift_DupontFinal)
        appFile.close()
    erase_Setup = open('SetupScheduleInfo.txt', 'w+')
    erase_Setup.close()
    SetupInfo_header = ['Schedule', 'EB Days', 'Double Days', 'OT Selection']
    with open("SetupScheduleInfo.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInfo_header)
        appFile1.close()
    with open("SetupScheduleInfo.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(["DuPont Schedule", tol_var.get(), double_var.get(), OTCrit_var.get()])
        appFile1.close()
    erase_appoints = open('Shift Appointments.txt', 'w+')
    erase_appoints.close()
    Appoints_header = ['Date', 'Name', 'Team', 'Appointment']
    with open("Shift Appointments.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(Appoints_header)
        appFile1.close()
    messagebox.showinfo('Success!', 'DuPont Schedule Successfully Created!')
    label_OTCrit.destroy()
    combo_OTCrit.destroy()
    label_tol.destroy()
    combo_tol.destroy()
    label_double.destroy()
    combo_double.destroy()
    shift_ready.destroy()
    open_CalB.destroy()
    blank_BSel.destroy()
    open_CalC.destroy()
    blank_CSel.destroy()
    open_CalD.destroy()
    blank_DSel.destroy()
    Selected_DateA.destroy()
    Selected_DateB.destroy()
    Selected_DateC.destroy()
    Selected_DateD.destroy()


def FourFour_Select():
    screen4.destroy()
    runpy.run_path(path_name="SetupFour.py")


def EOWEO_Select():
    screen4.destroy()
    runpy.run_path(path_name="SetupEOWEO.py")


def Seven_Select():
    screen4.destroy()
    runpy.run_path(path_name="SetupSeven.py")


def FourFive_Select():
    screen4.destroy()
    runpy.run_path(path_name="SetupFourFive.py")


ScdCal = Calendar(screen4, setmode="day", date_pattern="mm/dd/yyyy", selectforeground="red", showweeknumbers=False)
ScdCal.configure(font="Arial 14", foreground="black", background="Yellow", cursor="circle")
ScdCal.place(x=400, y=Set_Height)
ScdCal.bind("<<CalendarSelected>>", Select_DateA)

open_CalA = Label(screen4, text="Select A-Team Monday Days:", font=("Arial", 14), height=1) #, command=Select_DateA)
open_CalA.place(x=50, y=Set_Height)
blank_ASel = Label(screen4, text='                  ', font=("Arial", 14), height=1)
blank_ASel.place(x=265, y=Set_Height)

Dupont_tag = Label(screen4, text="DuPont Schedule Example", font="Arial 20 bold", fg='white',
                   bg='black')
Dupont_tag.place(x=210, y=470)

Dupont_image = Image.open('Dupont Schedule Image.png')
Dupont_resize = Dupont_image.resize((600, 90), Image.ANTIALIAS)
Dupont_new = ImageTk.PhotoImage(Dupont_resize)
DupontImage_label = Label(screen4, image=Dupont_new)
DupontImage_label.place(x=50, y=500)

Four_selectbutton = Button(screen4, text="4on/4off Schedule", width=17, font="Arial 14 bold", cursor="circle",
                           fg='blue', command=FourFour_Select)
Four_selectbutton.place(x=20, y=640)

EOWEO_selectbutton = Button(screen4, text="EOWEO Schedule", width=17, font="Arial 14 bold", cursor="circle",
                            fg='blue', command=EOWEO_Select)
EOWEO_selectbutton.place(x=170, y=640)

Seven_selectbutton = Button(screen4, text="7on/7off Schedule", width=17, font="Arial 14 bold", cursor="circle",
                            fg='blue', command=Seven_Select)
Seven_selectbutton.place(x=470, y=640)

FourFive_selectbutton = Button(screen4, text="Four/Five Schedule", width=17, font="Arial 14 bold", cursor="circle",
                               fg='blue', command=FourFive_Select)
FourFive_selectbutton.place(x=320, y=640)

exitschedset_button = Button(screen4, text="Exit", width=7, font="Arial 16 bold",
                             cursor="circle", fg='blue', command=exit_setsched)
exitschedset_button.place(x=600, y=680)

screen4.mainloop()
