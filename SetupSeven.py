from typing import List, Any
import numpy as np
from datetime import *
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkcalendar import *
import csv
import datetime
import pandas as pd
from PIL import ImageTk, Image
import runpy

screen3 = Toplevel()
screen3.geometry("700x730+350+0")
screen3.title("")
color = 'black'
screen3.configure(bg=color, bd=10, width=700, height=730, relief=RIDGE)
style = ttk.Style()
style.theme_use('clam')
entry_input = ''
my_DateA = ''
my_DateB = ''
my_DateC = ''
my_DateD = ''
my_DateAA = ''
my_DateBB = ''
my_DateCC = ''
my_DateDD = ''
button_knowA = 0
button_knowB = 0
button_knowC = 0
button_knowD = 0
Sched_Message = 'Press to Create Schedule'
shift_ready = ''
Set_Height = 60
global Temp_Shift

def exit_setsched():
    screen3.destroy()
    #runpy.run_path(path_name="PostScheduleMain.py")


def Dupont_Select():
    screen3.destroy()
    runpy.run_path(path_name="SetDuPont.py")


def FourFour_Select():
    screen3.destroy()
    runpy.run_path(path_name="SetupFour.py")


def EOWEO_Select():
    screen3.destroy()
    runpy.run_path(path_name="SetupEOWEO.py")


def FourFive_Select():
    screen3.destroy()
    runpy.run_path(path_name="SetupFourFive.py")


yearrange = 400

SevenOT_table = pd.read_table('ShiftSevenOTTable.txt', delimiter=',')
sdf_SevenOTTable = pd.DataFrame(SevenOT_table)
SevenOT_matrix = np.array(sdf_SevenOTTable)
SevenOT_shape = np.shape(SevenOT_matrix)[0]
schedmain_label = Label(screen3, text="7on/7off Schedule", width=20, font="Arial 20 bold")
schedmain_label.place(x=230, y=10)
first_team = "A"
second_team = ""
third_team = ""
fourth_team = ""

def Select_DateA(event):
    global my_DateAA, first_team, SecondTeam_var, Selected_DateA, SecondTeam_label, SecondTeam_combo, Friday_adj, my_WeekdayA
    my_DateAA = ScdCal.selection_get()
    my_WeekdayA = my_DateAA.weekday()
    #first_team ="A"
    if my_WeekdayA != 3 and my_WeekdayA != 4:
        messagebox.showwarning('Error', 'Thursday or Friday Date not Chosen!')
        my_DateAA = ''
    elif my_DateAA == my_DateBB or my_DateAA == my_DateCC or my_DateAA == my_DateDD:
        messagebox.showwarning('Error', 'This Data is already Chosen!')
        my_DateAA = ''
    else:
        my_DateA = ScdCal.get_date()
        Selected_DateA = Label(screen3, text=my_DateA, font=("Arial", 14), height=1)
        Selected_DateA.place(x=265, y=Set_Height)
        if my_WeekdayA == 4:
            Friday_adj = 0
        else:
            Friday_adj = 1

    if my_DateAA != "":
        SecondTeam_label = Label(screen3, text="Select Night Shift Team with Team A", font=("Arial", 14),
                                 height=1)
        SecondTeam_label.place(x=20, y=Set_Height+50)
        SecondTeam_var = StringVar()
        SecondTeam_combo = ttk.Combobox(screen3, font="Arial 14 bold", width=8, justify="center",
                                        textvariable=SecondTeam_var)
        SecondTeam_combo['value'] = ('B', 'C', 'D')
        SecondTeam_combo.place(x=265, y=Set_Height+50)
        SecondTeam_combo.bind("<<ComboboxSelected>>", Select_third)
    return my_DateAA, first_team, SecondTeam_var


def Select_third(event):
    global first_team, second_team, ThirdTeam_var, ThirdTeam_label, ThirdTeam_combo
    second_team = SecondTeam_var.get()
    ThirdTeam_label = Label(screen3, text="Select Next Rotation Day Shift Team", font=("Arial", 14),
                            height=1)
    ThirdTeam_label.place(x=20, y=Set_Height+100)
    ThirdTeam_var = StringVar()
    ThirdTeam_combo = ttk.Combobox(screen3, font="Arial 14 bold", width=8, justify="center",
                                   textvariable=ThirdTeam_var)
    ThirdTeam_combo['value'] = ('', 'B', 'C', 'D')
    ThirdTeam_combo.current(0)
    ThirdTeam_combo.place(x=265, y=Set_Height+100)
    ThirdTeam_combo.bind("<<ComboboxSelected>>", Select_FinalTeams)
    if second_team == "B":
        ThirdTeam_combo['value'] = ('C', 'D')
    if second_team == "C":
        ThirdTeam_combo['value'] = ('B', 'D')
    if second_team == "D":
        ThirdTeam_combo['value'] = ('B', 'C')
    return second_team, ThirdTeam_var


def Select_FinalTeams(event):
    global first_team, second_team, third_team, fourth_team
    third_team = ThirdTeam_var.get()
    if (second_team == "B" or second_team == "C") and (third_team == "B" or third_team == "C"):
        fourth_team = "D"
    if (second_team == "B" or second_team == "D") and (third_team == "B" or third_team == "D"):
        fourth_team = "C"
    if (second_team == "C" or second_team == "D") and (third_team == "C" or third_team == "D"):
        fourth_team = "B"
    Set_OTCriteria()
    #shift_ready = Button(screen2, text=Sched_Message, font=("Arial", 20), height=1, command=Shift_EOWEO)
    #shift_ready.place(x=60, y=300)
    return my_DateAA, first_team, second_team, third_team, fourth_team


def Set_OTCriteria():
    global OTCrit_var, label_OTCrit, combo_OTCrit
    OTCrit_var = StringVar()
    label_OTCrit = Label(screen3, text="Set OT Selection Criteria:", font=("Arial", 14), height=1)
    label_OTCrit.place(x=75, y=Set_Height+150)
    combo_OTCrit = ttk.Combobox(screen3, font="Arial 14 bold", width=8, justify="center",
                                textvariable=OTCrit_var)
    combo_OTCrit['value'] = ("OT Hours", "Seniority")
    combo_OTCrit.place(x=265, y=Set_Height+150)
    combo_OTCrit.bind("<<ComboboxSelected>>", Set_SlideTol)


def Set_SlideTol(event):
    global tol_var, label_tol, combo_tol
    tol_var = IntVar()
    label_tol = Label(screen3, text="Set Slider Tolerance:", font=("Arial", 14), height=1)
    label_tol.place(x=105, y=Set_Height+200)
    combo_tol = ttk.Combobox(screen3, font="Arial 14 bold", width=8, justify="center",
                             textvariable=tol_var)
    combo_tol['value'] = (1, 2, 3, 4, 5)
    combo_tol.place(x=265, y=Set_Height+200)
    combo_tol.bind("<<ComboboxSelected>>", Set_DoubleDay)


def Set_DoubleDay(event):
    global double_var, label_double, combo_double
    double_var = IntVar()
    label_double = Label(screen3, text="Set Double Time Day:", font=("Arial", 14), height=1)
    label_double.place(x=100, y=Set_Height+250)
    combo_double = ttk.Combobox(screen3, font="Arial 14 bold", width=8, justify="center",
                                textvariable=double_var)
    combo_double['value'] = (0, 8, 9, 10, 11, 12, 13)
    combo_double.place(x=265, y=Set_Height+250)
    combo_double.bind("<<ComboboxSelected>>", Confirm_Sched)


def Confirm_Sched(event):
    global shift_ready
    shift_ready = Button(screen3, text=Sched_Message, font=("Arial", 20), height=1, cursor="circle", fg='blue',
                         command=Shift_Seven)
    shift_ready.place(x=60, y=Set_Height+300)


def Shift_Seven():
    global first_team, second_team, third_team, fourth_team, my_DateAA, Temp_Shift
    file_sched = open("ShiftSeven.txt", "w+")
    file_sched.close()
    #Set up date matrix
    Shift_Seven = ["Date"]
    add_dates: int

    #Insert Shifts into matrix
    Temp_ShiftTitle = []
    Temp_Shift = [[0 for x in range(8)] for y in range(28)]
    Temp_ShiftTitle = [[0 for x in range(8)] for y in range(1)]
    Temp_ShiftTitle[0][0] = "Days"
    Temp_ShiftTitle[0][1] = "Nights"
    Temp_ShiftTitle[0][2] = "1stOTD"
    Temp_ShiftTitle[0][3] = "2ndOTD"
    Temp_ShiftTitle[0][4] = "3rdOTD"
    Temp_ShiftTitle[0][5] = "1stOTN"
    Temp_ShiftTitle[0][6] = "2ndOTN"
    Temp_ShiftTitle[0][7] = "3rdOTN"
    nil = 'nil'

    for add_dates in range(0, 28*yearrange):
        next_date = my_DateAA + timedelta(add_dates) - timedelta(my_WeekdayA)
        Shift_Seven += [datetime.datetime.strftime(next_date, '%Y-%m-%d')]
    Shift_Seven = np.reshape(Shift_Seven, (1+((29-1)*yearrange), 1))
    cycle_adder = 0
    for xx_row in range(0, 28):
        if xx_row >= 3 and xx_row <= 9:
            Temp_Shift[xx_row][0] = eval(SevenOT_matrix[xx_row+Friday_adj][0])
            Temp_Shift[xx_row][2] = eval(SevenOT_matrix[xx_row+Friday_adj][1])
            Temp_Shift[xx_row][3] = eval(SevenOT_matrix[xx_row+Friday_adj][2])
            Temp_Shift[xx_row][4] = eval(SevenOT_matrix[xx_row+Friday_adj][3])
            Temp_Shift[xx_row][1] = eval(SevenOT_matrix[xx_row+Friday_adj][4])
            Temp_Shift[xx_row][5] = eval(SevenOT_matrix[xx_row+Friday_adj][5])
            Temp_Shift[xx_row][6] = eval(SevenOT_matrix[xx_row+Friday_adj][6])
            Temp_Shift[xx_row][7] = eval(SevenOT_matrix[xx_row+Friday_adj][7])
        if xx_row >= 10 and xx_row <= 16:
            Temp_Shift[xx_row][0] = eval(SevenOT_matrix[xx_row+Friday_adj][0])
            Temp_Shift[xx_row][2] = eval(SevenOT_matrix[xx_row+Friday_adj][1])
            Temp_Shift[xx_row][3] = eval(SevenOT_matrix[xx_row+Friday_adj][2])
            Temp_Shift[xx_row][4] = eval(SevenOT_matrix[xx_row+Friday_adj][3])
            Temp_Shift[xx_row][1] = eval(SevenOT_matrix[xx_row+Friday_adj][4])
            Temp_Shift[xx_row][5] = eval(SevenOT_matrix[xx_row+Friday_adj][5])
            Temp_Shift[xx_row][6] = eval(SevenOT_matrix[xx_row+Friday_adj][6])
            Temp_Shift[xx_row][7] = eval(SevenOT_matrix[xx_row+Friday_adj][7])
        if xx_row >= 17 and xx_row <= 23:
            Temp_Shift[xx_row][0] = eval(SevenOT_matrix[xx_row+Friday_adj][0])
            Temp_Shift[xx_row][2] = eval(SevenOT_matrix[xx_row+Friday_adj][1])
            Temp_Shift[xx_row][3] = eval(SevenOT_matrix[xx_row+Friday_adj][2])
            Temp_Shift[xx_row][4] = eval(SevenOT_matrix[xx_row+Friday_adj][3])
            Temp_Shift[xx_row][1] = eval(SevenOT_matrix[xx_row+Friday_adj][4])
            Temp_Shift[xx_row][5] = eval(SevenOT_matrix[xx_row+Friday_adj][5])
            Temp_Shift[xx_row][6] = eval(SevenOT_matrix[xx_row+Friday_adj][6])
            Temp_Shift[xx_row][7] = eval(SevenOT_matrix[xx_row+Friday_adj][7])
        if xx_row >= 24 and xx_row <= 27:
            Temp_Shift[xx_row][0] = eval(SevenOT_matrix[xx_row+Friday_adj][0])
            Temp_Shift[xx_row][2] = eval(SevenOT_matrix[xx_row+Friday_adj][1])
            Temp_Shift[xx_row][3] = eval(SevenOT_matrix[xx_row+Friday_adj][2])
            Temp_Shift[xx_row][4] = eval(SevenOT_matrix[xx_row+Friday_adj][3])
            Temp_Shift[xx_row][1] = eval(SevenOT_matrix[xx_row+Friday_adj][4])
            Temp_Shift[xx_row][5] = eval(SevenOT_matrix[xx_row+Friday_adj][5])
            Temp_Shift[xx_row][6] = eval(SevenOT_matrix[xx_row+Friday_adj][6])
            Temp_Shift[xx_row][7] = eval(SevenOT_matrix[xx_row+Friday_adj][7])
        if xx_row >= 0 and xx_row <= 2:
            Temp_Shift[xx_row][0] = eval(SevenOT_matrix[xx_row+Friday_adj][0])
            Temp_Shift[xx_row][2] = eval(SevenOT_matrix[xx_row+Friday_adj][1])
            Temp_Shift[xx_row][3] = eval(SevenOT_matrix[xx_row+Friday_adj][2])
            Temp_Shift[xx_row][4] = eval(SevenOT_matrix[xx_row+Friday_adj][3])
            Temp_Shift[xx_row][1] = eval(SevenOT_matrix[xx_row+Friday_adj][4])
            Temp_Shift[xx_row][5] = eval(SevenOT_matrix[xx_row+Friday_adj][5])
            Temp_Shift[xx_row][6] = eval(SevenOT_matrix[xx_row+Friday_adj][6])
            Temp_Shift[xx_row][7] = eval(SevenOT_matrix[xx_row+Friday_adj][7])

    Temp_Shift1 = []
    for temp_stack in range(1, yearrange+1):
        Temp_Shift1 += Temp_Shift
    Temp_ShiftFinal = np.vstack((Temp_ShiftTitle, Temp_Shift1))
    Shift_SevenFinal = np.hstack((Shift_Seven, Temp_ShiftFinal))
    with open('ShiftSeven.txt', 'a') as appFile:
        writer = csv.writer(appFile)
        writer.writerows(Shift_SevenFinal)
        appFile.close()
    erase_Setup = open('SetupScheduleInfo.txt', 'w+')
    erase_Setup.close()
    SetupInfo_header = (["Schedule", "EB Days", "Double Days", "OT Selection"])
    with open("SetupScheduleInfo.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(SetupInfo_header)
        appFile1.close()
    with open("SetupScheduleInfo.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(["7on/7off Schedule", tol_var.get(), double_var.get(), OTCrit_var.get()])
        appFile1.close()
    erase_appoints = open('Shift Appointments.txt', 'w+')
    erase_appoints.close()
    Appoints_header = ['Date', 'Name', 'Team', 'Appointment']
    with open("Shift Appointments.txt", "a") as appFile1:
        writer = csv.writer(appFile1)
        writer.writerow(Appoints_header)
        appFile1.close()
    messagebox.showinfo("Success!", "7on/7off Schedule Successfully Created!")
    Selected_DateA.destroy()
    SecondTeam_combo.destroy()
    SecondTeam_label.destroy()
    ThirdTeam_label.destroy()
    ThirdTeam_combo.destroy()
    label_OTCrit.destroy()
    combo_OTCrit.destroy()
    label_tol.destroy()
    combo_tol.destroy()
    label_double.destroy()
    combo_double.destroy()
    shift_ready.destroy()


ScdCal = Calendar(screen3, setmode="day", date_pattern="mm/dd/yyyy", selectforeground="red", showweeknumbers=False)
ScdCal.configure(font="Arial 14", foreground=color, background="Yellow", cursor="circle")
ScdCal.place(x=400, y=Set_Height)
ScdCal.bind("<<CalendarSelected>>", Select_DateA)

open_CalA = Button(screen3, text="Select A-Team 1st Day Shift:", font=("Arial", 14), height=1)
open_CalA.place(x=20, y=Set_Height)
blank_ASel = Label(screen3, text='                  ', font=("Arial", 14), height=1)
blank_ASel.place(x=265, y=Set_Height)

Seven_tag = Label(screen3, text="7on/7off Schedule Example:", font="Arial 20 bold", fg='white',
                  bg=color)
Seven_tag.place(x=205, y=455)
Seven_tag2 = Label(screen3, text="(7on/7off starts on Thursday or "
                                 "Friday to adjust to regulatory requirements)",
                   font="Arial 14 bold", fg='white', bg=color)
Seven_tag2.place(x=90, y=480)
Seven_image = Image.open('Seven Schedule Image.png')
Seven_resize = Seven_image.resize((600, 90), Image.ANTIALIAS)
Seven_new = ImageTk.PhotoImage(Seven_resize)
SevenImage_label = Label(screen3, image=Seven_new)
SevenImage_label.place(x=50, y=500)

Dupont_selectbutton = Button(screen3, text="DuPont Schedule", width=17, font="Arial 14 bold", cursor="circle",
                             fg='blue', command=Dupont_Select)
Dupont_selectbutton.place(x=20, y=640)

Four_selectbutton = Button(screen3, text="4on/4off Schedule", width=17, font="Arial 14 bold", cursor="circle",
                           fg='blue', command=FourFour_Select)
Four_selectbutton.place(x=170, y=640)

EOWEO_selectbutton = Button(screen3, text="EOWEO Schedule", width=17, font="Arial 14 bold", cursor="circle",
                            fg='blue', command=EOWEO_Select)
EOWEO_selectbutton.place(x=320, y=640)

FourFive_selectbutton = Button(screen3, text="Four/Five Schedule", width=17, font="Arial 14 bold", cursor="circle",
                               fg='blue', command=FourFive_Select)
FourFive_selectbutton.place(x=470, y=640)

exitschedset_button = Button(screen3, text="Exit", width=7, font="Arial 16 bold", cursor="circle", fg='blue',
                             command=exit_setsched)
exitschedset_button.place(x=600, y=680)

screen3.mainloop()
